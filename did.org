#+OPTIONS: toc:nil 

* DONE ~step14~: Pccf's with controlled properties (TP>0, FP=0, FN=0) for real signals are obtained. 
  Optimized Pccf's are obtained using binary classification metrics.
  <2019-05-05 Sun>
  ~095ae9b~ commit
* DONE Calculate number of FP for Pccf
* TODO [#A] 3 ROCs: For Pccf predictions, for detections with Pccf and without
* TODO Pccf optimization: plot ROC curves for real signals and for CdEs
* TODO [#A] Pccf optimization using TP/FP/FN metrics
* DONE Calculate min/max Mu and Sigma automatically
* DONE ~step10~: brute force optimal Pccf for given changepoints locations ~OptimPccf.scala~
* DONE Depict beh. of CuSum stat in tikz ~tex/img-tikz/cusum-output-stat.tex~
* DONE Illustrative example like in Venugopal V., et.al., 2014
* DONE What happens with the detection delay when we reduce FA rate using Pccf / I.F. ? Can we control it?
  If we change sensitivity as a function s(t), where t - is a
  time stamp of the current observation, what is the expected
  value of the detection delay E(D | s(t), t)?

* [#A] TD20190304 (after the meeting in JYU with Tommi)

** Priority num.1: related work

** Dynamic switching between global optimal settings and local optimal settings.
   Outside of indicator function: use global optimal settings
   (delay is not important, FA rate low is important).  Local
   (within indicator function) optimal settings: delay is
   important.

*** What is the relation between local and global optimal settings?
    
    Are local optimal settings known? No, they are changing from
    interval to interval.

** Conditional pdf's of Delay and FArate

** When internal use of Pccf is better that post-processing? - In case if we can reduce detection delay?

** Scatter plot: delay vs loss, FA rate vs loss? 
** In the task to reduce FA rate subj. to delay constrais - how to we enforce the second condiotion on delay? Can we control the detection delay?

* DONE Cusum 1 change animation (scala lib)
* Use detections itself as a ground truth
* DONE Implemented dynamic programming based detector ~scalacode/dp-detector-scala-20190211/~
  <2019-02-12 Tue>
* TODO Implement Pccf backwards / offline Pccf
* DONE diag-bayes: Fix convergence of Julia and Matlab
  <2019-02-12 Tue>
* DONE Run orig Matlab code
* DONE step12: fix/check Adwin delays/ fa rate
* DONE Fix Bayesian detector.: Reverse Student updates. : Added Gaussian pdf
  <2019-02-07 Thu>
  - added Gaussian predictiove distribution and used inversed
    estimates.

* TODO Variate W?
* DONE Add delays to step12-multi
* DONE Test cost function: Monte-Carlo
  CLOSED: [2019-02-04 Mon 11:01]
* Add separate plots for delay, fa rate
* add tikz picture for : binary classif metrics problem

* DONE paper draft: 2 tikz pictures
  CLOSED: [2019-02-01 Fri 16:18]
* Steps 12,13: add cases with virtual change
* DONE Optimal Pccf (scala)
* DONE Implement dynamic programming detector (precise)
* TODO Bayesian update of non-stand. Student distribution
  Found in 
  Murphy, Kevin P. "Conjugate Bayesian analysis of the Gaussian distribution." def 1.2σ2 (2007): 16.
* Variate width and plot width vs difference between dynamic and static losses
* DONE ~step13~ : loss function (static and dynamic) for real signals using ideal pccf
  CLOSED: [2019-01-25 Fri 13:44]

* DONE Make tail of loss for Bayes higher in step12 . 
  CLOSED: [2019-01-25 Fri 15:33]
  Transfer from diag-all-detectors to step12 for Bayes
* DONE Check Bayesain detector
  CLOSED: [2019-01-09 Wed 14:08]
  It works OK, but it is very adaptive, therefore sensitive 

* DONE Why successive updates of muT vector are different in the Bayesian detector?
  CLOSED: [2019-01-09 Wed 13:19]
  It should be so.
* DONE Modified studentpdf function in the Bayesian detector
  CLOSED: [2018-12-13 Thu 08:09]
* Question How do we go from 1 change to multi-changes?
* DONE step12: Improve cost functions results, especially for the Bayesian detector)
  CLOSED: [2019-01-29 Tue 12:40]
* DONE step12: Optim settings search for dynamic case;  Get new cost function values
  CLOSED: [2018-11-21 Wed 15:06]
* DONE Implement optimality search for PH detector, dynamic settings case
  CLOSED: [2018-11-20 Tue 14:44]
* DONE diag-ii: fix Bayesian detector.
  CLOSED: [2018-11-26 Mon 14:40]
  ~diag-ii, iii~
  there was 10 instead of 1.0

* DONE step12: find static optim settings, run detectors with dynamic and static optim settings.
  CLOSED: [2018-11-15 Thu 16:22]
* Add: Younger Dryas data?
  - https://www.ncdc.noaa.gov/abrupt-climate-change/The%20Younger%20Dryas

* DONE Joint test of detectors and loss functions (diagnostics-ii and iii)
  CLOSED: [2018-11-13 Tue 13:13]

  Files:
  ~~Lib/PerformanceMetrics.jl~~,
  ~~diagnostics-iii-cost-function/main-cf.jl~~,
  ~~diagnostics-ii-all-detectors/diagnosticsDetectors.jl~~

* Add output statistics for 3 detectors into pdf files 
* DONE Read input signal function is fixed 
  CLOSED: [2018-11-07 Wed 15:13]

* DONE Adjust PH detector ~diagnostics-ii-all-detectors/~
  CLOSED: [2018-10-30 Tue 12:00]

* DONE Diagnistics for detectors (see output statistics to check if everything is OK) (step12)
  CLOSED: [2018-11-26 Mon 16:03]
* DONE Demonstrate NO improvement with ideal Pccf
  CLOSED: [2018-11-26 Mon 16:03]
*** DONE Use post-processing settings in this case (pccf backwards)
    CLOSED: [2018-11-26 Mon 16:03]
* DONE Plot new delays and FA rates along with old ~step07b/~
  CLOSED: [2018-08-01 Wed 12:37]

* DONE Find and save optimal settings automatically
   CLOSED: [2018-08-01 Wed 13:39]

* DONE Fix ~step07b/~, ~step08/~ (find and apply optimal settings)
  CLOSED: [2018-08-06 Mon 17:55]
  
  ~b409b1f~

  Manual settings for Adwin for 2nd signal:
  ~~"adwin" : {"s": 6.67, "min_len": 10}~~
  give Loss value = 35.32
  
  Settings obtained on ~step07b~ :
  ~~"Adwin",2.0, 66.3776836486016~~
  give loss = 66.37

* DONE Check carefully Bayesian detector on Signal 2
  CLOSED: [2018-08-07 Tue 13:14]
  
  repo: ~bayesian-detector-diagnostics/~
  commit: ~c1f90c59ee8a77cce494e9d1547226cbcd2c21b6~

* DONE TASK12 
  CLOSED: [2018-11-26 Mon 16:03]
  Library funcs based on step07b

* DONE Re-run ~step07b/~ using library functions ~LearnDetectors.jl~
  CLOSED: [2018-08-22 Wed 12:14]
  
  File: ~main07usinglib.jl~

* DONE Implement dynamic detection using inside static detectors (step12)
  CLOSED: [2018-08-30 Thu 09:34]
* Variate the width of ideal Pccf and record performance (step12)
* !!! To run ~step12~ on real data export ideal Pccfs from ~step11~
* !!! Or just redo ~step11~ using new Lib function
* !!! Run just PH dynamic (Bayes, Adwin later) for proof oc concept?
* DONE Plot cost function for dynamic detectors
  CLOSED: [2019-01-29 Tue 12:42]
* <2018-10-10 Wed>

  Restoring work on step12

* <2018-08-14 Tue>

  Doing: ~step12/LearnDetectors.jl~

* <2018-08-03 Fri>

  Doing ~step08/~ 

* <2018-08-01 Wed>

  - Commits: ~8085b46, 31dd4c1~
  - Applied new loss function ~step07b/~
  - Save optimal settings into CSV (~3286bca~)

* <2018-07-31 Tue>

Fixing ~step07b~ by fixinf loss function implementation

  - Run ~step07b-loss-func-approximator-optim-settings/main07.jl~ with ~Lib/Fractions2.jl~
  - Implemented ~/Lib/fn_LossFunc.j~
  - Check loss function values: ~3PlotLossFunctionsAllSignals.R~
  - Now loss function are much better! ~3PlotLossFunctionsAllSignals.R~, output from ~step07b-loss-func-approximator-optim-settings/main07.jl~

  - <2018-07-23 Mon>

Same output for  
#+BEGIN_SRC 
    src/Lib/clj-cost-function/src/clj_cost_function/core.clj
    Lib/Fraction2.jl
#+END_SRC

Next: apply for step11


* <2018-07-20 Fri>

  Delays, average delays, FA rate functions design and implementation

#+BEGIN_SRC 
    src/Lib/clj-cost-function/src/clj_cost_function/core.clj
    Lib/Fraction2.jl
#+END_SRC

use after that in 

#+BEGIN_SRC 
    src/step11-proof-of-concept
#+END_SRC
  
