
"""
M1: first transition matrix
M2: second transition matrix
Eigenvectors corresponding to \lambda = 1 are steady state vectors.
Function calculates cos(theta) of two steady states vectors corresponding to two transition matrices.
"""
function costh(M1,M2)
    sigmas1, eigvectors1 = eig(M1)
    sigmas2, eigvectors2 = eig(M2)
    steady_state1 = eigvectors1[:,1]
    steady_state2 = eigvectors2[:,1]
    cos_theta = dot(steady_state1, steady_state2)
end


include("./sax.jl")
xs=cumsum(randn(1000))
xs = readdlm("./data/indata.dat")
# r=running_dot_product(xs)
r=running_dot_product_learn(xs, 2000, 5, 2000)
writedlm("./img/dat/res.txt", r)
writedlm("./img/dat/sig.txt", xs)
