
function cut_points(alphabet_size)
    c_points = []
    if alphabet_size== 2
        c_points  = [0]
    elseif alphabet_size == 3
       c_points  = [-0.43, 0.43];
   elseif alphabet_size == 4
       c_points  = [-0.67, 0, 0.67]
   elseif alphabet_size == 5
       c_points  = [-0.84, -0.25, 0.25, 0.84]
   elseif alphabet_size == 6
       c_points  = [-0.97, -0.43, 0, 0.43, 0.97]
   elseif alphabet_size == 7
       c_points  = [-1.07, -0.57, -0.18, 0.18, 0.57, 1.07]
   elseif alphabet_size == 8
       c_points  = [-1.15, -0.67, -0.32, 0, 0.32, 0.67, 1.15]
   elseif alphabet_size == 9
       c_points  = [-1.22, -0.76, -0.43, -0.14, 0.14, 0.43, 0.76, 1.22]
   elseif alphabet_size == 10
       c_points = [-1.28, -0.84, -0.52, -0.25, 0.0, 0.25, 0.52, 0.84, 1.28]
   elseif alphabet_size == 11
       c_points = [-1.34, -0.91, -0.6, -0.35, -0.11, 0.11, 0.35, 0.6, 0.91, 1.34]
   elseif alphabet_size == 12
       c_points = [-1.38, -0.97, -0.67, -0.43, -0.21, 0, 0.21, 0.43, 0.67, 0.97, 1.38]
   elseif alphabet_size == 13
       c_points = [-1.43, -1.02, -0.74, -0.5, -0.29, -0.1, 0.1, 0.29, 0.5, 0.74, 1.02, 1.43]
   elseif alphabet_size == 14
       c_points = [-1.47, -1.07, -0.79, -0.57, -0.37, -0.18, 0, 0.18, 0.37, 0.57, 0.79, 1.07, 1.47]
   elseif alphabet_size == 15
       c_points = [-1.5, -1.11, -0.84, -0.62, -0.43, -0.25, -0.08, 0.08, 0.25, 0.43, 0.62, 0.84, 1.11, 1.5]
   elseif alphabet_size == 16
       c_points = [-1.53, -1.15, -0.89, -0.67, -0.49, -0.32, -0.16, 0, 0.16, 0.32, 0.49, 0.67, 0.89, 1.15, 1.53]
   elseif alphabet_size == 17
       c_points = [-1.56, -1.19, -0.93, -0.72, -0.54, -0.38, -0.22, -0.07, 0.07, 0.22, 0.38, 0.54, 0.72, 0.93, 1.19, 1.56]
   elseif alphabet_size == 18
       c_points = [-1.59, -1.22, -0.97, -0.76, -0.59, -0.43, -0.28, -0.14, 0, 0.14, 0.28, 0.43, 0.59, 0.76, 0.97, 1.22, 1.59]
   elseif alphabet_size == 19
       c_points = [-1.62, -1.25, -1, -0.8, -0.63, -0.48, -0.34, -0.2, -0.07, 0.07, 0.2, 0.34, 0.48, 0.63, 0.8, 1, 1.25, 1.62]
   elseif alphabet_size == 20
       c_points = [-1.64, -1.28, -1.04, -0.84, -0.67, -0.52, -0.39, -0.25, -0.13, 0, 0.13, 0.25, 0.39, 0.52, 0.67, 0.84, 1.04, 1.28, 1.64]
   else
       println("Error! alphabet_size is too big")
   end
   c_points
end


function timeseries2symbol(data, alphabet_size)
    alphabet = ['a':'z';]
    n = length(data)
    data = (data - mean(data))/std(data)
    symbrepr = []
    c_points = cut_points(alphabet_size)
    for i=1:n
        # println(i)
        symbol_num = sum(c_points .< data[i])+1
        push!(symbrepr, alphabet[symbol_num])
    end
    symbrepr
end

"""
> ['a','a','b','c', 'a', 'c']

|---+---+---+---|
|   | a | b | c |
|---+---+---+---|
| a | 1 | 1 | 1 |
| b | 0 | 0 | 1 |
| c | 1 | 0 | 0 |
|---+---+---+---|
"""
function calc_matrix(s, alph_size)
    # M = zeros(length(alph), length(alph))
    M = zeros(alph_size, alph_size)
    alph = collect('a':'z')[1:alph_size]
    n=length(s)
    @assert n>1
    #get all combinations
    # combinations = [(a1,a2) for a1 in alph for a2 in alph]
    # indices = [(find(alph .== e[1])[1], find(alph .== e[2])[1]) for e in combinations]
    actual_combinations = (x=zip(s[1:(end-1)], s[2:end]); collect(x))
    actual_indices = [(find(alph .== e[1])[1], find(alph .== e[2])[1]) for e in actual_combinations]
    for el in actual_indices
        M[el[1],el[2]] += 1
    end
    for i in 1:size(M)[1]
        s=sum(M[:,i])
        if s != 0
            M[:,i] = M[:,i]/s
        elseif s==0
            M[i,i]=1
        end
    end
    M
end
# r=calc_matrix(3, ['a','a'])
# println(r)
#
# function calc_matrix(s)
#     alph = sort(unique(s))
#     calc_matrix(length(alph), s)
# end

"""
Calc. steady state vector for a transition matrix calculated from the SAX
representation of a time series.
"""
function steady_state(s, alphabet_size)
    M = calc_matrix(s, alphabet_size)
    sigmas, vs = eig(M)
    #@assert isapprox(sigmas[1], 1.0 + 0.0im)
    v=vs[:,1]
    steady_state_vec = zeros(length(v))
    for k =  1:length(v)
        steady_state_vec[k] = real(v[k])# v[k].re
    end
    dp = M * steady_state_vec
    #@assert isapprox(dp, steady_state_vec)
    steady_state_vec
end

"""
Dot products of steady states vectors
"""
function running_dot_product(sig)
    r = timeseries2symbol(sig, 5)
    a_size  = length(unique(r))
    n=length(r)
    out=zeros(n)
    vecs = []
    for i=2:n
        v = steady_state(r[1:i], a_size)
        push!(vecs, v)
        if i > 2
           out[i] = dot(vecs[(end-1)], v)
        end
    end
    out
end

# [1:k_learn]
function running_dot_product_learn(sig, k_learn, a_size, slide_wnd)
    r_learn = timeseries2symbol(sig[1:k_learn], a_size)
    v_steady = steady_state(r_learn, a_size)
    n=length(sig)
    @assert n > k_learn
    products=zeros(n)
    r=timeseries2symbol(sig, a_size)
    for i=(k_learn+1):n
        v = steady_state(r[(max(1, (i-slide_wnd))):i], a_size)
        products[i-Int(slide_wnd/2)+1]=dot(v_steady, v)
        # println(products[i])
    end
    products
end




# function test_calc_matrix()
    # r = timeseries2symbol(cumsum(randn(10)), 3)
    #r=calc_matrix(['a':'e';], ['a','a','b','e', 'a', 'c'])
    #for i=1:size(r)[1]
#        println(r[i,:])#
    #end
    #println("\n")
    #println(sum(r,1))
    #println("\n")
    # println("Steady state:", steady_state(['a','a','b','e', 'a', 'c']))
# end
# test_calc_matrix()
