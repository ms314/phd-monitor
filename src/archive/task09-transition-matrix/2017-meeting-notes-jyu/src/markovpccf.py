import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm


def gen_ts_ps(mu=15, sigma=1, t=50, t_step=0.05):
    """
    Generate time stamps and normalized probabilities.
    """
    ts = np.arange(1, t, t_step)
    N = len(ts)
    ps = norm.pdf(ts, loc=mu,  scale=sigma)
    ps /= np.sum(ps)  # normalize to sum up to 1.0
    print("np.sum(ps)", np.sum(ps))
    expected_value = np.sum(ps)/len(ps)
    print("expected_value", expected_value)
    return ts, ps

