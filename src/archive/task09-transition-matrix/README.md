Structural change detection using transition Markov matrix obtained from the SAX representation of time series
==========================================================


Idea

- Input is a time series vector ```x=[1.01, 0.09, 1.5,...]```  
- Discretized representation is obtained using [SAX](http://www.cs.ucr.edu/~eamonn/SAX.htm) (Keogh et.al.)  
- After SAX representation is obtained we calculate the transition matrix from one state to other states  

Example

```
> ['a','a','b','c', 'a', 'c']

|---+---+---+---|
|   | a | b | c |
|---+---+---+---|
| a | 1 | 1 | 1 |
| b | 0 | 0 | 1 |
| c | 1 | 0 | 0 |
|---+---+---+---|


```

- Transition matrix is a Markov matrix  
- Every Markov matrix has an eigenvalue $λ = 1$ and associated with it a steady state vector  
- Using training subset we calculate a **stable state** steady vector  
- Further we update transition matrix using new data and calculate the dot product of it with the stable state steady vector  
- The change is alarmed when there is a large angle between two vectors  

_Note: earlier (WHEN WE DISCUSSED IT) I didn't know how to compare two transition matrices_
