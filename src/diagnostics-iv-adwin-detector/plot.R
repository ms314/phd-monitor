#setwd("~/gitlocal/phd-monitor/src/diagnostics-iv-adwin-detector")
ws <- read.table("./out/ws.dat")$V1
x <- read.table("./out/sig.dat")$V1
cdes <- read.table("./out/cdes.dat")$V1
chps <- read.table("./out/chps.dat")$V1

par(mfrow=c(2,1))
plot(x, type="b")
abline(v=chps,col="red")
plot(ws, type="l")
abline(v=cdes,col="blue")
abline(v=chps,col="red")

plotPrinterOutput <- function(){
    M <- read.table("./out/printerOutput.dat", header = FALSE, sep=",")
    ecut <- M$V1
    delay <- M$V2
    numFa <- M$V3
    plot(ecut, delay, type="l")
    abline(v=0.02)
}
plotPrinterOutput()
