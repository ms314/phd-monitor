
include("../Lib/DetectorPageHinkley.jl")
include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/common.jl")
include("../Lib/PerformanceMetrics.jl")

using CSV, DataFrames
using DelimitedFiles
using Printf

function adwinFinOptim(x; changes=[])
    println()
    println(".. Start: optim settings: ADWIN")
    ## START: FIND OPTIM SETTINGS
    best_so_far = 1000.
    best_ecut = -1.
    best_cdes = []
    count = 0
    best_delay = -1.
    best_fa_rate = -1.0
    best_num_fa = -1
    for ecut = 0.1:0.1:10.0
        minLen = 5
        ws = adwinNaive(x, ecut_custom=ecut, min_len=minLen, adaptiveSettings=NoSettings())
        cdes = extractChps(ws)
        lossV, delay, probFa, num_fa = lossFunction(;changes=changes, detections=cdes, lenOfSig=length(x), retAlsoDelayAndFa=true)
        count+=1
        if count == 1
            best_so_far = lossV
        else
            if lossV < best_so_far
                best_so_far = lossV
                best_cdes = cdes 
                best_ecut = ecut
                best_delay = delay
                best_fa_rate = probFa
                best_num_fa = num_fa
            end
        end
    end
    println("lossV=$best_so_far, ecut=$best_ecut, cdes = ", best_cdes)
    println("Avg. delay = $best_delay")
    println("FA rate = $best_fa_rate")
    println("Number of FAs = $best_num_fa")
    println("End: optim settings: ADWIN")
end

function diagRecCut()
    # r = recReduce!([1.,1.,1.,2.,2.,2.], 0.1)
    r = recReduce!([1.,2.], 0.1)
    println(r)
end

function printDelayVarEcut(x; chps::Array{Int64}=[])
    N = 100
    M = zeros(N, 3)
    c = 0
    for ecut in range(0.01, stop=1.0, length = 100)
        c += 1
        ws = adwinNaive(x, ecut_custom=ecut)
        cdes = extractChps(ws)
        lossV, delay, probFa, num_fa = lossFunction(;changes=chps, detections=cdes, lenOfSig=length(x), retAlsoDelayAndFa=true)
        M[c,:] = [ecut, delay, num_fa]
        if 1==0
            @printf("ecut = %0.3f, delay=%2d, num(fa)=%2d\n", ecut, delay, num_fa)
        end
    end
    fout ="./out/printerOutput.dat" 
    open(fout, "w") do io writedlm(io, M) end
    println("save into $fout")
end 

function runDiagAdwin()
    sigma = 0.3
    x = vcat(randn(100) .* sigma, 
             randn(100) .* sigma .+ 1.0,
             randn(100) .* sigma)
    chps=[100, 200]
    if 1==0
        println("Run Lib/optimization procedure to find optim. ADWIN settings.")
        adwinFinOptim(x, changes=chps)
    end
    if 1==0
        println("Run ADWIN with a given ecut")
        ## checking why when ecut is very small, delay is big
        ## 2019/02/08: why cdes is empty with a very small ecut??
        ws = adwinNaive(x, ecut_custom=0.02)
        # println(ws)
        cdes = extractChps(ws)
        # println("cdes=$cdes")
        open("./out/ws.dat", "w") do io writedlm(io, ws) end 
        open("./out/sig.dat", "w") do io writedlm(io, x) end 
        open("./out/cdes.dat", "w") do io writedlm(io, cdes) end 
        open("./out/chps.dat", "w") do io writedlm(io, chps) end 
    end
    if 1==1
        println("Run printerEcut: local implementation of optimization of ADWIN")
        printDelayVarEcut(x, chps=chps)
    end 
end
runDiagAdwin()

# diagRecCut()