include("../Lib/common.jl")
include("../Lib/AdwinNaive.jl")
include("../Lib/SignalGenerator.jl")
include("../Lib/Roc.jl")
include("../Lib/Fractions.jl")
include("../Lib/Pccf.jl")
include("../Lib/recmidpoints.jl")

using JSON
using SignalGenerator, Fractions


function sim2adwin(outputfolder)
    println("... Run sim2")
    r = open("./config.json") do f
        JSON.parse(f)
    end
    mu0        = r["sim2"]["mu0"]
    sd0        = r["sim2"]["sd0"]
    n_chps     = r["sim2"]["n_chps"]
    chpStep    = r["sim2"]["chpStep"]
    max_delay  = r["sim2"]["max_delay"]
    sig_std     = r["sim2"]["sig_std"]
    #threshold  = 0.3
    #max_d      = 500
    avg_frac_chps = []
    avg_frac_fa   = []
    for theta in 1:0.01:2
        ret          = genSig(n_chps, mu0, sd0, chpStep, sig_std)
        sig          = ret["sig"]
        changes      = ret["changes"]
        n_sig        = length(sig)
        ws = adwin_naive(sig, min_len=2, ecut_custom =  0.5/2.5)
        detections = extractChps(ws)
        println("length(detections)=", length(detections),",  theta=", theta)

        cc=fracOfDetected(chps=changes, cdes=detections, maxDelay=max_delay)
        ff=fracOfFa(; chps=changes, cdes=detections, maxDelay=max_delay, n=n_sig)
        push!(avg_frac_fa,ff)
        push!(avg_frac_chps, cc)
    end
    writedlm(outputfolder * "fractions_chps_adwin.txt", avg_frac_chps)
    writedlm(outputfolder * "fractions_fa_adwin.txt", avg_frac_fa)
    println("... Saved results into " * outputfolder * "fractions_chps_adwin.txt")
    println("... Saved results into " * outputfolder * "fractions_fa_adwin.txt")
end
sim2adwin("out/")
