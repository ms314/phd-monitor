package example
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
//import org.scalajs.dom.html
import scala.util.Random
import scala.io.Source
import org.scalajs.dom.{document => doc}
import org.scalajs.dom.raw._
import perfmetrics._

//object dataObj{
//  var content:String = "emptyData"
//}

class MapCanvas(w:Int, h:Int, ys:Array[Double]) {
  def mapX(x:Int):Int = {
    assert(x<=ys.length)
    val v = (w * x.toDouble/ys.length).toInt
    assert(v>=0)
    v
  }
  def mapY(y:Double):Int = {
    assert(y<=ys.max && y>=ys.min)
    val v = (h * (y-ys.min) / (ys.max-ys.min)).toInt
//    assert(v>=0)
    v
  }
}


@JSExport
object ScalaJSExample{

  val W = 900
  val H = 180

  val Wsmall = 900
  val Hsmall = 300
  val ShiftRight = (Wsmall/2)

  var signalLength = 100 // used in Julia
  var content:String = "empty"
//  var cdesFromFile = Array[Int]()
  var cdesFromMatrix = Array[Array[Int]](Array(100, 200), Array(300, 400), Array(500, 600))
  var contRead = false
  var changes = Array[Int](1)
  var costF = Array[Double]()
  //# Sensitivity, Delay, faRate. Loss, NumOfCdes
  var perfMatrix = Array[Array[Double]]()
  var sensitivityV = Array[Double](1.0, 2.0, 3.0)
  var delayV = Array[Double](1.0, 2.0, 3.0)
  var faRateV = Array[Double](1.0, 2.0, 3.0)
  var lossV = Array[Double](1.0, 2.0, 3.0)
  var numCdesV = Array[Int](1, 2, 3)

  val fileInput = dom.document.getElementById("myFile").asInstanceOf[dom.html.Input]

  var count = 0

  fileInput.onchange = _ => {
    clearPerfPlots()
    clear()
    count=0
    val reader = new FileReader()
    reader.onload = _ => {
      println(reader.result)
      // Read into global var inside .onload !
      content = reader.result.asInstanceOf[String]
      // # Sensitivity, Delay, faRate. Loss, NumOfCdes
      perfMatrix = content.replace("\\s", "").split('\n').map{lines => lines.split(",").flatMap(x => scala.util.Try(x.toDouble).toOption)}
//      dom.document.getElementById("txtOutput").innerHTML = perfMatrix(0).mkString(",")
      cdesFromMatrix = perfMatrix.map(r => r.slice(5, r.length).map(_.toInt))// r.tail.map(_.toInt))
      sensitivityV = perfMatrix.map(r => r(0))
      delayV = perfMatrix.map(r => r(1))
      faRateV  = perfMatrix.map(r => r(2))
      lossV = perfMatrix.map(r => r(3))
      numCdesV = perfMatrix.map(r => r(4).toInt)

    }
    reader.readAsText(fileInput.files(0))
    contRead = true
    if(1==0){
      // Maybe put insed .onload event!
//      cdesFromFile = content.replace(" ", "").split('\n').flatMap(x => scala.util.Try(x.toInt).toOption)
//      dom.document.getElementById("outText").innerHTML = "CDEs from file:" + dataObj.content + ";" + content
    }

  }

  def mapToCanvas(loc:Int):Int={ ((W/signalLength.toDouble) * loc).toInt }

  val ctx = dom.document
    .getElementById("canvas")
    .asInstanceOf[dom.html.Canvas]
    .getContext("2d")
    .asInstanceOf[dom.CanvasRenderingContext2D]

  val ctxDelayFaRate = dom.document
    .getElementById("canvasDelayFaRate")
    .asInstanceOf[dom.html.Canvas]
    .getContext("2d")
    .asInstanceOf[dom.CanvasRenderingContext2D]

  def clearPerfPlots()={
    ctxDelayFaRate.fillStyle = "black"
    ctxDelayFaRate.fillRect(0, 0, Wsmall, Hsmall)
    ctxDelayFaRate.fillStyle = "cyan"
    ctxDelayFaRate.fillRect(Wsmall/3, 0, 1, Hsmall)
    ctxDelayFaRate.fillRect(2*Wsmall/3, 0, 1, Hsmall)
    // text
    ctxDelayFaRate.fillStyle = "yellow"
    ctxDelayFaRate.fillText("DELAY", Wsmall/3 - 50, 20)
    ctxDelayFaRate.fillText("FA RATE", 2*Wsmall/3 - 50,20)
    ctxDelayFaRate.fillText("LOSS", Wsmall-50,20)
  }

  def clear() = {
    ctx.fillStyle = "black"
    ctx.fillRect(0, 0, W, H)
    for (chps <- changes){
      ctx.fillStyle="cyan"
      ctx.fillRect(mapToCanvas(chps), 0, 3, H)
    }
  }


  clear()
  clearPerfPlots()
  def run = for(i <- 0 to 2){
    val n = cdesFromMatrix.length
    count = (count + 1) % (n-1)
    if(count==0) {
      clear()
      clearPerfPlots()
    }
    clear()
    val cdes = cdesFromMatrix(count)

    for(c <- cdes){
        ctx.fillStyle="red"
        ctx.fillRect(mapToCanvas(c), 0, 1, H)
    }

    val mapDel  = new MapCanvas(Wsmall/3, Hsmall, delayV)
    val mapFrt  = new MapCanvas(Wsmall/3, Hsmall, faRateV)
    val mapLoss = new MapCanvas(Wsmall/3, Hsmall, lossV)

//    if(mapDel.mapY(delayV(count)) < 0){
//      dom.console.log("map(count)=" + mapDel.mapX(count), "map(delay)="+mapDel.mapY(delayV(count)), "count="+count, "delay="+delayV(count), "min(delay)="+delayV.min, "max(delay)="+delayV.max)
//    }
    ctxDelayFaRate.beginPath()
    ctxDelayFaRate.fillStyle = "white"
    ctxDelayFaRate.fillRect(mapDel.mapX(count), Hsmall-mapDel.mapY(delayV(count))+3, 3, 3)
    //
    ctxDelayFaRate.strokeStyle="white"
    ctxDelayFaRate.lineWidth = 3.0
    ctxDelayFaRate.moveTo(mapDel.mapX(count-1), Hsmall-mapDel.mapY(delayV(count-1)))
    ctxDelayFaRate.lineTo(mapDel.mapX(count)  , Hsmall-mapDel.mapY(delayV(count)))
    ctxDelayFaRate.stroke()

    //
    ctxDelayFaRate.fillStyle = "red"
    ctxDelayFaRate.fillRect(mapFrt.mapX(count)+Wsmall/3, Hsmall-mapFrt.mapY(faRateV(count)), 2, 2)
    //
    ctxDelayFaRate.beginPath()
    ctxDelayFaRate.strokeStyle="red"
    ctxDelayFaRate.moveTo(mapFrt.mapX(count-1)+Wsmall/3, Hsmall-mapFrt.mapY(faRateV(count-1)))
    ctxDelayFaRate.lineTo(mapFrt.mapX(count)+Wsmall/3, Hsmall-mapFrt.mapY(faRateV(count)))
    ctxDelayFaRate.stroke()
    //
    ctxDelayFaRate.fillStyle = "yellow"
    ctxDelayFaRate.fillRect(mapLoss.mapX(count)+2*Wsmall/3, Hsmall-mapLoss.mapY(lossV(count)), 2, 2)
    //
    ctxDelayFaRate.beginPath()
    ctxDelayFaRate.strokeStyle="yellow"
    ctxDelayFaRate.moveTo(mapLoss.mapX(count-1)+2*Wsmall/3, Hsmall-mapLoss.mapY(lossV(count-1)))
    ctxDelayFaRate.lineTo(mapLoss.mapX(count)+2*Wsmall/3, Hsmall-mapLoss.mapY(lossV(count)))
    ctxDelayFaRate.stroke()

    dom.document.getElementById("outLambda").innerHTML   = "Lambda : " + sensitivityV(count)
    dom.document.getElementById("outDelay").innerHTML  = "Delay  : " + delayV(count)
    dom.document.getElementById("outFaRate").innerHTML = "len(CDEs)-len(Chps): " + numCdesV(count)
    dom.document.getElementById("outObsNum").innerHTML = "N: " + count + ", out of " + n
//    dom.document.getElementById("outLoss").innerHTML   = "Loss   : " + lossV(count)
  }


  @JSExport
  def main(): Unit = {
     // https://vimeo.com/87845442
    val changesTwi = Array[Int](31,53,79,135,175,225,280)
    val changesArtSig = Array[Int](100, 200)
    val sigLenTwi = 304
    val sigLenArt = 300
    dom.console.log("main")
    changes = changesArtSig // changesTwi
    signalLength = sigLenArt
    clear()
    for (chps <- changes){
      ctx.fillStyle="cyan"
      ctx.fillRect(mapToCanvas(chps), 0, 3, H)
    }
    dom.window.setInterval(() => run, 100)
  }
}