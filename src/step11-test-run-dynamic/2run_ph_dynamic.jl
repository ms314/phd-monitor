#
# From task8

include("../Lib/detectors/PageHinkley.jl")
include("../Lib/detectors/PageHinkleyPccfDynamic.jl")
include("../Lib/detectors/BayesDetectorReplica.jl")
include("../Lib/Fractions.jl")
include("../Lib/detectors/adwin/Adwin.jl")
include("../Lib/detectors/adwin/AdwinNaive.jl")
include("../Lib/Common.jl")

using PageHinkley, BayesChpOrig, Fractions, AdwinModule, AdwinNaive, PageHinkleyPccfDynamic


function runPageHinkley(x, trueChp, maxDelayIn,sigNum::Int64)
    #s = readJSON("./config_task08.json")
    s = readJSON("./config_task11_manual_settings.json")
    delta = s["opts" * string(sigNum)]["PH"]["delta"]  #0.0
    lambda = 1.0 / s["opts" * string(sigNum)]["PH"]["s"]

    println("... json PH read, delta=$delta, lambda=$lambda")
    cdes = detectorPH(x, delta, lambda)

    delay = detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength = length(x))
    probFA = fracOfFa(chps=trueChp, cdes=cdes, maxDelay=maxDelayIn)
    (delay, probFA, cdes)
end


function runPageHinkleyDynamicPccf(x, trueChp, maxDelayIn, sigNum::Int64; dynamicHpath="./output/best_possible_pccf1.dat", FACTOR=2)
    println("..Read optimal settings (learnt in another task) from config file")
    # s = readJSON("./config_task08.json")
    s = readJSON("./config_task11_manual_settings.json")
    delta = s["opts" * string(sigNum)]["PH"]["delta"]  #0.0
    lambda = 1.0 / s["opts" * string(sigNum)]["PH"]["s"]

    lambdaDynamic = readInputSigInt(dynamicHpath)

    println("... json PH read, delta=$delta, lambda=$lambda")
    ## RUN DYNAMIC PH DETECTOR
    cdes = detectorPhPccfDynamic(x, delta, lambdaDynamic,
                                 lambdaWhenDynamic0 = lambda * FACTOR,
                                 lambdaWhenDynamic1 = lambda) ## So that lambda is the same as in the statis case.

    delay = detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength = length(x))
    probFA = fracOfFa(chps=trueChp, cdes=cdes, maxDelay=maxDelayIn)
    (delay, probFA, cdes)
end


function runDetectorsTask11(; mult_lambda0_factor=1.1)
    @assert mult_lambda0_factor >= 1.0
    MAX_DELAY = 5
    local_s = readJSON("./config_task08.json")
    s = readJSON("../config_datasets.json")
    n_of_signals = s["numofsignals"]
    signals = ["sig" * string(e) for e in 1:n_of_signals]
    chps = ["chps" * string(e) for e in 1:n_of_signals]

    @assert length(signals) == length(chps)

    delays_PH_avg   = []
    fa_rates_PH     = []
    delays_PH_D_avg = []
    fa_rates_PH_D   = []

    for i in 1: n_of_signals
        sigpath = s["dir"] * s[signals[i]]
        chpspath = s["dir"] * s[chps[i]]
        println("\nRead signal " * sigpath)
        println("Read changes " * chpspath)
        changes = readInputChanges(chpspath)
        sig01 = readInputSig(sigpath)

        # if i==1
        #     multFact = 2
        # elseif i == 2
        #     multFact = 2
        # elseif i == 3
        #     multFact = 2
        # elseif i == 4
        #     multFact = 2
        # elseif i == 5
        #     multFact = 2
        # elseif i == 6
        #     multFact = 2
        # else
        #     multFact = mult_lambda0_factor 
        # end 

        multFact = mult_lambda0_factor 

        ##(d_Bayes, fa_Bayes, cdesBayes) = runBayesian(sig01, changes, MAX_DELAY,i)
        ##(d_AdwNaive, fa_AdwNaive, cdesAdwinNaive) = runAdwinNaive(sig01, changes, MAX_DELAY,i)
        (d_PH  , fa_PH  , cdesPH) = runPageHinkley(sig01, changes, MAX_DELAY,i)
        (d_PH_D, fa_PH_D, cdesPH_D) = runPageHinkleyDynamicPccf(sig01,
                                                                changes,
                                                                MAX_DELAY,
                                                                i,
                                                                dynamicHpath="./output/best_possible_pccf" * string(i) * ".dat",
                                                                FACTOR = multFact)
        push!(delays_PH_avg   , mean(d_PH))
        push!(fa_rates_PH     , fa_PH)
        push!(delays_PH_D_avg , mean(d_PH_D))
        push!(fa_rates_PH_D   , fa_PH_D)

        println("Changes: ", changes, "; length(changes)=", length(changes))
        println("\n")
        # println("Bayesian detections:", cdesBayes, "; length(cdesBayes)=",length(cdesBayes))
        # println("Adwin detections:"   , cdesAdwinNaive, "; length(cdesAdwinNaive=", length(cdesAdwinNaive))
        println("PH detections:"      , cdesPH, "; length(cdesPH)=",length(cdesPH))

        outpathPH_cdes         = "./output2/sig" * string(i) * "_cdes_PH.dat" ## s["outputfolder"]*"task08/sig" * string(i) * "_cdes_PH" * ".dat"
        outpathPH_D_cdes       = "./output2/sig" * string(i) * "_cdes_PH_D.dat"

        writedlm(outpathPH_cdes, cdesPH, ',')
        writedlm(outpathPH_D_cdes, cdesPH_D, ',')

        println("..write:", outpathPH_cdes)
        println("..write:", outpathPH_D_cdes)

        #outpathBayes = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_Bayes" * ".txt"
        #println("Save results: $outpathBayes")
        #writedlm(outpathBayes, cdesBayes, ',')

        #outpathAdwin = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_Adwin" * ".txt"
        #println("Save results: $outpathAdwin")
        #writedlm(outpathAdwin, cdesAdwinNaive, ',')

    end

    outpathPH_AvgDelays    = "./output2/AvgDelays_PH.dat"
    outpath_PH_D_AvgDelays = "./output2/AvgDelays_PH_D.dat" ##dynamic
    outpathPH_farates      = "./output2/FaRates_PH.dat"
    outpathPH_D_farates    = "./output2/FaRates_PH_D.dat"

    writedlm(outpathPH_AvgDelays   , delays_PH_avg   , ',')
    writedlm(outpath_PH_D_AvgDelays, delays_PH_D_avg , ',')
    writedlm(outpathPH_farates     , fa_rates_PH     , ',')
    writedlm(outpathPH_D_farates   , fa_rates_PH_D   , ',') 

    println("... write:", outpathPH_AvgDelays)
    println("... write:", outpath_PH_D_AvgDelays)
    println("... write:", outpathPH_farates)
    println("... write:", outpathPH_D_farates)

end
runDetectorsTask11(mult_lambda0_factor = 10)
