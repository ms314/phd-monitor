
- 2018-11-27: here is a ad-hoc test run of detectors using dynamic settings 
  the next step is step13 using step12

# Best possible Pccf benchmark

    Increase detector's sensitivity right near and after actual
    changes locations to achieve the best possible result.
    
    When using calculated Pccf's we should obtain results slightly
    worse that obtained in this experiment.
