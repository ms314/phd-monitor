# J.Gama book 
# p. 41 (el.55) - CUSUM
# 
# https://github.com/SmileYuhao/ConceptDrift/blob/master/concept_drift/page_hinkley.py
# 

module PageHinkley 

export detectorPH

update_mu(x, muOld, nOld) = (nOld * muOld+x)/(nOld+1.0)

function detectorPH(y, δ, λ)
    n=length(y)
    runningSum = 0.0 ## running sum 
    changeDetected = false 
    nᵣ = 0
    μᵣ = 0.0 
    s_plus = 0.0
    s_minus = 0.0
    s_vec_plus = zeros(n) ## positive changes 
    s_vec_minus = zeros(n) ## negative changes 
    detections = []
    for i=1:n 
        x = y[i]
        if nᵣ == 0
            μᵣ = x
            nᵣ = 1
            s_plus = 0.0
            s_minus = 0.0
        else
            μᵣ = update_mu(x, μᵣ, nᵣ) 
            nᵣ += 1
            s_plus += (x - μᵣ - δ)
            s_minus -= (x - μᵣ - δ)
            if s_plus > λ || s_minus > λ
                changeDetected = true
                nᵣ = 0
                μᵣ = 0.0
                s_plus = 0.0
            else 
                changeDetected = false
            end 
        end 
        s_vec_plus[i] = s_plus
        s_vec_minus[i] = s_minus
        if changeDetected
            println("Change :", i)
            push!(detections, i)
        end 
    end 
    writedlm("C:/1maslov/tmp/sp.txt", s_vec_plus)
    writedlm("C:/1maslov/tmp/sm.txt", s_vec_minus)
    writedlm("C:/1maslov/tmp/detections.txt", detections)
end

function run()
    ## x=vcat(randn(15), randn(15)+10, randn(15)-10);  writedlm("C:/1maslov/tmp/tstsig_small.txt",x)
    ## 
    sig = readdlm("C:/1maslov/tmp/tstsig_small.txt")
    # sig = readdlm("C:/1maslov/tmp/tstsig2.txt")
    println(size(sig))
    detectorPH(sig, 0.0, 30)
end
run()

end # module 