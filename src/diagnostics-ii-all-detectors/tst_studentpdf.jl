using Statistics, SpecialFunctions
using UnicodePlots

function constant_hazard(r, lambda)
    p = 1.0 / lambda .* ones(size(r))
end

"""Non-standardized Student's t-distribution"""
## As in the Matlab code by author:
# c = exp(gammaln(nu/2 + 0.5) - gammaln(nu/2)) .* (nu.*pi.*var).^(-0.5);
# p = c .* (1 + (1./(nu.*var)).*(x-mu).^2).^(-(nu+1)/2);
## Check Eq.110: ~\cite{murphy2007conjugate}
function studentpdfold(x::Float64,
                       mu::Array{Float64,1},
                       var::Array{Float64,1},
                       nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (nu .* pi .* var) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ (nu .* var)) .* (x .- mu).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else 
        p
    end
end

"""
My version: possibly a fixed typo
"""
function studentpdfmine(x::Float64,
                        mu::Array{Float64,1},
                        sigma::Array{Float64,1},
                        nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (pi .* nu) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ nu ) .* ((x .- mu)./sigma).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else 
        p
    end
end

function main()
    f(x) = studentpdfmine(x, [1.,2.], [2., 3.], [1., 1.1])
    ms = [-5.1:0.1:5.1;]
    y1 = f.(ms)
    y2 = f(1.)
    println(y1)
    println(">>>")
    println(y2)
    # lineplot(ms, f.(ms))
    # lineplot(ms, ms)
end 
main()