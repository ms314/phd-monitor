include("../Lib/DetectorPageHinkley.jl")
include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/common.jl")
include("../Lib/PerformanceMetrics.jl")

using CSV, DataFrames
using DelimitedFiles

function sprint(a, n=10)
    if length(a) < n
        "[" * join(a, ",") * "]"
    else
        "[" * join(a[1:n], ",") * "...]"
    end
end

function diagPH(x; changes=[], outcdes="./outcdesph.csv", outstats="./outstats.csv", findOptimSettings=false, saveOutput=true)
    if findOptimSettings
        println()
        println(".. Start: optim settings: PH detector")
        best_so_far = 1000.
        best_l = -1.
        best_cdes = []
        count = 0
        for l in [1.0 : 1.0: 100.0; ]
            count += 1
            delta  = 0.0
            cdes = detectorPH(x, delta, l, returnTestStatistic=true, saveOutput=false)
            lossV = lossFunction(;changes=changes, detections=cdes, lenOfSig=length(x))
            if count == 1
                best_so_far = lossV
                # println("Init optim settings: lossV=$lossV, lambda=$l, cdes = ", sprint(cdes))
            else
                if lossV < best_so_far
                    best_so_far = lossV
                    best_l = l
                    best_cdes = cdes
                    # println("New optim settungs: lossV=$lossV, lambda=$l, cdes = ", sprint(cdes))
                end
            end
        end
        println("lossV=$best_so_far, lambda=$best_l, cdes = ", sprint(best_cdes))
        println(".. End: optim settings: PH detector")
    else
        # detectorPH = createPhDetector(delta=0.0)
        # resPHDiag = detectorPH(returnTestStatistic=true)
        # runDetectorWithSettings([1:9;], detector=detectorPH)
        # println(resPHDiag)
        optimVal1 = 1.0
        delta  = 0.0
        lambda = optimVal1
        cdes = detectorPH(x, delta, lambda, returnTestStatistic=true, outCsvTestStatistic=outstats)
        if saveOutput
            open(outcdes, "w") do io
                writedlm(io, cdes, ',')
            end
        end
    end
end

function diagAdwin(x; changes=[], out="./out.csv", outcdes="./outcdes.csv", saveOutput=false, findOptimSettings=false)
    if findOptimSettings
        println()
        println(".. Start: optim settings: ADWIN")
        ## START: FIND OPTIM SETTINGS
        best_so_far = 1000.
        best_ecut = -1.
        best_cdes = []
        count = 0
        for ecut = 0.1:0.1:10.0
            minLen = 5
            ws = adwinNaive(x, ecut_custom=ecut, min_len=minLen, adaptiveSettings=NoSettings())
            cdes = extractChps(ws)
            lossV = lossFunction(;changes=changes, detections=cdes, lenOfSig=length(x))
            count+=1
            if count == 1
                best_so_far = lossV
                # println("Init optim settungs: lossV=$lossV, ecut=$ecut, cdes = ", sprint(cdes))
            else
                if lossV < best_so_far
                    best_so_far = lossV
                    best_cdes = cdes 
                    best_ecut = ecut
                    # println("New optim settungs: lossV=$lossV, ecut=$ecut, cdes = ", sprint(cdes))
                end
            end
        end
        println("lossV=$best_so_far, ecut=$best_ecut, cdes = ", sprint(best_cdes))
        println(".. End: optim settings: ADWIN")
    else
        println("... Run detector with fixed setting")
        minLen = 5
        optimVal1 = 0.3
        ecut = optimVal1 # prev.: 0.76 (!!!much better!), 0.77+0.78(straight line, a lot of time), 0.75 (a lot of noise/changes), 0.8
        ws = adwinNaive(x, ecut_custom=ecut, min_len=minLen, adaptiveSettings=NoSettings())
        cdes = extractChps(ws)
        if saveOutput
            # CSV.write(out, DataFrame(width=ws))
            # CSV.write(outcdes, DataFrame(cdes=cdes))
            open(outcdes, "w") do io
                writedlm(io, cdes, ',')
            end
            println("... Diagnose Adwin detector: save widths into $out")
        end
    end
end


function diagBayes(x; 
                   mu0=0.0,
                   changes=[100],
                   out="./out.csv",
                   outcdes="./cdesbayes.dat",
                   saveOutput=true,
                   findOptimSettings=false,
                   gaussPdf=false,
                   gaussSigma=1.0,
                   newUpdate=false)
    if findOptimSettings
        # println()
        println("... Start: optim settings: BAYESIAN")
        best_so_far = 1000.
        best_l = -1. 
        best_cdes = []
        count = 0
        # Scaling parameters for the cost function
        for l in [1. : .1: 310.0;] #[1.0 : 0.1 : 16.0;] #
            count += 1
            #maxes, mat = bayesDetectorNew(x, lambda=l, mu0=mu0, adaptiveSettings=NoSettings(), gaussPdf=gaussPdf, gaussSigma=gaussSigma)
            maxes, mat = bayesDetector(x, lambda=l, mu0=mu0, adaptiveSettings=NoSettings(), gaussPdf=gaussPdf, gaussSigma=gaussSigma, newUpdate=newUpdate)
            #maxes, mat = bayesDetector3(x, lambda=l, mu0=mu0, adaptiveSettings=NoSettings())
            cdes = extractChangesBayes(maxes)

            lossV = lossFunction(; changes=changes, detections=cdes, lenOfSig=length(x))
            if count == 1
                best_so_far = lossV
            else
                if lossV < best_so_far
                    best_so_far = lossV
                    best_l = l 
                    best_cdes = cdes
                    # println("New optim settungs: lossV=$lossV, lambda=$l, cdes = ", sprint(cdes))
                    # println("New optim settungs: lossV=$lossV, lambda=$l")
                    # println("length(cdes)=",length(cdes))
                end
            end
        end
        println("lossV=$best_so_far, lambda=$best_l, cdes = ", sprint(best_cdes))
        println("... End: optim settings: BAYESIAN")
    else
        println("Run with fixed settings")
        #optimVal1 = 3. #95.
        lambda = 10. #6. #optimVal1
        maxes, mat = bayesDetectorNew(x, lambda=lambda, mu0=mu0, adaptiveSettings=NoSettings())
        # maxes, mat = bayesDetector3(x, lambda=lambda, mu0=mu0, adaptiveSettings=NoSettings())
        cdes = extractChangesBayes(maxes)
        # for i in 1: size(mat)[1] println(mat[i, :]) end
        println("cdes:")
        println(cdes)
        println("maxes:")
        println(maxes)
        println("... End Run with fixed settings")
        if saveOutput
            open(out, "w") do io
                writedlm(io, mat, ',')
            end
            open(outcdes, "w") do io
                writedlm(io, cdes, ',')
            end
        end
    end
end

# function genSig(;n=200, delta=1.0, sigma=1.0)::Array{Float64,1} vcat(randn(n) .* sigma, randn(n+200) .* sigma .+ delta) end

function diagDetectors()
    # x = readInputSig("./dat/signalExample1Chp.dat")
    #x = readInputSig("./dat/sigForBayes.dat")
    #x = readInputSig("./dat/signalExample1Chp2.dat")
    # for e in x println(e) end
    # x = scale01(x)
    # println("Input signal is scaled to (0,1)")
    ## Find optim settings
    sigma = 0.8
    #x = vcat(randn(100) .* sigma, randn(200) .* sigma .+ 1.0)
    x = vcat(randn(100) .* sigma, 
             randn(100) .* sigma .+ 1.0,
             randn(100) .* sigma)
    chps=[100, 200]
    if true
        diagPH(x, changes=chps, findOptimSettings=true)
        diagAdwin(x, changes=chps, findOptimSettings=true)
        # diagBayes(x, changes=chps, findOptimSettings=true, gaussPdf=false, mu0=0.0)
        #println("... BAYES: GAUSS")
        diagBayes(x, changes=chps, findOptimSettings=true, gaussPdf=true, gaussSigma=sigma, mu0=0.0)
        # println("\n... BAYES: NEW UPDATE")
        # diagBayes(x, changes=chps, findOptimSettings=true, gaussPdf=false, gaussSigma=sigma, mu0=0.0, newUpdate=true)
        # println("\n... BAYES: OLD UPDATE")
        # diagBayes(x, changes=chps, findOptimSettings=true, gaussPdf=false, gaussSigma=sigma, mu0=0.0, newUpdate=false)
    end
    ## Run detectors with optim settings
    if false
        #diagAdwin(x, out="./out/diagAdwin.csv", outcdes="./out/diagAdwinCdes.csv", saveOutput=false, findOptimSettings=false)
        #diagBayes(x, outcdes="./out/diagBayesCdes.csv", findOptimSettings=false, saveOutput=true)
        #diagPH(x, outcdes="./out/diagPhCdes.csv", findOptimSettings=false, saveOutput=true, outstats="./out/diagPhStats.csv")
    end
end
diagDetectors()
