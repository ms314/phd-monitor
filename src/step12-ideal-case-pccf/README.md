Experiment settings   

![img1](./img/1changeExample_step12.PNG)


- Generate a signal with one change point in the middle
- Red line depicts an indicator function (ideal Pccf)
- Run Page-Hinkley / Cusum, Bayesian and Adwin detectors using static and dynamic settings
- Obtain cost function for each case while variating sensitivity parameters.
- Plot 6 loss functions (3 for static setting case, 3 for dynamic settings)

Results are depicted on plots below.  
On each plot:  
- x-axis: sensitivity 
- y-axis: loss function value 
- 2 cases on each plot: black - static settings, red - dynamic settings

- 2019/01/10 

Improved results

Static settings case  

![img3](./img/lossf-2019-01-10.PNG)

Dynamic settings case 

![img4a](./img/lossf-2019-01-10-multi.PNG)

- Old (previous results)

if w=50  

![img2](./img/lossf.PNG)


if w=8  

![img2](./img/lossf2.PNG)
