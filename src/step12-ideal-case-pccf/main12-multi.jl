#
# Calculate averagre performance for many generated signals
# simulation , average
#
include("../Lib/DetectorPageHinkley.jl")
include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/PerformanceMetrics.jl")
include("../Lib/common.jl")
include("../Lib/fn_construct_indicator_function.jl")

using Statistics,DelimitedFiles


function genSig(;n=200, delta=1.0, sigma=1.0)::Array{Float64,1}
    vcat(randn(n) .* sigma, randn(n+200) .* sigma .+ delta)
end

function run_average_performance(; nSim::Int64, lengthParamRange::Int64=100, wIndFunc::Int64=30, signalStd=1.0)
    detectorPHref = createPhDetector(delta=0.0)
    detectorAdwin = createAdwinDetector(min_len=10)
    detectorBayes = createBayesianDetector(mu0=0.0, gaussPdf=true, gaussSigma=signalStd)

    ## Find static optimal settings
    n = nSim

    avgLoss1::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss2::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss3::Array{Float64,2} = zeros(lengthParamRange, n)

    avgDelay1::Array{Float64,2} = zeros(lengthParamRange, n)
    avgDelay2::Array{Float64,2} = zeros(lengthParamRange, n)
    avgDelay3::Array{Float64,2} = zeros(lengthParamRange, n)

    avgFaRate1::Array{Float64,2} = zeros(lengthParamRange, n)
    avgFaRate2::Array{Float64,2} = zeros(lengthParamRange, n)
    avgFaRate3::Array{Float64,2} = zeros(lengthParamRange, n)

    avgLoss1Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss2Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss3Dyn::Array{Float64,2} = zeros(lengthParamRange, n)

    avgDelay1Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgDelay2Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgDelay3Dyn::Array{Float64,2} = zeros(lengthParamRange, n)

    avgFaRate1Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgFaRate2Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgFaRate3Dyn::Array{Float64,2} = zeros(lengthParamRange, n)

    c=1.0

    paramRangePh       = range(0.01, stop=15, length=lengthParamRange)
    paramRangeAdw      = range(0.01, stop=1, length=lengthParamRange)
    paramRangeBayes    = range(1.00, stop=120, length=lengthParamRange)

    for i = 1:n
        c+=1.
        progress=c/nSim
        if c % Int(floor(n/10.0)) == 0
            println("..progress: ", progress*100, " %")
        end

        changeLocation=100
        x = genSig(n=changeLocation, delta=1.0, sigma=signalStd)

        ## STATIC
        r1,=optimDetector(x,
                          changes=[changeLocation],
                          detector=detectorPHref,
                          paramrange = paramRangePh,
                          name="PH",
                          printprogress=false)

        r2,=optimDetector(x,
                          changes=[changeLocation],
                          detector=detectorAdwin,
                          paramrange = paramRangeAdw,
                          name="Adwin",
                          printprogress=false)

        r3,=optimDetector(x,
                          changes=[changeLocation],
                          detector=detectorBayes,
                          paramrange = paramRangeBayes,
                          name="Bayes",
                          printprogress=false)


        ## DYNAMIC
        w = wIndFunc
        indFunc = constructIndicatorFunction(length(x), changeLocation, w)
        # sph = DynamicSettings(indFunc, 999.0, 0.1)
        ## 1: PH, 2: Adwin, 3: Bayes
        r1Dyn,=optimDetector(x,
                             changes=[changeLocation],
                             detector =createPhDetectorDynamic(delta=0.0, indicatorFunction=indFunc, sWhen0 = 999.0),
                             paramrange =paramRangePh,
                             name="PH",
                             printprogress=false)

        r2Dyn,=optimDetector(x,
                             changes=[changeLocation],
                             detector = createAdwinDetectorDynamic(min_len=10,indicatorFunction=indFunc, ecutWhen0=999.),
                             paramrange =paramRangeAdw,
                             name="Adwin",
                             printprogress=false)

        r3Dyn, = optimDetector(x,
                               changes=[changeLocation],
                               # detector = createBayesianDetectorDynamic(mu0=0.1,indicatorFunction=indFunc, lambdaWhen0=999., gaussPdf=true, gaussSigma=signalStd)
                               detector = createBayesianDetectorDynamic(mu0=0.1,indicatorFunction=indFunc, lambdaWhen0=999., gaussPdf=false),
                               paramrange =paramRangeBayes,
                               name="Bayes",
                               printprogress=false)

        avgLoss1[:,i]=r1[:loss_v]
        avgLoss2[:,i]=r2[:loss_v]
        avgLoss3[:,i]=r3[:loss_v]

        avgLoss1Dyn[:,i]=r1Dyn[:loss_v]
        avgLoss2Dyn[:,i]=r2Dyn[:loss_v]
        avgLoss3Dyn[:,i]=r3Dyn[:loss_v]

        avgDelay1[:,i]=r1[:delays]
        avgDelay2[:,i]=r2[:delays]
        avgDelay3[:,i]=r3[:delays]

        avgDelay1Dyn[:,i]=r1Dyn[:delays]
        avgDelay2Dyn[:,i]=r2Dyn[:delays]
        avgDelay3Dyn[:,i]=r3Dyn[:delays]

        avgFaRate1[:,i]=r1[:faRate]
        avgFaRate2[:,i]=r2[:faRate]
        avgFaRate3[:,i]=r3[:faRate]

        avgFaRate1Dyn[:,i]=r1Dyn[:faRate]
        avgFaRate2Dyn[:,i]=r2Dyn[:faRate]
        avgFaRate3Dyn[:,i]=r3Dyn[:faRate]
    end

    ## 1: PH, 2: Adwin, 3: Bayes
    averageLoss1    = mean(avgLoss1   , dims=2)
    averageLoss2    = mean(avgLoss2   , dims=2)
    averageLoss3    = mean(avgLoss3   , dims=2)

    averageLoss1Dyn = mean(avgLoss1Dyn, dims=2)
    averageLoss2Dyn = mean(avgLoss2Dyn, dims=2)
    averageLoss3Dyn = mean(avgLoss3Dyn, dims=2)

    avgDelay1 = mean(avgDelay1, dims=2)
    avgDelay2 = mean(avgDelay2, dims=2)
    avgDelay3 = mean(avgDelay3, dims=2)

    avgDelay1Dyn = mean(avgDelay1Dyn, dims=2)
    avgDelay2Dyn = mean(avgDelay2Dyn, dims=2)
    avgDelay3Dyn = mean(avgDelay3Dyn, dims=2)

    avgFaRate1 = mean(avgFaRate1, dims=2)
    avgFaRate2 = mean(avgFaRate2, dims=2)
    avgFaRate3 = mean(avgFaRate3, dims=2)

    avgFaRate1Dyn = mean(avgFaRate1Dyn, dims=2)
    avgFaRate2Dyn = mean(avgFaRate2Dyn, dims=2)
    avgFaRate3Dyn = mean(avgFaRate3Dyn, dims=2)


    function w(fpath, x)
        open(fpath, "w") do io
            writedlm(io,x,',')
        end
    end

    ## 1: PH, 2: Adwin, 3: Bayes
    # Avg Losses: Static
    w("./out/avg/avgLossPh.dat"      , averageLoss1)
    w("./out/avg/avgLossAdw.dat"     , averageLoss2)
    w("./out/avg/avgLossBayes.dat"   , averageLoss3)
    # Avg Losses: Dynamic
    w("./out/avg/avgLossPhDyn.dat"   , averageLoss1Dyn)
    w("./out/avg/avgLossAdwDyn.dat"  , averageLoss2Dyn)
    w("./out/avg/avgLossBayesDyn.dat", averageLoss3Dyn)
    # Avg Delays: Static
    w("./out/avg/avgDelayPh.dat"   , avgDelay1)
    w("./out/avg/avgDelayAdw.dat"  , avgDelay2)
    w("./out/avg/avgDelayBayes.dat", avgDelay3)
    # Avg Delays: Dynamic
    w("./out/avg/avgDelayPhDyn.dat"     , avgDelay1Dyn)
    w("./out/avg/avgDelayAdwDyn.dat"    , avgDelay2Dyn)
    w("./out/avg/avgDelayBayesDyn.dat"  , avgDelay3Dyn)
    # Avg FA rates: Static
    w("./out/avg/avgFaRatePh.dat"       ,avgFaRate1)
    w("./out/avg/avgFaRateAdwin.dat"    ,avgFaRate2)
    w("./out/avg/avgFaRateBayes.dat"    ,avgFaRate3)
    # Avg FA rates: Dynamic 
    w("./out/avg/avgFaRatePhDyn.dat"   ,avgFaRate1Dyn)
    w("./out/avg/avgFaRateAdwinDyn.dat",avgFaRate2Dyn)
    w("./out/avg/avgFaRateBayesDyn.dat",avgFaRate3Dyn)

    w("./out/avg/paramRangePh.dat"   , paramRangePh)
    w("./out/avg/paramRangeAdw.dat"  , paramRangeAdw)
    w("./out/avg/paramRangeBayes.dat", paramRangeBayes)
end
## run_average_performance(nSim=10, lengthParamRange=100)
run_average_performance(nSim=50, lengthParamRange=100, wIndFunc=80, signalStd=0.5)
# run_average_performance(nSim=12, lengthParamRange=300, wIndFunc=8)
