include("../Lib/DetectorPageHinkley.jl")
include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/PerformanceMetrics.jl")
include("../Lib/common.jl")
include("../Lib/fn_construct_indicator_function.jl")

#using LearnDetectors, PageHinkley, BayesDetector, AdwinNaive
using CSV, DataFrames

function genDat(;n=100,delta=1.0, sigma=1.0)
    x=vcat(randn(n)*sigma, randn(n)*sigma + delta)
    writeOutputSigDataFrame("./dat/sig1.dat", x)
end

function run12()
    if 1==0
        genDat()
        x = readInputSig("../commondata/diag-ii-signalExample1ChpShort.dat")
        xBayes = readInputSig("./dat/sigForBayes.dat")
    end
    
    if 1==1
        s = readJSON("../config_datasets.json")
        sigpath = s["dir"] * s["artsig2"]
        indpath = s["dir"] * s["artind2"]
        x = vec(readdlm(sigpath))
        changesLocations = [100, 200]
    end
    println("lexgth(X) = ", length(x))

    detectorPHref = createPhDetector(delta=0.0)
    detectorAdwin = createAdwinDetector(min_len=10)
    detectorBayes = createBayesianDetector(mu0=0.0)

    ##indFunc = constructIndicatorFunction(length(x), 250, w)
    ## Static optimal settings
    npoints = 300
    if 1==0
        # artsig1.dat 
        paramRangePH    = range(1.00, stop=10.0, length=npoints)
        paramRangeAdwin = range(0.01, stop=3.60, length=npoints)
        paramRangeBayes = range(1.00, stop=20.0, length=npoints)
    end

    if 1==1
        # artsig1_LessNoise.dat 
        paramRangePH    = range(0.01, stop=1.5  , length=npoints)
        paramRangeAdwin = range(0.01, stop=3.6  , length=npoints)
        paramRangeBayes = range(1.00, stop=60.0, length=npoints)
    end

    if true #false

        optimDetector(x,
                      "./out/rPH.csv",
                      "./out/rPH_Opt.csv",
                      changes=changesLocations,
                      outPerfMatrix="/home/ms314/tmp/step12PerfMatrixPH.dat",
                      detector=detectorPHref,
                      paramrange = paramRangePH, 
                      name="PH",
                      printprogress=true)


       optimDetector(x,
                     "./out/rAdw.csv",
                     "./out/rAdw_Opt.csv",
                     changes=changesLocations,
                     outPerfMatrix="/home/ms314/tmp/step12PerfMatrixAdwin.dat",
                     detector=detectorAdwin,
                     paramrange = paramRangeAdwin, 
                     name="Adwin",
                     printprogress=true)


        optimDetector(x,
                      "./out/rBayes.csv",
                      "./out/rBayes_Opt.csv",
                      changes=changesLocations,
                      outPerfMatrix="/home/ms314/tmp/step12PerfMatrixBayes.dat",
                      detector=detectorBayes,
                      paramrange = paramRangeBayes,
                      name="Bayes",
                      printprogress=true)
    end

    ## Dynamic optimal settings
    if true
        println(".. Start: find dynamic optimal settings")
        
        # Read R-generated signal and Ind func 
        if 1==1
            indFunc = [Int(e) for e in vec(readdlm(indpath))]
        end 
        # Gen. art signal inline of code
        if 1==0
            w = 50 # 30
            changeLocation=100
            indFunc = constructIndicatorFunction(length(x), changeLocation, w)
            open("./out/indfunc.dat", "w") do io
                writedlm(io, indFunc)
            end
        end

        sph = DynamicSettings(indFunc, 999.0, 0.1)
        detectorPHrefDynamic = createPhDetectorDynamic(delta=0.0, indicatorFunction=indFunc, sWhen0=999.0)

        optimDetector(x,
                      "./out/rPH_Dynamic.csv",
                      "./out/rPH_Opt_Dynamic.csv",
                      outPerfMatrix="/home/ms314/tmp/step12PerfMatrixPH_Dyn.dat",
                      changes=changesLocations,
                      detector=detectorPHrefDynamic,
                      paramrange = paramRangePH, 
                      name="PH",
                      printprogress=true)

        optimDetector(x,
                      "./out/rAdw_Dynamic.csv",
                      "./out/rAdw_Opt_Dynamic.csv",
                      outPerfMatrix="/home/ms314/tmp/step12PerfMatrixAdwin_Dyn.dat",
                      changes=changesLocations,
                      detector = createAdwinDetectorDynamic(min_len=10,indicatorFunction=indFunc, ecutWhen0=999.),
                      paramrange = paramRangeAdwin, 
                      name="Adwin",
                      printprogress=true)

        optimDetector(x,
                      "./out/rBayes_Dynamic.csv",
                      "./out/rBayes_Opt_Dynamic.csv",
                      outPerfMatrix="/home/ms314/tmp/step12PerfMatrixBayes_Dyn.dat",
                      changes=changesLocations,
                      detector = createBayesianDetectorDynamic(mu0=0.0,indicatorFunction=indFunc,lambdaWhen0=999.),
                      paramrange = paramRangeBayes, 
                      name="Bayes",
                      printprogress=true)


        println(".. End: find dynamic optimal settings")
    end


    ## Run static detectors with optimal settings
    if false #true

        runDetectorWithSettings(x,
                                detector=detectorPHref,
                                outcsv = "./out/cdes_PH_opt.csv",
                                optimSettings=OptimSettings(1.22),
                                name="PH")

        runDetectorWithSettings(x,
                                detector=detectorAdwin,
                                outcsv = "./out/cdes_Adwin_opt.csv",
                                optimSettings=OptimSettings(0.35, 0.0, 0),
                                name="Adwin")


        runDetectorWithSettings(x,
                                detector=detectorBayes,
                                outcsv = "./out/cdes_Bayes_opt.csv",
                                optimSettings=OptimSettings(0.01, 0.0,0),
                                name="Bayes")

    end

    ## Run dynamic detectors
    if false
        w = 10
        changeLocation=100
        indFunc = constructIndicatorFunction(length(x), changeLocation, w)
        println("TODO: Run dynamic detectors using optimal static settings")
        sph = DynamicSettings(indFunc, 999.0, 0.1)
        sbs = DynamicSettings(indFunc, 999.0, 0.1)
        sdw = DynamicSettings(indFunc, 999.0, 0.1)
        rd     = detectorPH(x, 0.0, -1.0, adaptiveSettings=sph)
        mxs, m = bayesDetectorNew(x, adaptiveSettings=sbs)
        rdw    = adwinNaive(x, adaptiveSettings=sdw)

        cdesBayes = extractChangesBayes(mxs)
        cdesAdwin = extractChps(rdw)

        println("Dynamic settings: CDEs PH:", rd)
        println("Dynamic settings: CDEs Adwin:", cdesAdwin)
        println("Dynamic settings: CDEs Bayes:", cdesBayes)
        saveCdes(fpath="./out/cdes_opt_ph.dat",cdes=rd)
        saveCdes(fpath="./out/cdes_opt_adwin.dat",cdes=cdesAdwin)
        saveCdes(fpath="./out/cdes_opt_bayes.dat",cdes=cdesBayes)
    end
    # println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", length(paramRangePH))
end

run12()
