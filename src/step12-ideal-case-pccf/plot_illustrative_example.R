set.seed(14)
N <- 500
chp <- floor(N/2)

pccf <- rep(0,N)
pccf[(chp-20):(chp+20)] <- 1

pccf2 <- rep(0,N)
pccf2[(chp-40):(chp+40)] <- 1

#x <- c(rnorm(floor(N/2), mean=0, sd=1.5), rnorm(floor(N/2), mean=3, sd=1.5))
x <- c(rnorm(180, mean=0, sd=1.5), 
       rnorm(20, mean=3, sd=1.5), 
       rnorm(50, mean=0, sd=1.5), 
       rnorm(250, mean=4, sd=1.5))
x[chp] <- 6
x[107] <- 4
x[180] <- 4
x[350] <- 5

cdes <- c(100, 185, 255, 400, 430)

pdf("./img/step12-illustrative-example.pdf", width=7, height=5)
#par(mfrow=c(2,1), mar = c(2,2,0,1) , oma = c(2,2,1,1))
plot(x, ann = F, axes = TRUE, ylim = c(-5,10))
abline(v=chp, lwd=2)
abline(v = cdes,lty=2, col="blue", lwd=2)
lines(pccf*9, col="red", lwd=2)
lines(pccf2*9, col="red",lty=2, lwd=2)
mtext(side = 1, "Time"  , line = 2, cex = 1.0 )
mtext(side = 2, "Signal", line = 3, cex = 1.1)
dev.off()

if (1==0){
  png("./img/1changeExample.PNG", width = 800, height = 600)
  plot(x, ylim = c(-5,10), xlab="t", type = 'l', ylab = "Y(t)",lwd=1.5)
  abline(v=chp, lwd=2)
  abline(v = cdes,lty=2, col="blue", lwd=2)
  lines(pccf*9, col="red", lwd=2)
  lines(pccf2*9, col="red",lty=2, lwd=2)
  dev.off()
}

if(1==0){
  write.table(x, file="./dat/signalExample1Chp_.dat", row.names = FALSE, col.names = FALSE)  
}

