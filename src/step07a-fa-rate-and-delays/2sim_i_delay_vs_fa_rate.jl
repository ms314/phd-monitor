#
#  Simulation I
#             : generate 1 signal with 1 change 
#             : run 3 detectors: Adwin, PH, Bayesian 
#             : measure detection delay and FA rate 
#             : print measured values

include("../Lib/detectors/PageHinkley.jl")
include("../Lib/detectors/BayesDetectorReplica.jl")
include("../Lib/Fractions.jl")
include("../Lib/detectors/adwin/Adwin.jl")
include("../Lib/detectors/adwin/AdwinNaive.jl")

module SimI

using PageHinkley, BayesChpOrig, Fractions, AdwinModule, AdwinNaive

function genSig()
    a = randn(50)
    b = randn(50) + 1.0
    vcat(a,b)
end 


function runPageHinkley(x, trueChp, maxDelayIn)
    println("... Run Page-Hinkley")
    delta = 0.0
    lambda = 5 ## sensitivity
    cdes = detectorPH(x, delta, lambda)
    delay = detectionDelay1Chp(trueChp, cdes)
    probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    println("Detected changes:", cdes)
    println("Delay = $delay")
    println("Probability of FA: $probFA")
    ##println(">>>>>>>>>>>>",length(cdes)/length(x))
    println("End\n")
    (delay, probFA)
end 


function runAdwin(x, trueChp, maxDelayIn)
    println("... Run Adwin")
    cdes = adwinDetector(x, .01)
    if length(cdes) > 0
        delay = detectionDelay1Chp(trueChp, cdes)
        probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    end 
    println("Detected changes:", cdes)
    println("End\n")
end 


function runAdwinNaive(x, trueChp, maxDelayIn)
    println("... Run Adwin Naive")
    y = scale01(x)
    cdes = extractChps(adwinNaive(y; ecut_custom=0.3, min_len=10))
    if false # true
        for e in 0.01:0.05:0.9
            cdes = extractChps(adwinNaive(y; ecut_custom=e, min_len=10))
            println("length(cdes) = ", length(cdes), " ecut = ", e)
        end 
    end 
    probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    if length(cdes) > 0
        delay = detectionDelay1Chp(trueChp, cdes)
        probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    end 
    println("Detected changes:", cdes)
    println("Delay = $delay")
    println("Probability of FA: $probFA")
    println("End\n")
    (delay, probFA)
end 


function runBayesian(x, trueChp, maxDelayIn)
    println("... Run Bayesian")
    # for h = 10:5:70
    #     maxes, mat = bayes_detector(x, lambda = h)
    #     cdes = extract_changes(maxes)
    #     println(length(cdes), " ; h=$h")
    # end 
    h = 27
    maxes, mat = bayes_detector(x, lambda = h)
    cdes = extract_changes(maxes)
    delay = detectionDelay1Chp(trueChp, cdes)
    probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    println("Detected changes:", cdes)
    println("Delay = $delay")
    println("Probability of FA: $probFA")
    println(">>>>>>>>>>>>",length(cdes)/length(x))
    println("End")
    (delay, probFA)
end 


function main()
    trueChp = 50
    maxDelay = 20
    sig = genSig()
    println("\n")
    runPageHinkley(sig, trueChp, maxDelay)
    runBayesian(sig, trueChp, maxDelay)
    runAdwin(sig, trueChp, maxDelay)
    runAdwinNaive(sig, trueChp, maxDelay)

end

#main()
end # module
