#
#  Simulation II
#             : generate signals with 1 change N times
#             : run 3 detectors: Adwin, PH, Bayesian 
#             : measure _average_ detection delay and FA rate 
#
include("../Lib/detectors/PageHinkley.jl")
include("../Lib/detectors/BayesDetectorReplica.jl")
include("../Lib/Fractions.jl")
include("../Lib/detectors/adwin/adwin.jl")
include("../Lib/detectors/adwin/AdwinNaive.jl")

module SimII 

using PageHinkley, BayesChpOrig, Fractions, AdwinModule, AdwinNaive

function genSig()
    a = randn(50)
    b = randn(50) + 1.0
    vcat(a,b)
end 


function runPageHinkley(x, trueChp, maxDelayIn)
    delta = 0.0
    lambda = 5 ## sensitivity
    cdes = detectorPH(x, delta, lambda)
    delay = detectionDelay1Chp(trueChp, cdes)
    probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    (delay, probFA)
end 


function runAdwin(x, trueChp, maxDelayIn)
    cdes = adwinDetector(x, .01)
    if length(cdes) > 0
        delay = detectionDelay1Chp(trueChp, cdes)
        probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    end 
end 


function runAdwinNaive(x, trueChp, maxDelayIn; scale = true)
    if scale
        y = scale01(x)
    end 
    cdes = extractChps(adwinNaive(y; ecut_custom=0.3, min_len=10))
    if false # true
        for e in 0.01:0.05:0.9
            cdes = extractChps(adwinNaive(y; ecut_custom=e, min_len=10))
            println("length(cdes) = ", length(cdes), " ecut = ", e)
        end 
    end 
    probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    
    if length(cdes) > 0
        delay = detectionDelay1Chp(trueChp, cdes)
        probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    else
        delay = Inf
        probFa = 0.0
    end 

    (delay, probFA)
end 


function runBayesian(x, trueChp, maxDelayIn)
    # for h = 10:5:70
    #     maxes, mat = bayes_detector(x, lambda = h)
    #     cdes = extract_changes(maxes)
    #     println(length(cdes), " ; h=$h")
    # end 
    h = 27
    maxes, mat = bayes_detector(x, lambda = h)
    cdes = extract_changes(maxes)
    delay = detectionDelay1Chp(trueChp, cdes)
    probFA = fracOfFa(chps=[trueChp], cdes = cdes, maxDelay = maxDelayIn)
    (delay, probFA)
end 


function main()
    trueChp = 50
    maxDelay = 20
    N = 100 

    delay_PH_avg = 0.0 
    delay_Bayes_avg = 0.0
    delay_AdwinNaive_avg = 0.0

    faRate_PH_avg = 0.0 
    faRate_Bayes_avg = 0.0
    faRate_AdwinNaive_avg = 0.0

    println("Number of generated signals: $N")
    
    for k = 1:N
        sig = genSig()

        (d_PH, fa_PH) = runPageHinkley(sig, trueChp, maxDelay)
        (d_Bayes, fa_Bayes) = runBayesian(sig, trueChp, maxDelay)
        #runAdwin(sig, trueChp, maxDelay)
        (d_AdwNaive, fa_AdwNaive) = runAdwinNaive(sig, trueChp, maxDelay)

        d_PH = d_PH == Inf ? length(sig) - trueChp : d_PH
        d_Bayes = d_Bayes == Inf ? length(sig) - trueChp : d_Bayes
        d_AdwNaive = d_AdwNaive == Inf ? length(sig) - trueChp : d_AdwNaive
        
        delay_PH_avg += d_PH
        delay_Bayes_avg += d_Bayes
        delay_AdwinNaive_avg += d_AdwNaive

        faRate_PH_avg += fa_PH
        faRate_Bayes_avg += fa_Bayes
        faRate_AdwinNaive_avg += fa_AdwNaive

    end 

    delay_PH_avg /= N 
    delay_Bayes_avg /= N
    delay_AdwinNaive_avg /= N

    faRate_PH_avg /= N 
    faRate_Bayes_avg /= N 
    faRate_AdwinNaive_avg /= N 

    if true 
        println("\nPH detector")
        println("    Avg delay = ", delay_PH_avg)
        println("    Avg FA rate = ", faRate_PH_avg)

        println("Bayesian detector")
        println("    Avg delay = ", delay_Bayes_avg)
        println("    Avg FA rate = ", faRate_Bayes_avg)


        println("dAdwin Naive etector")
        println("    Avg delay = ", delay_AdwinNaive_avg)
        println("    Avg FA rate = ", faRate_AdwinNaive_avg)
    end 

end


end 
