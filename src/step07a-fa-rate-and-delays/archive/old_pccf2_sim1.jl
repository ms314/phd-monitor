# - (2018-04-18)
#   Copied from 201610-pccf2-code/src_julia/
#
# V.0.3 (27.10.2016) No ROC yet
#
# ROC curve for artificial signal.
# Simulation - generate changes and detect them.
# Obtain ROC curve.

#include("./lib/metadata.jl")
#include("./lib/pccf.jl")
#include("./1naive_update.jl")
#include("./2bayes_detector_replica.jl")
include("../Lib/common.jl")
include("../Lib/BayesDetectorReplica.jl")
include("../Lib/Roc.jl")
include("../Lib/SignalGenerator.jl")
include("../Lib/Fractions.jl")
include("../Lib/Pccf.jl")
include("../Lib/recmidpoints.jl")

# using SignalGenerator, BayesChpOrig, Roc, Metadata, Pccf
using SignalGenerator, BayesChpOrig, Roc, Pccf, JSON
using Fractions

"""
In this simulation we variate the lambda parameter
of the Bayesian detector and plot ROC curve.
"""
function run_sim1(outputfolder)
    println("... sim1.jl.run_sim1 > save results into ", outputfolder)
    s = Dict()
    usePccf = false

    #--- START SIMULATION SETTINGS
    r = open("./config.json") do f
        JSON.parse(f)
    end
    max_delay        = r["sim1"]["max_delay"] #10  # 30
    n_sim_for_each_h = r["sim1"]["n_sim_for_each_h"] #2 # 10
    h_from           = r["sim1"]["h_from"] #1 #50  # 40
    h_to             = r["sim1"]["h_to"] #30 # 300
    h_by             = r["sim1"]["h_by"] #0.1 #5 #15  # 25  # 15
    #--- end SIMULATION SETTINGS

    avg_tp_rates_for_h = []
    avg_fa_rates_for_h = []
    avg_ppv_for_h      = []

    avg_frac_chps = []
    avg_frac_fa   = []

    if usePccf
        pccf_avg_tp_rates_for_h = []
        pccf_avg_fa_rates_for_h = []
        pccf_avg_ppv_for_h      = []
    end

    threshold  = r["sim1"]["threshold"]
    max_d      = r["sim1"]["max_d"]

    mu0        = r["sim1"]["mu0"]
    sd0        = r["sim1"]["sd0"]
    n_chps     = r["sim1"]["n_chps"]
    chpStep    = r["sim1"]["chpStep"]

    if usePccf
        n_pccf = round(Int64, mu0 * (n_chps + 5)) # Length of the prediction using PCCF (so that to not recalc on eachj iteration)
        probs  = scale01(pccf(n_pccf, mu0, sd0))
    end
    ##?? srand(1235) - we need a new data on every iteration

    # Variate lambda value
    thetas = []
    for h in h_from : h_by : h_to
        push!(thetas, h)
        if mod(h, 10)==0
            @printf("%0.2f %s \n", h/h_to * 100, "%")
        end

        l_common = h

        tp_rates_for_i = []
        fa_rates_for_i = []
        ppv_for_i      = []

        frac_chps_for_i = []
        frac_fa_for_i   = []

        if usePccf
            pccf_tp_rates_for_i = []
            pccf_fa_rates_for_i = []
            pccf_ppv_for_i      = []
        end

        for i_signals = 1 : n_sim_for_each_h
            ret          = genSig(n_chps, mu0, sd0, chpStep, 0.75)
            sig          = ret["sig"]
            changes      = ret["changes"]
            n_sig        = length(sig)

            ADDNOISE = true
            if ADDNOISE
                noise_ids = addNoise!(sig, changes, chpStep, width = 10)
            end

            if usePccf
                if n_sig > n_pccf
                    ## Update PCCF if its length is less than signal's length
                    n_pccf = n_sig
                    probs  = scale01(pccf(n_pccf, mu0, sd0))
                end
            end

            maxes, mat   = bayes_detector(sig, lambda = h)
            detections   = extract_changes(maxes)

            if usePccf
                pccf_maxes, pccf_mat = pccf_bayes_detector(sig, probs[1:n_sig], l_common + 100, l_common, threshold, h_common = l_common, max_d = max_d)
                pccf_detections      = extract_changes(pccf_maxes)
            end
            frac = 0.1/1.0 # importance factor of TN's

            tpr          = tpRate(detections, changes, max_delay)
            fpr          = faRateScaled(detections, changes, max_delay, n_sig, tn_price_scale = frac)
            ppv_val      = ppv(detections, changes, max_delay)

            if usePccf
                pccf_tpr     = tpRate(pccf_detections, changes, max_delay)
                pccf_fpr     = faRateScaled(pccf_detections, changes, max_delay, n_sig, tn_price_scale = frac)
                pccf_ppv_val = ppv(pccf_detections, changes, max_delay)

                push!(pccf_ppv_for_i     , pccf_ppv_val)
                push!(pccf_tp_rates_for_i, pccf_tpr)
                push!(pccf_fa_rates_for_i, pccf_fpr)
            end

            push!(ppv_for_i     , ppv_val)
            push!(tp_rates_for_i, tpr)
            push!(fa_rates_for_i, fpr)

            cc=fracOfDetected(chps=changes, cdes=detections, maxDelay=max_delay)
            ff=fracOfFa(; chps=changes, cdes=detections, maxDelay=max_delay, n=n_sig)
            push!(frac_chps_for_i, cc)
            push!(frac_fa_for_i, ff)

        end
        push!(avg_tp_rates_for_h, mean(tp_rates_for_i))
        push!(avg_fa_rates_for_h, mean(fa_rates_for_i))
        push!(avg_ppv_for_h     , mean(ppv_for_i))
        #
        push!(avg_frac_chps, mean(frac_chps_for_i))
        push!(avg_frac_fa, mean(frac_fa_for_i))

        if usePccf
            push!(pccf_avg_tp_rates_for_h, mean(pccf_tp_rates_for_i))
            push!(pccf_avg_fa_rates_for_h, mean(pccf_fa_rates_for_i))
            push!(pccf_avg_ppv_for_h     , mean(pccf_ppv_for_i))
        end
    end
    ## OLD
    # sim1_tp_rates.txt, sim1_fa_rates.txt,...
    #writedlm(outputfolder * "tp_rates.txt", avg_tp_rates_for_h)
    #writedlm(outputfolder * "fa_rates.txt", avg_fa_rates_for_h)
    #writedlm(outputfolder * "ppv.txt"     , avg_ppv_for_h)
    #
    #writedlm(outputfolder * "pccf_tp_rates.txt", pccf_avg_tp_rates_for_h)
    #writedlm(outputfolder * "pccf_fa_rates.txt", pccf_avg_fa_rates_for_h)
    #writedlm(outputfolder * "pccf_ppv.txt"     , pccf_avg_ppv_for_h)
    writedlm(outputfolder * "fractions_chps.txt", avg_frac_chps)
    writedlm(outputfolder * "fractions_fa.txt", avg_frac_fa)
    writedlm(outputfolder * "thetas.txt", thetas)
    println("... Saved results into " * outputfolder * "fractions_chps.txt")
    println("... Saved results into " * outputfolder * "fractions_fa.txt")
    println("... Saved results into " * outputfolder * "thetas.txt")
end
#run_sim1(outdir * "sim1/")
run_sim1("out/")
