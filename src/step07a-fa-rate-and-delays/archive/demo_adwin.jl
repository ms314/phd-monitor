include("../Lib/AdwinNaive.jl")
#include("../Lib/AdwinNaiveLocalCopy.jl")

"read output in inotebook and plot"
function demo_adwin_naive()
    srand(1234)
    N = 15
    dat = genSigWithSlope(N, 0.3)
    r = adwin_naive(dat, min_len=2, ecut_custom = 0.5/2.5)
    r,dat
end
tic()
r,sig = demo_adwin_naive()
toc()
detections = extractChps(r)
writedlm("./tmp/detections.txt", detections)
writedlm("./tmp/sig.txt", sig)
