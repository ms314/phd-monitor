lazy val root = (project in file("."))

  .settings(
    name := "Debug Bayesian detector",
    scalaVersion := "2.12.8",
    version := "0.1.0-SNAPSHOT",
    
    libraryDependencies ++= Seq(
      "org.scalanlp" %% "breeze" % "0.13.2",
      "org.scalanlp" %% "breeze-natives" % "0.13.2",
      "org.scalanlp" %% "breeze-viz" % "0.13.2",
      "org.apache.commons" % "commons-math3" % "3.3",
      "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"
    )

  )
