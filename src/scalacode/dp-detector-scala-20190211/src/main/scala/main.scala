import breeze.linalg._
import breeze.stats.distributions._
import perfmetrics._

import dpdetector._


object mn{

    def runDetector(inSig:String, outf:String):Unit={
        println(s".. Read signal from $inSig")
        val y = rwio.readSig(inSig)
        val r = dynamicProgrammingDetector.optPartition(y)
        // for(i <-0 to 10) println(y(i))
        rwio.writeSig(r, outf)
        println(s"Save CDEs into $outf")
        println(r.mkString(", "))
    }

    def main(args:Array[String]):Unit = {
        if(1==0){
            val sig = Array(1.0, 1.0, 1.0, 
                        2.0, 2.0, 2.0, 
                        1.0, 1.0, 1.0, 1.0, 1.1)
            dynamicProgrammingDetector.optPartition(sig) foreach println
        }
        val dir = "/home/ms314/gitlocal/phd-monitor/datasets/"
        val f1 = dir + "exchange-rate-bank-of-twi/data/sigTwi.dat"
        val f2 = dir + "internet-traffic-data-in-bits-fr/data/sigInternetTraffic.dat"
        val f3 = dir + "monthly-lake-erie-levels/data/sigLakeLevel.dat"
        val f4 = dir + "number-of-earthquakes-per-year/data/sigNumOfEarthQuakes.dat"
        val f5 = dir + "time-that-parts-for-industrial/data/sigTimesNeeded.dat"
        val f6 = dir + "wolfer-sunspot-numbers/data/sigSunspotNumber.dat"
        //runDetector(f1, "/home/ms314/gitlocal/phd-monitor/datasets/exchange-rate-bank-of-twi/data/changesTwiScalaDpDetector.dat")
        runDetector(f1, "./dat/cdes1.dat")
        //runDetector(f2, "./dat/cdes2.dat")
        // runDetector(f3, "./dat/cdes3.dat")
        //runDetector(f4, "./dat/cdes4.dat")
        //runDetector(f5, "./dat/cdes5.dat")
        //runDetector(f6, "./dat/cdes6.dat")
        // println("Real signal")
        // val y = rwio.readSig("/home/ms314/gitlocal/phd-monitor/datasets/exchange-rate-bank-of-twi/data/sigTwi.dat")
        // val r = dynamicProgrammingDetector.optPartition(y)
        // println("extractedChps:")
        // // r foreach println
        // println(r.mkString(","))
        // rwio.writeSig(r, "/home/ms314/tmp/out.dat")
    }

    def gpdf(x:Double, m:Double, s:Double): Double = {
        val G = new Gaussian(m, s)
        G.pdf(x)
    }

    def gpdf(x:Double, m:Array[Double], s:Array[Double]):Array[Double]={
        (m zip s).map(e => gpdf(x, e._1, e._2))
    }
    
    def seqMu(y:Array[Double]):Array[Double]={
        val n = y.length
        val mv = new Array[Double](n)
        mv(0) = y(0)
        for(k <- 1 to n-1){
            val nn = k+1
            mv(k) = ((nn-1)/nn.toDouble) * (mv(k-1) + mv(k)/(nn-1))
        }
        mv
    }


    def seqSd(y:Array[Double]):Array[Double]={
        val n = y.length
        val sv = new Array[Double](n)
        val m2 = (y(1)+y(0))/2.0
        sv(0) = 0.0
        sv(1) = math.pow(y(0)-m2,2) + math.pow(y(1)-m2,2)
        for(k <- 2 to n-1){
            val nn = k+1
            sv(k) = ((nn-2.0)/(nn-1.0).toDouble) * sv(k-1) + (1.0/nn.toDouble) * math.pow(y(k)-y(k-1), 2)
        }
        sv.map(math.sqrt)
    }

    def bayesianDetector(y:Array[Double], lambda:Double):Array[Array[Double]] = {
        assert(lambda >= 1.0)
        val mu0 = 0.0
        val n = y.length
        val probs = new Array[Double](n)
        val R = Array.ofDim[Double](n, n)
        R(0)(0) = 1.0
        for (t <- 1 to n){
            val subv = y.slice(0,n).reverse
            val possibleMu = seqMu(subv)
            val possibleSigma = seqSd(subv)
            val predprobs = gpdf(y(t), possibleMu, possibleSigma)
            R(t) = predprobs
        }
        R 
    }

}

