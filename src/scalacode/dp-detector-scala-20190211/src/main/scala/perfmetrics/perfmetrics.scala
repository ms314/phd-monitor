package perfmetrics

object PerformanceMetrics{

    // positive distance
    private def posDist(cde:Int, chp:Int, n:Int):Int={
        assert(cde >= 0 && chp >= 0 && cde <= n && chp <= n)
        if (cde >= chp) cde-chp else n-chp
    }

    private def closestDetection(chp:Int, detections:Array[Int], n:Int):Int={
        detections.map(x => posDist(x, chp, n)).min
    }

    def detectionDelays(detections:Array[Int], changes:Array[Int], n:Int):Array[Int]={
        changes.map(x=>closestDetection(x, detections, n))
    }

    def avgDetectionDelay(detections:Array[Int], changes:Array[Int], n:Int):Double={
        val delays = detectionDelays(detections, changes, n)
        delays.sum / delays.length.toDouble
    }
}