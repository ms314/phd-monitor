// 2019/02/11 - created
// Dynamic programming detector
// Jackson, Brad, et al. "An algorithm for optimal partitioning of data on an interval." IEEE Signal Processing Letters 12.2 (2005): 105-108.
//

package dpdetector

import scala.io.Source
import java.io._

object dynamicProgrammingDetector{
    
    private def fitnessOfPart(sigPart:Array[Double]):Double={
        val n = sigPart.length
        assert(n>1)
        val m = sigPart.foldLeft(0.0)((x,y) => x+y) / n.toDouble
        val fitness = sigPart.map(x => math.pow(x-m, 2)).sum / n.toDouble
        fitness
    }

    private def extractChanges(chpsRun:Array[Int]):Array[Int]={
        val n = chpsRun.length
        var c = chpsRun(n-1)
        var chps:Array[Int]=Array(c)
        var counter = 0 // avoid inf. loop
        while (c!=0 && counter < n+1){
            val loc = chpsRun(c)
            if (loc != c) {
                chps = chps :+ loc + 1
                c = loc
            } else {
                chps = chps :+ 0
                c=0
                }
            
            
            counter += 1
        }
        chps
    }

    def optPartition(sig:Array[Double]):Array[Int] = {
        // all possible starting locations of the last block: j <- (0 to n)
        val n = sig.length
        var chps: Array[Int] = new Array[Int](n+1)
        var opts: Array[Double] = new Array[Double](n+1)
        opts(0) = 0.0
        opts(1) = 0.0
        for(t <- 2 to 3){
            opts(t) = fitnessOfPart(sig.slice(0, t))
        }
        for (t <- 4 to n) {
            var best_so_far_v = fitnessOfPart(sig.slice(0,t))
            var best_so_far_j = t
            for (j <- 2 to t-2){
                val tot_cost = opts(j) + fitnessOfPart(sig.slice(j, t))
                if(tot_cost < best_so_far_v) {
                    best_so_far_v = tot_cost
                    best_so_far_j = j-1
                }
            }
            opts(t) = best_so_far_v
            chps(t) = best_so_far_j
        }
        println("..RAW")
        // for(t <- 0 to chps.length-1)  println(chps(t), "for t=", t)
        extractChanges(chps).reverse.tail
    }
}


object rwio{

    def readSig(fpath:String):Array[Double]={
        val x = Source.fromFile(fpath).getLines().toArray.map(_.toDouble)
        x
    }

    def writeSig(x:Array[Int], fpath:String):Unit={
        def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
            val p = new java.io.PrintWriter(f)
            try { op(p) } finally { p.close() }
        }
        printToFile(new File(fpath)) { p => x.foreach(p.println) }
    }
}
