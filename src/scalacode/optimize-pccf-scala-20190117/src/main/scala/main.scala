import breeze.linalg._
import breeze.stats.distributions._

object mn{

    def costf(changes:List[Int], locations:List[Int]):Option[Double] = {
        // cost function
        if (changes.length != locations.length || changes.length == 0 || locations.length == 0){
            None
        } else {
            val sse:Double = changes.zip(locations).map(x => Math.pow(x._1 - x._2, 2)).sum
            Some(sse)
        }
    }

    def genPeriodicLocations(acc:List[Int]=List(0), periodT:Int, n:Int):List[Int] = n match {
        // generate locations of periodic indicator function 1's
        case 0 => acc.reverse.tail
        case _ => {
            val ne = acc.head + periodT
            genPeriodicLocations(ne::acc, periodT, n-1)
        }

    }

    def findOptimIndFunLocations(changes:List[Int]):(Int,Int, List[Int]) ={
        // return starting point and period
        val startingPonts = List.range(0, changes.head)
        val n = changes.length
        val maxPeriod = (0::changes).init.zip((0::changes).tail).map(x=> x._2 - x._1).max
        println(s"max T = $maxPeriod")
        val periods = List.range(1, maxPeriod+1)
        var bestSoFar = 1000000.0
        var bestStartPos = 0
        var bestT = 0
        var bestLocations = List(0)
        for (st <- startingPonts){
            for (tt <- periods){
                // println(s"start=$st, T=$tt")
                val locs = genPeriodicLocations(List(st), tt, n)
                costf(changes, locs) match {
                    case Some(v:Double) => {
                        if (v < bestSoFar) {
                            bestSoFar = v
                            bestStartPos = st 
                            bestT = tt
                            bestLocations = locs
                            println(s"..New cost = $bestSoFar, start = $bestStartPos, T = $bestT")
                        }
                    } 
                    case None => println("")
                }
            }
        }
        println("Lowest cost function value = " + bestSoFar)
        println(bestLocations)
        (bestStartPos, bestT, bestLocations)
    }

    def constructIndicatorFunc(bestLocations:List[Int], w:Int, sigLen:Int):List[Int] = {
        List(0)
    }

    def pccf(mu:Double, sigma:Double, n:Int):List[Double]={
        // val r = new scala.util.Random
        // (1 to n+1).map(x=>r.nextDouble).toList
        val chpProbs = DenseVector.zeros[Double](n)
        val ts = DenseVector.tabulate(n){i => i+1}
        for(i <- 1 to n){
            val G = new Gaussian(i * mu, Math.sqrt(sigma*sigma*i))
            chpProbs += ts.map(x => G.pdf(x))
        }
        chpProbs.toArray.toList
    }

    def constructIndicatorFuncFromPccf(pccf:List[Double], h:Double):List[Int]={
        // val r = new scala.util.Random
        // val n = pccf.length
        // (1 to n+1).map(x=>r.nextInt(2)).toList
        pccf.map(x => {if (x < h) {0} else {1}})
    }

    def overlapOfIndFuncs(bestIndFunc:List[Int], pccfIndFunc:List[Int]):Double={
        val n = pccfIndFunc.length
        assert(n > 1)
        assert(bestIndFunc.length == n)
        (pccfIndFunc zip bestIndFunc).map(x=>{if (x._1 == x._2  && x._1 == 1) {1} else {0} }).sum / n.toFloat
    }


    def findOptimPccf(changes:List[Int], sigLen:Int):Unit={
        val sigLen = 100
        val (s, periodT, bestLocations) = findOptimIndFunLocations(changes)
        val optimIndFunc = constructIndicatorFunc(bestLocations, 10, sigLen)
        val sigmaS = List(0.01, 0.1, 0.2, 0.5)
        val mu = periodT
        val h = 0.1 // threshold for pccf
        var bestSoFar = 1.0
        for(s <- sigmaS){
            val probs = pccf(mu, s, sigLen)
            val pccfIndFunc = constructIndicatorFuncFromPccf(probs, h)
            val overlap = overlapOfIndFuncs(optimIndFunc, pccfIndFunc)
            if(bestSoFar < overlap) {
                bestSoFar = overlap
                println(s"better overlap=$bestSoFar")
            }
            println(s"sigma=$s, overlap=$overlap")
        }
        println("Optim pccf: ")
    }

    def main(args:Array[String]):Unit = {
        // val tt = optimize(List(11,19,21)) 
        // val tt = optimize(List(10,20,30)) 
        // val tt = overlapOfIndFuncs(List(1,1,1,1,1), List(0,0,0,1,0))
        val tt = pccf(10.0, 1.0, 100)
        println(tt)
    }
}