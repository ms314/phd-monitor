import java.util.NoSuchElementException

object SimpleMain {


  // Lec. 3.3 Polymorphism
  // https://www.coursera.org/learn/progfun1/lecture/HH19P/lecture-3-3-polymorphism
  trait List[T] {
    def isEmpty:Boolean
    def head: T
    def tail: List[T]
  }
  class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty = false
  }
  class Nil[T] extends List[T]{
    def isEmpty:Boolean = true
    def head:Nothing = throw new NoSuchElementException("Nil.head")
    def tail:Nothing =throw new NoSuchElementException("Nil.tail")
  }

  def singleton[T](elem:T) = new Cons[T](elem, new Nil[T])


  // Lec.4.6
  trait Expr
  case class Number(n:Int) extends Expr
  case class Sum(e1:Expr, e2:Expr) extends Expr
  def show(e:Expr):String= e match {
    case Number(x)=> x.toString
    case Sum(l,r) => show(l) + "+" + show(r)
  }

//  def constrIndFuct(changes:List[Int]):List[Int]= changes match {
//    case
//  }

  def main(args: Array[String]):Unit = {
		 println("***")
  }
}
