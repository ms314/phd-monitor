
# include("../Lib/detectors/BayesDetectorReplica.jl")
# include("../Lib/Common.jl")
#include("../Lib/DetectorPageHinkley.jl")
#include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")

include("../Lib/common.jl")
include("../Lib/PerformanceMetrics.jl")

using CSV, DataFrames
using DelimitedFiles
using Printf

function printDelayVarLambda(x; chps::Array{Int64}=[])
    N = length(x)
    M = zeros(N, 4)
    c = 0
    best_lambda = 0
    best_loss = 9999.0
    for lambda in range(1, stop=25, length=100)
        c += 1
        r,   = bayesDetector(vec(x), lambda = lambda, gaussPdf=false)
        cdes = extractChangesBayes(r)
        lossV, delay, probFa, num_fa = lossFunction(;changes=chps, detections=cdes, lenOfSig=length(x), retAlsoDelayAndFa=true)
        if lossV < best_loss
            best_loss = lossV
            best_lambda = lambda
        end 
        M[c,:] = [lambda, delay, num_fa, lossV]
        if 1==0
            @printf("lambda = %0.3f, delay=%2d, num(fa)=%2d\n", lambda, delay, num_fa)
        end
    end 
    @printf("... Best lambda = %0.3f, Min. loss=%0.4f\n", best_lambda, best_loss)
    fout ="./out/printerOutput.dat" 
    open(fout, "w") do io writedlm(io, M, ',') end
    println("save into $fout")
end 

function runDiagBayesTwiSig()
    #x = readdlm("./data/signal.dat")
    dir = "/home/ms314/gitlocal/phd-monitor/datasets/"
    x = vec(readdlm(dir*"exchange-rate-bank-of-twi/data/sigTwi.dat"))
    
    OUTCDESFILE = "./out/cdesTwi_L14.091.dat"
    
    if 1==1
        println("run with static settings")
        r, = bayesDetector(vec(x), lambda = 14.091)
        cdes = extractChangesBayes(r)
        fout = OUTCDESFILE
        open(fout, "w") do io writedlm(io, cdes, ',') end
        println("end: save into $fout")
    end

    if 1==1
        #chps = vec(readdlm(dir*"exchange-rate-bank-of-twi/data/changesTwi.dat"))
        ## Use chps from scala-dp-detector 
        #chps = vec(readdlm(dir*"exchange-rate-bank-of-twi/data/changesTwiScalaDpDetector.dat"))
        ## Use from detector itself )
        chps = [convert(Int64, e) for e in vec(readdlm(OUTCDESFILE))]
        printDelayVarLambda(x, chps=chps)
    end
    
end 
runDiagBayesTwiSig()

# smth. old:   function mavg(x::Array{Float64,1}; w=10, f = mean)
# smth. old:       n = length(x)
# smth. old:       out = zeros(n)
# smth. old:       for i=1:n
# smth. old:           if i == 1
# smth. old:               out[i] = f(x[1:w])
# smth. old:           elseif i <= w
# smth. old:               out[i] = f(x[1:w])
# smth. old:           else
# smth. old:               out[i] = f(x[(i-w+1):i])
# smth. old:           end
# smth. old:       end 
# smth. old:           out
# smth. old:   end 
# smth. old:   
# smth. old:   function run()
# smth. old:       s = readJSON("../config_datasets.json")
# smth. old:       sigpath = s["dir"] * s["sig2"]
# smth. old:       sig01 = readInputSig(sigpath)
# smth. old:       #sig01 = mavg(sig01, w=50, f=std)
# smth. old:       #sig01 = vcat(randn(150), randn(150)+1, randn(150))
# smth. old:       maxes, mat = bayes_detector(sig01, lambda=230, mu0=0.50)
# smth. old:       cdes = extract_changes(maxes)
# smth. old:       println(cdes)
# smth. old:       writedlm("./out/M.dat", mat, ',')
# smth. old:       CSV.write("./out/cdes.csv", DataFrame(cdes = cdes), header=true)
# smth. old:       CSV.write("./out/sig.csv", DataFrame(sig=sig01), header=true)
# smth. old:   end 
# smth. old:   run()
