using Statistics, SpecialFunctions

function constant_hazard(r, lambda::Float64)
    @assert lambda >= 1.
    (1.0 / lambda) .* ones(size(r))
end

# function studentpdf(x  ::Float64, mu ::Array{Float64,1}, var::Array{Float64,1}, nu ::Array{Float64,1})
#     c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (nu .* pi .* var) .^ (-0.5)
#     p = c .* (1.0 .+ (1.0 ./ (nu .* var)) .* (x .- mu).^2.0).^(-(nu .+ 1.0)./2.0)
#     if length(p)==1
#         p[1]
#     else
#         p
#     end
# end

function studentpdf(x  ::Float64, mu ::Array{Float64,1}, var::Array{Float64,1}, nu ::Array{Float64,1})
    a1 = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) 
    a2 =  (nu .* pi .* var) .^ (-0.5)
    a = a1 .* a2 
    b = 1.0 .+ (1.0 ./ (nu .* var)) .* (x .- mu).^2.0
    c = b .^ (-(nu .+ 1.0)./2.0)
    println("a1=$a1, a2=$a2, a=$a, b=$b, c=$c")
    d = a .* c
end

"Function to extract changepoints locations from the bayes_detector() output"
function extractChangesBayes(seq)::Array{Int64,1}
    n = length(seq)
    chps = zeros(n)
    for i in 1:n
        chps[i] = i - seq[i]
    end
    out = unique(chps)
    out = map(x->convert(Int,x), out)
    if length(out) > 1
        return out[1:end-1]
    else
        return out
    end
end


function bayesDetectorForScala(y::Array{Float64,1};lambda::Float64=200.0,mu0::Float64=0.0)
    T = length(y)
    @assert lambda >= 1.
    kappa0::Float64          = 1.
    alpha0::Float64          = 1. ## nu
    beta0 ::Float64          = 1.
    CP                       = [0]
    R                        = zeros(T+1, T+1)
    R[1, 1]                  = 1
    muT   ::Array{Float64,1} = [mu0]
    kappaT::Array{Float64,1} = [kappa0]
    alphaT::Array{Float64,1} = [alpha0]
    betaT ::Array{Float64,1} = [beta0]
    maxes                    = zeros(T+1)
    maxes_probs              = zeros(T+1)
    for t=1:T-1+1
        @assert typeof(y[t]) == Float64
        predprobs = studentpdf(y[t], muT, betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT), 2.0 .* alphaT)

        R[2:t+1, t+1] = R[1:t, t] .* predprobs .* (1.0 .- lambda^-1)
        R[1, t+1] = sum(R[1:t, t] .* predprobs .* lambda^-1)
        R[:, t+1] = R[:, t+1] / sum(R[:, t+1])
        
        muT0    = vcat(mu0 , (kappaT .* muT .+ y[t]) ./ (kappaT .+ 1.0))
        kappaT0 = vcat(kappa0, kappaT .+ 1.0)
        alphaT0 = vcat(alpha0, alphaT .+ 0.5)
        betaT0  = vcat(beta0, kappaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
        
        muT    = muT0
        kappaT = kappaT0
        alphaT = alphaT0
        betaT  = betaT0        
        maxes[t] = findmax(R[:,t])[2]
        maxes_probs[t] = findmax(R[:,t])[1]
    end
    maxes = map(x -> convert(Int, x), maxes)
    maxes, R
end 

function testStudentPdf()
    p1 = studentpdf(1.0, [1.0], [3.0], [4.0])
    p2 = studentpdf(1.0, [1.1, 2.0], [1.0, 3.0], [1.0, 4.0])
    # p2 = studentpdf(1.0, [1.0], [3.0], [4.0])
    println(p1)
    println(p2)
end

function run()
    testStudentPdf()
end 
run()

# gpdf(x::Float64, m::Float64, s::Float64) = (1.0/sqrt(2*pi*s^2)) * exp(-(x-m)^2/(2*s^2))
# function gpdf(x::Float64, m::Array{Float64}, s::Array{Float64})::Array{Float64}
#     map((m_val, s_val)-> gpdf(x, m_val, s_val), m, s)
# end
# function gpdf(x::Float64, m::Array{Float64}, s::Float64)::Array{Float64}
#     map((m_val, s_val)-> gpdf(x, m_val, s_val), m, repeat([s], length(m)))
# end
# function gauss_pdf(ms::Array{Float64}, val::Float64, sigma::Float64=1.)
#   gpdf(val, ms, sigma)
# end
