package example
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
//import org.scalajs.dom.html
import scala.util.Random
import scala.io.Source
import org.scalajs.dom.{document => doc}
import org.scalajs.dom.raw._

class MapCanvas(w:Int, h:Int, ys:Array[Double]) {
  def mapY(y:Double):Int = { (h * (y-ys.min) / (ys.max-ys.min)).toInt }
  def mapX(x:Int):Int={ (w * x.toDouble/ys.length).toInt }
}


@JSExport
object ScalaJSExample{
  val W:Int = 300
  val H:Int = 300
  val ys = (1 to 700).map(_ => scala.util.Random.nextGaussian).toArray.map{var s=0.0; x => {s+=x; s}}
  val f = new MapCanvas(W, H, ys)

  val ctx = dom.document
    .getElementById("canvas")
    .asInstanceOf[dom.html.Canvas]
    .getContext("2d")
    .asInstanceOf[dom.CanvasRenderingContext2D]

  def clear() = {
    ctx.fillStyle = "black"
    ctx.fillRect(0, 0, W, H)

  }


  var count = 0
  def run = {
    count = (count + 1) % (ys.length-1)
    if (count==0) clear()
//    clear()
    ctx.fillStyle = "white"
//    ctx.fillText("Random Walk", 0.0, 0.0)
    ctx.fillRect(f.mapX(count), H - f.mapY(ys(count)), 2, 2)
  }

  @JSExport
  def main(): Unit = {
    clear()
    dom.window.setInterval(() => run, 20)
  }
}