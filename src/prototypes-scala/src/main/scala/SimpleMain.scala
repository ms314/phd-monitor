import breeze.linalg._
import breeze.plot._

// plot indicator function from cdes 
object IndicatorFunction {

  // (1,2,3,5,7,9,12,15,19,23,27,)

  // Get midpoints between changes.
  def getMidPoints(changes:List[Int], n:Int):List[Int]={
    // augmented list of changes
    val a1 = 1::changes.filter(x => x!=0 && x!= 1 && x!=n) ::: List(n)
    val a2 = a1.init zip a1.tail
    val midPointsAbs = a2.map(x => x._2 - x._1).map(x => x / 2)
    println(midPointsAbs)
    (a1.init zip midPointsAbs).map(x => x._1 + x._2)
  }

  // Construct indicator function
  // n: length of signal 
  // w: width of window around changes
  def indFunction(changes:List[Int], w:Int, n:Int):List[Int] = changes match {
    case f::s::rest => {if (f>s) f::Nil else s::Nil}
    case _ => Nil
  }  


  def main(args: Array[String]):Unit = {
    //val r = indFunction(List(10,11,15,30,40), 9, 100) println(r) }
    // val r = getMidPoints(List(10,20,25), 100)
    val r = getMidPoints(List(10,11, 20,25), 100)
    println(r)
  }
}
