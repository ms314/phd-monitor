library(magrittr)

intervals <- read.table("./changepoints.dat")$V1 %>% diff 

## mu0, sigma0 - prior for unknown mu
## sigmaKnown - assumed to be known sigma of population
updateNormal <- function(mu0, sigma0, sigmaKnown, xn){
  tau <- 1.0/sigmaKnown**2
  tau0 <- 1.0/sigma0**2
  n <- length(xn)
  a <- tau0 * mu0 + tau * sum(xn) 
  b <- tau0 + n * tau
  updatedMu <- a / b
  updatedSigma <- tau0 + n * tau
  posteriorForMu <- c(updatedMu, updatedSigma)
  return(posteriorForMu)
}

demoGaussUpdate <- function(){
  cat("Plot prior for mu")
  mu0    <- 10.0
  sigma0 <- 9.0
  xs <- seq(- 2 * mu0, 3 * mu0, by=0.05)
  
  newData <- c(1,2,3,2)
  newParams <- updateNormal(mu0, sigma0, sd(newData), newData)
  posterior <- dnorm(xs, newParams[1], newParams[2])
  
  prior <-dnorm(xs, mu0, sigma0)
  plot(xs, prior, type = 'l', ylim = c(0, max(posterior)), main = "Prior and posterior for mu (Normal, sigma known)")
  lines(xs, posterior, col="red", lty=2)
  cat("\nPrior mu=", mu0, ", prior sigma=", sigma0)
  cat("\nPostrior mu=", newParams[1], ", posterior sigma = ", newParams[2])
  return(data.frame(x=xs, prior = prior, posterior = posterior))
}
rNorm <- demoGaussUpdate()


updateGamma <- function(a0, b0, alphaKnown, xn){
  n <- length(xn)
  updAlpha <- a0 + n * alphaKnown
  updBeta <- b0 + sum(xn)
  posteriorForBeta <- c(updAlpha, updBeta)
  return(posteriorForBeta)
}

demoGammaUpdate <- function(){
  alpha0 <- 2
  beta0 <- 5.0
  xs <- seq(0, 10, by = 0.1)
  
  xnew <- c(1,1,1,2)
  ## how to estimate the shape/scale parameter?
  updParams <- updateGamma(alpha0, beta0, 1.1, xnew)
  
  prior <- dgamma(xs, shape = alpha0, rate = beta0)
  posterior <- dgamma(xs, shape = updParams[1], rate = updParams[2])
  
  plot(xs, prior, type = 'l',ylim = c(0, max(posterior)), main = "Prior and posterior for beta (Gamma)")
  lines(xs, posterior, col="red", lty = 2)
  
  return(data.frame(x=xs, prior = prior, posterior = posterior))
}
rGamma <- demoGammaUpdate()

## Gamma(alpha0, beta0) - prior for \lambda
updatePoisson <- function(alpha0, beta0, xn){
  n <- length(xn)
  updA <- alpha0 + sum(xn)
  updB <- beta0 + n
  posteriorForLambda <- c(updA, updB)
  return(posteriorForLambda)
}

demoPoissonUpdate <- function(){
  alpha0 <- 2.0
  beta0 <- 3.0
  xs <- seq(0, 10, by = 0.1)
  xnew <- c(1,1,1,2)
  updParams <- updatePoisson(alpha0, beta0, xnew)
  
  
  prior <- dgamma(xs, shape = alpha0, rate = beta0)
  posterior <- dgamma(xs, shape = updParams[1], rate = updParams[2])
  
  plot(xs, prior, type = 'l',ylim = c(0, max(posterior)), main = "Prior and posterior for lambda (Poisson)")
  lines(xs, posterior, col="red", lty = 2)
  
  return(data.frame(x=xs, prior = prior, posterior = posterior))
}
rPoisson <- demoPoissonUpdate()

png("./demo.PNG")
par(mfrow=c(3,1))
par(mar = c(1.0, 3.0, 1.7, 1))
plot(rNorm$x, rNorm$prior, type='l', ylim = c(0, max(rNorm$posterior)), ann = FALSE, xaxt ="n", yaxt = "n")
mtext("Prior and posterior for mu in Normal (known Sigma)", side=3, line=0.5)
mtext("Prob.density", side=2, line=1.3)
lines(rNorm$x, rNorm$posterior,col="red", lty=2)

par(mar = c(1.0, 3.0, 2.0, 1))
plot(rGamma$x, rGamma$prior, type='l', ann = FALSE, xaxt ="n", yaxt = "n")
mtext("Prior and posterior for Beta in Gamma (known Alpha)", side=3, line=0.5)
mtext("Prob.density", side=2, line=1.3)
lines(rGamma$x, rGamma$posterior,col="red", lty=2)

par(mar = c(2.5, 3.0, 2.0, 1))
plot(rPoisson$x, rPoisson$prior, type='l', ann = FALSE, xaxt ="n", yaxt = "n")
lines(rPoisson$x, rPoisson$posterior,col="red", lty=2)
mtext("Prior and posterior for Lambda in Poisson", side=3, line=0.5)
axis(1, cex.axis = 0.9, tck = 0.03, mgp=c(3, .1, 0), labels = TRUE)
mtext("Parameter's value", side = 1, line=1.5)
mtext("Prob.density", side=2, line=1.3)
dev.off()