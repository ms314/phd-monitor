source("./pccfs.R")

bayesUpdateNormallPccf <- function(priorForMu, sigmaForTimeIntervalsKnown, newTimeInterval, makePlot=FALSE){
  N <- 100
  priorPccf <- gaussPccf(N, priorForMu$mu0, sigmaForTimeIntervalsKnown)
  ##
  cat("\n | Prior parameters for Mu, mu0 =",priorForMu$mu0,", sigma0 =",priorForMu$s0)
  cat("\nPrior Pccf parameters: mu =", priorForMu$mu0, "; sigma =", sigmaForTimeIntervalsKnown)
  cat("\n...Observing new data (time interval between changes)", newTimeInterval)
  ## updateNormal <- function(mu0, sigma0, sigmaKnown, xn){
  r <- updateNormal(priorForMu$mu0, priorForMu$s0, sigmaForTimeIntervalsKnown, c(newTimeInterval))
  cat("\n | Posterior parameters for Mu: mu =", r$mu,", sigma0 =", r$sigma)
  cat("\nPosterior Pccf parameters: mu =", r$mu, "; sigma =", sigmaForTimeIntervalsKnown)

  posteriorPccf <- gaussPccf(N, r$mu, r$sigma) ## !!! maybe sigma is sigmaForTimeIntervalsKnown

  if(makePlot){
    outimage <- "./img/PccfNormalBayesUpdate.PNG"
    cat("Save image into:", outimage)

    #png(outimage)

    par(mfrow=c(2,1))
    plot(priorPccf, type = 'l',main = "Prior and posterior Pccf's",xlab = "Time")
    lines(posteriorPccf, type = 'l',lty=2,col = "red")
    #
    xps <- seq(priorForMu$mu0-2*priorForMu$s0, priorForMu$mu0+ 2*priorForMu$s0,by=0.01)
    priorPs <- dnorm(xps, priorForMu$mu0, priorForMu$s0)
    postPs <- dnorm(xps, r$mu,r$sigma)

    plot(xps,priorPs,type = 'l', ylim = c(0, max(postPs)),
        main = "Prior and posterior distribution for Mu",ylab = "P")
    lines(xps,postPs,col="red",lty=2)

    #dev.off()
  }
  newPccfParameters = list(mu=r$mu, sigma=r$mu, sigma=sigmaForTimeIntervalsKnown)
  return(newPccfParameters)
}


bayesUpdateNormallPccfSequence <- function(detections,
                                           priorForMu = list(mu0=10.0, s0=5.0),
                                           s_known=1.1,
                                           makePlot=FALSE){
  iids <- diff(detections)
  n<-length(iids)
  r <- bayesUpdateNormallPccf(priorForMu = priorForMu, s_known, iids[1], makePlot = makePlot)
  if(n >1){
    for (i in 2:n) {
      r <- bayesUpdateNormallPccf(list(mu0=r$mu, s0=r$sigma), s_known, iids[i], makePlot = makePlot)
    }
  }
  return(r)
}
