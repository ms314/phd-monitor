#install.packages("plot3D")
library(plot3D)
library(magrittr)

sigmx <- function(x, s=10){ 
  1.0/(1.0 + exp(-x/s)) 
  }


costf <- function(delay, ncdes, a, b, alpha=0.5, nchps=1){
  alpha*sigmx(delay,a) + (1-alpha) * sigmx(abs(ncdes-nchps),b)
}

scaleF <- function(x,m,s){(x-m)/s}
scaleDelay <- function(val, expectedMu=50, s=10){scaleF(val,expectedMu,s)}
scaleNcdes <- function(val, expectedMu=5, s=1){scaleF(val,expectedMu,s)}

scaleDelay(85)
scaleNcdes(2)

costf3d <- function(d, n, alpha){
  costf(d, 1, n, alpha=alpha)
}


dls <- seq(1,100)
ncs <- seq(1,100)
M <- mesh(dls, ncs)
z <- costf3d(M$x, M$y)
surf3D(M$x, M$y, z)


ncdes <-seq(0, 200)
ds <- ncdes
plot(ds, sigmx(ds, 10), type='l', xlim = c(0, 200))
lines(ncdes, sigmx(ncdes, 20), col="red")

