#
include("../Lib/PerformanceMetrics.jl")

"""
An important test!
"""
function sim1CostFunction()
    bestSoFar = 10000.
    for k = 1 : 50
        randCdes = rand(103:200, k) |> sort
        l = lossFunction(; changes=[100], detections=randCdes, lenOfSig=200, scaleDelay = 10., scaleNcdes=3.)
        # println("l = $l")
        if k == 1
            bestSoFar = l
            println("Init l = $l, cdes = $randCdes, len(cdes) = ", length(randCdes))
        else
            if l < bestSoFar
                bestSoFar = l
                println("l = $l, cdes = $randCdes, len(cdes) = ", length(randCdes))
            end
        end
    end
end


function run()
    # l1 = lossFunction(; changes=[100], detections=[150], lenOfSig=200)
    # l2 = lossFunction(; changes=[100], detections=[149, 150], lenOfSig=200)
    # l3 = lossFunction(; changes=[100], detections=[5,6,7,149, 150, 151,155,160], lenOfSig=200)
    # #
    cdes4 = [0, 143,146,150,152, 154, 161, 163, 166 ,171, 172, 174, 177, 179, 181, 185, 190, 201]
    cdes5 = [0, 177, 179, 185, 201]
    ## ??? WHY LOSS FOR CDES4 IF SMALLER THATN FOR CDES5
    scDel=5.
    scNcd=10.
    ## !!! Now l4 is higher
    ## commit: 668517e
    l4 = lossFunction(; changes=[100], detections=cdes4, lenOfSig=200, scaleDelay=scDel, scaleNcdes=scNcd)
    l5 = lossFunction(; changes=[100], detections=cdes5, lenOfSig=200, scaleDelay=scDel, scaleNcdes=scNcd)
    # ds=fnDelays([100], [150], 200)
    # println("l1 = $l1")
    # println("l2 = $l2")
    # println("l3 = $l3")
    println("l4 = $l4")
    println("l5 = $l5")
    ##
    cdes6 = [105, 120]
    cdes7 = [115]
    println()
    println("Compare losses for cdes6=$cdes6 and cdes7=$cdes7")
    l6 = lossFunction(; changes=[100], detections=cdes6, lenOfSig=200, scaleDelay=scDel, scaleNcdes=scNcd)
    l7 = lossFunction(; changes=[100], detections=cdes7, lenOfSig=200, scaleDelay=scDel, scaleNcdes=scNcd)
    println("l6 = $l6")
    println("l7 = $l7")
    # fnDelays([100, 151], cdes4, 200) |> println
    # numberOfFalseAlarms([100], cdes5, 200) |> println
    # length(cdes5) |> println
end

function testFindDelays()
    r=fnDelays([100], [0, 101, 110], 200)
    r=fnDelays([100], [0], 200)
    r=fnDelays([100], [0, 201], 200)
    r=fnDelays([100], [0, 185, 201], 200)
    println(r)
    scDel=1.#5.
    scNcd=1.#10.
    # r1 = lossFunction(; changes=[100], detections=[0, 201], lenOfSig=200, scaleDelay = scDel, scaleNcdes=scNcd)
    # r2 = lossFunction(; changes=[100], detections=[0, 185, 201], lenOfSig=200, scaleDelay =scDel , scaleNcdes=scNcd)
    ## MinMax version:
    scDel=10.
    scNcd=3.
    r1 = lossFunction(; changes=[100], detections=[0, 201], lenOfSig=200, scaleDelayMinMax=scDel, scaleNcdesMinMax=scNcd)
    r2 = lossFunction(; changes=[100], detections=[0, 185, 201], lenOfSig=200, scaleDelayMinMax=scDel , scaleNcdesMinMax=scNcd)
    println("r1=$r1")
    println("r2=$r2")
end

function test20181126()
    scDel=15.
    scNcd=3.
    if 1==1
        r1 = lossFunction(; changes=[100], detections=[0, 201], lenOfSig=200, scaleDelayMinMax=scDel, scaleNcdesMinMax=scNcd)
        r2 = lossFunction(; changes=[100], detections=[0,101, 201], lenOfSig=200, scaleDelayMinMax=scDel, scaleNcdesMinMax=scNcd)
        r3 = lossFunction(; changes=[100], detections=[0,50, 101, 201], lenOfSig=200, scaleDelayMinMax=scDel, scaleNcdesMinMax=scNcd)
        r4 = lossFunction(; changes=[100], detections=[0,50, 201], lenOfSig=200, scaleDelayMinMax=scDel, scaleNcdesMinMax=scNcd)
        @assert(r2 < r1)
        @assert(r3 > r2)
        @assert(r4 > r3)
    end
    #
    r5 = lossFunction(; changes=[3],alpha= .5, detections=[0,3, 7], lenOfSig=6, scaleDelayMinMax=4., scaleNcdesMinMax=7.)
    r6 = lossFunction(; changes=[3],alpha= .5, detections=[0,7]   , lenOfSig=6, scaleDelayMinMax=4., scaleNcdesMinMax=7.)
    @assert r5 < r6
    r7 = lossFunction(; changes=[100], alpha= .5, detections=[0,100,200], lenOfSig=200, scaleDelayMinMax=4., scaleNcdesMinMax=7.)
    r8 = lossFunction(; changes=[100], alpha= .5, detections=[0,200], lenOfSig=200, scaleDelayMinMax=4., scaleNcdesMinMax=7.)
    # println("r1 = $r1")
    # println("r2 = $r2")
    # println("r3 = $r3")
    # println("r4 = $r4")
    println("r5 = $r5")
    println("r6 = $r6")
    println("r7 = $r7")
    println("r8 = $r8")
end

function test201902_test_cdes_empty()
    changes = [100]
    detections::Array{Int64} = []
    lenOfSig = 200
    delays = fnDelays(changes, detections, lenOfSig)
    # r = lossFunction(; changes=changes, detections=detections, lenOfSig=lenOfSig)
    # println("r = $r")
    println("delays = $delays")
end

function monteCarloTestSingle(Nsim::Int64; sigLen=100, chpLoc=50)
    bestSoFar = 10000000
    minPossible = lossFunction(changes=[chpLoc], detections=[chpLoc], lenOfSig=sigLen)
    for i in 1:Nsim
        cdes = rand([1:sigLen;], rand([1:sigLen;])) |> sort
        loss = lossFunction(changes=[chpLoc], detections=cdes, lenOfSig=sigLen)
        if loss < bestSoFar
            bestSoFar = loss
            println()
            println(loss)
            println("Best possible loss=$minPossible")
            println(cdes)
        end

    end
end

function monteCarloTestMulti(tolerance::Float64; sigLen=100, chps=[20, 30, 50, 90])
    bestSoFar = 10000000
    minPossible = lossFunction(changes=chps, detections=chps, lenOfSig=sigLen)
    while abs(bestSoFar - minPossible) > tolerance
        cdes = rand([1:sigLen;], rand([1:sigLen;])) |> sort
        loss = lossFunction(changes=chps, detections=cdes, lenOfSig=sigLen)
        if loss < bestSoFar
            bestSoFar = loss
            println("Measured loss=$loss; Best possible loss=$minPossible")
            println("CDEs:", cdes)
        end

    end
end

# sim1CostFunction()
# testFindDelays()
# test20181126()
# monteCarloTestSingle(1000000)
# monteCarloTestMulti(0.001)
test201902_test_cdes_empty()