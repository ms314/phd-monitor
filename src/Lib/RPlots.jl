using RCall, CSV. DataFrames

function rplot1sig(fpath::String)
    R"x=read.table($fpath, header=FALSE, sep=',')$V1; plot(x)"
end

function rplotSigAndCdes(sigpath, cdespath; out="./out/PNG")
    height=500
    R"png($out, height=$height, width=floor(1.618*$height)); x=read.table($sigpath, header=FALSE, sep=',')$V1; cdes=read.table($cdespath,header=TRUE)$cdes; plot(x,type='b'); abline(v=cdes); dev.off()"
    println("... Save plot into $out")
end 
