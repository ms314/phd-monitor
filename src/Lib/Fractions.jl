module Fractions

export fracOfDetected, fracOfFa, detectionDelay1Chp, detectionDelayMulti

using Base.Test


"""
Find the nearest changepoint to a given CdE.
"""
function nearestElem(x, ys::Array{Int,1})::Tuple{Int64, Float64}
    n = length(ys)
    @assert n > 0
    if n==1
        return (ys[1], abs(x-ys[1]))
    end
    distToCde = y -> abs(x-y)
    bestDist = distToCde(ys[1])
    nearestSoFar = ys[1]
    for chp in ys[2:end]
        newDist = distToCde(chp)
        if newDist < bestDist
            bestDist = newDist
            nearestSoFar = chp
        end
    end
    return (nearestSoFar, bestDist)
end
@test nearestElem(2, [1, 2]) == (2,0)
@test nearestElem(2, [1]) == (1,1)
@test nearestElem(0, collect(1:9)) == (1,1)
@test nearestElem(9, collect(1:9)) == (9,0)
@test nearestElem(19, collect(1:9)) == (9,10)



"""
Measure change detection delay
"""
function detectionDelay1Chp(change::Int64, cdes::Array{Int64,1}, sigLength::Int64 = 100)
    # @assert sigLength >= maximum(changes)
    if length(cdes) > 0
        @assert sigLength >= maximum(cdes)-1
    end 
    # @assert length(cdes) >= 1
    if length(cdes) == 0
        return sigLength
    end 
    best_so_far = sigLength ## Best so far delay
    for cde in cdes 
        if cde >= change
            if cde - change < best_so_far
                best_so_far = cde - change
            end 
        end 
    end
    best_so_far
end 
@test detectionDelay1Chp(5, [6, 7], 20) == 1
@test detectionDelay1Chp(5, [10], 20) == 5
@test detectionDelay1Chp(5, [3, 4], 20) == 20


"""
Measure change detection delay
"""
function detectionDelayMulti(;changes::Array{Int64,1}=[1,2], 
                              cdes::Array{Int64,1}=[4,5], 
                              sigLength::Int64 = 100)
    if length(changes) > 0
        @assert sigLength >= maximum(changes)
    end 
    if length(cdes) > 0 
        @assert sigLength >= maximum(cdes)-1
    end 
    @assert length(changes) > 0
    # minCde = minimum(cdes)
    if length(cdes) == 0
        return [sigLength]
    elseif length(changes) == 1
        return [detectionDelay1Chp(changes[1], cdes, sigLength)]
    else
        delays = [detectionDelay1Chp(e, cdes, sigLength) for e in changes]
        return delays

    end 
end 
@test detectionDelayMulti(changes=[5], cdes=[6, 7], sigLength=30) == [1]
@test detectionDelayMulti(changes=[5, 65, 75], cdes=[100], sigLength=130) == [100-5, 100-65, 100-75]
@test detectionDelayMulti(changes=[5, 65, 75], cdes=[67], sigLength=130) == [67-5, 67-65, 130]
@test detectionDelayMulti(changes=[10,20,30], cdes=[20], sigLength=30) == [10, 0, 30]



"""
Fraction of detected changepoints.
(detected changes / number of changes)
"""
function _numOfDetected(chps, cdes, maxDelay)
    # Check for every change if there is a CdE nearby
    numOfDetected = 0
    for chp in chps
        nearestCde, dist = nearestElem(chp, cdes)
        if abs(nearestCde - chp) <= maxDelay
            numOfDetected += 1
        end
    end
    @assert numOfDetected <= length(chps)
    numOfDetected
end

function fracOfDetected(;chps=[2,10], cdes=[1], maxDelay=1)
     _numOfDetected(chps,cdes,maxDelay) / length(chps)
end
@test fracOfDetected(chps=[1,10,20,30],cdes=[2],maxDelay=1) == 1/4
@test fracOfDetected(chps=[1,10,20,30],cdes=[2],maxDelay=0.1)==0.0
@test fracOfDetected(chps=[1,10,20,30],cdes=[1,10,20],maxDelay=0.1)==3/4
@test fracOfDetected(chps=[1,10,20,30],cdes=[1,10,20,30],maxDelay=0.1)==1


"""
Probability of FAs.
Calculate the number of detected changes and reduce this number from the number of CdEs.
    n: length of the signal
"""
function fracOfFa(; chps=[10,20], cdes=[15,16,17, 21], maxDelay=1, n=100)
    if length(cdes) == 0
        return 0
    end 
    nDetected = _numOfDetected(chps, cdes, maxDelay)
    (length(cdes)-nDetected) / (n-length(chps))
end
@test fracOfFa(n=1000, chps=[10,20,50], cdes=[1,2,3,4,5], maxDelay=1) == 5.0/(1000-3)

end # module
