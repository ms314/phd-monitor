#
# - 2019-02-07: copied before fixing it. Reversing how to update Student's distribution parameters. 
#
# 2018-08-28: New module with u to date functionality.
#             One function for static and dynamic detector.
#
# https://github.com/av-maslov/change-detection/blob/master/downloaded/BayesianOnlineChangepoint/gaussdemo_orig.m

#module BayesDetector
# using TypesHierarchy
##export bayesDetectorNew, extractChangesBayes

using Statistics, SpecialFunctions
#, Distributions

function constant_hazard(r, lambda::Float64)
    @assert lambda >= 1.
    (1.0 / lambda) .* ones(size(r))
end

"""Non-standardized Student's t-distribution"""
## As in the Matlab code by author:
# c = exp(gammaln(nu/2 + 0.5) - gammaln(nu/2)) .* (nu.*pi.*var).^(-0.5);
# p = c .* (1 + (1./(nu.*var)).*(x-mu).^2).^(-(nu+1)/2);
## Check Eq.110: ~\cite{murphy2007conjugate}
function studentpdfold(x::Float64,
                       mu::Array{Float64,1},
                       var::Array{Float64,1},
                       nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (nu .* pi .* var) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ (nu .* var)) .* (x .- mu).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else
        p
    end
end

"""
My version: possibly a fixed typo
"""
function studentpdfmine(x::Float64,
                        mu::Array{Float64,1},
                        sigma::Array{Float64,1},
                        nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (pi .* nu) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ nu ) .* ((x .- mu)./sigma).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else
        p
    end
end
""" Switcher function """
function studentpdf(x::Float64,
                    mu::Array{Float64,1},
                    var::Array{Float64,1},
                    nu::Array{Float64,1})
    USENEWVERSION = true
    if USENEWVERSION
        studentpdfmine(x,mu,var,nu)
    else
        studentpdfold(x,mu,var,nu)
    end
end

# function studentpdf(x::Array{Float64,1}, mu::Float64, var::Float64, nu::Float64)
#     c = exp.(lgamma.(nu/2 + 0.5) - lgamma.(nu/2)) .* (nu .* pi .* var).^(-0.5)
#     p = c .* (1.0 + (1.0 ./ (nu .* var)) .* (x .- mu).^2.0) .^ (-(nu .+ 1.0)/2.0)
# end

function extract_changes_from_output_naive(seq)
    chps = []
    for i in 2:length(seq)
        if seq[i] < seq[i-1]
            push!(chps, i)
        end
    end
    chps
end


"Function to extract changepoints locations from the bayes_detector() output"
function extractChangesBayes(seq)::Array{Int64,1}
    #
    # seq[j] contains current estimate
    # of the location of the last change at the time moment j
    #
    n = length(seq)
    chps = zeros(n)
    for i in 1:n
        chps[i] = i - seq[i]
    end
    out = unique(chps)
    out = map(x->convert(Int,x), out)
    if length(out) > 1
        return out[1:end-1]
    else
        return out
    end
end

function test_extract_changes()
    inp1 = [0,1,2,3,4,3]
    rt = extract_changes(inp1)
    print("... test changes extractor ", rt)
end


########################
### Start: Gauss Pdf ###
########################
# function invertedRunningMean(xs::Array{Float64,1})
#     # vcat([xs[end]], cumsum(reverse(xs)) ./ cumsum(ones(length(xs))))
#     cumsum(reverse(xs)) ./ cumsum(ones(length(xs)))
# end
# function estimate_mus(x)
#   n = length(x)
#   mus = Float64[]
#   for i = 1:(n-1)
#     subvec = x[(n-i) : end]
#     push!(mus, mean(subvec))
#   end
#   #if n == 1
#   #  push!(mus, x[1])
#   #end
#   mus
# end
# | using Distributions: | function gauss_pdf(ms, val, sigma = 1)
# | using Distributions: |   res = Float64[]
# | using Distributions: |   for m in ms
# | using Distributions: |     d = Normal(m, sigma)
# | using Distributions: |     pdfgauss = pdf(d, val)
# | using Distributions: |     push!(res, pdfgauss)
# | using Distributions: |   end
# | using Distributions: |   res
# | using Distributions: | end
gpdf(x::Float64, m::Float64, s::Float64) = (1.0/sqrt(2*pi*s^2)) * exp(-(x-m)^2/(2*s^2))
function gpdf(x::Float64, m::Array{Float64}, s::Array{Float64})::Array{Float64}
    map((m_val, s_val)-> gpdf(x, m_val, s_val), m, s)
end
function gpdf(x::Float64, m::Array{Float64}, s::Float64)::Array{Float64}
    map((m_val, s_val)-> gpdf(x, m_val, s_val), m, repeat([s], length(m)))
end
function gauss_pdf(ms::Array{Float64}, val::Float64, sigma::Float64=1.)
  gpdf(val, ms, sigma)
end
######################
### End: Gauss Pdf ###
######################

###             OLD
#
# studentpdf(x::Float64,
#            mu::Array{Float64,1},
#            var::Array{Float64,1},
#            nu::Array{Float64,1})
"""
Old version.

- 2019-02-07: Added Gaussain predictive distribution
  option. Seems to be outperforming Student distribution.

"""
function bayesDetectorNewOld(y::Array{Float64,1};
                          adaptiveSettings::AbstractSettings = NoSettings(),
                          lambda::Float64=200.0,
                          mu0::Float64=0.0,
                          gaussPdf::Bool=true, 
                          gaussSigma=1.0)
    T = length(y)
    @assert lambda >= 1.
    if adaptiveSettings != NoSettings()
        # println("Run Bayesian detector with dynamic settings")
        @assert T == length(adaptiveSettings.indicatorFunction)
        lambdaDynamic = adaptiveSettings.indicatorFunction
        lambdaWhenDynamic0 = adaptiveSettings.sWhen0
        lambdaWhenDynamic1 = adaptiveSettings.sWhen1
    end
    kappa0::Float64 = 1.
    alpha0::Float64 = 1. ## nu
    beta0 ::Float64 = 1.
    CP      = [0]
    R       = zeros(T+1, T+1)
    R[1, 1] = 1
    muT   ::Array{Float64,1} = [mu0]
    kappaT::Array{Float64,1} = [kappa0]
    alphaT::Array{Float64,1} = [alpha0]
    betaT ::Array{Float64,1} = [beta0]
    maxes       = zeros(T+1)
    maxes_probs = zeros(T+1)
    for t=1:T-1
        # muT - is a vector of all possible hypos
        # We calc. predictive distribution for each hypothesis
        @assert typeof(y[t]) == Float64
        if gaussPdf
            # subv = y[1:t]
            subv = vcat(mu0, y[1:(t-1)])
            mus = cumsum(reverse(subv)) ./ cumsum(ones(length(subv)))
            # mus = cumsum(subv) ./ cumsum(ones(length(subv)))
            predprobs = gauss_pdf(mus, y[t], gaussSigma)
        else
            ## (x, mu, sigma, nu)
            predprobs = studentpdf(y[t],
                                   muT,
                                   betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT),
                                   2.0 .* alphaT)
            # println(length(predprobs) - length(gauss_pdf(estimate_mus(y[1:(t+1)]), y[t], gaussSigma)))
        end
        if adaptiveSettings != NoSettings()
            lambda = lambdaDynamic[t] == 0 ? lambdaWhenDynamic0 : lambdaWhenDynamic1
        end
        # Growth probabilities
        # orig version: H = constant_hazard([1:t;], lambda)
        # orig version: R[2:t+1, t+1] = R[1:t, t] .* predprobs .* (1.0 .- H)
        # orig version: R[1, t+1] = sum(R[1:t, t] .* predprobs .* H)
        R[2:t+1, t+1] = R[1:t, t] .* predprobs .* (1.0 .- lambda^-1)
        # Probability of a changepoint
        R[1, t+1] = sum(R[1:t, t] .* predprobs .* lambda^-1)
        # Normalize
        R[:, t+1] = R[:, t+1] / sum(R[:, t+1])
        ## Update statistics
        ## for \mu it is similar to the running mean
        ## $mu_n = n^-1 * ((n-1) * mu_{n-1} + y_n)$
        ## so we consider all possible mean values up to the current time moment
        ## should it not be inverted???
        ## where is it in ~\cite{murphy2007conjugate}??
        muT0    = vcat(mu0 , (kappaT .* muT .+ y[t]) ./ (kappaT .+ 1.0))
        ## maybe, maybe NOT
        ##-muT0 = invertedRunningMean(y[1:t])
        kappaT0 = vcat(kappa0, kappaT .+ 1.0)
        alphaT0 = vcat(alpha0, alphaT .+ 0.5)
        ## the old version (maybe my typo)::
        ## Eq.100, 104~\cite{murphy2007conjugate}
        # betaT0  = vcat(beta0 , kappaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
        ## new version: based on Eq.101-104
        ## Eq.100, 104~\cite{murphy2007conjugate}
        betaT0  = vcat(beta0, betaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
        muT    = muT0
        kappaT = kappaT0
        alphaT = alphaT0
        betaT  = betaT0
        maxes[t] = findmax(R[:,t])[2]
        maxes_probs[t] = findmax(R[:,t])[1]
    end
    # R = - log.(R) todo: check why sometimes there are negative numbers in R
    # maxestst = findmax(R, dims=2)[2] |> (y -> map(x->x[1], y))
    maxes = map(x -> convert(Int, x), maxes)
    # println("!!!")
    # println(maxes)
    # println(maxes_probs)
    # println("!!!")
    # for kk=1:size(R)[1]
    #     println(R[:,kk])
    # end
    # show(R)
    maxes, R
end

#  """
#  Test prototype.
#  Not to be used! yet
#  """
#  function bayesDetector3(y::Array{Float64,1};
#                          adaptiveSettings::AbstractSettings = NoSettings(),
#                          lambda::Float64=200.0,
#                          mu0::Float64=0.0)
#      T = length(y)
#      if adaptiveSettings != NoSettings()
#          @assert T == length(adaptiveSettings.indicatorFunction)
#          lambdaDynamic = adaptiveSettings.indicatorFunction
#          lambdaWhenDynamic0 = adaptiveSettings.sWhen0
#          lambdaWhenDynamic1 = adaptiveSettings.sWhen1
#      end
#      kappa0::Float64 = 1.
#      alpha0::Float64 = 1. ## nu
#      beta0 ::Float64 = 1.
#      CP      = [0]
#      R       = zeros(T+1, T+1)
#      R[1, 1] = 1
#      muT   ::Array{Float64,1} = [mu0]
#      kappaT::Array{Float64,1} = [kappa0]
#      alphaT::Array{Float64,1} = [alpha0]
#      betaT ::Array{Float64,1} = [beta0]
#      maxes   = zeros(T+1)
#      for t=1:T
#          # muT - is a vector of all possible hypos
#          # We calc. predictive distribution for each hypothesis
#          @assert typeof(y[t]) == Float64
#          ## (x, mu, sigma, nu)
#          predprobs = studentpdf(y[t], muT, betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT), 2.0 .* alphaT)
#
#          if adaptiveSettings != NoSettings()
#              lambda = lambdaDynamic[t] == 0 ? lambdaWhenDynamic0 : lambdaWhenDynamic1
#          end
#
#          H = constant_hazard([1:t;], lambda)
#
#          # Growth probabilities
#          R[2:t+1, t+1] = R[1:t, t] .* predprobs .* (1.0 .- H)
#          # Probability that there was a changepoint
#          R[1, t+1] = sum(R[1:t, t] .* predprobs .* H)
#          # Normalize
#          R[:, t+1] = R[:, t+1] / sum(R[:, t+1])
#
#          ## start: new update
#          push!(muT, (kappaT[end]*muT[end] + y[t])/(kappaT[end] + 1.0))
#          println(muT)
#          push!(kappaT, kappaT[end] + 1.0)
#          push!(alphaT, alphaT[end] + 0.5)
#          push!(betaT,  betaT[end] + (kappaT[end] * (y[t] - muT[end])^2.) / (2. * (kappaT[end] + 1.)))
#          ## end: new update
#          ## start: old update
#          # muT0    = vcat(mu0 , (kappaT .* muT .+ y[t]) ./ (kappaT .+ 1.0))
#          # kappaT0 = vcat(kappa0, kappaT .+ 1.0)
#          # alphaT0 = vcat(alpha0, alphaT .+ 0.5)
#          # betaT0  = vcat(beta0, betaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
#          # muT    = muT0
#          # kappaT = kappaT0
#          # alphaT = alphaT0
#          # betaT  = betaT0
#          ## end: old update
#
#          maxes[t] = findmax(R[:,t])[2]
#      end
#      # R = - log.(R) todo: check why sometimes there are negative numbers in R
#      maxes = map(x -> convert(Int, x), maxes)
#      maxes, R
#  end
#end
