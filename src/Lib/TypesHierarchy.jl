# module TypesHierarchy
# export AbstractSettings, NoSettings, DynamicSettings
abstract type AbstractSettings end
struct NoSettings <: AbstractSettings end
struct DynamicSettings <: AbstractSettings
    ## 0/1 array
    indicatorFunction::Array{Int64,1}
    sWhen0::Float64
    sWhen1::Float64
end


