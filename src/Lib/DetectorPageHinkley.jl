# J.Gama book
# p. 41 (el.55) - CUSUM
#
# https://github.com/SmileYuhao/ConceptDrift/blob/master/concept_drift/page_hinkley.py
#

# module PageHinkley

#export detectorPH

include("./TypesHierarchy.jl")
update_mu(x, muOld, nOld) = (nOld * muOld+x)/(nOld+1.0)


"""
lambdaDynamic::Array{Int61,1}: binary vector [1,0,...]
1: indicates for increased sensitivity
0: indicates for optimal static settings
"""
function detectorPH(y::Array{Float64,1},
                    delta::Float64,
                    lambda::Float64;
                    saveOutput=true,
                    adaptiveSettings::AbstractSettings = NoSettings(),
                    wait_time::Int64=10,
                    returnTestStatistic::Bool=false,
                    outCsvTestStatistic="./out/testStat.csv",
                    alpha::Float64=1-0.001)::Array{Int64,1}

    n = length(y)

    if adaptiveSettings != NoSettings()
        # println("Run PH detector with dynamic settings")
        @assert n == length(adaptiveSettings.indicatorFunction)
        lambdaDynamic = adaptiveSettings.indicatorFunction
        lambdaWhenDynamic0 = adaptiveSettings.sWhen0
        lambdaWhenDynamic1 = adaptiveSettings.sWhen1
    end

    runningSum = 0.0 ## running sum
    changeDetected = false
    n_r = 0
    mu_r = 0.0
    s_plus = 0.0
    s_minus = 0.0
    s_vec_plus = zeros(n) ## positive changes
    s_vec_minus = zeros(n) ## negative changes
    detections = []

    for i=1:n
        if adaptiveSettings != NoSettings()
            lambda = lambdaDynamic[i] == 0 ? lambdaWhenDynamic0 : lambdaWhenDynamic1
        end
        x = y[i]
        if n_r == 0
            mu_r = x
            n_r = 1
            s_plus = 0.0
            s_minus = 0.0
        else
            mu_r = update_mu(x, mu_r, n_r)
            n_r += 1
            s_plus  = s_plus  * alpha + (x - mu_r - delta)
            s_minus = s_minus * alpha - (x - mu_r - delta)
            if s_plus > lambda || s_minus > lambda
                changeDetected = true
                n_r = 0
                mu_r = 0.0
                s_plus = 0.0
            else
                changeDetected = false
            end
        end
        s_vec_plus[i] = s_plus
        s_vec_minus[i] = s_minus
        if changeDetected
            # println("Change :", i)
            push!(detections, i)
        end
    end

    if returnTestStatistic
        n1=length(s_vec_minus)
        n2=length(s_vec_plus)
        @assert n1==n2
        n3= length(detections)
        if n3<n1
            cdes=zeros(n1)
            cdes[1:n3]=detections
        else
            cdes=detections
        end
        if saveOutput
            CSV.write(outCsvTestStatistic, DataFrame(sVecMinus=s_vec_minus, sVecPlus=s_vec_plus, cdes=cdes))
            println(".. Save test statistic values into $outCsvTestStatistic")
        end
    end
    detections |> sort
end


function run()
    ## x=vcat(randn(15), randn(15)+10, randn(15)-10);  writedlm("C:/1maslov/tmp/tstsig_small.txt",x)
    ##
    # sig = readdlm("C:/1maslov/tmp/tstsig_small.txt")
    sig = readdlm("C:/1maslov/tmp/tstsig2.txt")
    println(size(sig))
    detectorPH(sig, 0.0, 50)
end
#run()

#end # module
