# ROC for Adwin detector without Kalman / multisensor stuff

include("./Lib/Roc.jl")
include("./Lib/Adwin.jl")
include("./Lib/metadata.jl")

using Metadata: outdir, projectdir
using Distributions
using Roc
using Adwin: adwin, scale01, adwin_extract_changes
using Gadfly, Cairo, Fontconfig 

"For 1D signal"
function run_roc_adwin1()
    n = 100
    insig = vcat(rand(Normal(0.0,  1.0), n),
                 rand(Normal(12.0, 1.0), n),
                 rand(Normal(5.0, 1.0), n),
                 rand(Normal(-1.0, 1.0), n),
                 rand(Normal(7.0, 1.0), n)
                 )
    xs = scale01(insig)
    changes = [100, 200, 300, 400, 500]
    
    tp_rates = Array{Float64,1}()
    fa_rates = Array{Float64,1}()
    max_delay = 10
    n = length(xs)

    h_from = 0.1
    h_to   = 1.0
    h_by   = 0.1 # 100
    paramVals = [0.1 : 0.05 : 1.0;]
    for h in paramVals 
        r, ws = adwin(xs, dlt = h)
        detections = adwin_extract_changes(ws)
        tpr = tpRate(detections, changes, max_delay)
        fpr = faRateScaled(detections, changes, max_delay, n, tn_price_scale = 0.1)
        push!(tp_rates, tpr)
        push!(fa_rates, fpr)
    end
    tp_rates, fa_rates
end

function plotOut(ts, fs, pngfile)
    #p1 = layer(x=fs, y=ts)
    draw(PNG(pngfile, 8inch, 6inch),  plot(x=fs, y=ts))
    println(ts, fs)
end
t,f=run_roc_adwin1()
plotOut(t,f,"3_ROC.PNG")
