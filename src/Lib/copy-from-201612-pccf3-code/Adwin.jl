# "Learning from Time-Changing Data with Adaptive Windowing"
#
# - V.0.2 (14 Dec 2016) : While doing experiments for change detection in multi-sensor settings ./201612-pccf3-code
#
#    Fixed the bug while checking the main condition
#
# - V.0.1 (12 Dec 2016) : While doing experiments for change detection in multi-sensor settings ./201612-pccf3-code
#
#    First implementation of `adwin' to use in multi-sensor settings
#    with Kalman Filter.
#
# https://github.com/abifet/adwin/blob/master/Java/ADWIN.java
#
module Adwin

using Distributions

export scale01, adwin, demo_adwin, adwin_extract_changes

function scale01(sig)
    mx = maximum(sig)
    mn = minimum(sig)
    (sig - mn) / (mx-mn)
end

"Check if the condition is satisfied"
function condForReduce(subsig, delta)
    n = length(subsig)
    @assert n > 1
    for i = 1 : n-1
        s0 = subsig[1:i]
        s1 = subsig[(i+1):n]
        ecut = calcEcut(length(s0), length(s1), delta)
        #ecut = calcEcut(subsig[1:n], length(s0), length(s1), delta)
        m0 = mean(s0)
        m1 = mean(s1)
        if abs(m1-m0) >= ecut
            return true
        end
    end
    return false
end
# function test_cond()
#     println( cond([1, 2;], 1))
#     println( cond([1, 2;], 0.9))
# end
# test_cond()

function recReduce!(w, dlt)
    if length(w) > 1
        if condForReduce(w, dlt)
            deleteat!(w, 1)
            recReduce!(w, dlt)
        end
    else
        return w
    end
end

function test_recReduce()
    function loctest(_v; e = 1.0)
        recReduce!(_v, e)
        println(_v)
    end
    loctest([1.0])
    loctest([1.0, 3.0, 3.1])
    loctest([1.0, 3.0, 3.1, 3.1, 3.1])
    loctest([1.0, 1.0, 1.0, 1.0, 1.0])
    loctest([1.0, 1.0, 1.0, 4.0, 4.0])
end
#test_recReduce()


"Calc. ecut parameter for the partition of W = W0 W1"
function calcEcut(n0, n1, delta)
    @assert n0 >= 1
    @assert n1 >= 1
    m = 1.0/(1.0/n0 + 1.0/n1)
    deltaPrime = delta/(n0+n1)
    ecut = sqrt((1.0/(2.0*m)) * log(4/deltaPrime))
end

"Eq. 3.1"
function calcEcut(w, n0, n1, delta)
    @assert n0 >= 1
    @assert n1 >= 1
    @assert n0 + n1 == length(w)
    sigma = std(w)
    m = 1.0/(1.0/n0 + 1.0/n1)
    deltaPrime = delta/(n0+n1)
    A = sqrt( (2/m) * sigma^2 * log(2/deltaPrime) )
    B = (2.0/(3.0 * m)) * log(2/deltaPrime)
    ecut = A + B
end

function adwin_extract_changes(windowWidths; thr=0)
    n = length(windowWidths)
    @assert n > 1
    cdes = Array{Int64,1}()
    for i=2:n
        #if windowWidths[i] < windowWidths[i-1]
        if windowWidths[i-1] - windowWidths[i] > thr
            push!(cdes, i-1)
        end
    end
    cdes
end

function adwin(sig; dlt=0.1, thr=0)
    ys = scale01(sig)
    n = length(ys)
    w = Array{Float64,1}()
    r = Array{Float64,1}() # return value - mean values of W
    ws = Array{Int64,1}()  # dynamic widths of W
    push!(w, sig[1])
    push!(r, sig[1])
    push!(ws, 1)
    for t=2:n
        push!(w, ys[t])
        recReduce!(w, dlt)
        push!(ws, length(w))
        push!(r, mean(w))
    end
    cdes = adwin_extract_changes(ws, thr=thr)
    return (cdes, r, ws)
end


"read output in inotebook and plot"
function demo_adwin()
    n = 100
    insig = vcat(rand(Normal(0.0,  1.0), n),
                 rand(Normal(12.0, 1.0), n))
    insig = scale01(insig)
    res, ws = adwin(insig, dlt=2.0)
    res, ws, insig
end
#r,w,s=demo_adwin()
#println(w)

end
