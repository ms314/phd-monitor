library(TTR)
library(magrittr)
# Common setting for R-scripts


output_path = "D:/MyTemp/tmp_exp_out/"
cat("metadata.R var > output_path =", output_path, "\n")


project_path = "D:/MyTemp/gitlocal/bayesian-pccf-code/"
cat("metadata.R var > project_path = ", project_path, "\n")



#fpath <- function(f_name, main_d=main_dir){ return (paste(main_d, f_name, sep="")) }
# fpath("sdwdw.txt")
#cat("metadata.R fun > fpath(f_name, main_d=main_dir)", "\n")

run_julia <- function(script_path){
  julia_path = "D:/Install/Julia-0.5.0/bin/julia.exe"
  command = paste(julia_path, script_path)
  system(command)
}
# run_julia("D:/MyTemp/gitlocal/bayesian-pccf-code/src_julia/tst_proof_of_concept.jl")
# renamed: home_dir -> output_dir
# renamed: home_dir -> project_path
cat("metadata.R fun > run_julia(script_path)", "\n")

save_vector <- function(vec, outfile){
  write.table(vec, outfile, row.names = F, col.names = F) #, sep=","
}
cat("metadata.R fun > save_vector(vec, outfile)", "\n")
