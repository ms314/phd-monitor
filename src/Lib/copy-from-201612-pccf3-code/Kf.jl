module Kf
# References 
# - [1] G.Welch "An Introduction To The Kalman Filter"

using Distributions, Gadfly

export klm1d, klm2d

"""
To generate artifical input signal:
d_nu         = Normal(mu, R) 
observations = rand(d_nu, n)
"""
function klm1d(observations; q = 0.01, r = 0.1)
    n         = length(observations)
    xhat      = zeros(n)
    xhatminus = zeros(n)
    A         = 1.0 # prediction 1-d matrix
    Q         = q   # Process variance
    R         = r   # Measurement variance
    #d_w       = Normal(0, Q) # process noise covariance matrix
    Pminus    = zeros(n)
    P         = zeros(n)
    K         = zeros(n)
    # Initial guess
    xhat[1]   = 0.0
    P[1]      = 1.0
    for k = 2:n
        xhatminus[k] = xhat[k-1]
        Pminus[k]    = A * P[k-1] * A' + Q 
        K[k]         = Pminus[k] / (Pminus[k] + R)
        xhat[k]      = xhatminus[k] + K[k]*(observations[k] - xhatminus[k])
        P[k]         = (1-K[k])*Pminus[k]
    end
    xhat
end


"""
observations: [2 by m] matrix, 
2d measurements for every time moment are in columns 
http://www.bzarg.com/p/how-a-kalman-filter-works-in-pictures/ 
- to see dimensions for Q
"""
function klm2d(observations; q = 1.0, q12= 0.0, r=0.005, r12=0.0)
    m         = size(observations)[2]
    n         = size(observations)[1]
    xhat      = zeros(size(observations))
    xhatminus = zeros(size(observations))
    A         = eye(n) # Prediction matrix
    ### q pulls estimates to the mean value between them 
    Q = [q q12; q12 q] # Process variance (2 by 2) (n by n) - wind/slope
    ### ~ When R -> 0 the more we trust the measurement (See [1] p.4)
    R = [r  r12  ;  r12 r] # Measurement variance R;
    P = [1.0 0.0   ;   0.0 1.0]
    Pminus = zeros(n, n)
    # Initial guess
    xhat[:,1] = [0.0, 0.0]
    for k = 2 : m
        xhatminus[:,k] = A * xhat[:,k-1]
        Pminus = A * P * A' + Q
        K = Pminus * inv(Pminus + R)
        xhat[:,k] = xhatminus[:, k] + K * (observations[:, k] - xhatminus[:, k])
        P = (eye(2) - K) * Pminus
    end
    xhat
end

"Identical outputs of one 2D Kalman filter and two 1D filters."
function test1dand2d()
    d = MvNormal([1.0, 10.0], [1 0.0; 0.0 1]);
    observations2d = rand(d, 100);
    klm2dest  = f2dkalman(observations2d     , q = 0.01, r = 0.1);
    klm1dest1 = f1dkalman(observations2d[1,:], q = 0.01, r = 0.1);
    klm1dest2 = f1dkalman(observations2d[2,:], q = 0.01, r = 0.1);
    klm2dest, klm1dest1, klm1dest2, observations2d
end

end
