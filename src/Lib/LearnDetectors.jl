#
# 2018-08-14: based on: step07b/main07.jl
#

# include("../Lib/detectors/PageHinkley.jl")
# include("../Lib/detectors/BayesDetectorReplica.jl")
# include("../Lib/Fractions2.jl")
# include("../Lib/detectors/adwin/Adwin.jl")
# include("../Lib/detectors/adwin/AdwinNaive.jl")
# include("../Lib/Common.jl")#
##include("./detectors/BayesDetectorReplica.jl")
##include("./detectors/adwin/Adwin.jl")
##using PageHinkley, BayesChpOrig, Fractions2, AdwinModule, AdwinNaive
# abstract type AbstractDetector end
# abstract type AdwinDetector <: AbstractDetector end
# abstract type PhDetector <: AbstractDetector end
# abstract type BayesianDetector <: AbstractDetector end

# include("./Fractions2.jl")
# include("./fn_LossFunc.jl")
include("./PerformanceMetrics.jl")
include("./common.jl")
include("./DetectorPageHinkley.jl")
include("./DetectorAdwinNaive.jl")
include("./DetectorBayes.jl")
include("./TypesHierarchy.jl")
# module LearnDetectors

#using PageHinkley, BayesDetector, Fractions2, AdwinNaive, TypesHierarchy
# using DataFrames, CSV

# export
#     optimDetector,
#     createPhDetector,
#     createAdwinDetector,
#     createBayesianDetector,
#     runDetectorWithOptimSettings,
#     demo


""" Returns: CDEs """
function adwinLocal(x, ecut, minLen; adaptiveSettings::AbstractSettings=NoSettings())
    cdes=extractChps(adwinNaive(x, ecut_custom=ecut, min_len=minLen, adaptiveSettings=adaptiveSettings))
    cdes |> sort 
end


""" Returns: CDEs """
function bayesianLocal(x, lambda, mu0; adaptiveSettings=NoSettings(), gaussPdf=false, gaussSigma=1.0)
    ##maxes, mat = bayes_detector(x, lambda = lambda, mu0=mu0)
    #maxes, mat = bayesDetectorNew(x, lambda=lambda, mu0=mu0, adaptiveSettings=adaptiveSettings)
    #maxes, mat = bayesDetector(x, lambda=lambda, gaussPdf=gaussPdf, gaussSigma=gaussSigma, mu0=mu0, adaptiveSettings=adaptiveSettings)
    maxes, mat = bayesDetector(x, lambda=lambda, mu0=mu0, adaptiveSettings=adaptiveSettings)
    # , gaussPdf=true, gaussSigma=sigma, mu0=0.0
    ##cdes = extract_changes(maxes)
    cdes = extractChangesBayes(maxes)
    cdes |> sort
end

#
##
### START: IMPLEMENT CLOSURE FUNCTION FOR DETECTORS SO THAT IT ACCEPTS (X:SIGNAL, S: SENSITIVITY) FOR USING IN OPTIMISATION FUNCTIONS
##
#
""" Returns: CDEs """
function createPhDetector(;delta=0.0)
    f = function(x, sensitivity)
        return detectorPH(x, delta, sensitivity)
    end
    f
end
function createPhDetector(;delta=0.0,
                          adaptiveSettings::AbstractSettings=NoSettings(),
                          returnTestStatistic=false)
    f = function(x, sensitivity)
        return detectorPH(x, delta, sensitivity,
                          adaptiveSettings=adaptiveSettings,
                          returnTestStatistic=returnTestStatistic)
    end
    f
end
function createPhDetectorDynamic(;delta=0.0,
                                 indicatorFunction::Array{Int64,1},
                                 sWhen0::Float64=999.,
                                 returnTestStatistic=false)
    f = function(x, sensitivity)
        adaptiveSettingsLocal = DynamicSettings(indicatorFunction,sWhen0,sensitivity)
        return detectorPH(x, delta, sensitivity,
                          adaptiveSettings=adaptiveSettingsLocal,
                          returnTestStatistic=returnTestStatistic)
    end
    f
end


function createAdwinDetector(;min_len=10,
                             adaptiveSettings::AbstractSettings=NoSettings())
    f = function(x, ecut)
        return adwinLocal(x, ecut, min_len, adaptiveSettings=adaptiveSettings)
    end
    f
end
function createAdwinDetectorDynamic(;min_len=10,
                                    indicatorFunction::Array{Int64,1},
                                    ecutWhen0::Float64=999.)
    f = function(x, ecut)
        adaptiveSettingsLocal = DynamicSettings(indicatorFunction, ecutWhen0, ecut)
        return adwinLocal(x, ecut, min_len, adaptiveSettings=adaptiveSettingsLocal)
    end
    f
end


function createBayesianDetector(; mu0=0.0, 
                                  gaussPdf=false, 
                                  gaussSigma=1.0,
                                  adaptiveSettings::AbstractSettings=NoSettings())
    f = function(x, lambda)
        #return bayesianLocal(x, lambda, mu0, adaptiveSettings=adaptiveSettings, gaussSigma=gaussSigma, gaussPdf=gaussPdf)
        return bayesianLocal(x, lambda, mu0, adaptiveSettings=adaptiveSettings)
    end
    f
end
function createBayesianDetectorDynamic(;mu0=0.0,
                                       gaussPdf=true, gaussSigma=1.0,
                                       indicatorFunction::Array{Int64,1},
                                       lambdaWhen0::Float64=999.)
    f = function(x, lambda)
        adaptiveSettingsLocal = DynamicSettings(indicatorFunction, lambdaWhen0, lambda)
        # return bayesianLocal(x, lambda, mu0, adaptiveSettings = adaptiveSettingsLocal, gaussSigma=gaussSigma, gaussPdf=gaussPdf)
        return bayesianLocal(x, lambda, mu0, adaptiveSettings = adaptiveSettingsLocal)
    end
    f
end
#
##
### END: IMPLEMENT CLOSURE FUNCTION FOR DETECTORS SO THAT IT ACCEPTS (X:SIGNAL, S: SENSITIVITY) FOR USING IN OPTIMISATION FUNCTIONS
##
#


"""
Return (sensitivity value, loss value)
"""
##function findOptimSettings(resultMatrix::Array{Float64,2})
function findOptimSettings(; sensVec=[1.0], lossVec=[0.01])
    ##sensVec = resultMatrix[:,3]
    ##lossVec = resultMatrix[:,8]
    n = length(sensVec)
    @assert n == length(lossVec)
    @assert n > 0
    k = 1
    best_so_far_loss = lossVec[k]
    for i=1:n
        if lossVec[i] < best_so_far_loss
            best_so_far_loss = lossVec[i]
            k = i
        end
    end
    return (sensVec[k], best_so_far_loss, k)
end


"""
Output CSV with:
  - Sensitivity, Delay, FA rate, Loss function value
  - Optimal sensitivity value and corresponding Loss
"""
function optimDetector(x::Array{Float64,1};
                       changes::Array{Int64,1}=[1],
                       detector=createPhDetector(),
                       paramrange = 1:3,
                       name="Detector name",
                       printprogress=false)

    delays_v        = []
    fa_rate_v       = []
    loss_v          = []
    sensetivity_vec = []
    num_of_cdes     = []
    N               = length(x)

    k=length(paramrange)
    # Sensitivity, Delay, faRate. Loss, NumOfCdes
    cdesMatrix = zeros(k, N+5) 

    
    counter = 0

    lenOfSig=length(x)

    for sensitivity in paramrange
        counter += 1
        progress = counter/k

        if printprogress
            if counter % Int(floor(k/10.0)) == 0
                println("..progress ($name) :", progress*100, " %")
            end
        end

        cdes = detector(x, sensitivity)

        if length(cdes) > 0
            delays = fnDelays(changes, cdes, N)
            probFa = numberOfFalseAlarms(changes, cdes, N)/N
        else
            delays=[N]
            probFa=0.0
        end

        delay = length(delays) == 1 ? delays[1] : mean(delays)
        ##loss = lossFuncGeneral(delay, probFa, length(changes), length(cdes))
        # loss = lossFuncGeneral(delay, lenOfSig=lenOfSig, probFa, nchps=length(changes), ncdes=length(cdes), scaleDelayMinMax=scaleDelayMinMax, scaleNcdesMinMax=scaleNcdesMinMax)
        loss = lossFuncGeneral(delay, lenOfSig=lenOfSig, probFa, nchps=length(changes), ncdes=length(cdes))

        # if name=="Bayes" println(cdes, loss) end

        push!(num_of_cdes    , length(cdes))
        push!(delays_v       , delay)
        push!(fa_rate_v      , probFa)
        push!(loss_v         , loss)
        push!(sensetivity_vec, sensitivity)

        # Sensitivity, Delay, faRate. Loss, NumOfCdes
        cdesMatrix[counter, 1] = sensitivity
        cdesMatrix[counter, 2] = delay
        cdesMatrix[counter, 3] = probFa
        cdesMatrix[counter, 4] = loss
        cdesMatrix[counter, 5] = length(cdes) - length(changes)

        for k=5:length(cdes)
            cdesMatrix[counter, k+1] = cdes[k]
        end 
    end

    chpsLength = ones(length(delays_v)) * length(changes)
    # println("End")

    r = DataFrame(delays      = delays_v,
                  faRate      = fa_rate_v,
                  sensitivity = sensetivity_vec,
                  numOfCdes   = num_of_cdes,
                  chpsLength  = chpsLength,
                  loss_v      = loss_v)

    optimSettings = findOptimSettings(sensVec = sensetivity_vec, lossVec = loss_v)

    best_s       = optimSettings[1]
    best_l       = optimSettings[2]
    best_k_index = optimSettings[3]
    best_fa_rate = fa_rate_v[best_k_index]
    best_delay   = delays_v[best_k_index]

    if false
        println("Detector name: " * name)
        println("Optimal settings are: sensitivity=$best_s, loss=$best_l, avgDelay=$best_delay, faRate=$best_fa_rate")
    end
    return(r, optimSettings, cdesMatrix)
end

function optimDetector(x::Array{Float64,1},
                       outcsv::String,
                       outcsvOptSet::String;
                       outPerfMatrix="./perfMatrix.dat",
                       changes::Array{Int64,1}=[1],
                       detector=createPhDetector(),
                       paramrange = 0.1:0.1:10,
                       name="Detector name",
                       printprogress=true)
                       #scaleDelayMinMax=3., scaleNcdesMinMax=15.)

    rDF, opt,perfM = optimDetector(x,
                                   changes=changes,
                                   detector=detector,
                                   paramrange=paramrange,
                                   name=name,
                                   printprogress=printprogress)
                             #scaleDelayMinMax=scaleDelayMinMax, scaleNcdesMinMax=scaleNcdesMinMax)


    CSV.write(outcsv      , rDF)
    CSV.write(outcsvOptSet, DataFrame(sensitivity=opt[1], loss=opt[2], k=opt[3]))
    open(outPerfMatrix, "w") do io writedlm(io, perfM, ',') end
    println("Save perf, matrix into " * outPerfMatrix)
    println("Save results into " * outcsv)
    println("Save results into " * outcsvOptSet)
    println(">>> PerfM size = ", size(perfM))
end

struct OptimSettings
    sensitivity::Float64
    l::Float64
    k::Int64
    OptimSettings(x,y,z) = new(x,y,z)
    OptimSettings(x::Float64) = new(x, 0.0, 0)
end

function runDetectorWithSettings(x::Array{Float64,1};
                                 detector=createPhDetector(),
                                 optimSettings::OptimSettings = OptimSettings(.1, .1, 1),
                                 outcsv::String="./cdes.csv",
                                 name="Detector name")

    best_s       = optimSettings.sensitivity
    best_l       = optimSettings.l
    best_k_index = optimSettings.k

    # cdes = detector(x, best_s, returnTestStatistic=returnTestStatistic)
    cdes = detector(x, best_s)
    CSV.write(outcsv, DataFrame(cdes=cdes))
    println("Save results into: " * outcsv)
end

function demo()
    println("Run demo")
    sig=vcat(randn(100), randn(100)+1.0, randn(100))
    r,opt1 = optimDetector(sig, changes=[500], detector = createPhDetector(), name="PH")
    r,opt2 = optimDetector(sig, changes=[500], detector = createAdwinDetector(), name="Adwin")
    r,opt3 = optimDetector(sig, changes=[500], detector = createBayesianDetector(), name="Bayesian")
    #
    runDetectorWithOptimSettings(sig, detector = createPhDetector(), optimSettings = opt1, name="PH")
    runDetectorWithOptimSettings(sig, detector = createAdwinDetector(), optimSettings = opt2, name="Adwin")
    runDetectorWithOptimSettings(sig, detector = createBayesianDetector(), optimSettings = opt3, name="Bayesian")

    println("Stop")
end
#demo()

# end # module
