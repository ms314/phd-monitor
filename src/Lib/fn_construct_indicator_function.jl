
"""
n: signal length
chps: changes location
"""
function constructIndicatorFunction(n::Int64,
                                    chps::Array{Int64,1},
                                    preferredWidth::Int64)
end

function constructIndicatorFunction(n::Int64, chp::Int64, preferredWidth::Int64)::Array{Int64,1}
    pccf::Array{Int64,1} = zeros(n)
    w = convert(Int, floor(preferredWidth/2))
    @assert chp < n
    @assert chp-w > 0
    @assert chp+w < n
    pccf[(chp-w):(chp+w)].=1
    pccf
end
# constructIndicatorFunction(100, [1, 10, 12, 50])
#constructIndicatorFunction(100, 50, 10)
