(ns clj-cost-function.core
  (:gen-class))

;;; Core functions:
;;; (avg-delay [changes detections n])
;;; (fa-rate [changes detections n])

(defn mean [v] (/ (apply (+) v) (count v)))

(defn cdes-between-t-moments [t1 t2 detections]
  (filter (fn [x] (and (>= x t1) (< x t2))) detections))

(defn delay-last-cde [cde detections n]
  (let [cdes-between (cdes-between-t-moments cde n detections)]
    (if (empty? cdes-between)
      (- n cde)
      (- (first cdes-between) cde))))

(defn delay-two-cdes [cde1 cde2 detections n]
  (let [cdes-between (cdes-between-t-moments cde1 cde2 detections)]
    (if (empty? cdes-between)
      (- n cde1)
      (- (first cdes-between) cde1))))

(defn delays [changes detections n]
  (if (empty? detections)
        (map (fn [x] (- n x) ) changes)
        (loop [r changes acc '()]
          (let [[c1 c2 & l] r]
            (if (nil? c2)
              (reverse (cons (delay-last-cde c1 detections n) acc))
              (recur (rest r) (cons (delay-two-cdes c1 c2 detections n) acc)))))))

(defn avg-delay [changes detections n] (mean (delays changes detections n)))

(defn number-of-false-alarms [changes detections n]
  (if (empty? detections)
    0
    (let [k0 (count (filter (fn [x] (< x (first changes))) detections))]
      (loop [r changes acc 0]
        (if (empty? r)
          (+ acc k0)
          (let [[c1 c2 & l] r]
            (if (nil? c2)

              (let [cdes-between (cdes-between-t-moments c1 n detections)]
                (let [k (count cdes-between)]
                  (if (> k 1)
                    (recur (rest r) (+ acc (- k 1)))
                    (recur (rest r) acc))))

              (let [cdes-between (cdes-between-t-moments c1 c2 detections)]
                (let [k (count cdes-between)]
                  (if (> k 1)
                    (recur (rest r) (+ acc (- k 1)))
                    (recur (rest r) acc)))))))))))

(defn fa-rate [changes detections n] (/ (number-of-false-alarms changes detections n) n))

(defn -main [& args]
  ;; (println (cdes-between-changes 10 20 [11]))
  (println (delays [10 20 30] [] 100))
  (println (delays [10 20 30] [11] 100))
  (println (delays [10 20 30] [5 15 16 17] 100))
  (println (number-of-false-alarms [10 20 30] [1] 100))
  (println (number-of-false-alarms [10 20 30] [1 11 12] 100))
)
