#
# Loss function. lossfunction
# Input metrics are calculated using Fractions.jl
# Ported from fn_LossFunc.R
#
# 2018/11/09: Mergid this file functionality into : PerformanceMetrics.jl
#

# # D : delay
# function lossFuncGeneral(D,
#                          faRate,
#                          Nchps,
#                          Ncdes;
#                          alpha=0.5)

#     # println("lossFuncGeneral: avg(delay)=$D, faRate=$faRate, Nchps=$Nchps, Ncdes=$Ncdes")
#     (1-alpha)*faRate + alpha*(1.0-faRate) * D + abs(Nchps - Ncdes)

# end

