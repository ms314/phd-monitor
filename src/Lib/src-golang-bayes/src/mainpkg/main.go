package main

import (
	"fmt"
	"math"

	"github.com/montanaflynn/stats"
)

func main() {
	// chps := []int{10, 20, 30}
	// cdes := []int{11, 26, 30}
	// r2, _ := perfmetrics.Delays(chps, cdes, 100)
	// fmt.Println(r2)
	// detectorbayes.GaussPdf(1.0, 1.0, 1.0)
	sig := []float64{1.0, 1.0, 1.0, 2.0, 2.0, 2.0}
	// rb := bayesDetector(sig, 10., 0.0, 0.1)
	rm := runningMeanFromRight(sig)
	p := fmt.Println
	// fmt.Println(rb)
	p(rm)
	fmt.Printf("%0.7f\n", std([]float64{1.0, 2.0}))
	fmt.Println(runningMeanFromRight([]float64{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0}))
}

// Standard deviation of a vector
func std(v []float64) float64 {
	r, _ := stats.StandardDeviationSample(v)
	return r
}

// Running mean from right.
// (1 to 9).toList.reverse.map{var s = 0; var n=0; x => {s+=x; n+=1; s/n.toDouble}}
func runningMeanFromRight(xs []float64) []float64 {
	n := len(xs)
	rmean := []float64{}
	if n == 1 {
		return xs
	}
	for k := n - 1; k >= 0; k-- {
		if len(rmean) > 0 {
			rmean = append(rmean, xs[k]+rmean[len(rmean)-1])
		} else {
			rmean = append(rmean, xs[k])
		}
	}
	for k := 0; k < n; k++ {
		rmean[k] = rmean[k] / float64(k+1)
	}
	return rmean
}

func gaussPdf(x, m, s float64) float64 {
	return 1.0 / math.Sqrt(2*math.Pi*s*s) * math.Exp(-math.Pow(x-m, 2)/(2*s*s))
}

func bayesDetector(y []float64, lambda, mu0, gaussSigma float64) [][]float64 {
	T := len(y)
	R := make([][]float64, T+1)
	for i := 0; i < T+1; i++ {
		R[i] = make([]float64, T+1)
	}
	return R
}

// func make2d(m, n int) [][]int {
// 	a := make([][]int, m)
// 	for i := 0; i < m; i++ {
// 		a[i] = make([]int, n)
// 	}
// 	return (a)
// }

// func addder() func(int) int {
// 	init := 1
// 	return func(x int) int {
// 		return (init + x)
// 	}
// }
