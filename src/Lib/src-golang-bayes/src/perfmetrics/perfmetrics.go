package perfmetrics

import (
	"fmt"
	"errors"
	// "math/rand"
	// "gonum.org/v1/gonum"
)


func Delays(changes, detections []int, n int) ([]int, error) {
	nc := len(changes)
	nd := len(detections)
	var delays [] int
	fmt.Printf("len(changes)=%d, len(detections)=%d\n", nc, nd)
	
	if nc==0 {
		return []int{}, errors.New("Changes are empty")
	}
	
	if nd==0 {
		for _, v := range changes {
			delays = append(delays, n-v)
		}
		
	} else {
		for _, v := range changes {
			delays = append(delays, delayForChp(v, detections, n))
		}
	}
	return delays, nil
}


func delayForChp(chp int, detections []int, n int) int {
	var d int
	found := false
	for _, e := range detections {
		if e >= chp {
			d = e-chp
			found=true
			break
		}
	}
	if found {
		return d
	} else {
		return n - chp
	}
	
}

