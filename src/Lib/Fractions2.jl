#
# Based on  clj-cost-function/core.jl
#
# 2018/11/09: Mergid this file functionality into : PerformanceMetrics.jl
#
# using Statistics

# function cdesBetweenMoments(t1::Int64, t2::Int64, detections::Array{Int64,1})
#     r = filter(x -> (x >= t1) && (x < t2), detections)
# end

# function delayLastCde(cde::Int64, detections::Array{Int64,1}, n::Int64)
#     cdesBetween = cdesBetweenMoments(cde, n, detections)
#     if length(cdesBetween) == 0
#         return(n-cde)
#     else
#         return(cdesBetween[1] - cde)
#     end
# end

# function delayTwoCdes(cde1::Int64, cde2::Int64, detections::Array{Int64,1}, n::Int64)
#     cdesBetween = cdesBetweenMoments(cde1, cde2, detections)
#     if length(cdesBetween) == 0
#         return(n-cde1)
#     else
#         return(cdesBetween[1] - cde1)
#     end
# end

# function fnDelays(changes::Array{Int64,1}, detections::Array{Int64,1}, n::Int64)
#     if length(detections) == 0
#         return(map(x -> n-x, changes))
#     else
#         n_changes  = length(changes)
#         acc = []
#         if n_changes == 1
#             d = delayLastCde(changes[1], detections, n)
#             push!(acc,d)
#             acc
#         else
#             for k = 1:n_changes
#                 if k == n_changes
#                     d = delayLastCde(changes[n_changes], detections, n)
#                     push!(acc,d)
#                 else
#                     d = delayTwoCdes(changes[k], changes[k+1], detections, n)
#                     push!(acc,d)
#                 end
#             end
#         end
#         acc
#     end
# end


# function avgDelay(changes::Array{Int64,1}, detections::Array{Int64,1}, n::Int64)
#     fnd = fnDelays(changes, detections, n)
#     println("fnd=$fnd")
#     mean(fnd)
# end


# function numberOfFalseAlarms(changes::Array{Int64,1},
#                              detections::Array{Int64,1},
#                              n::Int64)::Int64
#     n_cdes = length(detections)
#     n_changes = length(changes)
#     if n_cdes == 0
#         return(0)
#     else
#         k0 = length([e for e in detections if e < changes[1]])
#         acc = k0
#         for i = 1 : n_changes
#             if i == n_changes
#                 cdes_between = cdesBetweenMoments(changes[i], n, detections)
#             else
#                 cdes_between = cdesBetweenMoments(changes[i], changes[i+1], detections)
#                 # println("cdes between ",changes[i]," and ",changes[i+1],":", cdes_between)
#             end
#             k = length(cdes_between)
#             if k > 1
#                 acc += k-1
#             end

#         end
#         return(acc)
#     end
# end

# function run()
#     # println(delays([10,20,30], Int[], 100))
#     # println(delays([10,20,30], [11], 100))
#     # println(delays([10,20,30], [5, 15, 16, 17], 100))
#     println(numberOfFalseAlarms([10,20,30], [1], 100))
#     println(numberOfFalseAlarms([10,20,30], [1,11,12], 100))
# end

