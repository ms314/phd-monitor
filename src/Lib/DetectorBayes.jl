#
# 2018-08-28: New module with u to date functionality.
#             One function for static and dynamic detector.
#
# https://github.com/av-maslov/change-detection/blob/master/downloaded/BayesianOnlineChangepoint/gaussdemo_orig.m

using Statistics, SpecialFunctions
# using Distributions

function constant_hazard(r, lambda::Float64)
    @assert lambda >= 1.
    (1.0 / lambda) .* ones(size(r))
end

"""Non-standardized Student's t-distribution"""
## As in the Matlab code by author:
# c = exp(gammaln(nu/2 + 0.5) - gammaln(nu/2)) .* (nu.*pi.*var).^(-0.5);
# p = c .* (1 + (1./(nu.*var)).*(x-mu).^2).^(-(nu+1)/2);
## Check Eq.110: ~\cite{murphy2007conjugate}
function studentpdf(x  ::Float64,
                    mu ::Array{Float64,1},
                    var::Array{Float64,1},
                    nu ::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (nu .* pi .* var) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ (nu .* var)) .* (x .- mu).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else
        p
    end
end


"""Don't use before check again. My version: possibly a fixed typo"""
function studentpdfmine(x::Float64,
                        mu::Array{Float64,1},
                        sigma::Array{Float64,1},
                        nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (pi .* nu) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ nu ) .* ((x .- mu)./sigma).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else
        p
    end
end


"Function to extract changepoints locations from the bayes_detector() output"
function extractChangesBayes(seq)::Array{Int64,1}
    #
    # seq[j] contains current estimate
    # of the location of the last change at the time moment j
    #
    n = length(seq)
    chps = zeros(n)
    for i in 1:n
        chps[i] = i - seq[i]
    end
    out = unique(chps)
    out = map(x->convert(Int,x), out)
    if length(out) > 1
        return out[1:end-1]
    else
        return out
    end
end

########################
### Start: Gauss Pdf ###
########################
# function invertedRunningMean(xs::Array{Float64,1})
#     # vcat([xs[end]], cumsum(reverse(xs)) ./ cumsum(ones(length(xs))))
#     cumsum(reverse(xs)) ./ cumsum(ones(length(xs)))
# end
# function estimate_mus(x)
#   n = length(x)
#   mus = Float64[]
#   for i = 1:(n-1)
#     subvec = x[(n-i) : end]
#     push!(mus, mean(subvec))
#   end
#   #if n == 1
#   #  push!(mus, x[1])
#   #end
#   mus
# end
# | using Distributions: | function gauss_pdf(ms, val, sigma = 1)
# | using Distributions: |   res = Float64[]
# | using Distributions: |   for m in ms
# | using Distributions: |     d = Normal(m, sigma)
# | using Distributions: |     pdfgauss = pdf(d, val)
# | using Distributions: |     push!(res, pdfgauss)
# | using Distributions: |   end
# | using Distributions: |   res
# | using Distributions: | end
gpdf(x::Float64, m::Float64, s::Float64) = (1.0/sqrt(2*pi*s^2)) * exp(-(x-m)^2/(2*s^2))
function gpdf(x::Float64, m::Array{Float64}, s::Array{Float64})::Array{Float64}
    map((m_val, s_val)-> gpdf(x, m_val, s_val), m, s)
end
function gpdf(x::Float64, m::Array{Float64}, s::Float64)::Array{Float64}
    map((m_val, s_val)-> gpdf(x, m_val, s_val), m, repeat([s], length(m)))
end
function gauss_pdf(ms::Array{Float64}, val::Float64, sigma::Float64=1.)
  gpdf(val, ms, sigma)
end
######################
### End: Gauss Pdf ###
######################

"""

 Main version of the bayesiandetector (starting from 2019-02-07)
 after fixing performance by adding Gaussian predictive
 distribution and by reversing the way a set of possible mu
 values is calculated.

    - 2019-02-12: Fixed big Error: in all previous versions betaT
      was updated wrongly!

    - 2019-02-07: Added Gaussain predictive distribution
      option. Seems to be outperforming Student
      distribution. Convergence with original MAtlab code is
      achieved.

"""
function bayesDetector(y::Array{Float64,1};
                       adaptiveSettings::AbstractSettings = NoSettings(),
                       lambda::Float64=200.0,
                       mu0::Float64=0.0,
                       gaussPdf::Bool=false, 
                       gaussSigma::Float64=1.0)
    T = length(y)
    @assert lambda >= 1.
    if adaptiveSettings != NoSettings()
        @assert T == length(adaptiveSettings.indicatorFunction)
        lambdaDynamic = adaptiveSettings.indicatorFunction
        lambdaWhenDynamic0 = adaptiveSettings.sWhen0
        lambdaWhenDynamic1 = adaptiveSettings.sWhen1
    end
    kappa0::Float64          = 1.
    alpha0::Float64          = 1. ## nu
    beta0 ::Float64          = 1.
    CP                       = [0]
    R                        = zeros(T+1, T+1)
    R[1, 1]                  = 1
    muT   ::Array{Float64,1} = [mu0]
    kappaT::Array{Float64,1} = [kappa0]
    alphaT::Array{Float64,1} = [alpha0]
    betaT ::Array{Float64,1} = [beta0]
    maxes                    = zeros(T+1)
    maxes_probs              = zeros(T+1)
    for t=1:T-1+1
        @assert typeof(y[t]) == Float64
        if gaussPdf
            subv = vcat(mu0, y[1:(t-1)])
            mus = cumsum(reverse(subv)) ./ cumsum(ones(length(subv)))
            predprobs = gauss_pdf(mus, y[t], gaussSigma)
        else
            ## (x, mu, sigma, nu)
            predprobs = studentpdf(y[t], muT, betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT), 2.0 .* alphaT)
        end
        if adaptiveSettings != NoSettings()
            lambda = lambdaDynamic[t] == 0 ? lambdaWhenDynamic0 : lambdaWhenDynamic1
        end
        R[2:t+1, t+1] = R[1:t, t] .* predprobs .* (1.0 .- lambda^-1)
        # Probability of a changepoint
        R[1, t+1] = sum(R[1:t, t] .* predprobs .* lambda^-1)
        # Normalize
        R[:, t+1] = R[:, t+1] / sum(R[:, t+1])
        if ! gaussPdf
            muT0    = vcat(mu0 , (kappaT .* muT .+ y[t]) ./ (kappaT .+ 1.0))
            kappaT0 = vcat(kappa0, kappaT .+ 1.0)
            alphaT0 = vcat(alpha0, alphaT .+ 0.5)
            ### !!! IN ALL PREV. VERSIONS betaT WAS UPDATED WRONGLY
            betaT0  = vcat(beta0, kappaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
            muT    = muT0
            kappaT = kappaT0
            alphaT = alphaT0
            betaT  = betaT0        
        end
        maxes[t] = findmax(R[:,t])[2]
        maxes_probs[t] = findmax(R[:,t])[1]
    end
    maxes = map(x -> convert(Int, x), maxes)
    maxes, R
end