module TypesHierarchy
export AbstractSettings, NoSettings, DynamicSettings

abstract type AbstractSettings end
struct NoSettings <: AbstractSettings end
struct DynamicSettings <: AbstractSettings
    ## 0/1 array
    indicatorFunction::Array{Int64,1}
    sWhen0::Float64
    sWhen1::Float64
end
end

module Detectors

using TypesHierarchy

include("./DetectorPageHinkley.jl")
include("./DetectorAdwinNaive.jl")
include("./DetectorBayes.jl")

export detectorPH
export bayesDetectorNew, extractChangesBayes
export adwinNaive, extractChps
end


module Perfromance
include("./Fractions2.jl")
export fnDelays, avgDelay, numberOfFalseAlarms
end 

module LearnDetectors
include("./Fractions2.jl")
include("./Common.jl")
include("./DetectorPageHinkley.jl")
include("./DetectorAdwinNaive.jl")
include("./DetectorBayes.jl")
##include("./TypesHierarchy.jl")
include("./fn_LossFunc.jl")
include("./LearnDetectors.jl")

using DataFrames, CSV
using Perfromance, Detectors

export
    optimDetector,
    createPhDetector,
    createAdwinDetector,
    createBayesianDetector,
    runDetectorWithSettings,
    demo
end



##module Lib

include("./Fractions2.jl")
include("./Common.jl")
include("./TypesHierarchy.jl")

using Distributions
# export adwinNaive, extractChps, demo_adwin_naive
# export bayesDetectorNew, extractChangesBayes
# export detectorPH


##end
