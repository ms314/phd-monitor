fn_plotAllDataAndChanges <- function() {
    datall <- read_changes_and_signals()
    png("/home/ms314/gitlocal/phd-monitor/src/step14-maybe-scala-lib/allSig.PNG", width = 640, res =100, height = 800)
    par(mfrow=c(6,1))
    for (k in 1:6) {
        par(mar=c(1,1,2,1))
        y <- datall[[paste0("sig", toString(k))]]
        plot(y, ann = FALSE, xaxt ="n", yaxt = "n")
        mtext(paste0("Sig.", toString(k)), side=3, line=0.5)
        abline(v=datall[[paste0("chps", toString(k))]])
    }
    dev.off()
}

#fn_plotAllDataAndChanges()
