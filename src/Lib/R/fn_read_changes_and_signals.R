library(jsonlite)

## s1=fromJSON("../../config_datasets.json")

read_changes_and_signals <- function(configFile="../config_datasets.json"){
    cat("... Adjust number of columns in data.frames manually if necessary")
    s = fromJSON(configFile)

    y1 <- read.table(paste0(s$dir, s$sig1))$V1
    y2 <- read.table(paste0(s$dir, s$sig2))$V1
    y3 <- read.table(paste0(s$dir, s$sig3))$V1
    y4 <- read.table(paste0(s$dir, s$sig4))$V1
    y5 <- read.table(paste0(s$dir, s$sig5))$V1
    y6 <- read.table(paste0(s$dir, s$sig6))$V1

    c1   <- read.table(paste0(s$dir, s$chps1))$V1
    c1dp <- read.table(paste0(s$dir, s$chps1dp))$V1 # from scala-dp-detector
    c2 <- read.table(paste0(s$dir, s$chps2))$V1
    c3 <- read.table(paste0(s$dir, s$chps3))$V1
    c4 <- read.table(paste0(s$dir, s$chps4))$V1
    c5 <- read.table(paste0(s$dir, s$chps5))$V1
    c6 <- read.table(paste0(s$dir, s$chps6))$V1

    signals <- list(sig1=y1, sig2=y2, sig3=y3, sig4=y4, sig5=y5, sig6=y6)
    changes <- list(chps1=c1, chps1dp=c1dp, chps2=c2, chps3=c3, chps4=c4, chps5=c5, chps6=c6)

    return(c(signals, changes))
}

if (1==1) {
    configFile="../config_datasets.json"
    cat("Read data rigth into env. variables\n")
    s = fromJSON(configFile)

    sig1 <- read.table(paste0(s$dir, s$sig1))$V1
    sig2 <- read.table(paste0(s$dir, s$sig2))$V1
    sig3 <- read.table(paste0(s$dir, s$sig3))$V1
    sig4 <- read.table(paste0(s$dir, s$sig4))$V1
    sig5 <- read.table(paste0(s$dir, s$sig5))$V1
    sig6 <- read.table(paste0(s$dir, s$sig6))$V1

    chps1   <- read.table(paste0(s$dir, s$chps1))$V1
    chps1dp <- read.table(paste0(s$dir, s$chps1dp))$V1 # from scala-dp-detector
    chps2 <- read.table(paste0(s$dir, s$chps2))$V1
    chps3 <- read.table(paste0(s$dir, s$chps3))$V1
    chps4 <- read.table(paste0(s$dir, s$chps4))$V1
    chps5 <- read.table(paste0(s$dir, s$chps5))$V1
    chps6 <- read.table(paste0(s$dir, s$chps6))$V1
    cat("\nEnd\n")
} 
