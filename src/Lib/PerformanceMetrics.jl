#
# 2018/11/09: merge of fn_LossFunc.jl and Fractions2.jl
#
# include("../Lib/Fractions2.jl")
# include("../Lib/fn_LossFunc.jl")

using Statistics

function cdesBetweenMoments(t1::Int64, t2::Int64, detections::Array{Int64,1})
    r = filter(x -> (x >= t1) && (x < t2), detections)
end

function delayLastCde(cde::Int64, detections::Array{Int64,1}, n::Int64)
    cdesBetween = cdesBetweenMoments(cde, n, detections)
    if length(cdesBetween) == 0
        return(n-cde)
    else
        return(cdesBetween[1] - cde)
    end
end

function delayTwoCdes(cde1::Int64, cde2::Int64, detections::Array{Int64,1}, n::Int64)
    cdesBetween = cdesBetweenMoments(cde1, cde2, detections)
    if length(cdesBetween) == 0
        return(n-cde1)
    else
        return(cdesBetween[1] - cde1)
    end
end

function fnDelays(changes::Array{Int64,1}, detections::Array{Int64,1}, n::Int64)::Array{Int64,1}
    if length(detections) == 0
        return(map(x -> n-x, changes))
    else
        n_changes  = length(changes)
        acc = []
        if n_changes == 1
            d = delayLastCde(changes[1], detections, n)
            push!(acc,d)
            acc
        else
            for k = 1:n_changes
                if k == n_changes
                    d = delayLastCde(changes[n_changes], detections, n)
                    push!(acc,d)
                else
                    d = delayTwoCdes(changes[k], changes[k+1], detections, n)
                    push!(acc,d)
                end
            end
        end
        acc
    end
end


function avgDelay(changes::Array{Int64,1}, detections::Array{Int64,1}, n::Int64)
    fnd = fnDelays(changes, detections, n)
    mean(fnd)
end


function numberOfFalseAlarms(changes::Array{Int64,1}, detections::Array{Int64,1}, n::Int64)::Int64
    n_cdes = length(detections)
    n_changes = length(changes)
    if n_cdes == 0
        return(0)
    else
        k0 = length([e for e in detections if e < changes[1]])
        acc = k0
        for i = 1 : n_changes
            if i == n_changes
                cdes_between = cdesBetweenMoments(changes[i], n, detections)
            else
                cdes_between = cdesBetweenMoments(changes[i], changes[i+1], detections)
                # println("cdes between ",changes[i]," and ",changes[i+1],":", cdes_between)
            end
            k = length(cdes_between)
            if k > 1
                acc += k-1
            end

        end
        return(acc)
    end
end

function sigmax(x; scale::Float64=1.0)
    1.0 / (1.0 + exp(-x/scale))
end

"""
scaleNcdes: the smaller value is (in comparison to scaleDelay) the more penalty is on the number od CDEs
"""
function lossFuncGeneral(delay,
                         faRate;
                         lenOfSig=200.,
                         nchps=1,
                         ncdes=10,
                         alpha = 0.5,
                         betta = 0.5,
                         # scaleDelayMinMax=1.,
                         # scaleNcdesMinMax=1.
                         ## , scaleDelayMinMax=scaleDelayMinMax, scaleNcdesMinMax=scaleNcdesMinMax
                         #,scaleDelay=5., scaleNcdes=1.
                         )

    ## paper text version:
    # (1-alpha)*faRate + alpha*(1.0-faRate) * D + abs(Nchps - Ncdes)
    ## Note:  farate ~ abs(nchp-ncde)
    #faRate + (1.0-faRate) *
    ### v.0.2
    ##alpha * sigmax(delay, scale=scaleDelay) + (1.0-alpha) * sigmax(abs(nchps - ncdes), scale=scaleNcdes)  ## what about number of false alarms?
    ### v.0.3 (2018/11/12): add log
    ##alpha * sigmax(log(delay), scale=1.) + (1.0-alpha) * sigmax(log(abs(nchps - ncdes)), scale=1.)  
    ### v.0.4 (2018/11/12): min-max normalization
    #alpha * delay/scaleDelayMinMax + (1.0-alpha) * abs(ncdes-nchps)/scaleNcdesMinMax
    ### v.0.5 (2019/01/10) alpha/beta
    lambda = 1.
    A = delay/lenOfSig + lambda
    ## 2019/02/07: what if ncdes == 0
    ## B = abs(ncdes-nchps)/lenOfSig + lambda
    if ncdes >= nchps
        B = abs(ncdes-nchps)/lenOfSig + lambda
    else    
       B = lambda 
    end
    alpha*(log(A)) + betta*(log(B))
end

function lossFunction(;
                      changes::Array{Int64,1}=[],
                      detections::Array{Int64,1}=[],
                      lenOfSig::Int64=100,
                      alpha=0.5,
                      retAlsoDelayAndFa=false
                      # scaleDelayMinMax=1., scaleNcdesMinMax=1.
                      # scaleDelay=10., scaleNcdes=5.
                      )

    delays = fnDelays(changes, detections, lenOfSig)
    delay = length(delays) == 1 ? delays[1] : mean(delays)
    numOfFalseAlarms =numberOfFalseAlarms(changes, detections, lenOfSig) 
    probFa = numOfFalseAlarms/lenOfSig
    if 1==0
        println("lossFunction(): delay value = $delay")
        println("lossFunction(): probFa value = $probFa")
        println("lossFunction(): length(changes) = ", length(changes))
        println("lossFunction(): length(detections) = ", length(detections))
        println()
    end
    lossValue = lossFuncGeneral(delay, probFa, nchps=length(changes), ncdes=length(detections),
                                lenOfSig=lenOfSig,
                                ## scaleDelay=scaleDelay, scaleNcdes=scaleNcdes,
                                # alpha=alpha,
                                # scaleDelayMinMax=scaleDelayMinMax, scaleNcdesMinMax=scaleNcdesMinMax
                                )
    if retAlsoDelayAndFa
        (lossValue, delay, probFa, numOfFalseAlarms)
    else
        lossValue
    end
end

# function lossFunction(;
#                       changes::Array{Int64,1}=[],
#                       detections::Array{Int64,1}=[],
#                       lenOfSig::Int64=100,
#                       alpha=0.5, scaleDelayMinMax=1., scaleNcdesMinMax=1., scaleDelay=10., scaleNcdes=5.)

#     lossFunction(changes=changes, detections=detections, lenOfSig=lenOfSig)

# end
