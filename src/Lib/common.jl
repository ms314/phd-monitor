using JSON
using CSV, DataFrames
using DelimitedFiles

function saveCdes(; fpath="./cdes.dat", cdes=[1,2,3])
    open(fpath, "w") do io
        writedlm(io, cdes)
    end
end

function readJSON(fpath)
    open(fpath) do f
        r = JSON.parse(f)
        return r
    end
end

update_mu(x, muOld, nOld) = (nOld * muOld+x)/(nOld+1.0)

# already implemented in Pccf
"Scale values of the vector to the range [0, 1]"
function scale01(v::Array{Float64,1})::Array{Float64,1}
    minv = minimum(v)
    maxv = maximum(v)
    (v .- minv) / (maxv .- minv)
end

function writeOutputSigDataFrame(fpath, x)
    CSV.write(fpath, DataFrame(sig=x), header=true)
end
function readInputSigDataFrame(fpath)
    x = CSV.read(fpath, header = true)
    x = convert(Array{Float64,1}, x[:sig])
    x
end

function readVec(fpath)::Array{Float64,1}
    # f = open(fpath, "r")
    # y = read(f, String)
    # close(f) - completely wrong readings
    # y = [convert(Float64, e) for e in y]
    y = readdlm(fpath, '\n')
    vec(y)
end

function readInputSig(fpath)
    ##y = convert(Array, CSV.read(fpath, header=false))
    y = readVec(fpath)
    # _sig01 = scale01(y)
    # if length(size(_sig01)) == 2
    #     sig01 = _sig01[:,1]
    # elseif length(size(_sig01))==1
    #     sig01 = _sig01
    # else
    #     sig01 = _sig01
    #     println(".... check!!!!!!")
    # end
    #sig01
    y
end

function readInputSigInt(fpath)::Array{Int64,1}
    y = readVec(fpath)
    _sig01 = scale01(y) ## scaled01 signal
    if length(size(_sig01)) == 2
        sig01 = _sig01[:,1]
    elseif length(size(_sig01))==1
        sig01 = _sig01
    else
        sig01 = _sig01
        println(".... check!!!!!!")
    end
    sig01
    [convert(Int64, e) for e in sig01]
end

function readInputChanges(fpath)
    y = convert(Array{Int64,1}, CSV.read(fpath, header=false))
    #_changes = (readdlm(fpath))
    #changes = [convert(Int64, e) for e in _changes[:,1]]
    changes = [convert(Int64, e) for e in y]
end
