#

using Statistics, SpecialFunctions

function constant_hazard(r, lambda::Float64)
    @assert lambda >= 1.
    (1.0 / lambda) .* ones(size(r))
end

"""Non-standardized Student's t-distribution"""
## As in the Matlab code by author:
# c = exp(gammaln(nu/2 + 0.5) - gammaln(nu/2)) .* (nu.*pi.*var).^(-0.5);
# p = c .* (1 + (1./(nu.*var)).*(x-mu).^2).^(-(nu+1)/2);
## Check Eq.110: ~\cite{murphy2007conjugate}
function studentpdfold(x::Float64,
                       mu::Array{Float64,1},
                       var::Array{Float64,1},
                       nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (nu .* pi .* var) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ (nu .* var)) .* (x .- mu).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else
        p
    end
end

"""
My version: possibly a fixed typo
"""
function studentpdf(x::Float64,
                    mu::Array{Float64,1},
                    sigma::Array{Float64,1},
                    nu::Array{Float64,1})
    c = exp.(lgamma.(nu ./ 2.0 .+ 0.5) .- lgamma.(nu ./ 2.0)) .* (pi .* nu) .^ (-0.5)
    p = c .* (1.0 .+ (1.0 ./ nu ) .* ((x .- mu)./sigma).^2.0).^(-(nu .+ 1.0)./2.0)
    if length(p)==1
        p[1]
    else
        p
    end
end

function getStudentPdfParams(y::Float64, y_hist::Array{Float64}, mu0::Float64)
	t = length(y_hist)
	kappa0::Float64 = 1.
    alpha0::Float64 = 1.
    beta0 ::Float64 = 1.
    muT   ::Array{Float64,1} = [mu0]
    kappaT::Array{Float64,1} = [kappa0]
    alphaT::Array{Float64,1} = [alpha0]
    betaT ::Array{Float64,1} = [beta0]
	while t != 0
		predprobs = studentpdf(y, muT, betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT), 2.0 .* alphaT)
		muT0    = vcat(mu0 , (kappaT .* muT .+ y_hist[t]) ./ (kappaT .+ 1.0))
    	kappaT0 = vcat(kappa0, kappaT .+ 1.0)
    	alphaT0 = vcat(alpha0, alphaT .+ 0.5)
    	betaT0  = vcat(beta0, betaT .+ (kappaT .* (y_hist[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
    	muT    = muT0
    	kappaT = kappaT0
    	alphaT = alphaT0
    	betaT  = betaT0
		t -= 1
	end
	@assert length(muT) == length(y_hist)+1
	(muT, kappaT, alphaT, betaT)
end

function updateImproved(y::Array{Float64}; mu0=0.0)
	T = length(y)
	@assert T > 1
	y_r = reverse(y)
	kappa0::Float64 = 1.
    alpha0::Float64 = 1.
    beta0 ::Float64 = 1.
    muT   ::Array{Float64,1} = [mu0]
    kappaT::Array{Float64,1} = [kappa0]
    alphaT::Array{Float64,1} = [alpha0]
    betaT ::Array{Float64,1} = [beta0]
    for t=1:T-1
    	predprobs = studentpdf(y[t], muT, betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT), 2.0 .* alphaT)
    	muT0    = vcat(mu0 , (kappaT .* muT .+ y[t]) ./ (kappaT .+ 1.0))
    	kappaT0 = vcat(kappa0, kappaT .+ 1.0)
    	alphaT0 = vcat(alpha0, alphaT .+ 0.5)
    	betaT0  = vcat(beta0, betaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
    	muT    = muT0
    	kappaT = kappaT0
    	alphaT = alphaT0
    	betaT  = betaT0
    end 
    @assert length(muT) == T
end	


function copyOfOrig(y::Array{Float64}; mu0=0.0)
	T = length(y)
	kappa0::Float64 = 1.
    alpha0::Float64 = 1. ## nu
    beta0 ::Float64 = 1.
    muT   ::Array{Float64,1} = [mu0]
    kappaT::Array{Float64,1} = [kappa0]
    alphaT::Array{Float64,1} = [alpha0]
    betaT ::Array{Float64,1} = [beta0]
    for t=1:T-1
    	predprobs = studentpdf(y[t], muT, betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT), 2.0 .* alphaT)
    	muT0    = vcat(mu0 , (kappaT .* muT .+ y[t]) ./ (kappaT .+ 1.0))
    	kappaT0 = vcat(kappa0, kappaT .+ 1.0)
    	alphaT0 = vcat(alpha0, alphaT .+ 0.5)
    	betaT0  = vcat(beta0, betaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0)))
    	muT    = muT0
    	kappaT = kappaT0
    	alphaT = alphaT0
    	betaT  = betaT0
    end 
    @assert length(muT) == T
end	

function run()
	x = randn(1)
	copyOfOrig(x)
	getStudentPdfParams(1., x, 0.0)
end	
run()