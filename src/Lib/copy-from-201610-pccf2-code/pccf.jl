#
# Version 1.0 (SDM-16)
# 
#     30.10.2016: - Tested with Octave/Matlab version. 
#                   Outputs are the same (very similar at least) 
#                   for the artificially generated and Human Activity signals.
#     
#                 - Added function scale01() to scale PCCF values to
#                   the range [0, 1].
#
# include("./probs.jl")
module Pccf
# SAC 2016 - Improved Matrix based version
using Distributions
export pccf, write_pccf, scale01

normalize = vec -> vec / sum(vec)

function pccf(T, mu, sigma)
    # exact analytical solution
    t = 1:T
    chp_probs = zeros(T)
    for dist_i in 1:T
        dist = Normal(dist_i * mu, sqrt(dist_i*sigma^2))
        chp_probs += pdf(dist, t)
    end
    # normalize(chp_probs)
    chp_probs
end

function weights_matrix(T)
  n = T
  M = zeros(n,n)
  M[1,:] = 1:n
  for i = 2:n
      for j = i:n
          M[i,j] = M[i-1, j-1]
      end
  end
  M'
end

function gaussPdf(x, mu, sigma)
    k = 1/(sigma * sqrt(2*pi))
    p = k * exp(-((x-mu)^2/(2*sigma^2)))
end

function pccf_mp(T, mu, sigma)
    # message passing version
    gaussDist = Normal(mu, sigma)
    P         = zeros(T, T)
    P[:,1]    = pdf(gaussDist, 1:T)
    W         = pdf(gaussDist, weights_matrix(T))
    for i = 1:T-1
        P[i+1:T, i+1] = W[1:T-i, 1:T-i] * P[i:end-1, i]
    end
    #normalize( sum(P,2) ) # change probabilities
    sum(P,2) # change probabilities
end

function pccfWithConfirmation(probs,delayInMu,changes)
    # PCCF with confirmation
    T=length(probs)
    confirmed=changes[1:delayInMu:length(changes)]
    out=Float64[]
    nchps=length(confirmed)
    for i=1:nchps
        if i==1
            out=vcat(out,probs[1:(confirmed[i]-1)])
        else
            out=vcat(out,probs[1:(confirmed[i]-confirmed[i-1])])
        end 
        if i==nchps
            out=vcat(out,probs[1:(T-confirmed[i]+1)])
        end 
    end  
    @assert length(out)==T
    out
end

function write_pccf(filepath, T = 100, mu = 25.0, sigma = 3.2)
    # r = pccf(T, mu, sigma)
    r = pccf_exact(T, mu, sigma)
    f = open(filepath, "w")
    writedlm(f, r)
    close(f)
    println("... PCCF function is written into $filepath")
end

"Scale values of the vector to the range [0, 1]"
function scale01(v)
    minv = minimum(v)
    maxv = maximum(v)
    (v - minv) / (maxv - minv)
end 


end
#------------------------- End module Pccf -----------------------------------#

# module PccfIcdm
# # using Probs
# export pccf, write_pccf
# # Old version based on the init window of size w
# 
# function gaussPdf(x, mu, sigma)
#     k = 1/(sigma * sqrt(2*pi))
#     p = k * exp(-((x-mu)^2/(2*sigma^2)))
# end
# 
# function initProbs(w, mu, sigma)
# 	f(x) = gaussPdf(x, mu, sigma)
# 	init = map(f, collect(1:w))
# 	init / sum(init)
# end
# 
# function possiblePrevPositions(prev_len, delta, i)
# 	if i >= 2 && i <= prev_len + delta
# 		collect(max(1, i-delta) : 1 : min(i-1, prev_len))
# 	else
# 		nothing
# 	end
# end
# 
# function forwardProbs(prevProbs, w, mu, sigma)
# 	delta = w
# 	n_prev = length( prevProbs )
# 	nextProbs = zeros(n_prev + w - 1)
# 	n_next = length(nextProbs)
# 	for i = 2 : (n_next+1)
# 		prevPositions = possiblePrevPositions( n_prev, delta, i)
# 		if prevPositions != nothing
# 			prevProbs1 = prevProbs[prevPositions]
# 			deltas = map(x -> i - x, prevPositions)
# 			prob_deltas = map(x -> gaussPdf(x, mu, sigma), deltas)
# 			nextProbs[i-1] = sum(prob_deltas .* prevProbs1)
# 		end
# 	end
# 	nextProbs = nextProbs / sum(nextProbs)
# end
# 
# function pccf(num_events, w, mu, sigma)
# 	tic()
# 	allM = zeros(num_events * w, num_events)
# 	allM[1:w, 1] = initProbs(w, mu, sigma)
# 	for i = 2:num_events
# 		prev_vect = allM[(i-1):((i-1)*w), i-1]
# 		next_vec_of_probs = forwardProbs(prev_vect, w, mu, sigma)
# 		allM[(i:(i*w)), i] = next_vec_of_probs
# 	end
# 	nn, mm = size(allM)
# 	cc = zeros(nn)
# 	for i = 1:nn
# 		s = 0.
# 		for j = 1:mm
# 			s += allM[i,j]
# 		end
# 		cc[i] = s
# 	end
# 	toc()
# 	cc = cc / sum(cc)
# end
# 
# function write_pccf(filepath; num_of_events = 30, w = 150, mu = 25.0, sigma = 3.2)
# 	r = pccf(num_of_events, w, mu, sigma)
# 	f = open(filepath, "w")
# 	writedlm(f, r)
# 	close(f)
# 	println("... PCCF function is written into $filepath")
# end
# end
# #----------------------- END MODULE PccfIcdm ---------------------------------#
