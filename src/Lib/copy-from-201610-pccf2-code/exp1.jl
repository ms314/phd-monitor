#
# V.0.1 (2016.11.04)
#
# ROC curve for the real data.
# Run experiment with the real data set.
# Plot ROC curve.
# Corresponding tests are in: test4_concept_proof_real.R, .jl
#
include("./lib/metadata.jl")
include("./lib/pccf.jl")
include("./2bayes_detector_replica.jl")
include("./lib/SignalGenerator.jl")
include("./lib/roc.jl")

using SignalGenerator
using BayesChpOrig
using Metadata
using Roc
using Pccf



"""
In this experiment we measure perfoemance of the Online Bayesian Detector 
1) without PCCF
2) with PCCF 
and plot correspondint ROC curves (exp1.R).
"""
function run_exp1(inputDataFolder, outputfolder)
    #work_path = "./data/activity-recognition-sample/"
    #signal = readdlm(work_path * "sample-sig-10.dat", ',')
    println("... exp1.jl.run_exp > read input data from ", inputDataFolder)
    println("... exp1.jl.run_exp > save results into ", outputfolder)
    
    signal      = readdlm(inputDataFolder * "sample-sig-10.dat", ',')    
    changes     = readdlm(inputDataFolder * "changes-sig-active-10.dat")
    changes     = reshape(changes, length(changes))
    mid_changes = readdlm(inputDataFolder * "mid-changes-sig-active-10.dat")
    #signal      = readdlm(projectdir * "src_julia/data/activity-recognition-sample/sample-sig-10.dat", ',')    
    #changes     = readdlm(projectdir * "src_julia/data/activity-recognition-sample/changes-sig-active-10.dat")
    #changes     = reshape(changes, length(changes))
    #mid_changes = readdlm(projectdir * "src_julia/data/activity-recognition-sample/mid-changes-sig-active-10.dat")    

    # BEFORE ADDING MID_CHANGES
    mu0 = mean(diff(changes)) # ~84.6
    sd0 = std(diff(changes))  # ~9.16
    @printf("... exp1.jl > mu = %0.2f sigma = %0.2f\n", mu0, sd0)
    # end BEFORE ADDING MID_CHANGES

    
    if 1 == 1
        # Add middle changes 
        mid_changes = reshape(mid_changes, length(mid_changes))
        changes = vcat(changes, mid_changes)
        sort!(changes)
    end

    x = signal[:,1]
    y = signal[:,2]
    n = length(x)

    max_delay = 10
    
    h_from    = 80   # target values (~ test4_concept_proof_real.jl) l_common = 100 
    h_to      = 300  # 300 - on ROC points become too close
    h_by      = 10
    threshold = 0.3

    tp_rates = []
    fa_rates = []
    tp_rates_pccf = []
    fa_rates_pccf = []
    h_vals = []

    probs = scale01(pccf(n, mu0, sd0)) # usefull only at the beginning

    # Make input signal shorter so that PCCF effect is more visible (also below in a loop)
    shortenInput = false # true
    if shortenInput
        max_t = 500
        x = x[1:max_t]        
        changes = filter(x -> x <= max_t, changes)
        probs = probs[1:max_t]
    end
    # end Make input signal shorter so that PCCF effect is more visible (also below in a loop)
    

    for h in h_from : h_by : h_to
        push!(h_vals, h)
        
        l_common = h
        if mod(h, 10)==0 
            # print(h/h_to,"\n")
        end

        maxes, mat = bayes_detector(x, lambda = l_common)
        detections = extract_changes(maxes)

        pccf_maxes, pccf_mat = pccf_bayes_detector(x, probs, l_common + 100, l_common, threshold, h_common = l_common, max_d = 500)
        pccf_detections = extract_changes(pccf_maxes)

        # Make input signal shorter so that PCCF effect is more visible
        if shortenInput
            detections      = filter(x-> x<= max_t, detections)
            pccf_detections = filter(x-> x<= max_t, pccf_detections)
        end
        # END Make input signal shorter so that PCCF effect is more visible

        tpr      = tpRate(detections, changes, max_delay)
        tpr_pccf = tpRate(pccf_detections, changes, max_delay)

        fpr      = faRateScaled(detections     , changes, max_delay, n, tn_price_scale = 0.1 * 0.5)
        fpr_pccf = faRateScaled(pccf_detections, changes, max_delay, n, tn_price_scale = 0.1 * 0.5)

        # To find optimal h values to demonstrate PCCF effect
        if fpr <= 0.30 && tpr >= 0.6
            @printf("... exp1.jl > TP      = %0.3f / FP      = %0.3f /  H = %0.3f \n"  , tpr, fpr, h)
            @printf("... exp1.jl > TP_PCCF = %0.3f / FP_PCCF = %0.3f /  H = %0.3f \n\n", tpr_pccf, fpr_pccf, h)
        end

        push!(tp_rates, tpr)
        push!(fa_rates, fpr)
        push!(tp_rates_pccf, tpr_pccf)
        push!(fa_rates_pccf, fpr_pccf)
        
    end
    
    writedlm(outputfolder * "tp_rates.dat"     , tp_rates)
    writedlm(outputfolder * "fa_rates.dat"     , fa_rates)
    writedlm(outputfolder * "tp_rates_pccf.dat", tp_rates_pccf)
    writedlm(outputfolder * "fa_rates_pccf.dat", fa_rates_pccf)
    writedlm(outputfolder * "h_vals.dat"       , h_vals)
    # writedlm(outdir * "exp1/tp_rates.dat", tp_rates)
    # writedlm(outdir * "exp1/fa_rates.dat", fa_rates)
    # writedlm(outdir * "exp1/tp_rates_pccf.dat", tp_rates_pccf)
    # writedlm(outdir * "exp1/fa_rates_pccf.dat", fa_rates_pccf)
    # writedlm(outdir * "exp1/h_vals.dat", h_vals)    
    
    println("... exp1.jl > Results are saved into ", outdir * "exp1/")

end 

run_exp1(projectdir * "src_julia/data/activity-recognition-sample/",
         outdir * "exp1/")

# changes = 
# max_delay = 10
# tpr = tpRate(detections, chps, max_delay)
# fpr = faRateScaled(detections, changes, max_delay, length(sig), tn_price_scale=0.1)
# println("... test_bd > TPrate = ", tpr, " / FArate = ", fpr)        
