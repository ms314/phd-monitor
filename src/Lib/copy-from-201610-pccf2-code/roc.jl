# 
# v.0.8 (fixing errors in Pccf/ - v.0.7)
#
module Roc

# faRate -> faRateScaled -> ppv
export tpRate, faRate, faRateScaled, ppv 

function makeBinsOneSide(changes, maxDelay)
    # returns an array of tuples
    [(c,b) for (c,b) in zip(changes, [c + maxDelay for c in changes])]
end

function makeBinsTwoSide(changes, maxDelay)
    # returns an array of tuples
    right=[c+maxDelay for c in changes]
    left=[c-maxDelay for c in changes]
    [(l,r) for (l,r) in zip(left,right)]
end

function makeBins(changes,maxDelay)
    makeBinsTwoSide(changes,maxDelay)
end

function fallsInBins(val,bins::Array{Tuple{Int64,Int64}})
    # returns bins as an array of tuples: [(left,right)]
    bins[[e[1]<=val<=e[2] for e in bins]]
end
# println(fallsInBins(18, makeBins([1,20,30],3))) 

function ifCdeFallsIntoBins(cde::Int64, bins::Array{Tuple{Int64,Int64}})
    length(fallsInBins(cde, bins))>0 ? true : false 
end
# println(ifCdeFallsIntoBins(18, makeBins([1,20,30],3))) 

function tpNumNaive(detections,changes,maxDelay)
    # Calculate number of true positives
    bins = makeBins(changes, maxDelay)
    cdesInBins = Set(filter(cde->ifCdeFallsIntoBins(cde, bins), detections))
    length(cdesInBins)
end

function tpNum(detections,changes,maxDelay)
    # Calculate number of true positives
    tpNumEffective(detections, changes, maxDelay)
end

"""
Calculate _effective_ number of true positives 
by removing TP's falling within the same bins.
!!! TO BE OPTIMIZDED 
"""
function tpNumEffective(detections, changes, maxDelay)
    bins = Roc.makeBins(changes, maxDelay)
    bins_dict = Dict{Int64, Int64}()
    for k in 1 : length(bins)
        bins_dict[k] = 0 
    end 
    for cde in detections
        for k in 1 : length(bins)
            if bins[k][1] <= cde <= bins[k][2]   
                bins_dict[k] += 1
            end 
        end 
    end 
    # Count effective number of TP's
    tp_effective_num = 0 
    for dict_val in values(bins_dict)
        if dict_val > 0
            tp_effective_num += 1 
        end  
    end 
    tp_effective_num
end

"""
Find the nearest to val element of sorted seq.
Returns index of seq's member.
"""
function find_nearest(val, seq)
#    
end 
# find_nearest(5, [1,2,3,4,5,6,7,8,9,10])

#
function tpNum(detections, changes)
    intersect(Set(detections), Set(changes))
end

function faNum(detections,changes,maxDelay)
    length(detections)-tpNum(detections,changes,maxDelay)
end

function fnNum(detections,changes,maxDelay)
    # Calculate number of false negatives
    bins=makeBins(changes.maxDelay)
    length(bins)-tpNum(detections, changes, maxDelay)
end


"""
Number Of True Negatives  = 1000 :   price = 0.1 
Number Of True Positives  = 5    :   price = 1.0  
Number Of False Positives = 100  :   price = -1.0  
Number Of False Negatives = 1    :   price = -1.0 

TP rate = TP/(TP+FN) = 5/(5+1) = 0.83 
FP rate = FP/(FP+TN) = 100/(100+1000) = 0.09  

https://en.wikipedia.org/wiki/Positive_and_negative_predictive_values

FDR: False discivery rate FP/(TP+FP) = 100/(5+100) = 0.95 
PPV: Positive Predicitve Value TP/(TP+FP) = 5/(5+100) = 0.05

""" 
function tpRate(detections,changes,maxDelay)
    # True positive TP rate = TP/(TP + FN); In case if there are
    # several true changes within the same bin we take min(1, tprate)
    tpn=tpNum(detections,changes,maxDelay)
    # min(1, )
    tpn / length(changes)
end

"PPV: Positive Predicitve Value TP/(TP+FP) = 5/(5+100) = 0.05"
function ppv(detections,changes,maxDelay)
    tpn=tpNum(detections,changes,maxDelay)
    fan = faNum(detections,changes,maxDelay)
    tpn/(tpn+fan)
end 

function faRate(detections,changes,maxDelay,totalNum)
    # False alarm/positive rate FPR = FP / (FP + TN) ; == specificity
    fan = faNum(detections,changes,maxDelay)
    tpn = tpNum(detections,changes,maxDelay)
    trueNegNum = totalNum - length(changes) * maxDelay
    fan/(fan+trueNegNum)
end

"""
Take into accout low price of True Negatives
"""
function faRateScaled(detections,changes,maxDelay,totalNum; tn_price_scale=0.1)
    # False alarm/positive rate FPR = FP / (FP + TN) ; == specificity
    fan = faNum(detections,changes,maxDelay)
    tpn = tpNum(detections,changes,maxDelay)
    trueNegNum = totalNum - length(changes) * maxDelay
    trueNegNum = tn_price_scale * trueNegNum
    fan/(fan+trueNegNum)
end

end
