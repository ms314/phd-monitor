function read_vec(fpath; builtin = false)
    if builtin
        f = open(fpath, "r")
        lines = readlines(f)
        close(f)
        map(x -> float(x), lines)
    else
        readdlm(fpath)
    end
end

function write_vec(outpath, vec; msg = true)
  f = open(outpath, "w")
  for elem in vec
    write(f, string(elem) * "\n")
  end
  close(f)
  if msg
    println("Write vector into $outpath")
  end
end

function writedlm_header(fpath, matrix, header::AbstractString)
    f = open(fpath, "w")
    write(f, header * "\n")
    writedlm(f, matrix, ",")
    close(f)
end

function join_two_columns(in1, in2, out)
  f1 = open(in1, "r")
  f2 = open(in2, "r")
  f3 = open(out, "w")
  lines1 = readlines(f1)
  lines2 = readlines(f2)
  println(length(lines1) - length(lines2))
  if length(lines1) == length(lines2)
    for i = 1:length(lines1)
        write(f3, string(replace(lines1[i], "\n", "")) * "," *
              string( replace(lines2[i],"\n","")) * "\n")
    end
  end
  close(f1)
  close(f2)
  close(f3)
end
# mpath = "D:/DATASETS/OUT_SAC16Data/bus1000/w1000/jl/"
# join_two_columns(mpath*"best_columns.txt", mpath*"best_sigmas.txt", mpath*"clsgm.txt")

function correct_path(fpath::AbstractString)
  print(fpath[end])
  if fpath[end] != '/'
    fpath = fpath * "/"
  end
  fpath
end

function write_items(fpath, vec, header)
    f=open(fpath,"w")
    write(f, header * "\n")
    for e in vec
        str=join(e, ",")*"\n"
        write(f,str)
    end 
    close(f)
end
function write_items(fpath, vec)
    f=open(fpath,"w")
    for e in vec
        str=join(e, ",")*"\n"
        write(f,str)
    end 
    close(f)
end
#write_items("D:/ttt.txt", [(1,2,'a'),(5,7,'v')])
#write_items("D:/ttt1.txt", [(1,2,'a'),(5,7,'v')],"dwdw,de,de,d")

function rms(v)
  n = length(v)
  sqrt(sum(v.*v)/n)
end
