import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import pck.*;

public class main {

    public static ArrayList<Double> ReadVec(String s) throws IOException
    {// read vector from file
        int counter = 0;
        ArrayList<Double> list = new ArrayList<Double>();
        BufferedReader inputStream = null;
        try
        {
            inputStream = new BufferedReader(new FileReader(s));

            String l;
            while ((l = inputStream.readLine()) != null)
            {
                counter ++;
                list.add(Double.parseDouble(l));
            }
        } finally {
            if (inputStream != null)
                inputStream.close();
        }
//        System.out.println(counter + " lines are read");
        return list;
    }

    public static void main(String[] args) throws IOException {
        // Prints "Hello, World" to the terminal window.
        System.out.println(">>> Read data from file.");
        ArrayList<Double> sig = ReadVec("C:/1maslov/tmp/tstsig2.txt");
//        for (int i=0; i< 10; i++)
//            System.out.println(sig.get(i));
        ADWINcleaned adwin=new ADWINcleaned(.01); // Init Adwin with delta=.01
        //for(int i=0; i < 20000; i++)
        int t = 0;
        for(int i=0; i < sig.size(); i++){
            t++;
            if(adwin.set_input(sig.get(i)))
                System.out.println("Change Detected: " + i);
        }
    }
}
