/*
* CLEANED A LOT CODE !!!
* https://github.com/Waikato/moa/blob/a3afaa0f885801d1c7eb78ab0c7448bba899afab/moa/src/main/java/moa/classifiers/core/driftdetection/ADWIN.java
 *    EDITED VERSION
  *
 *    Adwin.java
 *    Copyright (C) 2008 UPC-Barcelona Tech, Catalonia
 *    @author Albert Bifet
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package pck;


public class ADWINcleaned { //extends Estimator {

	private class List{
		protected int count;
		protected Bucket head;
		protected Bucket tail;

		public List()
        {
		    clear();
            this.head = new Bucket(this.head, null);

            if (this.tail == null)
                this.tail = this.head;
            this.count++;
		}

		public Bucket head() { return this.head; }
		public Bucket tail() { return this.tail; }

		public void clear()
		{
			this.head = null;
			this.tail = null;
			this.count = 0;
		}

		public void addToTail()
        {
			this.tail = new Bucket(null, this.tail);
			if (this.head == null)
				this.head = this.tail;
			this.count++;
		}

		public void removeFromTail()
        {
//            System.out.print("call remove ;");
			this.tail = this.tail.previous();
			if (this.tail == null)
				this.head = null;
			else
				this.tail.set_next(null);
			this.count--;
//			return ;
		}
	}

	private class Bucket {
		public Bucket next;
		public Bucket previous;
		protected int bucket_num_of_non_zeroes = 0;
		protected int MAXBUCKETS = ADWINcleaned.MAXBUCKETS;
		protected double bucket_vec_total[] = new double[MAXBUCKETS+1];
		protected double bucket_vec_variance[] = new double[MAXBUCKETS+1];

		public Bucket()
        {
			this.next = null;
			this.previous=null;
		}

		public void clear_bucket()
        {
			bucket_num_of_non_zeroes =0;
			for (int k=0; k <= MAXBUCKETS; k++)
				clear_bucket_position(k);
		}

		private void clear_bucket_position(int k)
        {
			bucket_vec_total[k] = 0;
			bucket_vec_variance[k] = 0;
		}

		public Bucket(Bucket nextNode, Bucket previousNode)
        {
			this.next = nextNode;
			this.previous=previousNode;
			if (nextNode != null)
				nextNode.previous = this;
			if (previousNode != null)
				previousNode.next = this;
			clear_bucket();
		}

		public void insert_new_values(double Value, double Variance){
			int k = this.bucket_num_of_non_zeroes;
			this.bucket_num_of_non_zeroes ++;
			this.bucket_vec_total[k] = Value; //todo: why not k+1 ??
            this.bucket_vec_variance[k] = Variance;
		}

		public void compress_buckets_vec(int num_of_items_to_delete)
        {
			for (int k = num_of_items_to_delete; k <= MAXBUCKETS; k++)
			{
				bucket_vec_total[k - num_of_items_to_delete] = bucket_vec_total[k];
				bucket_vec_variance[k - num_of_items_to_delete] = bucket_vec_variance[k];
			}

			for (int k = 1; k <= num_of_items_to_delete; k++)
				clear_bucket_position(MAXBUCKETS - k + 1);

			bucket_num_of_non_zeroes -= num_of_items_to_delete;
		}

		public Bucket previous() { return this.previous; }

		public Bucket next() { return this.next; }

		public void set_next(Bucket next) { this.next = next; }

	}

	private static final int mintMinimLongitudWindow = 10;
	private double mdbldelta = 0.001; //.002; //.1;
	private int mintTime = 0;
	private int mintClock = 32;
	private double mdblWidth = 0; // Mean of Width = mdblWidth/Number of items
	//BUCKET
	public static final int MAXBUCKETS = 5;
	private int number_of_buckets_in_the_list = 0;
	public double TOTAL = 0;
	private double VARIANCE = 0;
	public int WIDTH = 0;

	private int numberDetections = 0;
	private int MINT_MIN_WIN_LENGTH = 5;

	private List list_of_buckets;
	public double getEstimation(){return TOTAL/WIDTH;}

	public void insert_element_to_list_head(double Value){
		WIDTH++;
        list_of_buckets.head().insert_new_values(Value, 0);
		double incVariance = 0;
		if (WIDTH>1)
		    incVariance = (WIDTH-1)*(Value-TOTAL/(WIDTH-1))*(Value-TOTAL/(WIDTH-1))/WIDTH;
		VARIANCE += incVariance;
		TOTAL += Value;
		compress_buckets();
	}

	private int calc_bucket_size(int Row){ return (int) Math.pow(2,Row);}

	public int delete_values_from_tail_bucket(){
		Bucket Node = list_of_buckets.tail();
		int n1 = calc_bucket_size(this.number_of_buckets_in_the_list);
		WIDTH -= n1;
        TOTAL -= Node.bucket_vec_total[0];
        double u1 = Node.bucket_vec_total[0]/n1;

		double incVariance = Node.bucket_vec_variance[0] + n1 * WIDTH * (u1 - TOTAL/WIDTH) * (u1 - TOTAL/WIDTH)/(n1+WIDTH);

		VARIANCE -= incVariance;

		Node.compress_buckets_vec(1);

		if (Node.bucket_num_of_non_zeroes == 0)
		{
			list_of_buckets.removeFromTail();
			number_of_buckets_in_the_list--;
		}
		return n1;
	}

	public void compress_buckets(){
		int n1, n2;
		double u2,u1,incVariance;
		Bucket cursor;
		Bucket next_bucket;
		cursor = list_of_buckets.head();
		int i = 0;
		do {
			// IF CURRENT BUCKET IS FULL
			if(cursor.bucket_num_of_non_zeroes == MAXBUCKETS + 1)
			{
				next_bucket = cursor.next();
				if (next_bucket == null)
				{
					list_of_buckets.addToTail();
					next_bucket = cursor.next();
					number_of_buckets_in_the_list ++;
				}
                // == prepare data
				n1 = calc_bucket_size(i);
				n2 = calc_bucket_size(i);
                u1 = cursor.bucket_vec_total[0]/n1;
                u2 = cursor.bucket_vec_total[1]/n2;
				incVariance = n1*n2*(u1-u2)*(u1-u2)/(n1+n2);
                // == end prepare data

                // add data from the current bucket to the next bucket
				next_bucket.insert_new_values(
				        cursor.bucket_vec_total[0] + cursor.bucket_vec_total[1],
                        cursor.bucket_vec_variance[0] + cursor.bucket_vec_variance[1] + incVariance);

				// delete data from the current bucket
				cursor.compress_buckets_vec(2);

				if (next_bucket.bucket_num_of_non_zeroes <= MAXBUCKETS)
				    break;
			}
			else
                break;

			cursor = cursor.next();
			i++;
		}
		while (cursor != null);
	}

	public boolean set_input(double x)
    {
		return set_input(x, mdbldelta);
	}

	private int get_num_of_buckets(){
		Bucket cursor = this.list_of_buckets.head;
		int i  = 1;
		while (cursor.next != null){
			cursor = cursor.next;
			i ++;
		}
		return i;
	}

	private String str_bucket_vec(double [] v){
		String s = "";
		for (int i=0; i < v.length; i++)
			s = s + String.format("%.1f",v[i]) + ", ";
		return s;
	}

	public void print_content(){
	    int n_empirics = get_num_of_buckets();
		System.out.println("\n");
		System.out.println("Number of buckets:" + n_empirics + "; Obs. t = " + this.mintTime);
		Bucket cursor = this.list_of_buckets.head;
		int i  = 1;
        System.out.println("B"+ i + ": " + str_bucket_vec(cursor.bucket_vec_total));
		while (cursor.next != null){
			cursor = cursor.next;
			System.out.println("B"+ i + ": " + str_bucket_vec(cursor.bucket_vec_total));
			i ++;
		}
	}

	public boolean set_input(double x, double delta) {
		boolean if_change = false;
		boolean if_exit;

		Bucket cursor;

		this.mintTime ++;
		this.insert_element_to_list_head(x);
        boolean if_reduce_width = true;

		if (this.mintTime % this.mintClock == 0 && this.WIDTH > mintMinimLongitudWindow)
		{
			while (if_reduce_width)
			{
			    if_reduce_width = false;
				if_exit = false;
				double u0 = 0;
				double u1 = this.TOTAL;
				double bsz; //n2;
                int n0 = 0;
                int n1 = WIDTH;
				cursor = this.list_of_buckets.tail();
				int i = this.number_of_buckets_in_the_list;
				do { // while ( !if_exit && cursor!= null );
                    bsz = calc_bucket_size(i);
					for (int k = 0; k <= (cursor.bucket_num_of_non_zeroes-1); k++)
					{
						n0 += bsz;
						n1 -= bsz;
						u0 += cursor.bucket_vec_total[k];
						u1 -= cursor.bucket_vec_total[k];
						if (i == 0 && k == cursor.bucket_num_of_non_zeroes - 1)
						{
						    if_exit = true;
						    break;
						}
						double absvalue = (u0/n0)-(u1/n1);
						if (n0 > this.MINT_MIN_WIN_LENGTH+1 && n1 > this.MINT_MIN_WIN_LENGTH+1 &&
						    if_cut_condition(n0, n1, absvalue, delta))
						{
							if_reduce_width = true;
							if_change = true;
							if (this.WIDTH > 0)
							{
								n0 -= this.delete_values_from_tail_bucket();
								if_exit = true;
								break;
							}
						}
					}
					cursor = cursor.previous();
					i--;
				} while (!if_exit && cursor!= null); // TODO: || also works
			}//End While // Diference
		}//End if
	    this.mdblWidth += this.WIDTH;
		if (if_change) numberDetections++;
		if(false) this.print_content();
		return if_change;
	}

    private boolean if_cut_condition(int n0, int n1, double absvalue, double delta){
        double dd = Math.log(2*Math.log(this.WIDTH)/delta);
        // Formula Gener 2008
        double v = VARIANCE /WIDTH;
        double m = ((double)1/((n0- MINT_MIN_WIN_LENGTH +1)))+ ((double)1/((n1- MINT_MIN_WIN_LENGTH +1)));
        double epsilon= Math.sqrt(2*m*v*dd)+(double) 2/3 *dd* m;
        return (Math.abs(absvalue)>epsilon);
    }

	public ADWINcleaned(double d)
	{
		list_of_buckets = new List();
        mdbldelta=d;
		numberDetections=0;
	}
}
