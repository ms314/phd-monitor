module AdwinModule
using JSON

export adwinDetector

function read_config()
    open("./config.json") do f
        r = JSON.parse(f)
        return r
    end
end


calcBucketSize(x)  = Int(2^x) 


function ifCutCondition(n0, 
                        n1, 
                        absVal, 
                        delta, 
                        sigma, 
                        width::Int, 
                        min_wnd_length::Int)

    @assert sigma >= 0.0
    
    dd = log(2*log(width) / delta)
    
    v = sigma / width
    
    m = 1.0/(n0 - min_wnd_length + 1) + 1.0/(n1 - min_wnd_length + 1)
    
    if 2 * m * v * dd <= 0 
        println("m=$m, v=$v, sigma=$sigma, width=$width, dd=$dd ")
    end 
    
    epsilon = sqrt(2 * m * v * dd) + 2.0/3.0 * dd * m
    
    abs(absVal)>epsilon
end


function ifChpCheck(v,n0,n1,u0,u1,
                    ifRedW,ifChp,bsz,dlt,sigma,
                    width,minWlen)
    n0n = n0 + bsz
    n1n = n1 - bsz
    u0n = u0 + v
    u1n = u1 - v
    absv = u0n/n0n-u1n/n1n
    if (n0n > minWlen+1 && 
        n1n > minWlen+1 && 
        ifCutCondition(n0n, n1n, absv, dlt, sigma, width, minWlen)) 
        (n0n, n1n, u0n, u1n, true, true, true)
    else 
        (n0n, n1n, u0n, u1n, ifRedW, ifChp, false)
    end
end


abstract type AbstractNode end
struct NullNode <: AbstractNode end
const null = NullNode()


mutable struct Bucket <: AbstractNode
    prev                 :: AbstractNode
    nxt                  :: AbstractNode
    bucketNumOfNonZeroes :: Int
    bucketCapacity        :: Int
    vectot              :: Array{Float64,1}
    vecvar              :: Array{Float64,1}

    function Bucket(bucketCapacity::Int = 5)
        x                      = new()
        x.prev                 = null
        x.nxt                  = null
        x.bucketNumOfNonZeroes = 0
        x.bucketCapacity        = bucketCapacity 
        x.vectot              = zeros(x.bucketCapacity+1)
        x.vecvar              = zeros(x.bucketCapacity+1)
        clearBucket!(x)  ## todo: why? 
        x
    end 

    function Bucket(n::AbstractNode, p::AbstractNode, bucketCapacity::Int=5)
        x                      = new()
        x.prev                 = p
        x.nxt                  = n
        x.bucketNumOfNonZeroes = 0
        x.bucketCapacity        = bucketCapacity 
        x.vectot              = zeros(x.bucketCapacity+1)
        x.vecvar              = zeros(x.bucketCapacity+1)

        clearBucket!(x)  ## todo: why? 
        if n != null n.prev = x end 
        if p != null p.nxt  = x end
        x
    end
    
end


function clearBucket!(x::Bucket)
    x.vectot[1:(x.bucketCapacity+1)] = 0.0 
    x.vecvar[1:(x.bucketCapacity+1)] = 0.0 
    x.bucketNumOfNonZeroes = 0
end


function insertNewValues!(x::Bucket, value::Float64, variance::Float64)
    x.vectot[x.bucketNumOfNonZeroes+1] = value
    x.vecvar[x.bucketNumOfNonZeroes+1] = variance
    x.bucketNumOfNonZeroes += 1
end


function compressBucketsVec!(x::Bucket, numOfItemsToDel::Int)
    n = x.bucketCapacity+1
    
    x.vectot[1:(n-numOfItemsToDel)] = x.vectot[(numOfItemsToDel+1):n]
    x.vecvar[1:(n-numOfItemsToDel)] = x.vecvar[(numOfItemsToDel+1):n]

    x.vectot[(n-numOfItemsToDel+1):n] = 0.0
    x.vecvar[(n-numOfItemsToDel+1):n] = 0.0
    
    x.bucketNumOfNonZeroes -= numOfItemsToDel
    
    @assert x.bucketNumOfNonZeroes >= 0
end


mutable struct ListOfBuckets
    head           :: AbstractNode 
    tail           :: AbstractNode
    bucketCapacity :: Int 
    count          :: Int ## todo: needed?

    function ListOfBuckets(bucketCapacityIn::Int)
        x = new()
        x.head = Bucket(null, null, bucketCapacityIn)
        x.tail = x.head 
        x.bucketCapacity = bucketCapacityIn
        x.count = 1
        x
    end    

end

function printListOfBucket(lst::ListOfBuckets)
    #println("... count Buckets in the list")
    ## get num of buckets 
    function getNumOfB(cursor::AbstractNode, acc::Int)
        if cursor == null #|| cursor.nxt == null 
            acc 
        else
            getNumOfB(cursor.nxt, acc+1)
        end 
    end 
    n = getNumOfB(lst.head, 0)
    # println("\n ... $n buckets in the list")
    function printer(cursor::AbstractNode, counter::Int)
        if cursor !== null 
            println("Bucket $counter :")
            println("vectot:", cursor.vectot)
            println("vecvar:", cursor.vecvar)
            printer(cursor.nxt, counter+1)
        end 
    end 
    printer(lst.head, 1)
end 

function addToTail!(lst::ListOfBuckets, bucketCapacityIn::Int)
    lst.tail = Bucket(null, lst.tail, bucketCapacityIn)
    if lst.head == null 
        lst.head = lst.tail 
    end 
    lst.count += 1
end


function removeFromTail!(lst::ListOfBuckets)
    lst.tail = lst.tail.prev ## todo: better behavior when deleting from null 
    #-if lst.tail != null 
    lst.count -= 1
    @assert lst.count >= 0
    if lst.tail == null 
        lst.head = null 
    else 
        lst.tail.nxt = null 
    end 
end


function compressBuckets!(lst::ListOfBuckets, 
                          currentNumOfBuckets::Int, 
                          maxBuckets::Int)

    function compressBucketsAux(cursor::AbstractNode, i::Int, numOfBucketsInLst::Int)
        
        newNumOfBuckets = numOfBucketsInLst
        
        if cursor != null 
            if cursor.bucketNumOfNonZeroes == maxBuckets + 1

                    nextBucket = cursor.nxt

                    if nextBucket==null
                        addToTail!(lst, maxBuckets) 
                        nextBucket = cursor.nxt
                        newNumOfBuckets+=1
                    end

                    bucketSize = calcBucketSize(i)

                    u0 = cursor.vectot[1]
                    u1 = cursor.vectot[2]
                    v0 = cursor.vecvar[1]
                    v1 = cursor.vecvar[2]

                    incVariance = (u0-u1)^2/(2.0*bucketSize)

                    if nextBucket != null
                        ## Add data from the current bucket to the next bucket
                        insertNewValues!(nextBucket, u0+u1, v0+v1+incVariance)
                        ##  Delete data from the current bucket
                        compressBucketsVec!(cursor,2)
                    end
            end
        end 

        if cursor == null
            return newNumOfBuckets
        else
            compressBucketsAux(cursor.nxt, i+1, newNumOfBuckets)
        end
    end
    
    numOfBucketsNew = compressBucketsAux(lst.head, 0, currentNumOfBuckets)
    numOfBucketsNew
end


function insertElementToListHead(lst::ListOfBuckets, 
                                 x::Float64,
                                 width::Int, 
                                 total::Float64,
                                 variance::Float64, 
                                 numOfBucketsInList::Int,
                                 maxBuckets::Int)
        newWidth = width+1

        if lst.head!=null
            insertNewValues!(lst.head, x, 0.0)
        end

        if newWidth > 1
            incVariance = (newWidth-1)*(x-total/(newWidth-1))^2/newWidth
        else
            incVariance = 0.0
        end

        newNumOfBucketsInList = compressBuckets!(lst, numOfBucketsInList, maxBuckets)

        if variance+incVariance < 0.0 
            println("!!!!!!!!!!! variance+incVariance=", incVariance + variance)
        end 

        @assert incVariance >= 0.0

        (width+1, total+x, variance+incVariance, newNumOfBucketsInList)
end


function deleteValuesFromTailBucket(lst::ListOfBuckets, 
                                    numOfBucketsLst::Int, 
                                    width::Int, 
                                    total::Float64, 
                                    variance::Float64)
    node      = lst.tail
    n1        = calcBucketSize(numOfBucketsLst)
    newWidth  = width - n1
    newNumOfB = numOfBucketsLst 
    newTot    = total 
    newVar    = variance 

    if node!=null
        newTot -= node.vectot[1]
        u1 = node.vectot[1]/n1
        incVariance = node.vecvar[1]+n1*newWidth*(u1-newTot/newWidth)*(u1-newTot/newWidth)/(n1+newWidth)
        newVar = variance - incVariance 
        compressBucketsVec!(node, 1) 
        if node.bucketNumOfNonZeroes == 0
            removeFromTail!(lst) 
            newNumOfB = newNumOfB-1
        end 
    end 
    (newWidth, newNumOfB, newTot, newVar)
end 


mutable struct Adwin 
    mintMinLongitudWindow::Int   # aa
    mdblDelta::Float64           # ab
    currentTime::Int             # ac
    mintClock::Int               # ad
    mdblWidth::Int               # ae
    MAXBUCKETS::Int              # af 
    numOfBucketsInList::Int      # ag 
    TOTAL::Float64               # ah
    VARIANCE::Float64            # ai
    WIDTH::Int                   # aj 
    minWinLength::Int            # ak 
    listOfBuckets::ListOfBuckets # al
    function Adwin(aa,ab,ac,ad,ae,af,ag,ah,ai,aj,ak,al)
        new(aa,ab,ac,ad,ae,af,ag,ah,ai,aj,ak,al)
    end 
    Adwin() = Adwin(10,   # mintMinLongitudWindow 
                    0.01, # mdblDelta
                    0,    # currentTime
                    32,   # mintClock
                    0,    # mdblWidth
                    5,    # MAXBUCKETS
                    0,    # numOfBucketsInList
                    0.0,  # TOTAL
                    0.0,  # VARIANCE
                    0,    # WIDTH
                    5,    # minWinLength
                    ListOfBuckets(5)
                   )
end 

function setInput(obj::Adwin, x::Float64, delta ::Float64)
    ifChange        = false 
    ifReduceWidth   = true 
    obj.currentTime += 1
    rInsert = insertElementToListHead(obj.listOfBuckets,
                                      x,
                                      obj.WIDTH,
                                      obj.TOTAL,
                                      obj.VARIANCE, 
                                      obj.numOfBucketsInList, 
                                      obj.MAXBUCKETS)

    obj.WIDTH              = rInsert[1] # newWidth
    obj.TOTAL              = rInsert[2] # newTot
    obj.VARIANCE           = rInsert[3] # newVariance
    obj.numOfBucketsInList = rInsert[4] # numOfBuckets 

    if (obj.currentTime % obj.mintClock == 0 && obj.WIDTH > obj.mintMinLongitudWindow)
        while ifReduceWidth
            ifReduceWidth = false
            u0 = 0.0
            u1 = obj.TOTAL
            n0 = 0.0
            n1 = obj.WIDTH
            cursor = obj.listOfBuckets.tail
            i = obj.numOfBucketsInList

            while cursor != null
                bsz = calcBucketSize(i)

                ## todo: check if correctly tranlsated for julia case arrays
                kmax = (i==0) ? cursor.bucketNumOfNonZeroes-2 : cursor.bucketNumOfNonZeroes-1 

                k=1
                flag = false
                while k <= kmax && !flag 
                    if obj.VARIANCE<=0
                        println("! obj.VARIANCE=",obj.VARIANCE)
                    end 
                    rchp = ifChpCheck(cursor.vectot[k], n0, n1, u0, u1, ifReduceWidth, ifChange, bsz, delta, obj.VARIANCE, obj.WIDTH, obj.minWinLength)
                    n0 = rchp[1] # n0
                    n1 = rchp[2] # n1
                    u0 = rchp[3] # u0
                    u1 = rchp[4] # u1
                    ifReduceWidth = rchp[5] # ifRedW
                    ifChange      = rchp[6] # ifChp
                    flag          = rchp[7] # ifDelVals
                    if rchp[7]  
                        r = deleteValuesFromTailBucket(obj.listOfBuckets, obj.numOfBucketsInList, obj.WIDTH, obj.TOTAL, obj.VARIANCE)
                        obj.WIDTH              = r[1] # newWidth
                        obj.numOfBucketsInList = r[2] # newNumOfBuckets
                        obj.TOTAL              = r[3] # newTot
                        obj.VARIANCE           = r[4] # newVar
                    end 

                    k += 1
                end ## while k<=kmax && !flag 

                cursor = cursor.prev
                i -= 1
            end ## while cursor != null
        end ## while ifReduceWidth
    end 
    if false 
        println("\n  -----------------> Set input value: $x")
        printListOfBucket(obj.listOfBuckets)
    end 
    obj.mdblWidth += obj.WIDTH
    ifChange
end 


function testCases()
    println("... Run tests")
    b = Bucket(4)
    for k=1:5
        insertNewValues!(b,0.0+k,0.0+k)
    end 
    @assert b.vectot == [1.0,2.0,3.0,4.0,5.0]
    @assert b.bucketNumOfNonZeroes == 5
    compressBucketsVec!(b,1)
    @assert b.bucketNumOfNonZeroes == 4
    @assert b.vectot == [2.0,3.0,4.0,5.0, 0.0]
    println("... Tests are OK")
end 
# testCases()




function adwinDetector(xs, sensitivity = .01)
    A = Adwin()
    detections::Array{Int64,1} = []
    i = 0
    for x in xs
        i += 1
        if setInput(A, x, sensitivity)
            push!(detections,i)
        end 
    end 
    detections
end 



function runDetector()
    A = Adwin()
    fname = "C:/1maslov/tmp/tstsig2.txt"
    sig  = readdlm(fname)
    i = 0 
    for x in sig#[1:50] ## [1.0:30.0;]
        i += 1
        if setInput(A, x, .01) 
            println("Change detected: $i")
        end 
    end 
end 
##runDetector()

end # module
