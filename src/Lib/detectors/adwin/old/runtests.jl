using Base.Test

include("./List.jl")
include("./Adwin.jl")

#-- start test counter
a = ListIt()
b = ListIt()
c = ListIt()
@test myCounter(a) == 1
a.next = b
b.previous = a
@test myCounter(a) == 2
a.next.next = c
c.previous = b
@test myCounter(a) == 3
#-- end test counter

L = List()
for i = 1:4
  addToHead!(L)
end

removeFromHead!(L)
@test myCounter(L.head) == 4
removeFromHead!(L)
@test myCounter(L.head) == 3
#
removeFromTail!(L)
# @test myCounter(L.head) == 2   ----------- todo FIX test case
run_adwin()



# addToTail!(L)
# @test myCounter(L.head) == 3

#################### end
# @test myCounter(L.head) == 3
#removeFromHead!(L)

#println("L.count = ", L.count)
#clearLst!(L)
#println("L.count = ", L.count)
#addToTail!(L)
#addToTail!(L)
#println("L.count = ", L.count)
#removeFromTail!(L)
#println("L.count = ", L.count)
#removeFromTail!(L)
#println("L.count = ", L.count)

#addToHead!(L, item)
#println("L.count = ", L.count)
#println(isnull(item.next))

#mutable struct ListItem
#    id::Int
#    next::Nullable{ListItem}
#    ListItem(idnum) = (x = new();
#                       x.id=idnum;
#                       x.next = Nullable{ListItem}();
#                       x)
#end
#
#a = ListItem(1)
#b = ListItem(2)
#c = ListItem(3)
#a.next = b
#b.next = c
#
#println(isnull(a.next))
#println(isnull(b.next))
#println(isnull(c.next))
#
