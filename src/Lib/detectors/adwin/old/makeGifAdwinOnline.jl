using Plots
#using Blink
plotlyjs();
#Blink.AtomShell.install()

include("./AdwinNaive.jl")
r, y = demo_adwin_naive();
chps = extractChps(r)
println(chps)

n = length(y)
max_y = maximum(y)
min_y = minimum(y)
max_r = maximum(r)
chps_to_plot = []

anim = @animate for i=1:n
    p1 = plot(y[1:i], xlim = (1,n), ylim = (min_y-1, max_y+1), color=:black, marker=:circle, label="Signal")
    if i in chps
        push!(chps_to_plot, i)
    end
    if length(chps_to_plot) > 0
        plot!(chps_to_plot, linetype=:vline, color =:red, label="Change")
    end
    p2 = plot(r[1:i], xlim=(1,n), ylim=(0,max_r), linewidth=2, linecolor=:blue, label="Adaptive Window Size")
    if length(chps_to_plot) > 0
        plot!(chps_to_plot, linetype=:vline, color =:red, label="")
    end
    plot(p1, p2, layout=@layout([a; b]))
end

gif(anim, "./tmp/adwin_online.gif", fps = 5)
