


### Start 31.07.2017

# "Check if the condition holds"
# function condForReduce2(subsig, delta)
#     n = length(subsig)
#     @assert n > 1
#     for i = 1 : n-1
#         s0 = subsig[1:i]
#         s1 = subsig[(i+1):n]
#         ecut = calcEcut(length(s0), length(s1), delta)
#         #ecut = calcEcut(subsig[1:n], length(s0), length(s1), delta)
#         m0 = mean(s0)
#         m1 = mean(s1)
#         if abs(m1-m0) >= ecut
#             return true
#         end
#     end
#     return false
# end
#
# function condForReduce(subsig, ecut_custom)
#     n = length(subsig)
#     @assert n > 1
#     for i = 1 : n-1
#         s0 = subsig[1:i]
#         s1 = subsig[(i+1):n]
#         m0 = mean(s0)
#         m1 = mean(s1)
#         if abs(m1-m0) >= ecut_custom
#             return true
#         end
#     end
#     return false
# end

# function test_cond()
#     println( cond([1, 2;], 1))
#     println( cond([1, 2;], 0.9))
# end
# test_cond()
#
# function recReduce!(w, ecut_custom)
#     """OLD"""
#     if length(w) > 1
#         if condForReduce(w, ecut_custom)
#             recReduce!(w[1:(end-1)], ecut_custom)
#         end
#     else
#         return w
#     end
# end
#
# function test_recReduce()
#     function loctest(_v; e = 1.0)
#         recReduce!(_v, e)
#         println(_v)
#     end
#     loctest([1.0])
#     loctest([1.0, 3.0, 3.1])
#     loctest([1.0, 3.0, 3.1, 3.1, 3.1])
#     loctest([1.0, 1.0, 1.0, 1.0, 1.0])
#     loctest([1.0, 1.0, 1.0, 4.0, 4.0])
# end
# #test_recReduce()


### End 31.07.2017











include("./ListItem.jl")
include("./List.jl")

mutable struct AdwinObject
    DELTA                   :: Float64
    mintMinimLongitudWindow :: Int
    mdbldelta               :: Float64
    mintTime                :: Int
    mintClock               :: Int
    mdblWidth               :: Float64  # Mean of Width = mdblWidth/Number of items
    #
    #BUCKET
    MAXBUCKETS              :: Int
    lastBucketRow           :: Int
    TOTAL                   :: Float64
    VARIANCE                :: Float64
    WIDTH                   :: Int
    BucketNumber            :: Int
    #
    Detect                  :: Int
    numberDetections        :: Int
    DetectTwice             :: Int
    blnBucketDeleted        :: Bool
    BucketNumberMAX         :: Int
    mintMinWinLength        :: Int

    listRowBuckets          :: List
end

function initAdwinObject()
    AO = AdwinObject()

end

function adwin(d::Float64)
    DELTA::Float64 = 0.002
    mintMinimLongitudWindow::Int = 10
    mdbldelta::Float64 = 0.002
    mintTime::Int = 0
    mintClock::Int = 32
    mdblWidth::Float64 = 0 # Mean of Width = mdblWidth/Number of items

    #BUCKET
    MAXBUCKETS::Int = 5
    lastBucketRow::Int = 0
    TOTAL::Float64 = 0
    VARIANCE::Float64 = 0
    WIDTH::Int = 0
    BucketNumber::Int = 0

    Detect::Int = 0
    numberDetections::Int = 0
    DetectTwice::Int  = 0
    blnBucketDeleted::Bool = false
    BucketNumberMAX::Int = 0
    mintMinWinLength::Int = 5

    listRowBuckets::List = List()

    function insertElement!(value::Float64)
        WIDTH +=1
    end

    #function setInput!(intEntrada::Float64)
    #    setInput!(intEntrada, mdbldelta)
    #end

    function setInput!(intEntrada::Float64, delta::Float64 = mdbldelta)
        blnChange::Bool = false
        blnExit::Bool = false
        cursor::ListIt
        #mintTime += 1

        # 1,2)Increment window in one element
        #insertElement!(intEntrada)
    end

    for p in 0:999
        x = p < 500 ? 1000.0 : 500.0
        setInput!(x)
    end

end

adwin(0.01)

function a()
    a = 0
    function setInput(b)
        a = a+1
        c = b
    end
    println(a)
    setInput(5)
    println(a)
end
a()
