# J.Gama book
# p. 41 (el.55) - CUSUM
#
# 2018-08-22: Move dynamic sensitivity functionality into static
# detector.
#

module PageHinkleyPccfDynamic

export detectorPhPccfDynamic

update_mu(x, muOld, nOld) = (nOld * muOld+x)/(nOld+1.0)

"""
lambdaDynamic::Array{Int61,1}: binary vector [1,0,...]
1: indicates for increased sensitivity
0: indicates for optimal static settings
"""
function detectorPhPccfDynamic(y::Array{Float64,1},
                               delta::Float64,
                               lambdaDynamic::Array{Int64,1};
                               lambdaWhenDynamic0::Float64 = 0.05,
                               lambdaWhenDynamic1::Float64 = 0.1,
                               wait_time = 10,
                               alpha = 1-0.001)::Array{Int64,1}
    # println("len(y)=", length(y),"; len(lambdaDynamic)=", length(lambdaDynamic))
    @assert length(y) == length(lambdaDynamic)
    n=length(y)
    runningSum = 0.0 ## running sum
    changeDetected = false
    n_r = 0
    mu_r = 0.0
    s_plus = 0.0
    s_minus = 0.0
    s_vec_plus = zeros(n) ## positive changes
    s_vec_minus = zeros(n) ## negative changes
    detections = []
    for i=1:n
        lambda = lambdaDynamic[i] == 0 ? lambdaWhenDynamic0 : lambdaWhenDynamic1
        x = y[i]
        if n_r == 0
            mu_r = x
            n_r = 1
            s_plus = 0.0
            s_minus = 0.0
        else
            mu_r = update_mu(x, mu_r, n_r)
            n_r += 1
            s_plus = s_plus * alpha + (x - mu_r - delta)
            s_minus = s_minus * alpha - (x - mu_r - delta)
            if s_plus > lambda || s_minus > lambda
                changeDetected = true
                n_r = 0
                mu_r = 0.0
                s_plus = 0.0
            else
                changeDetected = false
            end
        end
        s_vec_plus[i] = s_plus
        s_vec_minus[i] = s_minus
        if changeDetected
            # println("Change :", i)
            push!(detections, i)
        end
    end
    # writedlm("C:/1maslov/tmp/sp.txt", s_vec_plus)
    # writedlm("C:/1maslov/tmp/sm.txt", s_vec_minus)
    #writedlm("C:/1maslov/tmp/detections.txt", detections)
    detections
end

function run()
    ## x=vcat(randn(15), randn(15)+10, randn(15)-10);  writedlm("C:/1maslov/tmp/tstsig_small.txt",x)
    ##
    # sig = readdlm("C:/1maslov/tmp/tstsig_small.txt")
    sig = readdlm("C:/1maslov/tmp/tstsig2.txt")
    println(size(sig))
    detectorPH(sig, 0.0, 50)
end
#run()

end # module
