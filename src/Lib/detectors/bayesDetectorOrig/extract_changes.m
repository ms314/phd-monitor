function chps = extract_changes(seq)
    n = length(seq);
    chps = zeros(n);
    for i = 1: n
        chps(i) = i - seq(i);
    end 
    chps = unique(chps);
end 

%ret=extract_changes([1,2,3,4,5,3]);
%disp(ret);
