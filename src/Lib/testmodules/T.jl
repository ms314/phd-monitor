
abstract type AType end
struct NoneVal <: AType end 
struct SomeVal <: AType
    x::Int64
end