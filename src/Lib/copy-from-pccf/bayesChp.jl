using Distributions

module BayesChp
export bchp

function hazard_func(r, lambda)
    (1.0 / lambda) * ones(size(r))
end

function gauss_pdf(ms, val, sigma = 1)
  res = Float64[]
  for m in ms
    d = Normal(m, sigma)
    pdfgauss = pdf(d, val)
    push!(res, pdfgauss)
  end
  res
end
#println( gauss_pdf([1,2,3], 2.9) )

function estimate_mus(x)
  n = length(x)
  mus = Float64[]
  for i = 1:(n-1)
    subvec = x[(n-i) : end]
    push!(mus, mean(subvec))
  end
  #if n == 1
  #  push!(mus, x[1])
  #end
  mus
end
r = estimate_mus([1, 1, 5, 6])
print(r)

function bchp(X, mu0, mdist)
  T = length(X)
  R = zeros(T+1, T+1)
  R[1,1] = 1
  maxes = zeros(T+1)
  for t = 1 : T-1

    mus = estimate_mus(X[1:(t+1)])
    predprobs = gauss_pdf(mus, X[t], 1.0)

    H = hazard_func(collect(1:t), mdist)

    # Growth probabilities
    R[2:t+1, t + 1] = R[1:t, t] .* predprobs .* (1 - H)

    # Probability that there was a changepoint
    R[1, t+1] = sum( R[1:t, t] .* predprobs .* H )

    # Normalize
    R[:, t+1] = R[:, t+1] ./ sum(R[:, t+1])
    #
    maxes[t] = indmax(R[:, t])
  end
  R
  maxes
end

end
sig = readdlm("D:/MyTemp/gitlocal/recurrency-code/src.jl/sigtest.txt")

R = bchp(sig, 1.1, 100)
writedlm("D:/MyTemp/gitlocal/recurrency-code/src.jl/probs100.txt", R)

# R = bchp(sig, 1.1, 50)
# writedlm("D:/MyTemp/gitlocal/recurrency-code/src.jl/probs50.txt", R)
#
# R = bchp(sig, 1.1, 200)
# writedlm("D:/MyTemp/gitlocal/recurrency-code/src.jl/probs200.txt", R)
