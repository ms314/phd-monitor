using JSON
using Gadfly

include("./detectors/cusum.jl")

function run_cusum()
  jcont = JSON.parsefile("./config.json")
  sig_dat = JSON.parsefile(jcont["in_art_sig1"])
  y = sig_dat["sig"]

  chps_detected = cusum(y, update_width=10, expected_change=1.0, threshold=0.3)

  println("True changes :", sig_dat["changes"])
  println("Detected changes:", chps_detected)

  ts = [1:length(y);]
  println(length(y))
  if true
    l1 = layer(x=ts, y=y, Geom.point)
    l2 = layer(xintercept=chps_detected, Geom.vline(color="black"))
    l3 = layer(xintercept=sig_dat["changes"], Geom.vline(color="red"))
    draw(PNG(jcont["out_art_sig1_fig"], 12inch, 6inch), plot(l1, l2, l3,  Guide.title("Read: changes, black: detections.")))
    println(".. Save PNG into ", jcont["out_art_sig1_fig"])
  end
end
run_cusum()
