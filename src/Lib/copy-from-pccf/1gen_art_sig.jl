using JSON
include("./lib/SignalGenerator.jl")
using SignalGenerator

# function genSig(n, mu, sigma, changeStep, sigStd)
function gen_sig1()
  jcont = JSON.parsefile("./config.json")
  saveto = jcont["out_art_sig1"]
  # n, mu, sigma, changeStep, sigStd)
  sig = genSig(n=10, mu=100, sigma=10, changeStep=1.0, sigStd = 0.3)
  f = open(saveto, "w")
  JSON.print(f, sig)
  close(f)
  println("Saving into $saveto")
end
gen_sig1()
