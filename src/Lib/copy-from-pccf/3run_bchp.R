# copy from IJCNN - test1_bd.R
library(jsonlite)
library(ggplot2)
s=fromJSON("./config.json")

sig <-read.table(s$output$bchp_test1$signal)$V1 #(fpath("test_bd_signal.txt"))$V1

changes <- read.table(s$output$bchp_test1$changes)$V1 #(fpath("test_bd_changes.txt"))$V1
detections <- read.table(s$output$bchp_test1$detections)$V1 #(fpath("test_bd_detections.txt"))$V1
matrix_max_vals <- read.table(s$output$bchp_test1$matrix_max_vals)$V1 # (fpath("test_bd_matrix_max_vals.txt"))$V1

pdata <- data.frame(sig=sig, t=seq(1, length(sig)))

pl <- ggplot(data=pdata, aes(x=t, y=sig)) + geom_line() + geom_point()
pl <- pl + theme_bw() + ylim(-3,3)
pl <- pl + geom_vline(xintercept = detections, color="blue")
pl <- pl + geom_vline(xintercept = changes, color="red")
pl
