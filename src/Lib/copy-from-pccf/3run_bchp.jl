# (2017.07.21) copied from IJCNN2017 code base (file: test1_bd.jl)
#
# Test Bayesian Detector

include("./detectors/2bayes_detector_replica.jl")
# include("./lib/SignalGenerator.jl")
include("./lib/roc.jl")
# using SignalGenerator
using BayesChpOrig
using JSON
# using Metadata
using Roc

function run_sig_gen()
    ret = genSig(5, 100.0, 1.0, 2.0, 0.5, 1)
    sig = ret["sig"]
    chps= ret["changes"]
    writedlm(outdir*"testsig.txt", sig)
    writedlm(outdir*"testsigchps.txt", chps)
    print("run1")
end


"""
Simply run Bayesian detector on a single one dimensional signal.
Save detections and changes into files.
Plot using R.
"""
function run_detector()
    jcont = JSON.parsefile("./config.json")

    sig_dat = JSON.parsefile(jcont["input"]["art_sig1"])
    sig = sig_dat["sig"]
    #ret = genSig(10, 100.0, 10.0, 1.3, 0.75, 1)
    chps= sig_dat["changes"]

    matrix_max_vals, matrix = bayes_detector(sig, lambda = 120)
    detections = extract_changes(matrix_max_vals)

    max_delay = 30
    tpr = tpRate(detections, chps, max_delay)
    fpr = faRateScaled(detections, chps, max_delay, length(sig), tn_price_scale=0.1)
    println("... test_bd > TPrate = ", tpr, " / FArate = ", fpr)

    writedlm(jcont["output"]["bchp_test1"]["detections"], detections)
    writedlm(jcont["output"]["bchp_test1"]["signal"], sig)
    writedlm(jcont["output"]["bchp_test1"]["changes"], chps)
    writedlm(jcont["output"]["bchp_test1"]["matrix"], matrix, ",")
    writedlm(jcont["output"]["bchp_test1"]["matrix_max_vals"], matrix_max_vals, ",")
end
run_detector()
