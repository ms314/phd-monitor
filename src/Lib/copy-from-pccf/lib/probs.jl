module Probs
include("./samplesGenerator.jl")
export generateChanges
# export ifDetected, notDetected, extractFps, extractTps, belowH, aboveH

function ifDetected(cde::Int64, changes::Vector{Int64}, delay::Int64)
    # 1) If change detection event is TP
    for chp in changes
        if abs(chp - cde) <= delay
            return true
        end
    end
    return false
end

function notDetected(cde::Int64, changes::Vector{Int64}, delay::Int64)
    # 2) If change detection event is not TP
    return ! ifDetected(cde, changes, delay)
end

function extractFps(detections::Vector{Int64},
                    trueChanges::Vector{Int64},
                    delay::Int64)
    # 3) Extract False Positives from a sequence of CDEs
    filter(x->notDetected(x, trueChanges, delay), detections)
end

function extractTps(detections::Vector{Int64},
                    trueChanges::Vector{Int64},
                    delay::Int64)
    # 4) Extract True Positives from a sequence of CDEs
    filter(x->ifDetected(x, trueChanges, delay), detections)
end

function ifPredictedTp(cde::Int64, probs::Vector{Float64}, h::Float64)
    # 5) Check if an individual TP is correctly predicted
    probs[cde] >= h
end

function ifPredictedFp(cde::Int64, probs::Vector{Float64}, h::Float64)
    # 6) Check if an individual FP is correctly ignored
    probs[cde] < h
end

function correctlyPredictedTps(probs::Vector{Float64},
                               trueChanges::Vector{Int64},
                               h::Float64)
    # 7) Given probs vector count those provided true changes for
    # which corresponding probs >= h. Time locations must be integers
    filter(x->ifPredictedTp(x, probs, h), trueChanges)
end

function correctlyPredictedFps(probs::Vector{Float64},
                               fPositives::Vector{Int64},
                               h::Float64)
    # 8) Given probs vector count those provided false alarms for
    # which corresponding probs < h. Time locations must be integers
    filter(x->ifPredictedFp(x, probs, h), fPositives)
end

function belowH(h::Float64, probs::Vector{Float64}, delay::Int64)
    # 9) Returns _indices_ given a vector of probs and threshold
    # function. Returs indices of those elements in probs vector which
    # are below threshold value
    ret :: Vector{Int64} = []
    subprobs = probs[1 : min(delay, length(probs))]
    ret = find( x -> x <= h, subprobs)
end

function aboveH(h, probs, delayConfirm)
    # 10)
    subprobs = probs[1 : min(delayConfirm, length(probs))]
    find(x->(x>h), subprobs)
end

#function initProbs(mu, sigma)
#    # 11)
#    0
#end
#
#function whichBin(num, edges)
#    # 12) Edges:[1,2,3] Bins:[(-inf,1) (1,2) (2,3) (3, inf)] Bins nums: 1,2,3,4
#    n = length(edges)
#    if num <= edges[1]
#        return 1
#    elseif num > edges[end]
#        return n+1
#    else
#        for i in 2:n
#            if edges[i-1] < num <= edges[i]
#                return i
#            end
#        end
#    end
#end

function gaussPdf(x, mu, sigma)
    k = 1/(sigma * sqrt(2*pi))
    p = k * exp(-((x-mu)^2/(2*sigma^2)))
end

# 13, 14 - don't need
function generateChanges(n, mu, sigma, width)
    # 15) Generate sequence of changes
    changes::Array{Int,1} =[]
    values = 1:width
    weights = map(x->gaussPdf(x, mu, sigma), 1:width)
    samples = genSamples(values, weights, n)
    changes = cumsum(samples)
end

function dynamicH(probs,threshold,hmin,hmax,tmax)
    dynamich::Vector{Float64}
    dynamich=map(probs) do p
        if p>=threshold
            hmin
        else
            hmax
        end
    end
    dynamich[1:tmax]
end

end
#------------------ End module Probs-------------------------#

#------------------------------------------------------------#
# module Pmf
# export gaussPmf
#
# using Distributions
# using Bisect
#
# type GaussPmf
#     # Prototype is in thinkbayes.py/def MakeGaussianPmf(mu, sigma, num_sigmas, n=201):
#     mu         :: Float64
#     sigma      :: Float64
#     num_sigmas :: Int
#     n          :: Int
#     edges      :: Vector{Float64}
#     probs      :: Vector{Float64}
#     function GaussPmf(mu::Float64, sigma::Float64)
#         n_default          = 251
#         num_sigmas_default = 3
#         low                = mu - num_sigmas_default * sigma
#         high               = mu + num_sigmas_default * sigma
#         d                  = Normal(mu, sigma)
#         edges              = linspace(low, high, n_default)
#         probs              = pdf(d, edges)
#         new(mu, sigma, num_sigmas_default, n_default, edges, probs)
#     end
# end
#
# function gaussPmf(gp::GaussPmf, val)
#      Bisect.idx_of_closest(gp.probs, float(val))
# end
#
#end
#----------------- End module Pmf ------------------------------#
