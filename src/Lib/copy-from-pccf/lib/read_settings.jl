import JSON

function readJSON(json_file="./config.json")
    open(json_file) do f
        r = JSON.parse(f)
        return r
    end
end


# using JSON

# function readJSON(fpath)
#   jcont = JSON.parsefile(fpath)
#   for (a,b) in jcont
#     println(jcont[a],b)
#   end
#   println(jcont["input"]["art_sig1"])
# end
# readJSON("./config.json")
