

"""

Find centers of the intervals defining locations of the noisy
observations recursively.

l       : left boundary 
r       : right boundary 
min_len : max length of the interval 

"""
function _recmidpoints(l, r, min_len)
    if r - l <= min_len
        [r]
    else
        m = div(r-l, 2)
        vcat(_recmidpoints(l, l + m, min_len), _recmidpoints(l + m, r, min_len))
    end
end

function recmidpoints(l, r, min_len)
    mids = _recmidpoints(l, r, min_len)
    mids[1:end-1]
end 

if 1 == 0
    ret = recmidpoints(1, 200, 50)
    println(ret)
end 
