source("../Lib/R/fn_read_changes_and_signals.R")
library(magrittr)
plt <- (function(x){plot(x, type = 'b', cex=0.5)})
plt2 <- (function(x,y){plot(x,y, type = 'l')})
rvec <- (function(x){read.table(x)$V1})

data <- read_changes_and_signals()

plt3 <- (function(pccf, chps, indf, indfPccf, titleText) {
  plot(pccf, type = "l", ann = FALSE)
  lines(indf*max(pccf)*0.5, col="red", type = "b")
  lines(indfPccf * max(pccf)*0.3, col="blue", type = "b", cex=0.7)
  mtext(titleText, side = 3)
  abline(v=chps)
})
# source("./plot11.R")

pccf1 <- "./target/step11/pccf_sig1.dat" %>% rvec
pccf2 <- "./target/step11/pccf_sig2.dat" %>% rvec
pccf3 <- "./target/step11/pccf_sig3.dat" %>% rvec
pccf4 <- "./target/step11/pccf_sig4.dat" %>% rvec
pccf5 <- "./target/step11/pccf_sig5.dat" %>% rvec
pccf6 <- "./target/step11/pccf_sig6.dat" %>% rvec

if1 <- "./target/step11/if_sig1.dat" %>% rvec
if2 <- "./target/step11/if_sig2.dat" %>% rvec
if3 <- "./target/step11/if_sig3.dat" %>% rvec
if4 <- "./target/step11/if_sig4.dat" %>% rvec
if5 <- "./target/step11/if_sig5.dat" %>% rvec
if6 <- "./target/step11/if_sig6.dat" %>% rvec

ifpccf1 <- "./target/step11/if_pccf_sig1.dat" %>% rvec
ifpccf2 <- "./target/step11/if_pccf_sig2.dat" %>% rvec
ifpccf3 <- "./target/step11/if_pccf_sig3.dat" %>% rvec
ifpccf4 <- "./target/step11/if_pccf_sig4.dat" %>% rvec
ifpccf5 <- "./target/step11/if_pccf_sig5.dat" %>% rvec
ifpccf6 <- "./target/step11/if_pccf_sig6.dat" %>% rvec


P = FALSE # TRUE

if(P){
  setEPS()
  postscript("./target/step11/step11_optim_pccfs_real_20190328_2.eps", width=14, height=5) 
}

par(mfrow=c(3,2),mar=c(1,2,2,1))
list(pccf=pccf1, chps=data$chps1, indf=if1, indfPccf=ifpccf1, titleText="sig1") %$% plt3(pccf, chps, indf, indfPccf, titleText)
list(pccf=pccf2, chps=data$chps2, indf=if2, indfPccf=ifpccf2, titleText="sig2") %$% plt3(pccf, chps, indf, indfPccf, titleText)
list(pccf=pccf3, chps=data$chps3, indf=if3, indfPccf=ifpccf3, titleText="sig3") %$% plt3(pccf, chps, indf, indfPccf, titleText)
list(pccf=pccf4, chps=data$chps4, indf=if4, indfPccf=ifpccf4, titleText="sig4") %$% plt3(pccf, chps, indf, indfPccf, titleText)
list(pccf=pccf5, chps=data$chps5, indf=if5, indfPccf=ifpccf5, titleText="sig5") %$% plt3(pccf, chps, indf, indfPccf, titleText)
list(pccf=pccf6, chps=data$chps6, indf=if6, indfPccf=ifpccf6, titleText="sig6") %$% plt3(pccf, chps, indf, indfPccf, titleText)
if(P){
  dev.off() 
}
