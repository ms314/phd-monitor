// Pccf optimization using binary classification metrics
package steps
import lib.pccf._
import lib.perfmetrics._
import lib.utils._

import lib.commondata._

object step13 extends inputData {

  def run():Unit = {
    val chps = Array(1,3)
    val indf = Array(0,2,0,5,0,0,5,7)
    assert(PerfMetricsBinPccf.tpNumIndf(indf, chps)==7)
//    println(PerfMetricsBinPccf.numOfChangesWithinIntervals(Array(1,1,1,0,0,0,1,1), Array(1)))
    PerfMetricsBinPccf.fpNumIndf(Array(1,1,1,0,0,0,1,1,1,0,0,0), Array(2, 3))
  }



}
