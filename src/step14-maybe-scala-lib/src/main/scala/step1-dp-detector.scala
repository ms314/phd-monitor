package steps
import lib.detectors._

object step1{
    def run():Unit={
      println(".. Run step1")
      val sig = Array(1.0, 1.0, 1.0,
                      2.0, 2.0, 2.0,
                      1.0, 1.0, 1.0, 1.0, 1.1)
      DpDetector.run(sig) foreach println
      println("End")
    }
}