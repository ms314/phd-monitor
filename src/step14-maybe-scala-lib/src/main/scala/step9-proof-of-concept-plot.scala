package steps
// make a plot like in paper by Venugopal V., et.al. paper

import breeze.linalg._
import breeze.plot._
import lib.detectors.Cusum._
import lib.utils._

object step9{
  def run():Unit={

    val outpathFig  = "./target/step9/cusum-out-stat.png"
    val outpathSig  = "./target/step9/inputSig.dat"
    val outpathStat = "./target/step9/cusumStat.dat"

    val r = new scala.util.Random(12) //8: FA case

    val sig1 = (1 to 500).map(_ => r.nextGaussian()*1.0).toArray //Array(1.0, 2.0, 3.0, 4.0)
    val sig2 = (1 to 500).map(_ => r.nextGaussian()*1.0 + 0.1).toArray
    val sig = sig1 ++ sig2
    val changes = Array(500)
    val q = cusumJustOutputStastic(sig, 0.0)

    UtilsIO.writeVec(outpathStat, q._1)
    UtilsIO.writeVec(outpathSig, sig)
    println(s"Save input signal and output stat into $outpathSig , $outpathStat")

    val f = Figure()
    f.subplot(2,1,1)

    val p1 = f.subplot(0)
    p1.title="Input signal"
    p1.xlabel="time"

    val p2 = f.subplot(1)
    p2.title = "Cusum output statistic"
    p2.xlabel="time"
    val x = (0 to sig.length-1).toArray.map(x=>x.toDouble)

    p1 += plot(DenseVector(x), DenseVector(sig),'-')

    p2 += plot(DenseVector(x), DenseVector(q._1),'.', "blue")
    p2 += plot(DenseVector(x), DenseVector(q._1),'.', "blue")

    //    p2 += plot(DenseVector(x), DenseVector(sig),'.', "red")

    f.saveas(outpathFig)
    println(s"Save figure into $outpathFig")
  }
}