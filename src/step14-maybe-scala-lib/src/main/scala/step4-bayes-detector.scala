package steps
import breeze.numerics._
import breeze.linalg.{linspace, _}
import breeze.plot._
import lib.detectors._
import lib.learndetectors.LearnDetectors
import lib.types._
import lib.utils.{AdaptiveSettings, PlotLib}

object step4{
  def runBayesDemo(): Unit ={
    BayesianDetector.demoBayesDetectorOutputMatrix("./target/bdetector_probs.png")
  }

  def runSimBayes()={
    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 0.5).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val changes = Array(100, 200)
    //
    val paramrange = linspace(1.0, 35.0, 100).toArray
    val optStaticBayes = LearnDetectors.optim(sig, changes, LearnDetectors.createBayesRef(0.0), paramrange)
    val indF = AdaptiveSettings.constructIndFunction(sig.length, changes.toList, 20, 10)
    val optDynamicBayes = LearnDetectors.optim(sig, changes, LearnDetectors.createBayesRefDynamic(indF, 0.0, 300.0), paramrange)
    //
//    PlotLib.plotIndicatorFunction(indF)
    //
    PlotLib.plotStatAndDynOutputStep4(optStaticBayes, optDynamicBayes, indF)
  }

  def simpleTest()={
    val sig = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    //    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 0.0).toArray
    //    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    //    val sig = sig1 ++ sig2 ++ sig3
    val q = BayesianDetector.bDetector(sig, StaticSettings(100.0), 0.0)
    //    val m = rb._1
    //    rb._2 foreach println
    //    val q= BayesianDetector.extractChanges(rb._2)
    println(">>>>>>>>>.")
    println(q.mkString(","))
  }

  def run()={
    runSimBayes()
  }
}
