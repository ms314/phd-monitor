package steps
import breeze.linalg._
import lib.utils._
import breeze.plot._
import lib.commondata.inputData
import lib.perfmetrics._
import lib.types._
import lib.detectors._
import lib.learndetectors._

object step3 extends inputData {

//  abstract class DetectorOutput
//  case class CusumOutput(cdes:Array[Int], sPos:Array[Double], sNeg:Array[Double]) extends DetectorOutput
//  case class SignAndOutStat(sig:Array[Double], outStat:CusumOutput) extends DetectorOutput
//  case class OptimOutput(sensitivityV:Array[Double], delays:Array[Double], nubOfFa:Array[Double], lossV:Array[Double]) extends DetectorOutput

  def fnPlot(data:DetectorOutput, outfig:String): Unit = data match {
    case CusumOutput(cdes, sPos, sNeg) => {
      val f = Figure()
      f.subplot(2,1,1)
      val p1 = f.subplot(0)
      val p2 = f.subplot(1)
      val n = sPos.length
      val x = (1 to n).toArray.map(_.toDouble)
      p1 += plot(DenseVector(x), DenseVector(sPos))
      p2 += plot(DenseVector(x), DenseVector(sNeg))
      f.saveas(outfig)
    }
    case SignAndOutStat(ys, CusumOutput(_, sPos, _)) => {
      val f = Figure()
      f.subplot(2,1,1)
      val p1 = f.subplot(0)
      val p2 = f.subplot(1)
      val n = ys.length
      val x = (1 to n).toArray.map(_.toDouble)
      p1 += plot(DenseVector(x), DenseVector(ys))
      p2 += plot(DenseVector(x), DenseVector(sPos))
      f.saveas(outfig)
    }
  }

  def plotCostFunctions(): Unit ={
    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray //Array(1.0, 2.0, 3.0, 4.0)
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 5.0).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val changes = Array(100, 200)
    val indF = AdaptiveSettings.constructIndFunction(sig.length, changes.toList, 20, 20)
    val optStatic = LearnDetectors.optim(sig, changes, LearnDetectors.createCusumRef(0.0))
    val optDynamic = LearnDetectors.optim(sig, changes, LearnDetectors.createCusumRefDynamic(indF, 0.0))

    val f = Figure()
    f.subplot(3,1,1)
    val p1 = f.subplot(0)
    p1.title = "Delay"
    p1.xlabel="sensitivity"
    val p2 = f.subplot(1)
    p2.title="Num. of FAs"
    p2.xlabel="sensitivity"
    val p3 = f.subplot(2)
    p3.title="Cost function"
    p3.xlabel="sensitivity"
    val x = optStatic.sensitivityV
    p1 += plot(DenseVector(x), DenseVector(optStatic.delays),'.')
    p1 += plot(DenseVector(x), DenseVector(optDynamic.delays),'.', "red")
    p2 += plot(DenseVector(x), DenseVector(optStatic.numOfFa),'.')
    p2 += plot(DenseVector(x), DenseVector(optDynamic.numOfFa),'.', "red")
    p3 += plot(DenseVector(x), DenseVector(optStatic.lossV),'.')
    p3 += plot(DenseVector(x), DenseVector(optDynamic.lossV),'.', "red")
    val outfig = "./target/out-costf-2019-02-24.PNG"
    f.saveas(outfig)
  }

  def plotOutputStatistics(): Unit ={
    Cusum.demoPlotCusumOutputStatistic("./target/demo-cusum-out-stat.png")
  }

  def diagCusumRunWithSettings():Unit={
    val sig = UtilsIO.read(sigMap(1))
    val chps = UtilsIO.read(chpMap(1)).map(_.toInt)
    val n = sig.length
    var r = Cusum.cusumPh(sig, 0.0, StaticSettings(50))

    val f = Figure()
    f.subplot(3,1,1)
    val p1 = f.subplot(0)
    p1.title = "Input signal"
//    p1.xlabel="sensitivity"
    val p2 = f.subplot(1)
//    p2.title="Num. of FAs"
//    p2.xlabel="sensitivity"
    val p3 = f.subplot(2)
//    p3.title="Cost function"
//    p3.xlabel="sensitivity"
    val x = (1 to n).toArray.map(_.toDouble)
    p1 += plot(DenseVector(x), DenseVector(sig),'.', "black")
    p1 += plot(DenseVector(x), DenseVector(sig),'-', "black")
    p2 += plot(DenseVector(x), DenseVector(r.sPos),'.', "black")
    p2 += plot(DenseVector(x), DenseVector(r.sPos),'-', "black")
    p3 += plot(DenseVector(x), DenseVector(r.sNeg),'.', "black")
    p3 += plot(DenseVector(x), DenseVector(r.sNeg),'-', "black")
    val outfig = "./target/step3/example.PNG"
    f.saveas(outfig)
  }

  def run()={
    plotCostFunctions()
//    plotOutputStatistics()
  }
}