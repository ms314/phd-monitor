package tasks

class Runner{
  val taskNum:Int=9
  val msg = "P" + taskNum.toString

  def sayBegin(msg:String):Unit={
    println("..Run task P"+msg)
  }

  def sayStop():Unit={
    println("End")
  }
}

package binarytree {

  sealed abstract class Tree[+T]

  case class Node[+T](value: T, left: Tree[T], right: Tree[T]) extends Tree[T] {
    override def toString = "T(" + value.toString + " " + left.toString + " " + right.toString + ")"
  }

  case object End extends Tree[Nothing] {
    override def toString = "."
  }

  object Node {
    def apply[T](value: T): Node[T] = Node(value, End, End)
  }

  object Tree{
    def p55():Unit={
      // http://aperiodic.net/phil/scala/s-99/
      // p55
//      def cBalanced(n:Int, content:String): Tree[String] = {
//
//      }
    }
  }


}



object tasks99 extends Runner {

  def p01(): Unit = {
    // Find the last element of a list.
    // Example: scala> last(List(1, 1, 2, 3, 5, 8))
    // res0: Int = 8
    def lastElem[A](ys:List[A]):A = ys match {
      case x::Nil => x
      case _::tail => lastElem(tail)
      case _ => throw new NoSuchElementException
    }
    val q = lastElem(List(1, 1, 2, 3, 5, 8))
    println(q)
  }


  def p02()={
    // Find the last but one element of a list.
    // Example: penultimate(List(1, 1, 2, 3, 5, 8))
    // res0: Int = 5
    def lastButOne[A](ys:List[A]):A = ys match {
      case x::_::Nil => x
      case _ :: tail => lastButOne(tail)
      case _ => throw new NoSuchElementException
    }
    val q = lastButOne(List(1, 1, 2, 3, 5, 8))
    println(q)
  }


  def p03(): Unit ={
    // Find the Kth element of a list.
    // By convention, the first element in the list is element 0.
    // Example: scala> nth(2, List(1, 1, 2, 3, 5, 8)) > res0: Int = 2
    def nthNoCases[A](k:Int, ys:List[A]):A={
      if(ys.length < k)
        throw new NoSuchElementException
      else {
        def aux[A](k:Int, ys:List[A], acc:Int):A={
          if (acc == k) ys.head
          else aux(k, ys.tail, acc+1)
        }
        aux(k, ys, 1)
      }
    }

    def nthRecursive[A](n:Int, ls:List[A]):A = (n, ls) match {
      case (0, h:: _) => h
      case (k, _::t) => nthRecursive(k-1, t)
      case _ => throw new NoSuchElementException
    }

    val q = nthNoCases(6, List(1, 1, 2, 3, 5, 8))
    val q2 = nthRecursive(5, List(1, 1, 2, 3, 5, 8))
    println(">>> ", q,q2)
  }


  def p04():Unit={
    //scala> length(List(1, 1, 2, 3, 5, 8))
    //res0: Int = 6
    sayBegin("04")
    def lengthRec[A](lst:List[A]):Int = lst match {
      case Nil => 0
      case _::t => 1 + lengthRec(t)
    }
    val r = lengthRec(List(1,2,3))
    println(s"result = $r")
    sayStop()
  }

  // def p05():Unit={
  //   // Reverse a list.
  //   // scala> reverse(List(1, 1, 2, 3, 5, 8))
  //   // res0: List[Int] = List(8, 5, 3, 2, 1, 1)
  //   def revLst[A](ls: List[A], acc:List[A]): List[A] = ls match {
  //     case Nil => ls::acc
  //     case h::t =>
  //   }
  // }


  def p05():Unit={
    // scala> reverse(List(1, 1, 2, 3, 5, 8))
    // res0: List[Int] = List(8, 5, 3, 2, 1, 1)
    def reverse[A](ls:List[A]):List[A]={
      ls.foldRight(List[A]())((a,b) => b :+ a)
    }
    sayBegin("p05")
    println(reverse(List(1,2,3)))
    sayStop()
  }

  def p08():Unit={
    // P08: Eliminate consecutive duplicates of list elements.
    // compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
    //  List[Symbol] = List('a, 'b, 'c, 'a, 'd, 'e)
    def compress[A](ls:List[A], acc:List[A]=Nil):List[A] = ls match {
      case Nil => acc.reverse
      case h::t => acc match {
        case Nil => compress(t, h::acc)
        case _ => if (h == acc.head) compress(t, acc) else compress(t, h::acc)
      }
    }
    def compressWithDrop[A](ls:List[A]):List[A] = ls match {
      case Nil => Nil
      case h::tail => h :: compressWithDrop(tail.dropWhile(_==h))
    }
    def compressWithFoldR[A](ls:List[A]):List[A] =  {
      ls.foldRight(List[A]()){ (h,r) => {if (r.isEmpty || r.head != h) h::r else r} }
    }
    println(compress(List('a,'a,'a,'a,'b,'c,'c,'a,'a,'d,'e,'e,'e,'e)))
    println(compressWithDrop(List('a,'a,'a,'a,'b,'c,'c,'a,'a,'d,'e,'e,'e,'e)))
    println(compressWithFoldR(List('a,'a,'a,'a,'b,'c,'c,'a,'a,'d,'e,'e,'e,'e)))
  }


  def p09():Unit = {
    // P09: Pack consecutive duplicates of list elements into sub-lists.
    // pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a))
    //  List[List[Symbol]] = List(List('a, 'a, 'a, 'a), List('b), ...
    def pack[A](ls: List[A]): List[List[A]] = {
      if (ls.isEmpty) List(List())
      else {
        val (p, n) = ls span { _ == ls.head }
        if (n == Nil) List(p) else p :: pack(n)
      }
    }

    println("p09()")
    println(pack(List(1, 1, 1, 2, 3, 3, 3)))
  }

  def p10():Unit = {
    // encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
    // res0: List[(Int, Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
    def encode[A](ls:List[A]):List[(Int, A)] = {
      if (ls.isEmpty) List():List[(Int, A)] //List((0, ls.head))
      else {
        val (p, n) = ls span {_ == ls.head}
        if (n==Nil) List((p.length, p.head)) else (p.length, p.head) :: encode(n)
      }
    }
    println("p10()")
    println(encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)))
  }

  def p12():Unit={
    def decode[A](ls:List[(Int, A)]):List[A]={
      ls.flatMap{x => List.fill(x._1)(x._2)}
    }
    println("p12()")
    println(decode(List((1, 'a), (3, 'b))))
  }

  def run():Unit={
    p05()
  }


}
