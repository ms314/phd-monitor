// Plot I.F. from changepoints locations
// Plot Pccf on top of it
// calc overlap
// plot optimal Pccf
///
package steps

import breeze.linalg._
import breeze.stats.distributions._
import breeze.plot._
import lib.pccf.IndFunc
import lib.pccf._
import lib.utils._

object step10{


  // moved to separate script
  //  def pccf(mu:Double, sigma:Double, n:Int):List[Double]={
  //    // val r = new scala.util.Random
  //    // (1 to n+1).map(x=>r.nextDouble).toList
  //    val chpProbs = DenseVector.zeros[Double](n)
  //    val ts = DenseVector.tabulate(n){i => i+1}
  //    for(i <- 1 to n){
  //      val G = new Gaussian(i * mu, Math.sqrt(sigma*sigma*i))
  //      chpProbs += ts.map(x => G.pdf(x))
  //    }
  //    chpProbs.toArray.toList
  //  }

  def run():Unit={
    // optimize Pccf for a single signal
    val segmentLength = 100
    val sigma=1.0
    //    val sig1 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
    //    val sig2 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma + 1.0).toArray
    //    val sig3 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
    //    val sig = sig1 ++ sig2 ++ sig3
    //    val changes = List(100, 200)
    val dataDir = "/home/ms314/gitlocal/phd-monitor/datasets/"

    val sig1 = "exchange-rate-bank-of-twi/data/sigTwi.dat"
    val chp1 = "exchange-rate-bank-of-twi/data/changesTwi.dat"

    val sig2 = "internet-traffic-data-in-bits-fr/data/sigInternetTraffic.dat"
    val chp2 = "internet-traffic-data-in-bits-fr/data/changesInternetTraffic.dat"

    val sig = UtilsIO.read(dataDir + sig2)
    val changes = UtilsIO.read(dataDir + chp2).map(_.toInt)

    val IF = IndFunc.constructIndicatorFunc(changes,sig.length, IndFunc.settingsIF(5,5,10))
    val pccfOpt = OptimPccf.findOptimPccfUsingIndFunc(changes, IF, 5, 0.9, 20)
    //    val pccfOptLik = OptimPccf.optimPccfMaxLik(changes, sig.length, 0.9, changes(2))

    val f = Figure()
    f.subplot(3,1,1)
    val p1 = f.subplot(0)
    p1.title = "Input signal"
    val p2 = f.subplot(1)
    p2.title = "IF and Optimal IF (blue)"
    val p3 = f.subplot(2)
    p3.title = "Optimal Pccf"
    val x = (0 to sig.length-1).toArray.map(x=>x.toDouble)

    p1 += plot(DenseVector(x), DenseVector(sig),'-')
    p1 += plot(DenseVector(x), DenseVector(pccfOpt._2.map(x=>x.toDouble * sig.max * 0.5 )),'-')

    p2 += plot(DenseVector(x), DenseVector(IF.map(_.toDouble)),'-', "red")
    p2 += plot(DenseVector(x), DenseVector(pccfOpt._2.map(_.toDouble)),'-', "blue")

    p3 += plot(DenseVector(x), DenseVector(pccfOpt._1),'-', "red")

    val outpathFig  = "./target/step10/TwiSig_20190327.png"
    f.saveas(outpathFig)
  }
}