import breeze.linalg._
import breeze.stats.distributions._
import breeze.stats.hist
import utils._
import steps._
import tasks._
import tests._

object mn {
  def randn(n:Int):Array[Double]={
    (1 to n).map{_ => scala.util.Random.nextGaussian()}.toArray
  }

  def optimPccfMain(inSig:String, chps:String, outf:String):Unit={
    println("Find optimal Pccf")
    println(s"Input signal: $inSig")
    println(s"Input true changes: $chps")
    println(s"Save optimal Pccf into: $outf")
  }

  def main(args:Array[String]):Unit={
    // https://stackoverflow.com/questions/2315912/best-way-to-parse-command-line-parameters
    val usage =
      """
        Usage: main: [--in-sig str] [--out-file-pccf str]
      """
    if(args.length==0) println(usage)
    type OptionMap = Map[Symbol, String]
    def nextOption(map:OptionMap, list: List[String]): OptionMap = list match {
        case "--changes"::value::tail => nextOption(map ++ Map(Symbol("changes") -> value), tail)
        case string::tail => nextOption(map ++ Map(Symbol("signal") -> string), tail)
        case string::Nil => nextOption(map ++ Map(Symbol("signal") -> string), list.tail)
        case Nil => map
    }
    val options = nextOption(Map(), args.toList)
    println(options)
    optimPccfMain(options(Symbol("signal")), options(Symbol("signal")).toString, options(Symbol("signal")).toString)

    // step1.run()
    // step2.run()
    // step3.run()
//    step4.run()
//    step5.run()
//    step6.runSim()
//    step7.runSim7()
//    step8.run()
//    step9.run()
//    step10.run()
//    step11.run()
//    step12.run()
//    step3.diagCusumRunWithSettings()
//    step13.run()
//    tasks99.run()
    //step14.runPccfsForRealSignals()

//    tests.TestPccfBinary.run()
  }

}
