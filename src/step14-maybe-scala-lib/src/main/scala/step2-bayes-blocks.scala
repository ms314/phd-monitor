package steps
import breeze.linalg._
import breeze.stats.distributions._
import lib.perfmetrics._
import lib.detectors._

object step2{

    def run():Unit={
      println(".. Run step2")
        val sig = Array(1.0, 1.0, 1.0,
                        2.0, 2.0, 2.0, 
                        1.0, 1.0, 1.0, 1.0, 1.1)
      val r = BayesBlocks.run(Array[Double](1.0, 1.0, 1.0, 1.0, 1.0,
        100.0, 100.0, 100.1, 100.2, 100.3,
        200.0), 10)
      // val r = bayesianBlocks(Array[Double](1.0, 1.0, 1.0, 1.2, 1.2, 1.2, 1.2, 2.0, 3.0, 2.1), true)
      // val r = bayesianBlocks(Array[Double](1.0, 1.0, 1.0, 1.2, 1.2, 1.2, 1.2), true)
      println("Optimal edges:")
      r foreach println
      println("End")
    }
}