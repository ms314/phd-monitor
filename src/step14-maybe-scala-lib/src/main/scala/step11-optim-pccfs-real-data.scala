package steps

import lib.pccf._
import lib.utils._
import steps.step12.{chpMap, sigMap}
//import lib.datajson.dataAttributes
import lib.commondata._

object step11 extends inputData with indFnSettings {
  // Output:
  // Input IFs
  // Output Pccf and IF from it.


  def run():Unit={
    Utils.sayRun("11")
    val outDir = "./target/step11/"
    //    val lstSigs = Array(sig1,sig2,sig3,sig4,sig5,sig6)
    //    val lstChps = Array(chp1,chp2,chp3,chp4,chp5,chp6)
    var Nsample = 30
    // default parameters of indicator function
//    var minLeft=5
//    var minRight=5
//    var minBetween=10
    for(i <- 1 to 6){

      println(s"///// Calc. optim Pccf for signal $i /////")
      //      val sig = UtilsIO.read(dataDir+lstSigs(i))
      //      val changes = UtilsIO.read(dataDir+lstChps(i)).map(_.toInt)
      val sig = UtilsIO.read(sigMap(i))
      val changes = UtilsIO.read(chpMap(i)).map(_.toInt)

      if(i==0){
        Nsample=10
      } else {
        Nsample=50
      }

      val (minLeft, minRight, minBetween) = indFnSettings(i)
      val IF  = IndFunc.constructIndicatorFunc(changes,sig.length, IndFunc.settingsIF(minLeft, minRight, minBetween))
      val pccfOpt = OptimPccf.findOptimPccfUsingIndFunc(changes, IF, 5, 0.9, 20)

      val outfile= outDir+"pccf_sig"+ (i+1) + ".dat"
      val outfileIF= outDir+"if_sig"+ (i+1) + ".dat"
      val outfileIFpccf = outDir+"if_pccf_sig"+ (i+1) + ".dat"

//      UtilsIO.writeVec(pccfOptLik, outfile)
      UtilsIO.writeVec(pccfOpt._1, outfile)
      UtilsIO.writeVec(pccfOpt._2, outfileIFpccf)
      UtilsIO.writeVec(IF, outfileIF)
      println(s"Save output: $outfile")
      println(s"Save output: $outfileIF")
      println(s"Save output: $outfileIFpccf")
    }
    Utils.sayStop()
  }

}
