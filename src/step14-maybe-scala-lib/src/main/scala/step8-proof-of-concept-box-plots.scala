package steps

import breeze.linalg.{DenseVector, linspace}
import lib.learndetectors.LearnDetectors
import lib.utils.AdaptiveSettings
import breeze.stats.DescriptiveStats.percentile
import breeze.plot.{Figure, plot, _}
import lib.types._
import lib.utils._
import scala.io.Source
import java.io._

class AllOptimOutput(sensitivity:Array[Double], nSim:Int){
  val nParamRange = sensitivity.length
  var numFaM = Array.ofDim[Double](nSim, nParamRange)
  var delayM = Array.ofDim[Double](nSim, nParamRange)
  var lossM  = Array.ofDim[Double](nSim, nParamRange)
}


object step8{

  val boxPlot = (a : List[Double]) => List(0.01, 0.25, 0.5, 0.75, 0.99).map(x => percentile(a, x))
  def addTwoVec(v1:Array[Double], v2:Array[Double]):Array[Double]={(v1 zip v2).map(x => x._1 + x._2)}

  def run(outFolder:String = "./target/step8/")={
    val Nsim = 100
    val Nparamrange = 30
    val changes = Array(100, 200)
    val sigma = 0.5
    val segmentLength = 100
    val indF = AdaptiveSettings.constructIndFunction(3 * segmentLength, changes.toList, 20, 20)

    val paramRangeCusum = linspace(3.0, 5.0  , Nparamrange).toArray
    val paramRangeAdwin = linspace(1.5, 3.5  , Nparamrange).toArray
    val paramrangeBayes = linspace(1.0, 35.0 , Nparamrange).toArray

    val s0Cusum = 999.0 // paramRangeCusum(paramRangeCusum.length-1)
    val s0Adwin = 999.0 // paramRangeAdwin(paramRangeAdwin.length-1)
    val s0Bayes = 999.0 // paramrangeBayes(paramrangeBayes.length-1)

    val allCusumStat = new AllOptimOutput(paramRangeCusum, Nsim)
    val allCusumDyn  = new AllOptimOutput(paramRangeCusum, Nsim)

    val allAdwinStat = new AllOptimOutput(paramRangeAdwin, Nsim)
    val allAdwinDyn  = new AllOptimOutput(paramRangeAdwin, Nsim)

    val allBayesStat = new AllOptimOutput(paramrangeBayes, Nsim)
    val allBayesDyn  = new AllOptimOutput(paramrangeBayes, Nsim)

    def update(statsContainer:AllOptimOutput, newData: OptimOutput, rowNum:Int)={
      for(i <- 0 to Nparamrange-1){
        statsContainer.delayM(rowNum)(i) = newData.delays(i)
        statsContainer.numFaM(rowNum)(i) = newData.numOfFa(i)
        statsContainer.lossM(rowNum)(i)  = newData.lossV(i)
      }
    }

    for(k <- 0 to Nsim-1){
      if(10*k/Nsim % 10 == 0) println(">>> ", k.toDouble/Nsim)
      val sig1 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
      val sig2 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma + 1.0).toArray
      val sig3 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
      val sig = sig1 ++ sig2 ++ sig3

      val optStaticCusum = LearnDetectors.optim(sig, changes, LearnDetectors.createCusumRef(0.0), paramRangeCusum)

      val optDynamicCusum = LearnDetectors.optim(sig, changes,
        LearnDetectors.createCusumRefDynamic(indF, 0.0, s0Cusum), paramRangeCusum)

      if(1==0){
        val optStaticAdwin = LearnDetectors.optim(sig, changes, LearnDetectors.createAdwinRef(), paramRangeAdwin)

        val optDynamicAdwin = LearnDetectors.optim(sig, changes,
          LearnDetectors.createAdwinRefDynamic(indF, s0Adwin), paramRangeAdwin)

        val optStaticBayes = LearnDetectors.optim(sig, changes, LearnDetectors.createBayesRef(0.0), paramrangeBayes)

        val optDynamicBayes = LearnDetectors.optim(sig, changes,
          LearnDetectors.createBayesRefDynamic(indF, 0.0, s0Bayes), paramrangeBayes)

        update(allAdwinStat, optStaticAdwin , k)
        update(allAdwinDyn , optDynamicAdwin, k)
        update(allBayesStat, optStaticBayes , k)
        update(allBayesDyn , optDynamicBayes, k)
      }


      update(allCusumStat, optStaticCusum , k)
      update(allCusumDyn , optDynamicCusum, k)

    }

    UtilsIO.writeMatrix(allCusumStat.delayM, "./target/step8/CusumDelayStat.dat")
    UtilsIO.writeMatrix(allCusumStat.numFaM, "./target/step8/CusumNumOfFaStat.dat")
    UtilsIO.writeMatrix(allCusumDyn.delayM, "./target/step8/CusumDelayDyn.dat")
    UtilsIO.writeMatrix(allCusumDyn.numFaM, "./target/step8/CusumNumOfFaDyn.dat")
    UtilsIO.writeVec(paramRangeCusum, "./target/step8/CusumSensitivity.dat")

  }

}


