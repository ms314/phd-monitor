package lib.learndetectors

import lib.types._
import lib.detectors._
import lib.perfmetrics._

object LearnDetectors{

  def createCusumRef(mu0:Double): (Array[Double], Double) => Array[Int] = {
    (sig, s) => Cusum.cusumPh(sig, mu0, StaticSettings(s)).cdes
  }

  def createAdwinRef(): (Array[Double], Double) => Array[Int] ={
    (sig, s) => Adwin.adwinNaive(sig, StaticSettings(s))
  }

  def createBayesRef(mu0:Double): (Array[Double], Double) => Array[Int] = {
    (sig, s) => BayesianDetector.bDetector(sig, StaticSettings(s), mu0)
  }

  def createCusumRefDynamic(indFunc:Array[Int], mu0:Double, s0:Double=9999.0): (Array[Double], Double) => Array[Int] = {
    (sig, s) => Cusum.cusumPh(sig, mu0, DynamicSettings(indFunc, s0, s)).cdes
  }

  def createAdwinRefDynamic(indFunc:Array[Int], s0:Double=999.0): (Array[Double], Double) => Array[Int] = {
    (sig, s) => Adwin.adwinNaive(sig, DynamicSettings(indFunc, s0, s))
  }

  def createBayesRefDynamic(indFunc:Array[Int], mu0:Double, s0:Double=999.0):(Array[Double], Double) => Array[Int] ={
    (sig, s) => BayesianDetector.bDetector(sig, DynamicSettings(indFunc, s0, s), mu0)
  }



  def optim(sig        : Array[Double],
            changes    : Array[Int],
            detectorRef: (Array[Double], Double) => Array[Int],
            paramRange : Array[Double]=(1 to 250).map(_.toDouble).toArray): OptimOutput = {
    // when dynamic: use as s0 - the largest from param range for static case
    val n = sig.length
    val sensV    = new Array[Double](paramRange.length)
    val delaysV  = new Array[Double](paramRange.length)
    val numFaV   = new Array[Double](paramRange.length)
    val lossV    = new Array[Double](paramRange.length)
    var bestSoFar = 10000.0
    var bestSoFar_Sens = paramRange(0)
    var bestSoFar_Delay = 1000.0
    var bestSoFar_NumOfFa = 0
    var lossCurrent = 0.0
    var count = -1
    for (s <- paramRange){
      count += 1
      val r = detectorRef(sig, s)
      val delays = PerformanceMetrics.detectionDelays(n, changes, r)
      val numFa = PerformanceMetrics.numOfFalseAlarms(n, changes, r)
      sensV(count) = s
      delaysV(count) = delays.sum / delays.length
      numFaV(count) = numFa
      lossCurrent = PerformanceMetrics.costF(n, delaysV(count), numFa)
      if(lossCurrent < bestSoFar) {
        bestSoFar = lossCurrent
        bestSoFar_Sens = s
        bestSoFar_Delay = delaysV(count)
        bestSoFar_NumOfFa = numFa
      }
      lossV(count) = lossCurrent
    }
    OptimOutput(sensV, delaysV, numFaV, lossV)
  }

}