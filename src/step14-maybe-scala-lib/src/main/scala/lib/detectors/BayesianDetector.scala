package lib.detectors
import lib.types._
import breeze.linalg.{DenseMatrix, DenseVector}
import breeze.numerics.lgamma
import breeze.plot.{Figure, image}

object BayesianDetector{

  def extractChanges(maxes:Array[Int]):Array[Int]={
    val n = maxes.length
    var chpLoc = n-1
    chpLoc = chpLoc - maxes(n-1)
    var chps = Array[Int](chpLoc+1)
    var count = 0
    while (chpLoc!=0 && count < n+2){
      count += 1
      chpLoc = chpLoc - maxes(chpLoc)
      chps = (chpLoc+1) +: chps
      if(maxes(chpLoc)==0 && chpLoc>0){
        chpLoc = chpLoc - 1
        chps = (chpLoc+1) +: chps
      }
    }
    chps.tail
  }

  def studentPdf(x: Double, mu: Double, s: Double, nu: Double): Double = {
    val a1 = math.exp(lgamma(nu/2.0 + 0.5) - lgamma(nu/2.0))
    val a2 = math.pow(nu * math.Pi * s, -0.5)
    val a = a1 * a2
    val b = 1.0+math.pow(nu*s,-1.0) * math.pow(x-mu,2.0)
    val c = math.pow(b, -(nu+1.0)/2.0)
    val d = a * c
    d
  }

  def studentPdf(x: Double, mu: Array[Double], s: Array[Double], nu: Array[Double]):Array[Double] = {
    val n = mu.length
    assert(n==s.length)
    assert(n==nu.length)
    val mV = DenseVector(mu)
    val sV = DenseVector(s)
    val nV = DenseVector(nu)
    val a1 = breeze.numerics.exp(lgamma(nV/2.0 + 0.5) - lgamma(nV/2.0))
    val a2 = breeze.numerics.pow(nV * sV * math.Pi, -0.5)
    val a = a1*a2
    val b = breeze.numerics.pow(nV*sV,-1.0) * breeze.numerics.pow(x-mV, 2.0) + 1.0
    val c = breeze.numerics.pow(b, -(nV+1.0)/2.0)
    val d = a * c
    d.toArray
  }

  def testStudentPdf(){
    val q1 = studentPdf(1.0, 1.0, 3.0, 4.0)
    val q2 = studentPdf(1.0, Array(1.1, 2.0), Array(1.0,3.0), Array(1.0, 4.0))
    println(q1)
    println()
    q2 foreach println
  }

  def paramsForPredProbs(alphaT: Array[Double], betaT: Array[Double], kappaT:Array[Double]):(Array[Double], Array[Double])={
    // betaT .* (kappaT .+ 1.0) ./ (alphaT .* kappaT),
    // 2.0 .* alphaT
    val num = (betaT zip kappaT.map(x=>x+1.0)).map(x => x._1 * x._2)
    val denom = (alphaT zip kappaT).map(x => x._1 * x._2)
    val a = (num zip denom).map(x => x._1 / x._2)
    val b = alphaT.map(x=>x * 2.0)
    (a, b)
  }

  def paramsForMu(muT:Array[Double], kappaT:Array[Double], y:Double):Array[Double]={
    val a = (kappaT zip muT).map(x=>x._1*x._2 + y)
    val b = kappaT.map(x=>x+1.0)
    (a zip b).map(x => x._1 / x._2)
  }

  def paramForBeta(muT:Array[Double], kappaT:Array[Double], y:Double):Array[Double] ={
    // kappaT .+ (kappaT .* (y[t] .- muT).^2.0) ./ (2.0 .* (kappaT .+ 1.0))
    val sDeviations = muT.map(m => math.pow(y-m, 2))
    val sDeviationsDorKappaT = (sDeviations zip kappaT).map(x => x._1 * x._2)
    val denom = kappaT.map(x => 2.0 * (x + 1.0))
    val devide = (sDeviationsDorKappaT zip denom).map(x => x._1 / x._2)
    (kappaT zip devide).map(x=> x._1 + x._2)
  }

  def bDetectorFullOutput(ys: Array[Double], lambda: AbstractSettings, mu0: Double): (Array[Array[Double]], Array[Int], Array[Double]) = lambda match {
    case StaticSettings(h) => {
      assert(h >= 1.0)
      val n = ys.length
      val kappa0 = 1.0
      val alpha0 = 1.0
      val beta0  = 1.0
      val maxes = new Array[Int](n)
      val maxesProbs = new Array[Double](n)
      //
      var muT    = Array[Double](mu0)
      var kappaT = Array[Double](kappa0)
      var alphaT = Array[Double](alpha0)
      var betaT  = Array[Double](beta0)
      //
      val R = Array.ofDim[Double](n+1, n+1)
      R(0)(0) = 1.0
      for (t <- 1 to n-1){
        val params = paramsForPredProbs(alphaT, betaT, kappaT)
        val predProbs = studentPdf(ys(t), muT, params._1, params._2)
        for(k <- 1 to t){
          R(t)(k) = R(t-1)(k-1) * (1.0 - math.pow(h, -1.0)) * predProbs(k-1)
        }
        R(t)(0) = (R(t-1).slice(1, t) zip predProbs).map(x => x._1 * x._2).sum * math.pow(h, -1.0)
        val s = R(t).sum
        R(t) = R(t).map(x => x / s)
        val muT0    = mu0    +: paramsForMu(muT ,kappaT, ys(t))
        val kappaT0 = kappa0 +: kappaT.map(x => x + 1.0)
        val alphaT0 = alpha0 +: alphaT.map(x => x + 0.5)
        val betaT0  = beta0  +: paramForBeta(muT, kappaT, ys(t))
        muT    = muT0
        kappaT = kappaT0
        alphaT = alphaT0
        betaT  = betaT0
        maxes(t) = R(t).zipWithIndex.maxBy(_._1)._2
        maxesProbs(t) = R(t).zipWithIndex.maxBy(_._1)._1
      }
      (R, maxes, maxesProbs)
    }
    case DynamicSettings(indF, s0, s1) => {
//      assert(h >= 1.0)
      val n = ys.length
      val kappa0 = 1.0
      val alpha0 = 1.0
      val beta0  = 1.0
      val maxes = new Array[Int](n)
      val maxesProbs = new Array[Double](n)
      //
      var muT    = Array[Double](mu0)
      var kappaT = Array[Double](kappa0)
      var alphaT = Array[Double](alpha0)
      var betaT  = Array[Double](beta0)
      //
      val R = Array.ofDim[Double](n+1, n+1)
      R(0)(0) = 1.0
      for (t <- 1 to n-1){
        val hDynamic = if (indF(t)==0) s0 else s1
        val params = paramsForPredProbs(alphaT, betaT, kappaT)
        val predProbs = studentPdf(ys(t), muT, params._1, params._2)
        for(k <- 1 to t){
          R(t)(k) = R(t-1)(k-1) * (1.0 - math.pow(hDynamic, -1.0)) * predProbs(k-1)
        }
        R(t)(0) = (R(t-1).slice(1, t) zip predProbs).map(x => x._1 * x._2).sum * math.pow(hDynamic, -1.0)
        val s = R(t).sum
        R(t) = R(t).map(x => x / s)
        val muT0    = mu0    +: paramsForMu(muT ,kappaT, ys(t))
        val kappaT0 = kappa0 +: kappaT.map(x => x + 1.0)
        val alphaT0 = alpha0 +: alphaT.map(x => x + 0.5)
        val betaT0  = beta0  +: paramForBeta(muT, kappaT, ys(t))
        muT    = muT0
        kappaT = kappaT0
        alphaT = alphaT0
        betaT  = betaT0
        maxes(t) = R(t).zipWithIndex.maxBy(_._1)._2
        maxesProbs(t) = R(t).zipWithIndex.maxBy(_._1)._1
      }
      (R, maxes, maxesProbs)
    }
  }

  def bDetector(ys: Array[Double], lambda: AbstractSettings, mu0: Double):Array[Int]= {
      val rb = bDetectorFullOutput(ys, lambda, mu0)
      extractChanges(rb._2)
  }

  def demoBayesDetectorOutputMatrix(imageout:String="./target/bdetector_probs.png")={
    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 1.0).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val changes = Array(100, 200)
    val lambda = 100.0
    val rb = bDetectorFullOutput(sig, StaticSettings(lambda), 0.0)
    val m = rb._1
    val f1 = Figure()
    f1.subplot(0) += image(DenseMatrix(m:_*).t)
    f1.saveas(imageout)
    val cdes = extractChanges(rb._2)
    println("cdes: ", cdes.mkString(","))
  }

}