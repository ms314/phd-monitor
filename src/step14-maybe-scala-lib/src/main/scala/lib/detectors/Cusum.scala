package lib.detectors

import breeze.linalg.DenseVector
import breeze.plot.{Figure, plot}
import lib.types._
import breeze.stats.{mean, variance}
import lib.utils.{Utils, UtilsIO}

object Cusum {

  def cusumJustOutputStastic(ys:Array[Double], delta:Double): (Array[Double], Array[Double]) = {
    // for the illustration - plot in R
    val n = ys.length
    val mu_r = new Array[Double](n)
    val s_pos_v = new Array[Double](n)
    val s_neg_v = new Array[Double](n)
    var s_pos = 0.0
    var s_neg = 0.0
    var n_r = 0
    mu_r(0) = ys(0)

    for (k <- 1 to n-1){
      mu_r(k) = (1.0/(k+1)) * (mu_r(k-1) * k  + ys(k))

      s_pos = s_pos + (ys(k) - mu_r(k) - delta)
      s_neg = s_neg - (ys(k) - mu_r(k) - delta)

      s_pos_v(k) = s_pos
      s_neg_v(k) = s_neg
    }
    //    Utils.writeVec("./target/outstat.dat", s_pos_v)
    //    Utils.writeVec("./target/sig.dat", ys)
    (s_pos_v, s_neg_v)
  }

  def demoPlotCusumOutputStatistic(outpath:String)={
    val sig1 = (1 to 30).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig2 = (1 to 30).map(_ => scala.util.Random.nextGaussian() + 15.0).toArray
    val sig = sig1 ++ sig2
    val q = cusumJustOutputStastic(sig, 1.0)

    val f = Figure()
    f.subplot(2,1,1)
    val p1 = f.subplot(0)
    p1.title = "Cusum output statistic"
    p1.xlabel="time"
    val p2 = f.subplot(1)
    p2.title="Input signal"
    p2.xlabel="time"
    val x = (0 to sig.length-1).toArray.map(x=>x.toDouble)
    p1 += plot(DenseVector(x), DenseVector(q._1),'.')
    p1 += plot(DenseVector(x), DenseVector(q._1),'.', "red")
    p2 += plot(DenseVector(x), DenseVector(sig),'-')
//    p2 += plot(DenseVector(x), DenseVector(sig),'.', "red")
    f.saveas(outpath)
  }

  def cusumPhStatic(ys:Array[Double], muInit: Double, h:Double): CusumOutput ={
    // just static settings
    val n = ys.length
    val mu_r = new Array[Double](n)
    val sPos_v = new Array[Double](n)
    val sNeg_v = new Array[Double](n)
    var sPos = 0.0
    var sNeg = 0.0
    mu_r(0) = ys(0)
    var cdes = Array[Int]()
    var n_r = 0 // run length since last change
    var mu0 = muInit
    for (k <- 1 to n-1){
      n_r += 1
      mu_r(k) = (1.0/(n_r+1)) * (mu_r(n_r-1) * n_r  + ys(k))
      if(sPos > h || sNeg > h){
        cdes = cdes :+ k
        n_r = 0
        sPos = 0
        sNeg = 0
        mu0 = ys(k)
        mu_r(k) = mu0
      } else {
        sPos = sPos + (ys(k) - mu_r(k) - mu0)
        sNeg = sNeg - (ys(k) - mu_r(k) - mu0)
      }
      sPos_v(k) = sPos
      sNeg_v(k) = sNeg
    }
    //    Utils.writeVec("./target/outstat.dat", s_pos_v)
    //    Utils.writeVec("./target/sig.dat", ys)
    //    Utils.writeVec("./target/cdes.dat", changes)
    CusumOutput(cdes, sPos_v, sNeg_v)
  }

  def cusumPh(ys:Array[Double], muInit: Double, h:AbstractSettings): CusumOutput = h match {
    // add dynamic settings
    case StaticSettings(h) => {
      val n = ys.length
      val mu_r = new Array[Double](n)
      val sPos_v = new Array[Double](n)
      val sNeg_v = new Array[Double](n)
      var sPos = 0.0
      var sNeg = 0.0
      mu_r(0) = ys(0)
      var cdes = Array[Int]()
      var n_r = 0

      var mu0 = muInit

      for (k <- 1 to n-1){
        n_r += 1
        mu_r(k) = (1.0/(n_r+1)) * (mu_r(n_r-1) * n_r  + ys(k))

        if(Math.abs(sPos) > h || Math.abs(sNeg) > h){
          cdes = cdes :+ k
          n_r = 0
          mu0 = ys(k)
          mu_r(k) = mu0
          sPos = 0.0
          sNeg = 0.0
        }
        sPos = sPos + (ys(k) - mu0)
        sNeg = sNeg - (ys(k) - mu0)
        sPos_v(k) = sPos
        sNeg_v(k) = sNeg
      }
//      UtilsIO.writeVec(mu_r, "./target/step3/mur.dat")
      CusumOutput(cdes, sPos_v, sNeg_v)
    }

    case DynamicSettings(indF, s0, s1) => {
      val n = ys.length
      val mu_r = new Array[Double](n)
      val sPos_v = new Array[Double](n)
      val sNeg_v = new Array[Double](n)
      var sPos = 0.0
      var sNeg = 0.0
      mu_r(0) = ys(0)
      var cdes = Array[Int]()
      var n_r = 0
      var mu0 = muInit
      var hDynamic = -1.0
      for (k <- 1 to n-1) {
        //todo: from real data: sometimes s0 < s1 gives better results
        //

//        val defaultCase = true
        val defaultCase = false

        if(defaultCase){ // Case A
          hDynamic = if (indF(k)==0) s0 else s1
        } else { // Case B
          hDynamic = if (indF(k)==0) s1*2.5 else s1
        }

        n_r += 1
        mu_r(k) = (1.0/(n_r+1)) * (mu_r(n_r-1) * n_r  + ys(k))

        if(Math.abs(sPos) > hDynamic || Math.abs(sNeg) > hDynamic){
          cdes = cdes :+ k
          n_r = 0
          mu0 = ys(k)
          mu_r(k) = mu0
          sPos = 0.0
          sNeg = 0.0
        }
//        sPos = sPos + (ys(k) - mu_r(k) - mu0)
//        sNeg = sNeg - (ys(k) - mu_r(k) - mu0)
        sPos = sPos + (ys(k) - mu0)
        sNeg = sNeg - (ys(k) - mu0)
        sPos_v(k) = sPos
        sNeg_v(k) = sNeg
      }
      CusumOutput(cdes, sPos_v, sNeg_v)
    }
  }
}

// old1: mu_r(k) = (1.0/(k+1)) * (mu_r(k-1) * (k)  + ys(k))
// old2: mu_r(k) = (1.0/(n_r+1)) * (mu_r(n_r-1) * n_r  + ys(n_r))