package lib.detectors
import breeze.linalg._ //{DenseMatrix, DenseVector}
import breeze.plot._ //{Figure, image}
import lib.types._

object Adwin{

  def extractChpsAdwin(ws:Array[Int]):Array[Int]={
    var acc = List[Int]()
    val n = ws.length
    for(i <- 2 to n-1){
      if(ws(i) <= ws(i-1) && ws(i-1) >= ws(i-1))
        acc = i :: acc
    }
    acc.toArray.reverse
  }

  def genSplits(ys:List[Double]):Array[(Double, Double)] = {
    var acc = List((0.0, 0.0))
    val n = ys.length
    if(n==1){
      acc = (ys.head, ys.head) :: acc
    }
    else{
      for (k <- 1 to n-1){
        val sp:(List[Double], List[Double]) = ys.splitAt(k)
        val s1 = sp._1.sum/ sp._1.length
        val s2 = sp._2.sum / sp._2.length
        acc = (s1, s2) :: acc
      }
    }
    acc.init.reverse.toArray
  }


  def adwinNaiveOutStat(ys:Array[Double], eCut:AbstractSettings):Array[Int] = eCut match {
    case StaticSettings(s) => {
      var ws = List[Int]()
      var acc = List[Double]()
      val n = ys.length
      for(i <- 0 to n-1 ) {
        acc = ys(i)::acc
        val splits = genSplits(acc)
        var coll = List[Int]()
        for(j <- 0 to splits.length-1){
          if(math.abs(splits(j)._1 - splits(j)._2) > s)
            coll = (j+1) :: coll
        }
        if(! coll.isEmpty){
          val splitIdx = coll.last
          acc = acc.slice(0, splitIdx)
        }
        ws = acc.length :: ws
      }
      ws.reverse.toArray
    }

    case DynamicSettings(indF, s0, s1) => {
      //
      var ws = List[Int]()
      var acc = List[Double]()
      val n = ys.length
      for(i <- 0 to n-1 ) {
        acc = ys(i)::acc
        val splits = genSplits(acc)
        var coll = List[Int]()
        val sDynamic = if (indF(i)==0) s0 else s1
        for(j <- 0 to splits.length-1){

          if(math.abs(splits(j)._1 - splits(j)._2) > sDynamic)
            coll = (j+1) :: coll
        }
        if(! coll.isEmpty){
          val splitIdx = coll.last
          acc = acc.slice(0, splitIdx)
        }
        ws = acc.length :: ws
      }
      ws.reverse.toArray
    }
  }


  def adwinNaive(ys:Array[Double], eCut:AbstractSettings=StaticSettings(1.1)):Array[Int]={
    val r = adwinNaiveOutStat(ys, eCut)
    extractChpsAdwin(r)
  }

  def demoAdwinOutputStatistic(outfig:String)={
    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 3.0).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val q = Adwin.adwinNaiveOutStat(sig, StaticSettings(3.0)).map(x=>x.toDouble)
    val f = Figure()
    f.subplot(2,1,1)
    val p1 = f.subplot(0)
    p1.title = "Input signal"
    val p2 = f.subplot(1)
    p2.title = "Output statistic: adaptive window width"
    val x:Array[Double] = (0 to sig.length-1).toArray.map(x=>x.toDouble)
    p1 += plot(DenseVector(x), DenseVector(sig), '.')
    p2 += plot(DenseVector(x), DenseVector(q), '-')
//    f1.subplot(0) += plot()// image(DenseMatrix(m:_*).t)
    f.saveas(outfig)
  }

}
