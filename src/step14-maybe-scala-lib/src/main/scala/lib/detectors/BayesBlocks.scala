package lib.detectors
import breeze.linalg._
import breeze.stats.distributions._
import breeze.stats.hist

object BayesBlocks{
        
    def costf(count:Int, width:Double):Double={
        count * (math.log(count) - math.log(width))
    }

    def countElemsBetweenEdges(edges:Array[Double], sig:Array[Double]):Array[Int]={
        val bins = edges.slice(0, edges.length-1) zip edges.slice(1, edges.length)
        var counts = Array[Int]()
        for(b  <- bins){
            var k = sig.filter(x => (x >= b._1 && x <= b._2)).length
            counts = counts :+ k
        }
        assert(counts.sum == sig.length)
        counts
    }

  
    def run(x:Array[Double], prior:Double  =0 , printLog:Boolean=false):Array[Double] = {
        val minV = x.min 
        val maxV = x.max
        val v = x.distinct.sorted
        val n = v.length
        // val edges = DenseVector.vertcat(DenseVector(v(0)), (v(0 to -2) + v(1 to -1)) /:/ 2.0, DenseVector(v(-1)))
        val edges = v(0) +: (v.slice(0, v.length-1) zip v.slice(1, v.length)).map(e => 0.5 * (e._1 + e._2)) :+ v(v.length-1)
        // calculate number of observation within each bin
        val countsVec = countElemsBetweenEdges(edges, x)
        assert(countsVec.sum == x.length)
        var changeEdgesIndices = new Array[Int](n)
        var bestCost = new Array[Double](n)
        var newCost = 0.0
        // START TEST
        var bestCostTest = new Array[Double](n)
        var K = 0
        bestCostTest(K) = costf(countsVec(K), edges(K+1)-edges(0))
        K = 1
        var c0 = costf(countsVec.slice(0,K+1).sum, edges(K+1)-edges(0))
        var c1 = bestCostTest(0) + costf(countsVec.slice(1,K+1).sum, edges(K+1)-edges(1)) - prior
        bestCostTest(K) = c0 min c1
        K = 2
        c0 = costf(countsVec.slice(0,K+1).sum, edges(K+1) - edges(0))
        c1 = bestCostTest(0)     + costf(countsVec.slice(1,K+1).sum, edges(K+1) - edges(1)) - prior
        var c2 = bestCostTest(1) + costf(countsVec.slice(2,K+1).sum, edges(K+1) - edges(2)) - prior
        bestCostTest(K) = (c0 min c1) min c2
        // END TEST
        bestCost(0) = costf(countsVec(0), edges(1)-edges(0)) 
        changeEdgesIndices(0) = 0
        for(k <- 1 to n-1){
            newCost = costf(countsVec.slice(0,k+1).sum, edges(k+1)-edges(0)) 
            changeEdgesIndices(k) = k
            for(j <- 0 to k-1){
                val v = bestCost(j) + costf(countsVec.slice(j+1, k+1).sum, edges(k+1) - edges(j+1)) - prior
                if(v <= newCost){
                    newCost = v
                    changeEdgesIndices(k) = j
                }
            }
            bestCost(k) = newCost
        }
        if(true){
            assert (bestCost(0)==bestCostTest(0))
            assert (bestCost(1)==bestCostTest(1))
            assert (bestCost(2)==bestCostTest(2))
        }
        
        if (printLog){
            println()
            println("Input vector:")
            println(x.toArray.mkString(", "))
            println("Edges:")
            println(edges.toArray.mkString(", ") ,"len=", edges.length)
            println("Counts vec:")
            println(countsVec.mkString(","))
            println()
        }
        var changes = Array[Int]()
        var c = changeEdgesIndices(changeEdgesIndices.length-1)
        // changeEdgesIndices foreach println
        var countLoop = 0
        while(c!=0 && countLoop <= x.length+1){
            changes = c +: changes
            c = changeEdgesIndices(c)
            if(c == changeEdgesIndices(c)){
                c = 0
                changes = c +: changes
            }
            countLoop += 1
        }
        var optEdges = for (e <- changes) yield edges(e)
        if (optEdges(0) != edges(0)) optEdges = edges(0) +: optEdges
        if (optEdges(optEdges.length-1) != edges(edges.length-1)) optEdges = optEdges :+ edges(edges.length-1)
        optEdges
    }
}

  // def scale01(x:Array[Double]):Array[Double]={
    //     val minV = x.min 
    //     val maxV = x.max
    //     x.map(x => (x - minV)/(maxV - minV))
    // }
    // def scale01back(x:Array[Double], minV:Double, maxV:Double):Array[Double]={
    //     x.map(x => x * (maxV - minV) + minV )
    // }