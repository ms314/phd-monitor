package lib.pccf

import breeze.linalg.DenseVector
import breeze.stats.distributions.Gaussian

object Pccf{

  def pccf(mu:Double, sigma:Double, n:Int):Array[Double]={
    // val r = new scala.util.Random
    // (1 to n+1).map(x=>r.nextDouble).toList
    val chpProbs = DenseVector.zeros[Double](n)
    val ts = DenseVector.tabulate(n){i => i+1}
    for(i <- 1 to n){
      val G = new Gaussian(i * mu, Math.sqrt(sigma*sigma*i))
      chpProbs += ts.map(x => G.pdf(x))
    }
    chpProbs.toArray //.toList
  }

  def indFuncFromPccf(pccf:Array[Double], h:Double):Array[Int] = {
    pccf.map(x => {if (x < h) {0} else {1}})
  }

  def numOfIntervals(indf:Array[Int]):Int={
    // number of 1 time intervals
    encodeIndf(indf).map(_._2).sum
  }

  def encodeIndf(indf:Array[Int]):Array[(Int, Int)]={
    def encode[A](ls:List[A]):List[(Int, A)] = {
      if (ls.isEmpty) List():List[(Int, A)] //List((0, ls.head))
      else {
        val (p, n) = ls span {_ == ls.head}
        if (n==Nil) List((p.length, p.head)) else (p.length, p.head) :: encode(n)
      }
    }
    encode(indf.toList).toArray
  }

  def decodeIndf(encodedIndf:Array[(Int, Int)]):Array[Int]={
    def decode[A](ls:List[(Int, A)]):List[A]={
      ls.flatMap{x => List.fill(x._1)(x._2)}
    }
    decode(encodedIndf.toList).toArray
  }

}
