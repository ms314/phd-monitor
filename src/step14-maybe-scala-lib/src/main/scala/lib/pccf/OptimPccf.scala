package lib.pccf
//import lib.pccf.OptimPccf.{costf, genPeriodicLocations}
import java.io.PrintWriter

import breeze.linalg._
import breeze.stats.{mean, variance}
import lib.utils._
import lib.perfmetrics._

object OptimPccf{

  def indFuncFromPccf(pccf:Array[Double], h:Double):Array[Int] = {
    //pccf.map(x => {if (x < h) {0} else {1}})
    Pccf.indFuncFromPccf(pccf, h)
  }

  def overlapOfIndFuncs(indFunc:Array[Int], pccfIndFunc:Array[Int]):Double={
    val n = pccfIndFunc.length
    assert(n > 1)
    assert(indFunc.length == n)
    (pccfIndFunc zip indFunc).map(x=>{if (x._1 == x._2  && x._1 == 1) {1} else {0} }).sum / n.toFloat
  }

  def shiftPccf(shift:Int, pccf:Array[Double]):Array[Double]={
    val n = pccf.length
    Array.fill[Double](shift)(0.0) ++ pccf.take(n-shift)
  }


  def calcParamsBoundaries(changes:Array[Int], sigLen:Int, sigmaVariationfactor:Double):(Double,Double, Double, Double)={
    val changesAugmented = Array(0) ++ changes ++ Array(sigLen)
    val diff = (changesAugmented.init zip changesAugmented.tail).map(x => (x._2-x._1).toDouble)
    val avgDist = mean(diff)
    val stdDist = math.sqrt(variance(diff))
    val minDiff = diff.min
    val maxDiff = diff.max

    val minMu:Double = minDiff + 2.0
    val maxMu:Double = diff.max * 1.0 // do not cross maxMy bcs it must converge to Poisson
    val N = (maxMu.toInt+2).min(30)
    val minSigma = stdDist - sigmaVariationfactor*stdDist
    val maxSigma = stdDist + 0.0 // sigmaVariationfactor*stdDist

    if(1==1){
      println("/// optimPccfMaxLik() describe")
      println("Input changepoints:", changes.mkString(","))
      println(s" Min. dist. = $minDiff, \n Max distance = $maxDiff")
      println(s" Avg. distance: $avgDist, \n Sigma=$stdDist")
      println(s"/// Calculated parameters for optimization describe: \n minMu=$minMu, \n maxMu=$maxMu, \n minSigma=$minSigma, \n maxSigma=$maxSigma")
      println("/// End describe")
    }

    (minMu, maxMu, minSigma, maxSigma)
  }

  case class optimPccfBinaryOut(tpOpt:Double, fpOpt:Double, fnOpt:Double)

  def optimPccfBinary(changes:Array[Int],
                      indFunc:Array[Int],
                      maxStartingPoint:Int,
                      sigmaVariationfactor:Double=0.9,
                      Nrange:Int = 30,
                      statsFile:String="./stats.dat"): (Array[Double], Array[Int], optimPccfBinaryOut) = {
    // find optimal Pccf using binary classification performance metrics
    val sigLen = indFunc.length
    val b = calcParamsBoundaries(changes, sigLen, sigmaVariationfactor)
    val minMu:Double = b._1
    val maxMu:Double = b._2
    val minSigma:Double = b._3
    val maxSigma:Double = b._4
    val muRange = linspace(minMu, maxMu, Nrange)
    val sRange = linspace(minSigma, maxSigma, Nrange)
//    val epsForThreshold = 0.01
    val n = indFunc.length
    var bestSoFar = 0.0
    var bestMu = 0.0
    var bestS = 0.0
    var bestH = 0.0
    var bestShift = 0
    //
    var bestTp = 0.0
    var bestFp = 0.0
    var bestFn = 0.0
    var bestFscore = 0.0
    val shiftVals = List.range(0, maxStartingPoint+1)

    for(m <- muRange){
      val minThreshold = 1.0/m.toDouble
      for(s <- sRange){
        val pccfVec = Pccf.pccf(m,s,n)
        val thresholdVec = linspace(minThreshold + minThreshold * 0.05, minThreshold + minThreshold * 0.1, 5)
        for(shift <- shiftVals){
          val pccfShifted = shiftPccf(shift, pccfVec)
          for(h <- thresholdVec) {

            val indFuncPccf = indFuncFromPccf(pccfShifted, h)
            val R = PerfMetricsBinPccf.allStats(indFuncPccf,changes)
            val tpPccf = R.tpNum
            val fpPccf = R.fpNum
            val fnPccf = R.fnNum

            val fScore:Double = 2.0 * tpPccf / (2.0 * tpPccf + fpPccf + fnPccf)
            val cost = fScore
            if(cost > bestSoFar && fpPccf <= 1 && tpPccf >= 1){
              bestSoFar = cost
              println(s"tp=$tpPccf, fp = $fpPccf, fn = $fnPccf, F score = $fScore")
              println(s"bestSoFar = $bestSoFar")
              bestMu = m
              bestS = s
              bestH = h
              bestShift = shift
              bestTp = tpPccf
              bestFp = fpPccf
              bestFn = fnPccf
              bestFscore = bestSoFar
            }
          }
        }
      }
    }
    println(s"findOptimPccf(): Better overlap = $bestSoFar with a threshold = $bestH, mu=$bestMu, s=$bestS, shift=$bestShift")

    // todo: is it the same as above?
//    val pccfShiftedOld_ = Array.fill[Double](bestShift)(0.0) ++ Pccf.pccf(bestMu, bestS, n).drop(bestShift)
    val pccfVecFinal = Pccf.pccf(bestMu,bestS,n)
    val pccfShiftedFinal = shiftPccf(bestShift, pccfVecFinal)
//    val likelihood = changes.foldLeft(0.0){val a = pccfShiftedFinal; var i = -1; (c,_)=>{i+=1; c+a(i)}}
//    println(s"findOptimPccf(): likelihood=$likelihood")
    val pw = new PrintWriter(statsFile)
    pw.println(s"best TP = $bestTp,\nbest FP = $bestFp,\nbest FN = $bestFn,\nbest F1-score = $bestFscore")
    println(s"save statistics into $statsFile")
    pw.close()
    println(s"best TP = $bestTp, \nbest FP = $bestFp, \nbest FN = $bestFn, \nbest F1-score = $bestFscore")
    (pccfShiftedFinal, indFuncFromPccf(pccfShiftedFinal,bestH), optimPccfBinaryOut(bestTp, bestFp, bestFn))
  }


  def findOptimPccfUsingIndFunc(changes:Array[Int],
                                indFunc:Array[Int],
                                maxStartingPoint:Int,
                                sigmaVariationfactor:Double=0.9,
                                Nrange:Int = 30): (Array[Double], Array[Int]) = {
    // for a given I.F.
    // variate \mu, \sigma, threshold of Pccf
    // get I.F. from Pccf by applying threshold
    // calc. cost function for two I.F.'s
    // todo: lowest threshold will always get best overlap?

    val sigLen = indFunc.length
    val b = calcParamsBoundaries(changes, sigLen, sigmaVariationfactor)
    val minMu:Double = b._1
    val maxMu:Double = b._2
    val minSigma:Double = b._3
    val maxSigma:Double = b._4


    val muRange = linspace(minMu, maxMu, Nrange)
    val sRange = linspace(minSigma, maxSigma, Nrange)

    val epsForThreshold = 0.01
    val n = indFunc.length

    var bestSoFar = 0.0
    var bestMu = 0.0
    var bestS = 0.0
    var bestH = 0.0
    var bestShift = 0

    val shiftVals = List.range(0, maxStartingPoint+1)

    for(m <- muRange){

      val minThreshold = 1.0/m.toDouble

      for(s <- sRange){

        val pccfVec = Pccf.pccf(m,s,n)

        val thresholdVec = linspace(minThreshold + minThreshold * 0.05, minThreshold + minThreshold * 0.1, 5)

        for(shift <- shiftVals){

          val pccfShifted = shiftPccf(shift, pccfVec)

          for(h <- thresholdVec) {

            val indFuncPccf = indFuncFromPccf(pccfShifted, h)

            val negCost = minusCost(m, shift, changes, sigLen)

            val overlap = overlapOfIndFuncs(indFunc, indFuncPccf) // - negCost

            if(overlap > bestSoFar){
              bestSoFar = overlap
              bestMu = m
              bestS = s
              bestH = h
              bestShift = shift
            }
          }
        }
      }
    }
    println(s"findOptimPccf(): Better overlap = $bestSoFar with a threshold = $bestH, mu=$bestMu, s=$bestS, shift=$bestShift")
    val pccfShifted = Array.fill[Double](bestShift)(0.0) ++ Pccf.pccf(bestMu, bestS, n).drop(bestShift)
    val likelihood = changes.foldLeft(0.0){val a = pccfShifted; var i = -1; (c,_)=>{i+=1; c+a(i)}}
    println(s"findOptimPccf(): likelihood=$likelihood")
    (pccfShifted, indFuncFromPccf(pccfShifted,bestH))
  }



  def minusCost(mu:Double, shift:Int=0, changes:Array[Int], sigLen:Int):Double={
    def aux(k:Int, mu:Double, acc:List[Double]=List(0.0)):List[Int] = k match {
      case 0 => acc.reverse.map(_.toInt)
      case _ => aux(k-1, mu, (acc.head+mu)::acc)
    }
    val n = sigLen / mu.toInt
    val peaks = aux(n, mu).map(x=>x+shift).filter(x => x <= sigLen)
//    val cost = Utils.sumArrayElements(peaks.toArray, shiftPccf(shift,pccf))
    val d = peaks.length-changes.length
    val cost = if(d > 0) d else 0
    cost.toDouble
  }



  def optimPccfMaxLik(changes:Array[Int],
                      sigLen:Int,
                      sigmaVariationfactor:Double,
                      maxStartingPoint:Int):Array[Double] = {
    // todo: add extra -cost for extra peaks
    // todo: maybe shift doesn't need to be variated, isn't it enough to variate just \mu?
    assert(sigmaVariationfactor < 1.0)
    assert(sigmaVariationfactor >= 0.0)
    val b = calcParamsBoundaries(changes, sigLen, sigmaVariationfactor)
    val minMu:Double = b._1
    val maxMu:Double = b._2
    val minSigma:Double = b._3
    val maxSigma:Double = b._4
    val N = (maxMu.toInt+2).min(30)

    val muRange = linspace(minMu, maxMu, N)
    val sigmaRange = linspace(minSigma, maxSigma, N)
    val shiftVals = List.range(0, maxStartingPoint+1)

    // vars
    var bestSoFar = 0.0
    var bestMu = 0.0
    var bestSigma = 0.0
    var bestShift = 0

    for(m <- muRange) {

      for (sigma <- sigmaRange) {

        val pccfVec = Pccf.pccf(m, sigma, sigLen)

        for(shift <- shiftVals){

          val pccfLength = pccfVec.length

          val pccfShifted = shiftPccf(shift, pccfVec)

          val negCost = minusCost(m, shift, changes, sigLen)

          val cost = Utils.sumArrayElements(changes, pccfShifted) - negCost

          if(cost > bestSoFar){
            bestSoFar=cost
            bestMu = m
            bestSigma = sigma
            bestShift = shift
          }
        }
      }
    }

    val pccfBestNotShifted = Pccf.pccf(bestMu, bestSigma, sigLen)
    val pccfShiftedFinal = shiftPccf(bestShift, pccfBestNotShifted)
    //Array.fill[Double](bestShift)(0.0) ++ pccfBestNotShifted.drop(bestShift)
    println(s"/// optimPccfMaxLik(): Found optimal Pccf parameters: \n mu=$bestMu, \n s=$bestSigma, \n shift=$bestShift")
    println("///")
    pccfShiftedFinal
  }

}


object Commented{

  def costf(changes:List[Int], locations:List[Int]):Option[Double] = {
    // cost function
    if (changes.length != locations.length || changes.length == 0 || locations.length == 0){
      None
    } else {
      val sse:Double = changes.zip(locations).map(x => Math.pow(x._1 - x._2, 2)).sum
      Some(sse)
    }
  }

  def genPeriodicLocations(acc:List[Int]=List(0), periodT:Int, n:Int):List[Int] = n match {
    // generate locations of periodic indicator function 1's
    case 0 => acc.reverse.tail
    case _ => {
      val ne = acc.head + periodT
      genPeriodicLocations(ne::acc, periodT, n-1)
    }
  }

  //  def findOptimPccf(changes:List[Int], sigLen:Int):Unit={
  //    val sigLen = 100
  //    val (s, periodT, bestLocations) = findOptimIndFunLocations(changes)
  //    val optimIndFunc = IndFunc.constructIndicatorFunc(bestLocations, 10, sigLen)
  //    val sigmaS = List(0.01, 0.1, 0.2, 0.5)
  //    val mu = periodT
  //    val h = 0.1 // threshold for pccf
  //    var bestSoFar = 1.0
  //    for(s <- sigmaS){
  //      val probs = Pccf.pccf(mu, s, sigLen)
  //      val pccfIndFunc = constructIndicatorFuncFromPccf(probs, h)
  //      val overlap = overlapOfIndFuncs(optimIndFunc, pccfIndFunc)
  //      if(bestSoFar < overlap) {
  //        bestSoFar = overlap
  //        println(s"better overlap=$bestSoFar")
  //      }
  //      println(s"sigma=$s, overlap=$overlap")
  //    }
  //    println("Optim pccf: ")
  //  }

  def findOptimIndFunLocations(changes:List[Int]):(Int,Int, List[Int]) ={
    // return starting point and period
    val startingPonts = List.range(0, changes.head)
    val n = changes.length
    val maxPeriod = (0::changes).init.zip((0::changes).tail).map(x=> x._2 - x._1).max
    println(s"max T = $maxPeriod")
    val periods = List.range(1, maxPeriod+1)
    var bestSoFar = 1000000.0
    var bestStartPos = 0
    var bestT = 0
    var bestLocations = List(0)
    for (st <- startingPonts){
      for (tt <- periods){
        val locs = genPeriodicLocations(List(st), tt, n)
        costf(changes, locs) match {
          case Some(v:Double) => {
            if (v < bestSoFar) {
              bestSoFar = v
              bestStartPos = st
              bestT = tt
              bestLocations = locs
              println(s"..New cost = $bestSoFar, start = $bestStartPos, T = $bestT")
            }
          }
          case None => println("")
        }
      }
    }
    println("Lowest cost function value = " + bestSoFar)
    println(bestLocations)
    (bestStartPos, bestT, bestLocations)
  }



}