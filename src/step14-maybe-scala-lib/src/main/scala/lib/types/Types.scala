package lib.types

abstract class DetectorOutput
case class CusumOutput(cdes:Array[Int], sPos:Array[Double], sNeg:Array[Double]) extends DetectorOutput
case class SignAndOutStat(sig:Array[Double], outStat:CusumOutput) extends DetectorOutput
case class OptimOutput(sensitivityV:Array[Double], delays:Array[Double], numOfFa:Array[Double], lossV:Array[Double]) extends DetectorOutput

abstract class AbstractSettings
case class StaticSettings(s:Double) extends AbstractSettings
case class DynamicSettings(indFunction:Array[Int], sWhen0:Double, sWhen1:Double) extends AbstractSettings
