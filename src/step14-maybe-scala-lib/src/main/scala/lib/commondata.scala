package lib.commondata

trait inputData{
  val dataDir = "/home/ms314/gitlocal/phd-monitor/datasets/"

  val sig1 = dataDir + "exchange-rate-bank-of-twi/data/sigTwi.dat"
  val sig2 = dataDir + "internet-traffic-data-in-bits-fr/data/sigInternetTraffic.dat"
  val sig3 = dataDir + "monthly-lake-erie-levels/data/sigLakeLevel.dat"

  val sig4 = dataDir + "number-of-earthquakes-per-year/data/sigNumOfEarthQuakes.dat"

  val sig5 = dataDir + "time-that-parts-for-industrial/data/sigTimesNeeded.dat"
  val sig6 = dataDir + "wolfer-sunspot-numbers/data/sigSunspotNumber.dat"

  val chp1 = dataDir + "exchange-rate-bank-of-twi/data/changesTwi.dat"
  val chp2 = dataDir + "internet-traffic-data-in-bits-fr/data/changesInternetTraffic.dat"
  val chp3 = dataDir + "monthly-lake-erie-levels/data/changesErieLake.dat"

  val chp4 = dataDir + "number-of-earthquakes-per-year/data/changesNumOfEarthQuakes.dat"

  val chp5 = dataDir + "time-that-parts-for-industrial/data/changesParts.dat"
  val chp6 = dataDir + "wolfer-sunspot-numbers/data/changes.dat"

  val sigMap = Map(
    1 -> sig1,
    2 -> sig2,
    3 -> sig3,
    4 -> sig4,
    5 -> sig5,
    6 -> sig6
  )
  val chpMap = Map(
    1 -> chp1,
    2 -> chp2,
    3 -> chp3,
    4 -> chp4,
    5 -> chp5,
    6 -> chp6
  )
}

trait indFnSettings{
  // minLeft, minRight, minBetween
  val indFnSettings = Map(
    1 -> (10, 7,10),
    2 -> (5,50,10),
    3 -> (5,5,10),
    4 -> (6,4,3),
    5 -> (5,5,10),
    6 -> (2,5,3)
  )
  // val (minLeft, minRight, minBetween) = indFnSettings(i)
}

trait outputStep11{
  var outDir11 = "./target/step11/"
//  val optIndfPccf1 = outDir11 + "if_pccf_sig1.dat"
//  val optIndfPccf2 = outDir11 + "if_pccf_sig2.dat"
//  val optIndfPccf3 = outDir11 + "if_pccf_sig3.dat"
//  val optIndfPccf4 = outDir11 + "if_pccf_sig4.dat"
//  val optIndfPccf5 = outDir11 + "if_pccf_sig5.dat"
//  val optIndfPccf6 = outDir11 + "if_pccf_sig6.dat"
//
//  val optPccfFromIndF = Map(
//    1 -> optIndfPccf1,
//    2 -> optIndfPccf2,
//    3 -> optIndfPccf3,
//    4 -> optIndfPccf4,
//    5 -> optIndfPccf5,
//    6 -> optIndfPccf6
//  )

  def getPaths(sigNum:Int):(String, String, String)={
    val pccfPath = outDir11 + "pccf_sig" + sigNum + ".dat"
    val indFunInput = outDir11 + "if_sig" + sigNum + ".dat"
    val indFunOutput = outDir11 + "if_pccf_sig" + sigNum + ".dat"
    (pccfPath, indFunInput, indFunOutput)
  }
}

//val pccfOptLik = OptimPccf.optimPccfMaxLik(changes, sig.length, 0.9, 1) //changes(0)-1: max start point

/// MOVED to commondata.scala
//      if(i==4){
//        minLeft=5
//        minRight=5
//        minBetween=10
//      } else if (i==0){
//        minLeft=5
//        minRight=5
//        minBetween=10
//      } else if (i==1){
//        minLeft=50
//        minRight=50
//        minBetween=10
//      } else if (i==2){
//
//      } else if (i==3){
//        minLeft=6
//        minRight=4
//        minBetween=3
//      } else if (i==4) {
//
//      } else if (i==5) {
//        minLeft=6
//        minRight=4
//        minBetween=3
//      }
