package lib.perfmetrics

import lib.pccf.Pccf

object PerformanceMetrics{
  //
  // Delay and FA metrics
  //
  private def posDist(cde:Int, chp:Int, n:Int):Int={
    assert(cde >= 0 && chp >= 0 && cde <= n && chp <= n)
    if (cde >= chp) cde-chp else n-chp
  }

  private def closestDetection(chp:Int, detections:Array[Int], n:Int):Int={
    if(detections.length>0)
      detections.map(x => posDist(x, chp, n)).min
    else
      n-chp
  }

    def detectionDelays(n:Int, changes:Array[Int], detections:Array[Int]):Array[Int]={
        changes.map(x=>closestDetection(x, detections, n))
    }

//    def avgDetectionDelay(n:Int, changes:Array[Int], detections:Array[Int]):Double={
//        val delays = detectionDelays(n, changes, detections)
//        delays.sum / delays.length.toDouble
//    }

    def numOfFalseAlarms(n:Int, changes:Array[Int], cdes:Array[Int]): Int = {
//        val cdesBeforeFirstChp = cdes.filter(x => x < changes.head)
        val changesAug = 0 +: changes :+ n
        val changesBins = (changesAug.init zip changesAug.tail).toArray
        // count cdes between changes
        val numsInBins = changesBins.map{ x => cdes.filter(e=> (e >= x._1 && e < x._2)).length}
        // numsInBins(0): cdes before first change
        val numFa = numsInBins(0) + numsInBins.tail.map(x => if (x>1) x-1 else 0).sum
        numFa
    }

    def costF(n:Int, delayAvg:Double, numOfFa:Int):Double={
        math.log(delayAvg/n + 1.0) + math.log(numOfFa.toDouble/n + 1.0)
    }
}

object PerfMetricsBinPccf{

  private def numOfChangesWithinIntervals(indf:Array[Int], chps:Array[Int]):Array[Int]={
    // calculate number of changpoints within each interval of indicator function
    val enc = Pccf.encodeIndf(indf)
    val runs = 0 +: enc.map{var i=0; (x) => {i+=x._1; i}}
    val bounds = runs.init zip runs.tail
    val encChps = Array.fill(indf.length)(0)
    for (i<-chps) encChps(i) = 1
    val counts = bounds.map(x => encChps.slice(x._1, x._2).sum)
    counts
  }

  def tpNumIndf(indf:Array[Int], chps:Array[Int]):Int = {
    // number of changes inside indf
    chps.foldLeft(0){ var i = -1; (a,_) => {i+=1; a + indf(chps(i))} }
  }

  def fpNumIndf(indf:Array[Int], chps:Array[Int]):Int={
    val counts = Pccf.encodeIndf(indf) zip numOfChangesWithinIntervals(indf, chps)
    val fps = counts.filter(x => x._1._2 == 1 && x._2 ==0)
    fps.length
  }

  def fnNumIndf(indf:Array[Int], chps:Array[Int]):Int={
    // number of changes outside indf
    val counts = Pccf.encodeIndf(indf) zip numOfChangesWithinIntervals(indf, chps)
    val fns = counts.filter(x => x._1._2 == 1 && x._2 ==0)
    fns.length
//    chps.foldLeft(0){ var i = -1; (a,_) => {
//      i+=1
//      val acc = if(indf(chps(i))==0) 1 else 0
//      a + acc}
//    }
  }

  case class PerfMetricsBinPccfOutput(tpNum:Int, fpNum:Int, fnNum:Int)

  def allStats(indf:Array[Int], chps:Array[Int]):PerfMetricsBinPccfOutput={
    PerfMetricsBinPccfOutput(tpNumIndf(indf, chps), fpNumIndf(indf,chps), fnNumIndf(indf,chps))
  }

}

object PerfMetricsBinaryCdes{
  // binary classification metrics for CDEs
  // tp: CDE after change but before net change
  case class PerformanceRes(tpNum: Int, fpNum:Int, fnNum:Int)
  def tpNum(changes:Array[Int], cdes:Array[Int], sigLen:Int):PerformanceRes={
    if (cdes.isEmpty) PerformanceRes(0, 0, changes.length)
    else {
      val changesAug = changes :+ sigLen
      val bins = changesAug.init zip changesAug.tail
      val cdesWithBins = for(c <- bins) yield {(c, cdes)}
      val cdesInBins = cdesWithBins.map(e => (((e._1._1, e._1._2), e._2.filter(x => (x >= e._1._1 && x<=e._1._2)))))
      val tps = cdesInBins.map(e => e._2).filter(_.nonEmpty).map(_.head)
      val fps = cdesInBins.map(e => e._2).filter(_.nonEmpty).map(_.tail)
      val fns = cdesInBins.filter(x => x._2.isEmpty).map(x => x._1._1)
      PerformanceRes(tps.length, fps.length, fns.length)
    }
  }
}