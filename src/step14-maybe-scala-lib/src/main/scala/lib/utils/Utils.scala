package lib.utils

import scala.io.Source
import java.io._

import lib.types.OptimOutput

object UtilsIO{
  def read(fpath:String):Array[Double]={
    val x = Source.fromFile(fpath).getLines().toArray.map(_.toDouble)
    x
  }

  def writeMatrix(M:Array[Array[Double]], fpath:String="./target/")={
    val p = new PrintWriter(new File(fpath))
    for(rNum <- 0 to M.length-1){
      val r = M(rNum)
      val s = r.mkString(", ")
      p.println(s)
    }
    p.close()
    println(s".. Save matrix into $fpath")
  }

  def writeVec[N: math.Numeric](fpath:String, x:Array[N]):Unit={
    def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
      val p = new java.io.PrintWriter(f)
      try { op(p) } finally { p.close() }
    }

    printToFile(new File(fpath)) { p => x.foreach(p.println) }
  }

  def writeVec[N: math.Numeric](x:Array[N], fpath:String):Unit={writeVec(fpath, x)}

  def saveOptimOutput(df:OptimOutput, suffix:String="", outFolder:String="./target/")={
    writeVec(outFolder+"sensitivity_" + suffix + ".dat", df.sensitivityV)
    writeVec(outFolder+"numOfFa_" + suffix + ".dat", df.numOfFa)
    writeVec(outFolder+"delay_"+suffix+".dat", df.delays)
    writeVec(outFolder+"loss_" + suffix + ".dat", df.lossV)
    println(s".. Save optimization results into $outFolder")
  }

  def saveOptimOutputWithNum(df:OptimOutput, sigNum:Int, suffix:String="", outFolder:String="./target/")={
    writeVec(outFolder+ sigNum + "sensitivity_" + suffix + ".dat", df.sensitivityV)
    writeVec(outFolder+ sigNum + "numOfFa_" + suffix + ".dat", df.numOfFa)
    writeVec(outFolder+ sigNum + "delay_"+suffix+".dat", df.delays)
    writeVec(outFolder+ sigNum + "loss_" + suffix + ".dat", df.lossV)
    println(s".. Save optimization results into $outFolder")
  }

}

object UtilsMath{

}

object Utils{

  def sayRun(tasknum:String):Unit={
    println(s"... Step: $tasknum")
  }
  def sayStop():Unit={
    println("End")
  }

  def sumArrayElements(indices:Array[Int], m:Array[Double]):Double={
    indices.foldLeft(0.0){val a = m; var i = -1; (c,_) => {i+=1; c+a(indices(i))} }
  }


}
