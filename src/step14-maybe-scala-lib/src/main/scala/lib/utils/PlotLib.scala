package lib.utils

import breeze.linalg.DenseVector
import breeze.plot.{Figure, plot}
import lib.types._

object PlotLib{

  def plotIndicatorFunction(indf:Array[Int]): Unit ={
    val fNew = Figure("Indicator Function")
    val p=fNew.subplot(0)
    p += plot(DenseVector((0 to indf.length-1).toArray.map(_.toDouble)),DenseVector(indf.map(_.toDouble)))
  }

  def plotStatAndDynOutputStep4(optimStatOut: OptimOutput,
                                optimDynamicOut: OptimOutput,
                                indF:Array[Int],
                                outfig:String="./target/out-costf-bayes-2019-02.PNG"): Unit ={
    val f = Figure("Bayes")
    f.height=640
    //    f.width=480
    f.subplot(4,1,1)
    val p1 = f.subplot(0)
    p1.title = "Delay"
    p1.xlabel="sensitivity"
    val p2 = f.subplot(1)
    p2.title="Num. of FAs"
    p2.xlabel="sensitivity"
    val p3 = f.subplot(2)
    p3.title="Cost function"
    p3.xlabel="sensitivity"
    val p4 = f.subplot(3)
    p4.title = "Indicator function"
    val x = optimStatOut.sensitivityV
    p1 += plot(DenseVector(x), DenseVector(optimStatOut.delays),'.',"black")
    p1 += plot(DenseVector(x), DenseVector(optimStatOut.delays),'-',"black")
    p1 += plot(DenseVector(x), DenseVector(optimDynamicOut.delays),'.', "red")
    p1 += plot(DenseVector(x), DenseVector(optimDynamicOut.delays),'-', "red")
    //    p1.xaxis.setUpperBound(100)
    //    p1.yaxis.setUpperBound(20)
    //
    p2 += plot(DenseVector(x), DenseVector(optimStatOut.numOfFa),'.',"black")
    p2 += plot(DenseVector(x), DenseVector(optimStatOut.numOfFa),'-',"black")
    p2 += plot(DenseVector(x), DenseVector(optimDynamicOut.numOfFa),'.', "red")
    p2 += plot(DenseVector(x), DenseVector(optimDynamicOut.numOfFa),'-', "red")
    //
    p3 += plot(DenseVector(x), DenseVector(optimStatOut.lossV),'.',"black")
    p3 += plot(DenseVector(x), DenseVector(optimStatOut.lossV),'-',"black")
    p3 += plot(DenseVector(x), DenseVector(optimDynamicOut.lossV),'.', "red")
    p3 += plot(DenseVector(x), DenseVector(optimDynamicOut.lossV),'-', "red")
    //
    p4 += plot(DenseVector((0 to indF.length-1).toArray.map(_.toDouble)),
               DenseVector(indF.map(_.toDouble)), '-', "blue")
    f.saveas(outfig)
    println(s"Save Figure into $outfig")
  }
}
