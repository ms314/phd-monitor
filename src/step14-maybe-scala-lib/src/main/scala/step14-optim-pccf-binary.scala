package steps

import lib.commondata.{indFnSettings, inputData}
import lib.pccf._
import lib.utils._
import steps.step11.{chpMap, indFnSettings, sigMap}
import java.io._


object step14 extends inputData with indFnSettings {
  def runPccfsForRealSignals(outDir:String = "./target/step14/"):Unit={
    // Calculate and save to files optimal Pccfs for real signals
    var Nsample = 30
    for(i <- 1 to 6){
      println(s"///// Calc. optim Pccf for signal $i /////")
      val sig = UtilsIO.read(sigMap(i))
      val changes = UtilsIO.read(chpMap(i)).map(_.toInt)
      val (minLeft, minRight, minBetween) = indFnSettings(i)

      val indFn  = IndFunc.constructIndicatorFunc(changes,sig.length, IndFunc.settingsIF(minLeft, minRight, minBetween))

      val outfile       = outDir + "pccf_sig"+ (i+0) + ".dat"
      val outfileIF     = outDir + "if_sig"+ (i+0) + ".dat"
      val outfileIFpccf = outDir + "if_pccf_sig"+ (i+0) + ".dat"
      val pccfOpt = OptimPccf.optimPccfBinary(changes, indFn, 5, 0.9, 20, outDir + "stats_sig" + i + ".dat")

      if(1==0){
        // old optimization
        val outfileOld       = outDir + "old_pccf_sig"+ (i+0) + ".dat"
        val pccfOptOld = OptimPccf.findOptimPccfUsingIndFunc(changes, indFn, 5, 0.9, 20)
        val outfileIFOld     = outDir + "old_if_sig"+ (i+0) + ".dat"
        val outfileIFpccfOld = outDir + "old_if_pccf_sig"+ (i+0) + ".dat"
        UtilsIO.writeVec(pccfOptOld._1, outfileOld)
        UtilsIO.writeVec(pccfOptOld._2, outfileIFpccfOld)
        println(s"Save output: $outfileOld")
        println(s"Save output: $outfileIFpccfOld")
      }

      UtilsIO.writeVec(pccfOpt._1, outfile)
      UtilsIO.writeVec(pccfOpt._2, outfileIFpccf)
      UtilsIO.writeVec(indFn, outfileIF)
      println(s"Save output: $outfile")
      println(s"Save output: $outfileIFpccf")
      println(s"Save output: $outfileIF")
    }
    Utils.sayStop()
  }



}
