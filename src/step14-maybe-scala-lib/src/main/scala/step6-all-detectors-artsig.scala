package steps

import breeze.linalg.{DenseVector, linspace}
import breeze.plot.{Figure, plot}
import lib.learndetectors.LearnDetectors
import lib.utils.AdaptiveSettings
import lib.types._
import scala.io.Source
import java.io._
import lib.utils._

object step6{

  def runSim(): Unit ={
    val sigma = 1.0
    val N = 100

    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian()*sigma + 1.0).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val changes = Array(100, 200)
    val indF = AdaptiveSettings.constructIndFunction(sig.length, changes.toList, 20, 20)

    val paramRangeCusum = linspace(1.0, 25.00, N).toArray
    val paramRangeAdwin = linspace(1.5, 3.5, N).toArray
    val paramrangeBayes = linspace(1.0, 35.0, N).toArray

    val optStaticCusum = LearnDetectors.optim(sig, changes, LearnDetectors.createCusumRef(0.0), paramRangeCusum)
    val optDynamicCusum = LearnDetectors.optim(sig, changes,
      LearnDetectors.createCusumRefDynamic(indF, 0.0, paramRangeCusum(paramRangeCusum.length-1)), paramRangeCusum)

    val optStaticAdwin = LearnDetectors.optim(sig, changes, LearnDetectors.createAdwinRef(), paramRangeAdwin)
    val optDynamicAdwin = LearnDetectors.optim(sig, changes,
      LearnDetectors.createAdwinRefDynamic(indF, paramRangeAdwin(paramRangeAdwin.length-1)), paramRangeAdwin)

    val optStaticBayes = LearnDetectors.optim(sig, changes, LearnDetectors.createBayesRef(0.0), paramrangeBayes)
    val optDynamicBayes = LearnDetectors.optim(sig, changes,
      LearnDetectors.createBayesRefDynamic(indF, 0.0, paramrangeBayes(paramrangeBayes.length-1)), paramrangeBayes)

    val outF = "./target/step6/"
    UtilsIO.saveOptimOutput(optStaticCusum , "CusumStat", outF)
    UtilsIO.saveOptimOutput(optDynamicCusum, "CusumDyn" , outF)
    UtilsIO.saveOptimOutput(optStaticAdwin , "AdwinStat", outF)
    UtilsIO.saveOptimOutput(optDynamicAdwin, "AdwinDyn" , outF)
    UtilsIO.saveOptimOutput(optStaticBayes , "BayesStat", outF)
    UtilsIO.saveOptimOutput(optDynamicBayes, "BayesDyn" , outF)
    UtilsIO.writeVec(outF+"inputSig.dat", sig)
    UtilsIO.writeVec(outF+"indFunc.dat", indF)

    if(1==0){
      val f = Figure()
      f.subplot(4,1,1)
      val p1 = f.subplot(0)
      p1.title = "Input signal"
      p1.xlabel="time"
      val p2 = f.subplot(1)
      p2.title = "Delay"
      p2.xlabel="sensitivity"
      val p3 = f.subplot(2)
      p3.title="Num. of FAs"
      p3.xlabel="sensitivity"
      val p4 = f.subplot(3)
      p4.title="Cost function"
      p4.xlabel="sensitivity"
      var x = (0 to sig.length-1).toArray.map(x=>x.toDouble)
      // !!! val x = optStatic.sensitivityV
      p1 += plot(DenseVector(x), DenseVector(sig),'-')
      // Cusum
      val cusumColor = "black"
      x = (0 to N-1).toArray.map(x => x.toDouble)
      p2 += plot(DenseVector(x), DenseVector(optStaticCusum.delays),'-', cusumColor)
      p2 += plot(DenseVector(x), DenseVector(optDynamicCusum.delays),'.', cusumColor)
      p2 += plot(DenseVector(x), DenseVector(optDynamicCusum.delays),'-', cusumColor)
      //
      p3 += plot(DenseVector(x), DenseVector(optStaticCusum.numOfFa),'-', cusumColor)
      p3 += plot(DenseVector(x), DenseVector(optDynamicCusum.numOfFa),'.', cusumColor)
      p3 += plot(DenseVector(x), DenseVector(optDynamicCusum.numOfFa),'-', cusumColor)
      //
      p4 += plot(DenseVector(x), DenseVector(optStaticCusum.lossV),'-', cusumColor)
      p4 += plot(DenseVector(x), DenseVector(optDynamicCusum.lossV),'.', cusumColor)
      p4 += plot(DenseVector(x), DenseVector(optDynamicCusum.lossV),'-', cusumColor)
      //
      // Adwin
      val adwinColor = "green"
      p2 += plot(DenseVector(x), DenseVector(optStaticAdwin.delays),'-', adwinColor, "Adwin")
      p2 += plot(DenseVector(x), DenseVector(optDynamicAdwin.delays),'.', adwinColor)
      p2 += plot(DenseVector(x), DenseVector(optDynamicAdwin.delays),'-', adwinColor)
      //
      p3 += plot(DenseVector(x), DenseVector(optStaticAdwin.numOfFa),'-', adwinColor)
      p3 += plot(DenseVector(x), DenseVector(optDynamicAdwin.numOfFa),'.', adwinColor)
      p3 += plot(DenseVector(x), DenseVector(optDynamicAdwin.numOfFa),'-', adwinColor)
      //
      p4 += plot(DenseVector(x), DenseVector(optStaticAdwin.lossV),'-', adwinColor)
      p4 += plot(DenseVector(x), DenseVector(optDynamicAdwin.lossV),'.', adwinColor)
      p4 += plot(DenseVector(x), DenseVector(optDynamicAdwin.lossV),'-', adwinColor)
      //
      // Bayes
      val bayesColor = "red"
      p2 += plot(DenseVector(x), DenseVector(optStaticBayes.delays),'-', bayesColor)
      p2 += plot(DenseVector(x), DenseVector(optDynamicBayes.delays),'.', bayesColor)
      p2 += plot(DenseVector(x), DenseVector(optDynamicBayes.delays),'-', bayesColor)
      //
      p3 += plot(DenseVector(x), DenseVector(optStaticBayes.numOfFa),'-',bayesColor)
      p3 += plot(DenseVector(x), DenseVector(optDynamicBayes.numOfFa),'.', bayesColor)
      p3 += plot(DenseVector(x), DenseVector(optDynamicBayes.numOfFa),'-', bayesColor)
      //
      p4 += plot(DenseVector(x), DenseVector(optStaticBayes.lossV),'-', bayesColor)
      p4 += plot(DenseVector(x), DenseVector(optDynamicBayes.lossV),'.', bayesColor)
      p4 += plot(DenseVector(x), DenseVector(optDynamicBayes.lossV),'-', bayesColor)
      //
      val outfig = "./target/step6-all.PNG"
      f.saveas(outfig, 270)
    }
  }
}

