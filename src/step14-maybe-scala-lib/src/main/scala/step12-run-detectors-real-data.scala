package steps

import breeze.linalg.linspace
import lib.commondata._
import lib.learndetectors.LearnDetectors
import lib.pccf._
import lib.utils._
//import steps.step11.indFnSettings


object step12 extends inputData with outputStep11 with indFnSettings {
  // Input: using output from step11
  //
  // Outputs:
  // Delay and FA
  // for static detectors
  // for ideal IF
  // for IF from Pccf

  def run():Unit={
    Utils.sayRun("12")

    val N = 2000

    for(i <- 1 to 6){
      println(s"Process signal $i")
      val p = getPaths(i)
      val pccf = p._1
      val indFn = p._2
      val indFnPccf = UtilsIO.read(p._3).map(_.toInt)

      val sig = UtilsIO.read(sigMap(i))
      val chps = UtilsIO.read(chpMap(i)).map(_.toInt)
      val (minLeft, minRight, minBetween) = indFnSettings(i)
      val IF  = IndFunc.constructIndicatorFunc(chps, sig.length, IndFunc.settingsIF(minLeft, minRight, minBetween))
//      val IF = indFnPccf
      val paramRangeCusum = linspace(1.0, 100.0, N).toArray
//      val paramRangeAdwin = linspace(1.5, 3.5, N).toArray
//      val paramrangeBayes = linspace(1.0, 35.0, N).toArray

      val optStaticCusum = LearnDetectors.optim(sig, chps, LearnDetectors.createCusumRef(0.0), paramRangeCusum)
      val cusumRefDyn = LearnDetectors.createCusumRefDynamic(IF, 0.0, 999.0)
      val optDynamicCusum = LearnDetectors.optim(sig, chps, cusumRefDyn, paramRangeCusum)

//      val optStaticAdwin = LearnDetectors.optim(sig, chps, LearnDetectors.createAdwinRef(), paramRangeAdwin)
//      val optDynamicAdwin = LearnDetectors.optim(sig, chps,
//        LearnDetectors.createAdwinRefDynamic(IF, paramRangeAdwin(paramRangeAdwin.length-1)), paramRangeAdwin)
//
//      val optStaticBayes = LearnDetectors.optim(sig, chps, LearnDetectors.createBayesRef(0.0), paramrangeBayes)
//      val optDynamicBayes = LearnDetectors.optim(sig, chps,
//        LearnDetectors.createBayesRefDynamic(IF, 0.0, paramrangeBayes(paramrangeBayes.length-1)), paramrangeBayes)


      val outF = "./target/step12/"
      UtilsIO.saveOptimOutputWithNum(optStaticCusum , i, "CusumStat", outF)
      UtilsIO.saveOptimOutputWithNum(optDynamicCusum, i, "CusumDyn" , outF)
//      UtilsIO.saveOptimOutputWithNum(optStaticAdwin , i, "AdwinStat", outF)
//      UtilsIO.saveOptimOutputWithNum(optDynamicAdwin, i, "AdwinDyn" , outF)
//      UtilsIO.saveOptimOutputWithNum(optStaticBayes , i, "BayesStat", outF)
//      UtilsIO.saveOptimOutputWithNum(optDynamicBayes, i, "BayesDyn" , outF)
      UtilsIO.writeVec(outF+"inputSig.dat", sig)
      UtilsIO.writeVec(outF+"indFunc" + i + ".dat", IF)

    }

    Utils.sayStop()
  }

}