package tests

import lib.perfmetrics._
import lib.utils.UtilsIO
import steps.step13.chpMap
import lib.commondata._

object TestPccfBinary extends inputData {
  def run():Unit={
//    val indf = UtilsIO.read("./target/step14/if_pccf_sig1.dat")
//    val chps = UtilsIO.read(chpMap(1))
//    val numFn = PerfMetricsBinPccf.fnNumIndf(indf.map(_.toInt), chps.map(_.toInt))


    val r1 = PerfMetricsBinPccf.allStats(Array(1,1,1,0,0,0,1), Array(1,2,3,4,5))

    val ind4 = UtilsIO.read("./target/step14/if_pccf_sig4.dat").map(_.toInt)
    val chps4 = UtilsIO.read(chpMap(4)).map(_.toInt)
    println(ind4.mkString(","))
    println(chps4.mkString(","))
    println(ind4.length, chps4.max)
    val rSig4 = PerfMetricsBinPccf.allStats(ind4, chps4)
    println(s"tp/fp/fn num sig4 = ", rSig4.tpNum, rSig4.fpNum, rSig4.fnNum)

//    assert(r1.tpNum == 2)
//    assert(r1.fnNum == 1) // correct fn num, now it counts by number of changes
//    assert(r1.fpNum == 1)

  }
}
