package steps

import breeze.linalg.{DenseVector, linspace}
import breeze.plot.{Figure, plot, _}
import lib.learndetectors.LearnDetectors
import lib.utils.AdaptiveSettings
import lib.types._

import scala.io.Source
import java.io._
import lib.utils.UtilsIO._
import lib.utils.UtilsIO.writeVec
import lib.utils._

class AveragedOptimOutput(sensitivityVec:Array[Double]){
  val N = sensitivityVec.length
  val sensitivity = sensitivityVec
  var avgDelay = new Array[Double](N)
  var avgNumFa = new Array[Double](N)
  var avgLoss  = new Array[Double](N)

  def calcAveraged()={
    avgLoss = avgLoss.map(x=>x/N.toDouble)
    avgDelay = avgDelay.map(x=>x/N.toDouble)
    avgNumFa = avgNumFa.map(x=>x/N.toDouble)
  }
}


object step7{

  def run(outF:String = "./target/step7/")={
    val Nsim = 50
    val Nparamrange = 100
    val changes = Array(100, 200)
    val sigma = 0.5
    val segmentLength = 100
    val indF = AdaptiveSettings.constructIndFunction(3 * segmentLength, changes.toList, 20, 20)

    val paramRangeCusum = linspace(3.0, 5.0 , Nparamrange).toArray
    val paramRangeAdwin = linspace(1.5, 3.5  , Nparamrange).toArray
    val paramrangeBayes = linspace(1.0, 35.0 , Nparamrange).toArray

    val s0Cusum = 999.0 // paramRangeCusum(paramRangeCusum.length-1)
    val s0Adwin = 999.0 // paramRangeAdwin(paramRangeAdwin.length-1)
    val s0Bayes = 999.0 // paramrangeBayes(paramrangeBayes.length-1)

    val avgCusumStat = new AveragedOptimOutput(paramRangeCusum)
    val avgCusumDyn  = new AveragedOptimOutput(paramRangeCusum)

    val avgAdwinStat = new AveragedOptimOutput(paramRangeAdwin)
    val avgAdwinDyn  = new AveragedOptimOutput(paramRangeAdwin)
    val avgBayesStat = new AveragedOptimOutput(paramrangeBayes)
    val avgBayesDyn  = new AveragedOptimOutput(paramrangeBayes)

    def addTwoVec(v1:Array[Double], v2:Array[Double]):Array[Double]={(v1 zip v2).map(x => x._1 + x._2)}

    def updAvgPerf(avgPerf:AveragedOptimOutput, newData:OptimOutput) = {
      avgPerf.avgDelay = addTwoVec(avgPerf.avgDelay, newData.delays)
      avgPerf.avgNumFa = addTwoVec(avgPerf.avgNumFa, newData.numOfFa)
      avgPerf.avgLoss  = addTwoVec(avgPerf.avgLoss, newData.lossV)
    }

    var lossAvgDyn    = Array[Double]()

    for(k<-0 to Nsim-1){
      println(">>> ", k.toDouble/Nsim)
      val sig1 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
      val sig2 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma + 1.0).toArray
      val sig3 = (1 to segmentLength).map(_ => scala.util.Random.nextGaussian()*sigma).toArray
      val sig = sig1 ++ sig2 ++ sig3

      val optStaticCusum = LearnDetectors.optim(sig, changes, LearnDetectors.createCusumRef(0.0), paramRangeCusum)

      val optDynamicCusum = LearnDetectors.optim(sig, changes,
        LearnDetectors.createCusumRefDynamic(indF, 0.0, s0Cusum), paramRangeCusum)

      val optStaticAdwin = LearnDetectors.optim(sig, changes, LearnDetectors.createAdwinRef(), paramRangeAdwin)

      val optDynamicAdwin = LearnDetectors.optim(sig, changes,
        LearnDetectors.createAdwinRefDynamic(indF, s0Adwin), paramRangeAdwin)

      val optStaticBayes = LearnDetectors.optim(sig, changes, LearnDetectors.createBayesRef(0.0), paramrangeBayes)

      val optDynamicBayes = LearnDetectors.optim(sig, changes,
        LearnDetectors.createBayesRefDynamic(indF, 0.0, s0Bayes), paramrangeBayes)

      updAvgPerf(avgCusumStat, optStaticCusum)
      updAvgPerf(avgCusumDyn , optDynamicCusum)

      updAvgPerf(avgAdwinStat, optStaticAdwin)
      updAvgPerf(avgAdwinDyn , optDynamicAdwin)

      updAvgPerf(avgBayesStat, optStaticBayes)
      updAvgPerf(avgBayesDyn , optDynamicBayes)

    }


    avgCusumStat.calcAveraged()
    avgCusumDyn.calcAveraged()
    avgAdwinStat.calcAveraged()
    avgAdwinDyn.calcAveraged()
    avgBayesStat.calcAveraged()
    avgBayesDyn.calcAveraged()

    def plotAveragedResults(outfig:String)={
      val f = Figure("avg")
      f.subplot(2,1,1)
      val p1 = f.subplot(0)
      p1.title  = "Avg. Delay"
      p1.ylabel = "Delay"
      p1.xlabel = "Sensitivity"
      val p2 = f.subplot(1)
      p2.title="Avg. Num. of FAs"
      p2.xlabel="sensitivity"
      //
      p1 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumStat.avgDelay),'.',"black")
      p1 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumStat.avgDelay),'-',"black")
      p1 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumDyn.avgDelay),'.',"red")
      p1 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumDyn.avgDelay),'-',"red")
      //
      p2 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumStat.avgNumFa),'.',"black")
      p2 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumStat.avgNumFa),'-',"black")
      p2 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumDyn.avgNumFa),'.',"red")
      p2 += plot(DenseVector(paramRangeCusum), DenseVector(avgCusumDyn.avgNumFa),'-',"red")

      f.saveas(outfig)
      println(s"Save figure into $outfig")
    }
    plotAveragedResults("./target/step7-avg-2019-03-04_7.PNG")

    def saveAveragedResults(df:AveragedOptimOutput, suffix:String="", outFolder:String="./target/")={
      writeVec(outFolder+"sensitivity_" + suffix + ".dat", df.sensitivity)
      writeVec(outFolder+"numOfFa_" + suffix + ".dat", df.avgNumFa)
      writeVec(outFolder+"delay_"+suffix+".dat", df.avgDelay)
      writeVec(outFolder+"loss_" + suffix + ".dat", df.avgLoss)
      println(s".. Save optimization results into $outFolder")
    }
    saveAveragedResults(avgCusumStat, "statCusum", "./target/step7/")
    saveAveragedResults(avgCusumDyn, "dynCusum", "./target/step7/")
  }

}

