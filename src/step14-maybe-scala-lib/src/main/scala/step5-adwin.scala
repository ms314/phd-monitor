package steps
import lib.detectors._
import lib.learndetectors.LearnDetectors
import breeze.linalg._
import breeze.plot.{Figure, plot}
import lib.types._
import lib.utils.AdaptiveSettings

object step5{

  def runSimAdwin()={
    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 1.0).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val changes = Array(100, 200)
    val optStaticAdwin = LearnDetectors.optim(sig, changes, LearnDetectors.createAdwinRef(), linspace(1.5, 3.5, 100).toArray)
    val indF = AdaptiveSettings.constructIndFunction(sig.length, changes.toList, 20, 20)
    val optDynamicAdwin = LearnDetectors.optim(sig, changes, LearnDetectors.createAdwinRefDynamic(indF), linspace(1.5, 3.5, 100).toArray)
    //
    val f = Figure()
    f.subplot(3,1,1)
    val p1 = f.subplot(0)
    p1.title = "Delay"
    p1.xlabel="sensitivity"
    val p2 = f.subplot(1)
    p2.title="Num. of FAs"
    p2.xlabel="sensitivity"
    val p3 = f.subplot(2)
    p3.title="Cost function"
    p3.xlabel="sensitivity"
    val x = optStaticAdwin.sensitivityV
    p1 += plot(DenseVector(x), DenseVector(optStaticAdwin.delays),'.', "black")
    p1 += plot(DenseVector(x), DenseVector(optStaticAdwin.delays),'-', "black")
    p1 += plot(DenseVector(x), DenseVector(optDynamicAdwin.delays),'-', "red")
    p1 += plot(DenseVector(x), DenseVector(optDynamicAdwin.delays),'.', "red")
    //
    p2 += plot(DenseVector(x), DenseVector(optStaticAdwin.numOfFa),'.', "black")
    p2 += plot(DenseVector(x), DenseVector(optStaticAdwin.numOfFa),'-', "black")
    p2 += plot(DenseVector(x), DenseVector(optDynamicAdwin.numOfFa),'.', "red")
    p2 += plot(DenseVector(x), DenseVector(optDynamicAdwin.numOfFa),'-', "red")
    //
    p3 += plot(DenseVector(x), DenseVector(optStaticAdwin.lossV),'.', "black")
    p3 += plot(DenseVector(x), DenseVector(optStaticAdwin.lossV),'-', "black")
    p3 += plot(DenseVector(x), DenseVector(optDynamicAdwin.lossV),'.', "red")
    p3 += plot(DenseVector(x), DenseVector(optDynamicAdwin.lossV),'-', "red")
    val outfig = "./target/out-costf-adwin-2019-02-26.PNG"
    f.saveas(outfig)
  }

  def simpleTest()={
    val sig1 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig2 = (1 to 100).map(_ => scala.util.Random.nextGaussian() + 3.0).toArray
    val sig3 = (1 to 100).map(_ => scala.util.Random.nextGaussian()).toArray
    val sig = sig1 ++ sig2 ++ sig3
    val q = Adwin.adwinNaive(sig, StaticSettings(3.0))
    println(q.mkString(","))
  }

  def runAdwinOutStatDemo(): Unit ={
    Adwin.demoAdwinOutputStatistic("./target/demo-adwin-output-stat.PNG")
  }
  def run(): Unit ={
//    simpleTest()
    runSimAdwin()
//    runAdwinOutStatDemo()
  }
}
