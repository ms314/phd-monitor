lazy val root = (project in file("."))

  .settings(
    name := "Scala lib.",
    scalaVersion := "2.12.8",
    version := "0.1.0-SNAPSHOT",
    
    libraryDependencies ++= Seq(
      "org.scalanlp" %% "breeze" % "0.13.2",
      "org.scalanlp" %% "breeze-natives" % "0.13.2",
      "org.scalanlp" %% "breeze-viz" % "0.13.2"

    )

  )
