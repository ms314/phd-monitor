# Library description

```
// Optimize Pccf for given true changes
run changes.dat --optim-pccf optimPccf.dat

// Run detectors with Pccf
run signal.dat --detector adwin/cusum --out detections.dat --pccf optimPccf.dat

// Run detectors without Pccf 
run signal.dat --detector adwin/cusum --out detections.txt
```

# Next




Maybe: reimplement detectors in Scala.

Adwin output statistic (commit `ee5034b`)

![img4](./img/step5-adwin-demo-out-stat.png)

Cusum output statistic (commit `e01ce0f`)

![img3](./img/demo-cusum-out-stat.png)

Use later as a submodule in visualization steps.

![img1](./img/out-costf-2019-02-24.png)

(commit: `27dd5a1`)

(step4) Bayes detector output statistic example:

![img2](./img/bdetector_probs.png)