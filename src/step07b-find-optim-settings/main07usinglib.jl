include("../Lib/detectors/PageHinkley.jl")
include("../Lib/detectors/BayesDetectorReplica.jl")
include("../Lib/Fractions.jl")
include("../Lib/Fractions2.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/fn_LossFunc.jl")
include("../Lib/detectors/adwin/Adwin.jl")
include("../Lib/detectors/adwin/AdwinNaive.jl")
include("../Lib/Common.jl")

using PageHinkley, BayesChpOrig, Fractions, Fractions2, AdwinModule, AdwinNaive
using DataFrames, CSV
using LearnDetectors

function runAll()
    signals = ["sig" * string(e) for e in 1:6]
    chps = ["chps" * string(e) for e in 1:6]
    s = readJSON("../config_datasets.json")
    println("replace 6 by n_of_signals = s[numofsignals]")

    for i=1:6
        sigpath = s["dir"] * s[signals[i]]
        chpspath = s["dir"] * s[chps[i]]
        println(".. Signal $i:", sigpath)
        println("... Changes $i:", chpspath)

        sig01    = readInputSig(sigpath)
        changes  = readInputChanges(chpspath)

        detectorPH = createPhDetector(delta=0.0)
        detectorAdwin = createAdwinDetector(min_len=10)
        detectorBayes = createBayesianDetector(mu0=0.5)

        optimDetector(sig01,
                      "./out_optim_settings/sig" * string(i) * "_PH" * ".csv",
                      changes=changes,
                      detector=detectorPH,
                      paramrange=0.1:0.1:10,
                      name="PH")


        optimDetector(sig01,
                      "./out_optim_settings/sig" * string(i) * "_Adwin" * ".csv",
                      changes=changes,
                      detector=detectorAdwin,
                      paramrange=0.12:0.01:0.15,
                      name="Adwin")


        optimDetector(sig01,
                      "./out_optim_settings/sig" * string(i) * "_Bayes" * ".csv",
                      changes=changes,
                      detector=detectorBayes,
                      paramrange=100:10:270,
                      name="Bayes")

    end
end 
runAll()
