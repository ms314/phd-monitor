### Find optimal settings for all detectors for all datasets
include("../Lib/detectors/PageHinkley.jl")
include("../Lib/detectors/BayesDetectorReplica.jl")
include("../Lib/Fractions.jl")
include("../Lib/Fractions2.jl")
include("../Lib/fn_LossFunc.jl")
include("../Lib/detectors/adwin/Adwin.jl")
include("../Lib/detectors/adwin/AdwinNaive.jl")
include("../Lib/Common.jl")

using PageHinkley, BayesChpOrig, Fractions, Fractions2, AdwinModule, AdwinNaive
using DataFrames, CSV

function readInputChanges(fpath)
        _changes = (readdlm(fpath))
        changes = [convert(Int64, e) for e in _changes[:,1]]
end

function readInputSig(fpath)
    _sig01 = scale01(readdlm(fpath)) ## scaled01 signal
    if length(size(_sig01)) == 2
        sig01 = _sig01[:,1]
    elseif length(size(_sig01))==1
        sig01 = _sig01
    else
        sig01 = _sig01
        println(".... check!!!!!!")
    end
    sig01
end

function findOptimSettings(resultMatrix)
    sensVec = resultMatrix[:,3]
    lossVec = resultMatrix[:,8]
    n = length(sensVec)
    @assert n == length(lossVec)
    @assert n > 0
    k = 1
    best_so_far = lossVec[k]
    for i=1:n
        if lossVec[i] < best_so_far
            best_so_far = lossVec[i]
            k = i
        end
    end
    return (sensVec[k], best_so_far)
end


function optimAdwin(x::Array{Float64,1}, trueChp::Array{Int64,1}
                        ; maxDelay=10, printLog=false)
    println("... run optimAdwin()")
    delay_vec       = []
    fa_rate_vec     = []
    ## Start: apply new loss function
    delay_vec2      = []
    fa_rate_vec2    = []
    lossValsVec     = []
    ## End: apply new loss function
    sensetivity_vec = []
    num_of_cdes     = []
    N               = length(x)
    minLen          = 10
    for ecut = 0.12:0.01:0.15
        sensitivity = 1/ecut
        cdes=extractChps(adwinNaive(x; ecut_custom=ecut,min_len=minLen))
        if length(cdes) > 0
            delays=detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength=N)
            probFa=fracOfFa(chps=trueChp, cdes=cdes, maxDelay=maxDelay)
            ## Start: apply new loss function
            delays2 = fnDelays(trueChp, cdes, N)
            probFa2 = numberOfFalseAlarms(trueChp, cdes, N)/N
            ## End: apply new loss function
        else
            delays=[N]
            probFa=0.0
            ## Start: apply new loss function
            delays2=[N]
            probFa2=0.0
            ## End: apply new loss function
        end
        delay = length(delays) == 1 ? delays[1] : mean(delays)
        ## Start: apply new loss function
        delay2 = length(delays2) == 1 ? delays2[1] : mean(delays2)
        lossValue = lossFuncGeneral(delay2, probFa2, length(trueChp), length(cdes))
        ## End: apply new loss function
        if printLog
            @printf("Delay = %3.3f; probFA = %0.3f; ecut=%0.3f; Loss = %0.3f\n",
                    delay, probFa, ecut, lossValue)
        end
        push!(num_of_cdes    , length(cdes))
        push!(delay_vec, delay)
        push!(fa_rate_vec, probFa)
        ## Start: apply new loss function
        push!(delay_vec2     , delay2)
        push!(fa_rate_vec2   , probFa2)
        push!(lossValsVec    , lossValue)
        ## End: apply new loss function
        push!(sensetivity_vec, sensitivity)
    end
    println("end")
    chpsLength = ones(length(delay_vec)) * length(trueChp)
    #hcat(delay_vec, fa_rate_vec, sensetivity_vec, num_of_cdes, chpsLength)
    hcat(delay_vec,
         fa_rate_vec,
         sensetivity_vec, # col 3
         num_of_cdes,
         chpsLength,
         delay_vec2,
         fa_rate_vec2,
         lossValsVec)  # col 8
end



function optimPH(x::Array{Float64,1}, trueChp::Array{Int64,1}; maxDelay=10, printLog=false)
    println("... run optimPH()")
    delay_vec       = []
    fa_rate_vec     = []
    ## Start: apply new loss function
    delay_vec2      = []
    fa_rate_vec2    = []
    lossValsVec     = []
    ## End: apply new loss function
    sensetivity_vec = []
    num_of_cdes     = []
    N               = length(x)
    delta           = 0.0
    # lambda = 5 ## sensitivity
    for lambda in 0.1:0.1:10 ##1:10
        sensitivity = 1.0/lambda
        cdes = detectorPH(x, delta, lambda)
        if length(cdes) > 0
            delays  = detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength=N)
            probFa  = fracOfFa(chps=trueChp, cdes=cdes, maxDelay=maxDelay)
            ## Start: apply new loss function
            delays2 = fnDelays(trueChp, cdes, N)
            probFa2 = numberOfFalseAlarms(trueChp, cdes, N)/N
            ## End: apply new loss function
        else
            delays=[N]
            probFa=0.0
            ## Start: apply new loss function
            delays2=[N]
            probFa2=0.0
            ## End: apply new loss function
        end
        delay  = length(delays) == 1 ? delays[1] : mean(delays)
        ## Start: apply new loss function
        delay2 = length(delays2) == 1 ? delays2[1] : mean(delays2)
        lossValue = lossFuncGeneral(delay2, probFa2, length(trueChp), length(cdes))
        ## End: apply new loss function
        if printLog
            @printf("Delay = %3.3f; probFA = %0.3f; S = %0.3f\n", delay, probFa, sensitivity)
            # @printf("Delay2 = %3.3f; probFA2 = %0.3f; S = %0.3f; Loss = %0.3f\n", delay2, probFa2, sensitivity, lossValue)
        end
        push!(num_of_cdes    , length(cdes))
        push!(delay_vec      , delay)
        push!(fa_rate_vec    , probFa)
        ## Start: apply new loss function
        push!(delay_vec2     , delay2)
        push!(fa_rate_vec2   , probFa2)
        push!(lossValsVec    , lossValue)
        ## End: apply new loss function
        push!(sensetivity_vec, sensitivity)
    end
    println("end")
    chpsLength = ones(length(delay_vec)) * length(trueChp)
    #hcat(delay_vec, fa_rate_vec, sensetivity_vec, num_of_cdes, chpsLength)
    hcat(delay_vec,
         fa_rate_vec,
         sensetivity_vec,
         num_of_cdes,
         chpsLength,
         delay_vec2,
         fa_rate_vec2,
         lossValsVec)
end


function optimBayes(x::Array{Float64,1}, trueChp::Array{Int64,1}; maxDelay=10, printLog=false)
    println("... run optimBayes()")
    delay_vec       = []
    fa_rate_vec     = []
    ## Start: apply new loss function
    delay_vec2      = []
    fa_rate_vec2    = []
    lossValsVec     = []
    ## End: apply new loss function
    sensetivity_vec = []
    num_of_cdes     = []
    N               = length(x)
    for h = 100:10:270
        sensitivity = 1./h
        maxes, mat = bayes_detector(x, lambda = h, mu0=0.5)
        cdes = extract_changes(maxes)
        delays = detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength=N)
        probFa = fracOfFa(chps=trueChp, cdes = cdes, maxDelay = maxDelay)
        delay = length(delays) == 1 ? delays[1] : mean(delays)
        ## Start: apply new loss function
        delays2 = fnDelays(trueChp, cdes, N)
        probFa2 = numberOfFalseAlarms(trueChp, cdes, N)/N
        ## End: apply new loss function
        if printLog
            @printf("Delay = %3.3f; probFA = %0.3f; S = %0.3f\n", delay, probFa, sensitivity)
        end
        push!(num_of_cdes    , length(cdes))
        push!(delay_vec      , delay)
        push!(fa_rate_vec    , probFa)
        push!(sensetivity_vec, sensitivity)
        ## Start: apply new loss function
        delay2 = length(delays2) == 1 ? delays2[1] : mean(delays2)
        lossValue = lossFuncGeneral(delay2, probFa2, length(trueChp), length(cdes))
        ## End: apply new loss function
        ## Start: apply new loss function
        push!(delay_vec2     , delay2)
        push!(fa_rate_vec2   , probFa2)
        push!(lossValsVec    , lossValue)
        ## End: apply new loss function
    end
    println("end")
    chpsLength = ones(length(delay_vec)) * length(trueChp)
    #hcat(delay_vec, fa_rate_vec, sensetivity_vec, num_of_cdes, chpsLength)
    hcat(delay_vec,
         fa_rate_vec,
         sensetivity_vec,
         num_of_cdes,
         chpsLength,
         delay_vec2,
         fa_rate_vec2,
         lossValsVec)
end



function runAll()
    # uncomment: s = readJSON("../config_datasets.json")
    # uncomment: n_of_signals = s["numofsignals"]
    # uncomment: signals = ["sig" * string(e) for e in 1:n_of_signals]
    # uncomment: chps = ["chps" * string(e) for e in 1:n_of_signals]

    optimSettings = DataFrame(sig=[], detector=[], sensitivity=[], loss=[])

    signals = ["sig" * string(e) for e in 1:6]
    chps = ["chps" * string(e) for e in 1:6]
    s = readJSON("../config_datasets.json")
    println("replace 6 by n_of_signals = s[numofsignals]")

    for i=1:6
        sigpath = s["dir"] * s[signals[i]]
        chpspath = s["dir"] * s[chps[i]]
        println(".. Signal $i:", sigpath)
        println("... Changes $i:", chpspath)

        sig01    = readInputSig(sigpath)
        changes  = readInputChanges(chpspath)

        rAdw     = optimAdwin(sig01, changes, maxDelay=10, printLog=true)
        rPH      = optimPH(sig01, changes, printLog=false)
        rBs      = optimBayes(sig01, changes,printLog=false)

        osADW = findOptimSettings(rAdw)
        osPH  = findOptimSettings(rPH)
        osBS  = findOptimSettings(rBs)

        push!(optimSettings, [i, "Adwin", osADW[1], osADW[2]])
        push!(optimSettings, [i, "PH", osPH[1], osPH[2]])
        push!(optimSettings, [i, "Bayes", osBS[1], osBS[2]])

        outpathAdwin = s["outputfolder"]*"task07a/adwin_sig" * string(i) * ".txt"
        println("... save adwin results into: ", outpathAdwin)
        writedlm(outpathAdwin, rAdw, ',')

        outpathPH = s["outputfolder"]*"task07a/ph_sig" * string(i) * ".txt"
        println("... save PH results into: ", outpathPH)
        writedlm(outpathPH, rPH, ',')

        outpathBayes = s["outputfolder"]*"task07a/bayes_sig" * string(i) * ".txt"
        println("... save Bayes results into: ", outpathBayes)
        writedlm(outpathBayes, rBs, ',')
    end
    csvpath = "../commondata/step07b_optim_settings_20180806.csv"
    println("Save optimal settings into: ", csvpath)
    CSV.write(csvpath, optimSettings, header=true)
end
runAll()
