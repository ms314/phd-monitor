#
# Run all detectors for all signals.
# Save detections to output files.
# Plot signals and detections.
#
include("../Lib/detectors/PageHinkley.jl")
include("../Lib/detectors/BayesDetectorReplica.jl")
include("../Lib/Fractions.jl")
include("../Lib/Fractions2.jl")
include("../Lib/fn_LossFunc.jl")
include("../Lib/detectors/adwin/Adwin.jl")
include("../Lib/detectors/adwin/AdwinNaive.jl")
include("../Lib/Common.jl")

using PageHinkley, BayesChpOrig, Fractions, AdwinModule, AdwinNaive, Fractions2

using DataFrames, CSV, Query



"""
Read settings from CSV file.
Return sensitivity for a given detector and signal number.
"""
function read_s(detector::String, sigNum::Int64;
                csvpath = "../commondata/step07b_optim_settings_20180806.csv")
    b = readtable(csvpath, header=true, separator=',')
    x = @from e in b begin
        @where (e.detector == detector) && (e.sig == sigNum)
        @select e
        @collect DataFrame
    end
    @assert size(x) == (1,4)
    s = convert(Array{Float64,1},x[:sensitivity]) ## x[1,3]
    loss = convert(Array{Float64,1},x[:loss])
    (s[1], loss[1])
end


function runPageHinkley(x, trueChp, maxDelayIn,sigNum::Int64)
    s = readJSON("./config_task08.json")
    delta = s["opts" * string(sigNum)]["PH"]["delta"]  #0.0
    lambda = 1.0 / s["opts" * string(sigNum)]["PH"]["s"]

    println("... json PH read, delta=$delta, lambda=$lambda")
    cdes  = detectorPH(x, delta, lambda)
    # Start: use optimal settings
    s2, l2 = read_s("PH", sigNum)
    lambda2 = 1.0/s2
    N = length(x)
    cdes2 = detectorPH(x, delta, lambda2)
    delays2 = fnDelays(trueChp, cdes2, N)
    probFa2 = numberOfFalseAlarms(trueChp, cdes2, N)/N
    delay2 = length(delays2) == 1 ? delays2[1] : mean(delays2)
    lossValue = lossFuncGeneral(delay2, probFa2, length(trueChp), length(cdes2))
    # End: use new optimal settings

    delay = detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength = length(x))
    probFA = fracOfFa(chps=trueChp, cdes=cdes, maxDelay=maxDelayIn)
    ##(delay, probFA, cdes)
    (delay, probFA, cdes, delay2, probFa2, cdes2)
end


function runAdwinNaive(x, trueChp, maxDelayIn, sigNum::Int; scale = true)
    s = readJSON("./config_task08.json")
    # ecut_json = parse(Float64, s["opts" * string(sigNum)]["adwin"]["ecut"])
    ecut_json = 1.0 / s["opts" * string(sigNum)]["adwin"]["s"]
    min_len_json = Int(s["opts" * string(sigNum)]["adwin"]["min_len"])
    println(".. json Adwin read, ecut=$ecut_json, min_len_json=$min_len_json")
    #if scale
    #    y = scale01(x)
    #end
    N = length(x)
    cdes = extractChps(adwinNaive(x; ecut_custom=ecut_json, min_len=min_len_json))
    probFA = fracOfFa(chps=trueChp, cdes = cdes, maxDelay = maxDelayIn)


    # Start: use optimal settings
    s2, l2 = read_s("Adwin", sigNum)
    println("...... Read new optim parame: $s2, corresponding loss is $l2")

    # lambda2 = 1.0/s2[1]
    ##cdes2 = extractChps(adwinNaive(x; ecut_custom=1.0/s2, min_len=min_len_json))
    cdes2 = extractChps(adwinNaive(x; ecut_custom=1.0/s2, min_len=min_len_json))
    delays2 = fnDelays(trueChp, cdes2, N)
    probFa2 = numberOfFalseAlarms(trueChp, cdes2, N)/N
    delay2 = length(delays2) == 1 ? delays2[1] : mean(delays2)
    lossValue = lossFuncGeneral(delay2, probFa2, length(trueChp), length(cdes2))
    # End: use new optimal settings

    if length(cdes) > 0
        # delay = detectionDelay1Chp(trueChp, cdes)
        ##delay = mean(detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength=N))
        delays = fnDelays(trueChp,cdes,N)
        delay = length(delays) == 1 ? delays[1] : mean(delays)
        ##probFa = fracOfFa(chps=trueChp, cdes=cdes, maxDelay=maxDelayIn)
        probFa = numberOfFalseAlarms(trueChp, cdes2,N)/N
    else
        delay = [N]
        probFa = 0.0
        delay2 = [N]
        probFa2 = 0.0
    end
    lossValueOld = lossFuncGeneral(delay, probFa, length(trueChp), length(cdes))
    # (delay, probFA, cdes)
    if sigNum == 2
        println("=== start")
        println("Loss1 = $lossValueOld with ecut=$ecut_json ")
        ec2 = 1.0/s2
        println("Loss2 = $lossValue with ecut = $ec2")
        println("=== end")
    end
    (delay, probFa, cdes, delay2, probFa2, cdes2)
end


function runBayesian(x, trueChp, maxDelayIn, sigNum::Int)
    s = readJSON("./config_task08.json")
    h = 1.0 / float(s["opts" * string(sigNum)]["bayes"]["s"])
    println(".. json Bayes read, h=$h")
    if false ##true
        for h = 100:10:270
            maxes, mat = bayes_detector(x, lambda = h)
            cdes = extract_changes(maxes)
            println(length(cdes), " ; h=$h")
        end
    end
    N = length(x)
    # h = 27
    maxes, mat = bayes_detector(x, lambda = h)
    cdes = extract_changes(maxes)
    delay = detectionDelayMulti(changes=trueChp, cdes=cdes, sigLength=N)
    probFA = fracOfFa(chps=trueChp, cdes = cdes, maxDelay = maxDelayIn)
    # Start: use optimal settings
    s2, l2 = read_s("Bayes", sigNum)
    lambda2 = 1.0/s2[1]
    maxes2, mat2 = bayes_detector(x, lambda = lambda2)
    cdes2 = extract_changes(maxes2)
    delays2 = fnDelays(trueChp, cdes2, N)
    probFa2 = numberOfFalseAlarms(trueChp, cdes2, N)/N
    delay2 = length(delays2) == 1 ? delays2[1] : mean(delays2)
    lossValue = lossFuncGeneral(delay2, probFa2, length(trueChp), length(cdes2))
    # End: use new optimal settings
    # (delay, probFA, cdes)
    (delay, probFA, cdes, delay2, probFa2, cdes2)
end


#### Start: Import from Common.jl
#
# function readInputSig(fpath)
#     _sig01 = scale01(readdlm(fpath)) ## scaled01 signal
#     if length(size(_sig01)) == 2
#         sig01 = _sig01[:,1]
#     elseif length(size(_sig01))==1
#         sig01 = _sig01
#     else
#         sig01 = _sig01
#         println(".... check!!!!!!")
#     end
#     sig01
# end
#
# function readInputChanges(fpath)
#     _changes = (readdlm(fpath))
#     changes = [convert(Int64, e) for e in _changes[:,1]]
# end
#### End: Import from Common.jl

function runDetectors()
    MAX_DELAY = 5
    local_s = readJSON("./config_task08.json")
    s = readJSON("../config_datasets.json")
    n_of_signals = s["numofsignals"]
    signals = ["sig" * string(e) for e in 1:n_of_signals]
    chps = ["chps" * string(e) for e in 1:n_of_signals]

    @assert length(signals) == length(chps)

    for i in 1:n_of_signals
        sigpath = s["dir"] * s[signals[i]]
        chpspath = s["dir"] * s[chps[i]]
        println("\nRead signal " * sigpath)
        println("Read changes " * chpspath)
        changes = readInputChanges(chpspath)
        sig01 = readInputSig(sigpath)

        (d_Bayes, fa_Bayes, cdesBayes, d_Bayes2, fa_Bayes2, cdesBayes2) = runBayesian(sig01, changes, MAX_DELAY, i)
        (d_AdwNaive, fa_AdwNaive, cdesAdwinNaive, d_AdwNaive2, fa_AdwNaive2, cdesAdwinNaive2) = runAdwinNaive(sig01, changes, MAX_DELAY, i)
        (d_PH, fa_PH, cdesPH, d_PH2, fa_PH2, cdesPH2) = runPageHinkley(sig01, changes, MAX_DELAY, i)


        outpathPH = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_PH" * ".txt"
        outpathPH2 = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_PH2" * ".txt"
        println("Save results: $outpathPH")
        writedlm(outpathPH, cdesPH, ',')
        writedlm(outpathPH2, cdesPH2, ',')

        outpathBayes = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_Bayes" * ".txt"
        outpathBayes2 = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_Bayes2" * ".txt"
        println("Save results: $outpathBayes")
        writedlm(outpathBayes, cdesBayes, ',')
        writedlm(outpathBayes2, cdesBayes2, ',')

        outpathAdwin = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_Adwin" * ".txt"
        outpathAdwin2 = s["outputfolder"]*"task08/sig" * string(i) * "_cdes_Adwin2" * ".txt"
        println("Save results: $outpathAdwin")
        writedlm(outpathAdwin, cdesAdwinNaive, ',')
        writedlm(outpathAdwin2, cdesAdwinNaive2, ',')

    end

end
runDetectors()
