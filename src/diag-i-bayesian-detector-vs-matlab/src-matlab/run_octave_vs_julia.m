lambda        = 50;
hazard_func  = @(r) constant_hazard(r, lambda);
mu0    = 0;
kappa0 = 1;
alpha0 = 1;
beta0  = 1;
CP = [0];
X=load("../data/signal.dat");
T = length(X);
T = length(X);
R = zeros([T+1 T]);
R(1,1) = 1;
muT    = mu0;
kappaT = kappa0;
alphaT = alpha0;
betaT  = beta0;
maxes  = zeros([T+1]);

for t=1:T
  
  predprobs = studentpdf(X(t), muT, ...
                         betaT.*(kappaT+1)./(alphaT.*kappaT), ...
                         2 * alphaT);
  %disp(">>> predprobs")
  %disp(predprobs)
  H = hazard_func([1:t]');
  
  R(2:t+1,t+1) = R(1:t,t) .* predprobs .* (1-H);
  
  R(1,t+1) = sum( R(1:t,t) .* predprobs .* H );

  R(:,t+1) = R(:,t+1) ./ sum(R(:,t+1));

  muT0    = [ mu0    ; (kappaT.*muT + X(t)) ./ (kappaT+1) ];
  kappaT0 = [ kappa0 ; kappaT + 1 ];
  alphaT0 = [ alpha0 ; alphaT + 0.5 ];
  betaT0  = [ beta0  ; kappaT + (kappaT .*(X(t)-muT).^2)./(2*(kappaT+1)) ];
  muT     = muT0;
  kappaT  = kappaT0;
  alphaT  = alphaT0;
  betaT   = betaT0;
  %disp(">>> kappaT")
  %disp(kappaT)
  %disp(">>> alphaT")
  %disp(alphaT)
  %disp(">>> betaT")
  %disp(betaT)
  maxes(t) = find(R(:,t)==max(R(:,t)));  
end

%disp(R)
changes = extract_changes(maxes);
disp(changes);

if 1==0
  % Plot the data and we'll have a look.
  subplot(2,1,1);
  plot([1:T]', X, 'b-', CP, zeros(size(CP)), 'rx');
  % plot also changes 
  hold on;
  for k = 1:length(changes)
    plot([changes(k), changes(k)], [-1,1], color='r');
    hold on;
  end 
  % plot(changes, ones(length(changes),1) )
  %hold on;
  %plot([200, 200], [-1, 1]);
  % end plot also changes
  grid;
  % Show the log smears and the maximums.
  subplot(2,1,2);
  colormap(gray());
  imagesc(-log(R));
  hold on;
  plot([1:T+1], maxes, 'r-');
  hold off;
end 
% plot(muT0)
