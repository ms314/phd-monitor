
# include("../Lib/detectors/BayesDetectorReplica.jl")
# include("../Lib/Common.jl")
#include("../Lib/DetectorPageHinkley.jl")
#include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")

include("../Lib/common.jl")
include("../Lib/PerformanceMetrics.jl")

using CSV, DataFrames
using DelimitedFiles
using Printf

function runDiagBayes()
    ## AND RUN OCATVE VERSION
    x = readdlm("./data/signal.dat")
    r, y  = bayesDetector(vec(x), lambda = 50., gaussPdf=false)
    chps = extractChangesBayes(r)
    println(chps)
end 
runDiagBayes()

# smth. old:   function mavg(x::Array{Float64,1}; w=10, f = mean)
# smth. old:       n = length(x)
# smth. old:       out = zeros(n)
# smth. old:       for i=1:n
# smth. old:           if i == 1
# smth. old:               out[i] = f(x[1:w])
# smth. old:           elseif i <= w
# smth. old:               out[i] = f(x[1:w])
# smth. old:           else
# smth. old:               out[i] = f(x[(i-w+1):i])
# smth. old:           end
# smth. old:       end 
# smth. old:           out
# smth. old:   end 
# smth. old:   
# smth. old:   function run()
# smth. old:       s = readJSON("../config_datasets.json")
# smth. old:       sigpath = s["dir"] * s["sig2"]
# smth. old:       sig01 = readInputSig(sigpath)
# smth. old:       #sig01 = mavg(sig01, w=50, f=std)
# smth. old:       #sig01 = vcat(randn(150), randn(150)+1, randn(150))
# smth. old:       maxes, mat = bayes_detector(sig01, lambda=230, mu0=0.50)
# smth. old:       cdes = extract_changes(maxes)
# smth. old:       println(cdes)
# smth. old:       writedlm("./out/M.dat", mat, ',')
# smth. old:       CSV.write("./out/cdes.csv", DataFrame(cdes = cdes), header=true)
# smth. old:       CSV.write("./out/sig.csv", DataFrame(sig=sig01), header=true)
# smth. old:   end 
# smth. old:   run()
