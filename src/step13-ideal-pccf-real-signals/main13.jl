include("../Lib/DetectorPageHinkley.jl")
include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/PerformanceMetrics.jl")
include("../Lib/common.jl")
include("../Lib/fn_construct_indicator_function.jl")

#using LearnDetectors, PageHinkley, BayesDetector, AdwinNaive
using CSV, DataFrames
using UnicodePlots

function run13()
    s = readJSON("../config_datasets.json")

    n_of_signals = s["numofsignals"]
    signals = ["sig" * string(e) for e in 1:n_of_signals]
    chps = ["chps" * string(e) for e in 1:n_of_signals]
    inds = ["ind" * string(e) for e in 1:n_of_signals]


    detectorPHref = createPhDetector(delta=0.0)
    detectorAdwin = createAdwinDetector(min_len=10)
    detectorBayes = createBayesianDetector(mu0=0.0)

    for i in 1:1 #n_of_signals
        sigpath = s["dir"] * s[signals[i]]
        chpspath = s["dir"] * s[chps[i]]
        indpath = s["dir"] * s[inds[i]]
        x = vec(readdlm(sigpath))
        indf = [Int(e) for e in vec(readdlm(indpath))]
        changes = [Int(e) for e in vec(readdlm(chpspath))]
        # scDel=15.
        # scNcd=3.

        if 1==1
            ## Start: static settings

            optimDetector(x,
                          "./out/rPH" * string(i) * ".csv",
                          "./out/rPH_Opt" * string(i) * ".csv",
                          outPerfMatrix="/home/ms314/tmp/perfMatrixPH" * string(i) * ".dat",
                          changes=changes,
                          detector=detectorPHref,
                          paramrange = range(0.01, stop=10, length=500),
                          name="PH",
                          printprogress=true)



            optimDetector(x,
                          "./out/rAdw" * string(i) * ".csv",
                          "./out/rAdw_Opt" * string(i) * ".csv",
                          changes=changes,
                          outPerfMatrix="/home/ms314/tmp/perfMatrixAdwin" * string(i) * ".dat",
                          detector=detectorAdwin,
                          paramrange = range(0.01, stop=20.0, length=500), #0.01:0.05:10,
                          name="Adwin",
                          printprogress=true)


            optimDetector(x,
                          "./out/rBayes" * string(i) * ".csv",
                          "./out/rBayes_Opt" * string(i) * ".csv",
                          changes=changes,
                          outPerfMatrix="/home/ms314/tmp/perfMatrixBayes" * string(i) * ".dat",
                          detector=detectorBayes,
                          paramrange = range(1., stop=220, length=500), ##0.01:0.05:10,
                          name="Bayes",
                          printprogress=true)

            ## End: static settings
        end

        if 1==1

            ## Start: dynamic setting

            optimDetector(x,
                          "./out/rPH_Dynamic" * string(i) * ".csv",
                          "./out/rPH_Opt_Dynamic" * string(i) * ".csv",
                          outPerfMatrix="/home/ms314/tmp/perfMatrixPH_Dynamic" * string(i) * ".dat",
                          changes = changes,
                          detector = createPhDetectorDynamic(delta=0.0, indicatorFunction=indf, sWhen0=999.0),
                          paramrange = range(0.01, stop=10, length=500),
                          name = "PH",
                          printprogress = true)

            optimDetector(x,
                          "./out/rAdw_Dynamic" * string(i) * ".csv",
                          "./out/rAdw_Opt_Dynamic" * string(i) * ".csv",
                          outPerfMatrix="/home/ms314/tmp/perfMatrixAdwin_Dynamic" * string(i) * ".dat",
                          changes=changes,
                          detector = createAdwinDetectorDynamic(min_len=10,indicatorFunction=indf, ecutWhen0=999.),
                          paramrange = range(0.01, stop=20.0, length=500),
                          name="Adwin",
                          printprogress=true)

            optimDetector(x,
                          "./out/rBayes_Dynamic" * string(i) * ".csv",
                          "./out/rBayes_Opt_Dynamic" * string(i) * ".csv",
                          outPerfMatrix="/home/ms314/tmp/perfMatrixBayes_Dynamic" * string(i) * ".dat",
                          changes=changes,
                          detector = createBayesianDetectorDynamic(mu0=0.0,indicatorFunction=indf,lambdaWhen0=999.),
                          paramrange = range(1., stop=220., length=500),
                          name="Bayes",
                          printprogress=true)

            ## End: dynamic setting
        end
    end
end
run13()
