#
# Calculate averagre difference between performances for many generated signals dynamic vs static
#
include("../Lib/DetectorPageHinkley.jl")
include("../Lib/DetectorAdwinNaive.jl")
include("../Lib/DetectorBayes.jl")
include("../Lib/LearnDetectors.jl")
include("../Lib/TypesHierarchy.jl")
include("../Lib/PerformanceMetrics.jl")
include("../Lib/Common.jl")
include("../Lib/fn_construct_indicator_function.jl")

using Statistics, DelimitedFiles

function genSig(;n=200, delta=1.0, sigma=1.0)::Array{Float64,1}
    vcat(randn(n) .* sigma, randn(n) .* sigma .+ delta)
end

function avgDifferences(; nSim::Int64, lengthParamRange::Int64=100, wIndFunc::Int64=30)
    detectorPHref = createPhDetector(delta=0.0)
    detectorAdwin = createAdwinDetector(min_len=10)
    detectorBayes = createBayesianDetector(mu0=0.0)

    ## Find static optimal settings
    n = nSim
    avgLoss1::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss2::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss3::Array{Float64,2} = zeros(lengthParamRange, n)

    avgLoss1Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss2Dyn::Array{Float64,2} = zeros(lengthParamRange, n)
    avgLoss3Dyn::Array{Float64,2} = zeros(lengthParamRange, n)

    c=1.0

    paramRangePh       = range(0.01, stop=10, length=lengthParamRange)
    paramRangeAdw      = range(0.01, stop=10, length=lengthParamRange)
    # paramRangeBayes    = range(1, stop=1000, length=lengthParamRange)
    paramRangeBayes    = range(0.01, stop=100, length=lengthParamRange)

    for i = 1:n
        c+=1.
        progress=c/nSim
        if c % Int(floor(n/10.0)) == 0
            println("..progress: ", progress*100, " %")
        end
        x = genSig(n=200)
        changeLocation=100

        scDelPh=15.
        scNcdPh=3.
        ##!!OK
        scDelAdw=15.
        scNcdAdw=3.
        ##
        scDelBayes=15.
        scNcdBayes=3.

        r1,=optimDetector(x,
                          changes=[changeLocation],
                          detector=detectorPHref,
                          paramrange = paramRangePh,
                          name="PH",
                          printprogress=false,
                          scaleDelayMinMax=scDelPh,
                          scaleNcdesMinMax=scNcdPh)

        r2,=optimDetector(x,
                          changes=[changeLocation],
                          detector=detectorAdwin,
                          paramrange = paramRangeAdw,
                          name="Adwin",
                          printprogress=false,
                          scaleDelayMinMax=scDelAdw,
                          scaleNcdesMinMax=scNcdAdw)

        r3,=optimDetector(x,
                          changes=[changeLocation],
                          detector=detectorBayes,
                          paramrange = paramRangeBayes,
                          name="Bayes",
                          printprogress=false,
                          scaleDelayMinMax=scDelBayes,
                          scaleNcdesMinMax=scNcdBayes)


        ## Find dynamic optimal settings
        w = wIndFunc ##50
        indFunc = constructIndicatorFunction(length(x), changeLocation, w)
        sph = DynamicSettings(indFunc, 999.0, 0.1)

        ## 1: PH
        ## 2: Adwin
        ## 3: Bayes
        r1Dyn,=optimDetector(x,
                             changes=[changeLocation],
                             detector =createPhDetectorDynamic(delta=0.0, indicatorFunction=indFunc, sWhen0 = 999.0),
                             paramrange =paramRangePh,
                             name="PH",
                             printprogress=false,
                             scaleDelayMinMax=scDelPh,
                             scaleNcdesMinMax=scNcdPh)

        r2Dyn,=optimDetector(x,
                             changes=[changeLocation],
                             detector = createAdwinDetectorDynamic(min_len=10,indicatorFunction=indFunc, ecutWhen0=999.),
                             paramrange =paramRangeAdw,
                             name="Adwin",
                             printprogress=false,
                             scaleDelayMinMax=scDelAdw,
                             scaleNcdesMinMax=scNcdAdw)

        r3Dyn, = optimDetector(x,
                               changes=[changeLocation],
                               detector = createBayesianDetectorDynamic(mu0=0.1,indicatorFunction=indFunc,lambdaWhen0 = 300.),
                               paramrange =paramRangeBayes,
                               name="Bayes",
                               printprogress=false,
                               scaleDelayMinMax=scDelBayes,
                               scaleNcdesMinMax=scNcdBayes)

        ## 1: PH
        ## 2: Adwin
        ## 3: Bayes
        avgLoss1[:,i]=r1[:loss_v]
        avgLoss2[:,i]=r2[:loss_v]
        avgLoss3[:,i]=r3[:loss_v]
        avgLoss1Dyn[:,i]=r1Dyn[:loss_v]
        avgLoss2Dyn[:,i]=r2Dyn[:loss_v]
        avgLoss3Dyn[:,i]=r3Dyn[:loss_v]
    end

    ## 1: PH
    ## 2: Adwin
    ## 3: Bayes
    averageLoss1    = mean(avgLoss1, dims=2)
    averageLoss2    = mean(avgLoss2, dims=2)
    averageLoss3    = mean(avgLoss3, dims=2)
    averageLoss1Dyn = mean(avgLoss1Dyn, dims=2)
    averageLoss2Dyn = mean(avgLoss2Dyn, dims=2)
    averageLoss3Dyn = mean(avgLoss3Dyn, dims=2)

    ## static case
    s1,l1, = findOptimSettings(sensVec=paramRangePh   , lossVec=averageLoss1)
    s2,l2, = findOptimSettings(sensVec=paramRangeAdw  , lossVec=averageLoss2)
    s3,l3, = findOptimSettings(sensVec=paramRangeBayes, lossVec=averageLoss3)

    # dynamic
    s1d,l1d,k = findOptimSettings(sensVec=paramRangePh, lossVec=averageLoss1Dyn)
    s2d,l2d,k = findOptimSettings(sensVec=paramRangeAdw  , lossVec=averageLoss2Dyn)
    s3d,l3d,k = findOptimSettings(sensVec=paramRangeBayes, lossVec=averageLoss3Dyn)

    diff1=s1-s1d
    diff2=s2-s2d
    diff3=s3-s3d

    return (diff1, diff2, diff3)
    # function w(fpath, x)
    #     open(fpath, "w") do io
    #         writedlm(io,x,',')
    #     end
    # end
    # w("./out/avg/avgLossPh.dat"      , averageLoss1)
    # w("./out/avg/avgLossAdw.dat"     , averageLoss2)
    # w("./out/avg/avgLossBayes.dat"   , averageLoss3)
    # w("./out/avg/avgLossPhDyn.dat"   , averageLoss1Dyn)
    # w("./out/avg/avgLossAdwDyn.dat"  , averageLoss2Dyn)
    # w("./out/avg/avgLossBayesDyn.dat", averageLoss3Dyn)
    # w("./out/avg/paramRangePh.dat"   , paramRangePh)
    # w("./out/avg/paramRangeAdw.dat"  , paramRangeAdw)
    # w("./out/avg/paramRangeBayes.dat", paramRangeBayes)
end

function variateW(; nSim::Int64=10, lengthParamRange::Int64=100, widths::Array{Int64,1}=[], fpath="./out")
    wrange = widths
    n = length(wrange)
    m = zeros(n, 4)
    t=0
    for w in wrange
        t+=1
        println(". Step number $t out of $n")
        r= avgDifferences(nSim=nSim, lengthParamRange=lengthParamRange, wIndFunc=w)
        m[t, :] = [w, r[1], r[2], r[3]]
    end
    println(".. Save results into $fpath")
    open(fpath, "w") do io
        writedlm(io, m, ",")
    end
end
# variateW(nSim = 5,lengthParamRange=100, widths=[1:1:180;], fpath="./out/out.dat")
variateW(nSim=15, lengthParamRange=30, widths=[1:1:180;], fpath="./out/out3.dat")
