enablePlugins(ScalaJSPlugin, WorkbenchPlugin)
name := "Example"
scalaVersion := "2.12.8" // or any other Scala version >= 2.10.2
// This is an application with a main method
// scalaJSUseMainModuleInitializer := true
mainClass in (Compile, run) := Some("example.ScalaJSExample")
// libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.6"
libraryDependencies ++= Seq(
  "org.scala-js" %%% "scalajs-dom" % "0.9.6"
)