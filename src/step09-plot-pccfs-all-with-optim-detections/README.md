Plot signals, changes via optim settings, Pccf's on top

#### 
#### 1)
#### 
#### Run
#### 
####     1runLengthsDistributions.R
#### 
#### 
#### to read Pccf's from *dat file and to estimate PDF of
#### inter-arrival times of changes and save manually estimated
#### mixture components into
#### 
#### 
####     1config_kde_pccf_estim.json
#### 
#### 
#### Use values from it to fit Pccfs to data.
#### 
#### 2)
#### 
#### Run 
#### 
####     2main.jl
####     
#### to calculated Pccfs and save them into `*.dat` files.

