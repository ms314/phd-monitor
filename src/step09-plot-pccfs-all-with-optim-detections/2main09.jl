#
#   Calculate Pccf's (theoretical and mixture) and save Pccf's
#   into *.dat files.
#
#
include("../Lib/Common.jl")
include("../Lib/Pccf.jl")

using Pccf

function runPccfs(outdir="./out/")
    local_s = readJSON("./config_pccfs.json")
    s = readJSON("../config_datasets.json")
    pccf_params_mixture = readJSON("./1config_kde_pccf_estim.json")
    n_of_signals = s["numofsignals"]
    signals = ["sig" * string(e) for e in 1:n_of_signals]
    chps = ["chps" * string(e) for e in 1:n_of_signals]
    params = zeros(n_of_signals, 4) ## sigNum, mu, sigma, n
    params_fitted = zeros(n_of_signals, 4) ## sigNum, mu, sigma, n
    for i in 1: n_of_signals
        sigpath = s["dir"] * s[signals[i]]
        chpspath = s["dir"] * s[chps[i]]
        println("\nRead signal " * sigpath)
        println("Read changes " * chpspath)
        changes = readInputChanges(chpspath)
        sig01 = readInputSig(sigpath)
        lenOfSignal = length(sig01)

        mu = mean(diff(changes))
        sigma = std(diff(changes))
        params[i,1:4] = [i, mu, sigma, lenOfSignal]

        println("... Sig. num. $i: mu = $mu, sigma = $sigma")
        probsPccf = pccf(lenOfSignal, mu, sigma)

        if i == 1
            mu_fitted = pccf_params_mixture["sig1"]["mu1"] ### using R kde estimator
            s_fitted = sigma/2.0
        elseif i==2
            mu_fitted = pccf_params_mixture["sig2"]["mu1"] ##mu
            s_fitted = sigma
        elseif i==3
            mu_fitted = pccf_params_mixture["sig3"]["mu1"]
            s_fitted = sigma/3.0
        elseif i==4
            mu_fitted = pccf_params_mixture["sig4"]["mu1"]
            s_fitted = sigma
        elseif i==5
            mu_fitted = pccf_params_mixture["sig5"]["mu1"]
            s_fitted = sigma
        elseif i==6
            mu_fitted = pccf_params_mixture["sig6"]["mu1"]
            s_fitted = sigma
        else
            mu_fitted = mu
            s_fitted = sigma
        end
        probsPccf_fitted = pccf(lenOfSignal, mu_fitted, s_fitted)
        params_fitted[i,1:4] = [i, mu_fitted, s_fitted, lenOfSignal]

        out_fname = outdir * "task09_pccf_sig" * string(i) * ".dat"
        out_fname_fitted = outdir * "task09_pccf_sig" * string(i) * "_fitted.dat"
        writedlm(out_fname, probsPccf, ",")
        writedlm(out_fname_fitted, probsPccf_fitted, ",")
        println("Save Pccf into " * out_fname)
        println("Save Pccf into " * out_fname_fitted)
    end

    fparams = outdir * "_pccf_params.csv"
    fparams_fitted = outdir * "_pccf_params_fitted.csv"
    println("... Save Pccf params into $fparams")
    writedlm(fparams, params, ",")
    writedlm(fparams_fitted, params_fitted, ",")
end
runPccfs()
