
# Meeting summary

- Text

  - Check related works: theoretical bounds. **Link to our contributions.**
  - Decouple (in the text): experiment: 1) practical part and 2) where we check what is going on

- Experiments

  - Instead of averaged values put box-plots.
  - Describe experiments settings.
  - Explain - why to do averaging?
  - Run experiment without averaging - just for 1 signal.
  - Describe what is the motivation: what do we capture in the experiment?

# Before meeting

Previous meeting: [meeting_2019_01_30/](https://bitbucket.org/ms314/phd-monitor/src/master/meeting_2019_01_30/) 

- **question: can we optimize detectors against their detections?**

## What has been done

*Most of the time spent fixing Bayesian and Adwin detectors and
 fixing experiments (`step12`), also implemented a new detector
 (is going to be very useful, probably) while trying to fix
 Bayesian detector.*

- ! Fixed errors in Adwin and Bayesian detectors

- Updated the draft: section: "15 Experiments"

  https://bitbucket.org/ms314/phd-monitor/downloads/journal-pccf-structured-plan.v0.2.pdf

- Added plots for detection delay and FA rate

Experiment with artificially generated (many times) signal.
Detection delays and FA rates are averaged.


![img1](./img/step12-delVsFaRate-2019-02-12_2.PNG)


- ! Implemented **parametric-free** pseudo-online dynamic programming based change detector.

  Maybe it is now worth trying to implement Pccf offline.


## Ongoing work

Bayesian detector performance example for Twi signal.

![img2](./img/diag_bayes_perf_twi_2019-02-12.PNG)

![img3](./img/diag_bayes_cdes_example_twi_2019-02-12.PNG)