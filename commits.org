Major commits

- Reference code for scala.js
  https://bitbucket.org/ms314/phd-monitor/commits/d3063e8440b734e0c50d2a17e1fd63939fbeaf19 (2019/02/19)
- Major improvement of the Bayesian detector. Gaussian is
  added. Reversed way of hypothesis set calculations.
  ~eede956addba26b33c5ff743ee2a9172ec488d52~, ~dbf25~, ~017f8~
  
