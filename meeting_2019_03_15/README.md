

Links from Nikolai

- https://arxiv.org/pdf/1711.02186.pdf
- https://ieeexplore.ieee.org/abstract/document/8509641/
- https://www.researchgate.net/publication/3959806_An_efficient_sequential_procedure_for_detecting_changes_in_multichannel_and_distributed_systems


- agreed to do everything just for CuSum at the beginning
- take/adapt simplified math from [**"Quickest Change Detection."**](https://arxiv.org/pdf/1210.5552.pdf) by Venugopal V., et.al., 2014.