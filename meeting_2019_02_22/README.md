

Notes before meeting
===

Videos 

- [Experiment 1](https://youtu.be/4q5378pF3Tc)
- [Experiment 2])https://youtu.be/xAz1OTOX2rk)
- [Experiment 3](https://youtu.be/Jjq1ALiWxPY)


```
\section{Research questions, goal}

The main goal is 
\begin{itemize}
    \item to reduce change detection delay subject to FA rate constrains
    \item or, to reduce FA rate subject to change detection delay constrains?
\end{itemize}
How do we enforce these conditions using cost function?
At the moment it is symmetric to the delay and FA rate.

```
