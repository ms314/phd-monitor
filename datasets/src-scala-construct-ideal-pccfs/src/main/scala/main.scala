moved to scala-lib: step14

import scala.io.Source 
import java.io._

object Obj{
    def ifIntersect(zippedIntervals:((Int, Int), (Int, Int))):Boolean = zippedIntervals match {
        case ((l1, r1), (l2, r2)) => if (l2 <= r1 || r2 <= r1) false else true
    }

    def removeIntersections(intervals:List[(Int, Int)]):List[(Int,Int)] = {
        val z = (intervals.init zip intervals.tail).toList // ((l1, r1), (l2, r2))
        val zf = z.filter(ifIntersect)
        zf.map(x=>x._1):::List((zf.last._2))
    }

    // def indFuncFromBorders(borders:List[(Int, Int)], sigLen:Int):List[Int]={
        // List()
    // }

    def indFuncFromBorders(obstacles:List[Int], n:Int):List[Int] = {
        // fill by 1 open-ended intervals
        assert (obstacles.length % 2 == 0)
        assert (obstacles.last < n+1)
        def aux(n:Int, obstacles:List[Int], acc:List[Int], passedObstacles:Int, k:Int=0):List[Int] = obstacles match {
            case Nil => 
                if (k==n) 
                    acc.reverse
                else    
                    aux(n, obstacles, (passedObstacles % 2) :: acc, passedObstacles, k+1)
            case x::xs => 
                if (k==n){
                    acc.reverse
                } else {
                    if (k == x) {
                        aux(n, xs, ((passedObstacles+1) % 2) :: acc, passedObstacles+1, k+1)
                    } else {
                        aux(n, x::xs, (passedObstacles % 2) :: acc, passedObstacles, k+1)
                    }
               }   
            }
        aux(n, obstacles, List(), 0, 0)
    }

    def checkAndApply(chps:List[Int], 
                      sigLen:Int, 
                      outf:String,
                      minLeft:Int=10,
                      minRight:Int=3,
                      minBetween:Int=10):Unit={
        val augChps = if(chps.head == 0) chps:::List(sigLen) else (0::chps):::List(sigLen)
        val diffs = (augChps.tail zip augChps.init).map(x => x._1 - x._2)
        val minDist = diffs.min
        val maxDist = diffs.max
        // now use not augmented chps
        val lBorders = chps.map(x => x-minLeft)
        val rBorders = chps.map(x => x+minRight)
        val borders = lBorders.zip(rBorders)
        // remove intersections and possible negative values
        val cleanBorders = removeIntersections(borders).filter(x => (x._1 > 0 || x._2 > 0))
        val indFunc = indFuncFromBorders(cleanBorders.flatMap(x => List(x._1, x._2)), sigLen)
        // fraction of covered changepoints
        val frac = cleanBorders.length / chps.length.toDouble
        if (1==1){
            // report
            println("Changes locations including last signal point:")
            println(augChps.mkString(", "))
            println("Time intervals between changes:")
            println(diffs.mkString(", "))
            println(s"Min. dist. = $minDist")
            println(s"Max. dist. = $maxDist")
            println("Borders (initial version):")
            println(borders.mkString)
            println("Borders cleaned:")
            println(cleanBorders.mkString)
            println(s"Fraction of covered by indicator function changepoints: $frac")
            println(indFunc.mkString(","))
        }
        println(s"save indicator function into the file $outf")
        printToFile(new File(outf)){p => indFunc.foreach(p.println)}
        println("..done")
    }

    def readVec(fpath:String):List[Int] = {
        println(s"Read change locations from file $fpath")
        val chps = Source.fromFile(fpath).getLines().toList 
        chps.map(_.toInt)
    }

    def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
        val p = new java.io.PrintWriter(f)
        try { op(p) } finally { p.close() }
    }

    def main(args:Array[String]):Unit = {
        val outputfolder = "/home/ms314/gitlocal/phd-monitor/datasets/src-scala-construct-ideal-pccfs-output/"
        val dir = "/home/ms314/gitlocal/phd-monitor/datasets/"
        val chps1 = "exchange-rate-bank-of-twi/data/changesTwi.dat"
        val chps2 = "internet-traffic-data-in-bits-fr/data/changesInternetTraffic.dat"
        val chps3 = "monthly-lake-erie-levels/data/changesErieLake.dat"
        val chps4 = "number-of-earthquakes-per-year/data/changesNumOfEarthQuakes.dat"
        val chps5 = "time-that-parts-for-industrial/data/changesParts.dat"
        val chps6 = "wolfer-sunspot-numbers/data/changes.dat"
        checkAndApply(readVec(dir+chps1), 304 , outputfolder + "step13-scala-ind-func-twi.dat")
        checkAndApply(readVec(dir+chps2), 1231, outputfolder + "step13-scala-ind-func-internet.dat")
        checkAndApply(readVec(dir+chps3), 600 , outputfolder + "step13-scala-ind-func-lake.dat")
        checkAndApply(readVec(dir+chps4), 99  , outputfolder + "step13-scala-ind-func-earthquakes.dat")
        checkAndApply(readVec(dir+chps5), 90  , outputfolder + "step13-scala-ind-func-parts.dat")
        checkAndApply(readVec(dir+chps6), 92  , outputfolder + "step13-scala-ind-func-sunspot.dat")
    }   
}
