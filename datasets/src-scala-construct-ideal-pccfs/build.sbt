lazy val root = (project in file("."))

  .settings(
    name := "construct-ideal-pccfs-aka-indicator-functions",
    scalaVersion := "2.12.8",
    version := "0.1.0-SNAPSHOT",
    
    libraryDependencies ++= Seq(
      "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"
    )

  )

