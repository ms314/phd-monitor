# Joint Skype meeting on 30 Jan 2019

## Meeting summary (agreed to do, next steps)

- Make separate plots for delay anf FA rate
- Write down detailed experiment design
- Formulate the logic: what is the statement and what / how / why we proove it. E.g. there are cost functions for dynamic and static cases. Why do we present them?


## Meeting plan

- [ ] Discuss performance metrics / New cost function

    - https://docs.google.com/presentation/d/15X94QWpagP7BUHyzQWPnNs9jIHNmGBnVmQq1IeTcQGg/edit?usp=sharing

- [ ] [step12 results](https://bitbucket.org/ms314/phd-monitor/src/master/src/step12-1change-ideal-pccf/)
- [ ] [step13 results](https://bitbucket.org/ms314/phd-monitor/src/master/src/step13-ideal-pccf-real-signals/img/costf_real_data.PNG): improvement is needed
- [ ] Additional experiments?
- [ ] Pccf backwards / offline Pccf ?


