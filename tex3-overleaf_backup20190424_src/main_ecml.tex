\documentclass[runningheads]{llncs}
\usepackage{graphicx}
\usepackage{mathptmx} 
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{hyperref}
\usepackage{algorithm}% http://ctan.org/pkg/algorithms
\usepackage{algpseudocode}% http://ctan.org/pkg/algorithmicx
\usepackage{tikz}
\usepackage[subpreambles=true]{standalone}
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

\newcommand*{\LargeCdot}{\raisebox{-0.5ex}{\scalebox{1.8}{$\cdot$}}}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\begin{document}
\title{Online Change Detection With Predilection}
\author{Alexandr Maslov\inst{1,2}
%\orcidID{0000-1111-2222-3333} 
\and
Mykola Pechenizkiy\inst{1}
%\orcidID{1111-2222-3333-4444} 
\and
Tommi K\"{a}rkk\"{a}inen\inst{2}
%\orcidID{2222--3333-4444-5555} 
\and 
Alexander Shklyaev\inst{3} 
% tommi.karkkainen@jyu.fi
% ashklyaev@gmail.com
% m.pechenizkiy@tue.nl
%\and
%Indr\.e~\v{Z}liobait\.e\inst{4}
}

\authorrunning{A. Maslov et al.}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
\institute{TU Eindhoven, Dept. of Mathematics and Computer Science, The Netherlands
\email{avmaslov3@gmail.com, m.pechenizkiy@tue.nl}
\\ \and
University of Jyv\"{a}skyl\"{a}, Faculty of Information Technology, Finland\\
\email{tommi.karkkainen@jyu.fi}\\
%\url{http://www.springer.com/gp/computer-science/lncs} 
\and
Lomonosov Moscow State University, Department of Mechanics and Mathematics
\email{ashklyaev@gmail.com}
\\ 
%\and 
%Aalto University, Department of Computer Science; Helsinki Institute for Information Technology HIIT; University of Helsinki, Department of Geosciences and Geography, Finland
%\email{\{abc,lncs\}@uni-heidelberg.de}
}
\maketitle
\begin{abstract} 
	!!! MOVED BACK TO OVRERLEAF -check if changes are there
	
	In the real world, the data tends to change over time. 
Machine learning models need to have mechanisms for continuous diagnostics of performance, and for handling concept drifts, i.e. sudden or gradual changes in underlying data distribution.

The so-called informed methods for handling concept drift are based on online change detection mechanisms.  
Most of these mechanisms assume that changes are not predictable. 
However, there are various application settings in which concept drifts are expected to reappear.  
Intuitively, using some form of recurrence or external contextual information may help to perform online change detection with shorter delays or with lower false alarm rates.  
We study the settings when the prior information about when changes are likely to happen is available. 
We show the potential and limitation of using such prior information when it is available as background knowledge. 
We demonstrate how we canlearn such priors from the data as a predictive confidence change function and how we can integrate it into CuSum, Adwin, and Bayesian detectors to adjust their sensitivity dynamically.  
We explore the relation of optimal settings of dynamic and static detectors on both synthetic and real word datasets and provide guidelines how to use the prior information for online change detection in practice.
\end{abstract}

\section{Introduction}

\subsection{Importance of the change-detection problem}
Time series depicts the temporal evolution of a process or phenomenon under study.  
The underlying system or data generation mechanism may change its behavior between different phases or stages of the process.  
Therefore, the problem of on-line change detection is important across many application domains in science, medicine, and engineering.

\subsection{Sub-problem of recurrent changes}
Here we address the problem of on-line detection of \textit{recurrent} changepoints in time series data when the prior information about probability distribution of time intervals between consecutive changes is available.  
We explore how the prior information in can be used to improve the change detection performance and what are the limitations of such approach.  
We construct a predictive confidence change function (Pccf) for commonly used statistical distributions of time intervals between consecutive events and integrate it into CuSum, Adwin, and Bayesian changepoint detectors.  
Pccf is used to predict sequence of changes in a time series in the future and adapt detector's sensitivity dynamically to the prediction in order to achieve a better performance than when using static sensitivity settings.

\subsection{Contributions}
We compare the performance of static and dynamic detectors and demonstrate that the naive strategy of turning the static detector on or off inside and outside the predicted time intervals with changes either doesn't work or achieves the same performance as when using the static detector only. 
We also show that in case of dynamic settings the detection delay and false alarm (FA) \textit{rate} can be decreased in spite of increased probability of FA. 
We explore the relation of optimal settings of dynamic and static detectors and limitations of both approaches and give practical instructions how to use the prior information.

\section{Change-detection problem and process}
- The goal of a changedetection process is to detect changes\\
- Output statistic\\
- Stopping rule\\

\subsection{Performance of the change detection process}
Performance of a change detector is characterized by the detection delay and by the false alarm rate (FA rate). 
Stopping rule is defined by a logical condition on the output statistic.
Change is alarmed by detector when stopping rule is satisfied.

\begin{definition}
    %Change detector is an algorithm which alarms the change when calculated output statistic \\
    stopping rule based definition.
\end{definition}
\begin{definition}
    Change detection event is an event when detector's 
\end{definition}
\begin{definition}
    The detection delay is a time after the change until the first change detection event (CDE).
    False alarms (FA events) are all other CDEs.
\end{definition}
%- maybe describe a loss function, or just delay and FA rate\\
Performance of the change detection process (Fig.~\ref{fig:binary_class_metrics_issues}) is defined by the detection delay and FA rate.
We can define a loss function as a sum of log-likelihood of the FA rate and logarithm of the likelihood of the probability of the detection delay value given the detector's sensitivity value.
Eq.~\ref{eq:cost_function}
\begin{equation}\label{eq:cost_function}
\text{Loss}(S) = \log(1 + \text{FArate}(S)) + \log(1+P(D|S))
\end{equation}
\begin{figure}[!htb]
    \centering
    \includestandalone[width=0.5\textwidth]{./img/tikz/binary-classif-metrics-vs-new-sc1}
    \caption{Performance metrics of the changedetection process: FA rate and detection delay}
    \label{fig:binary_class_metrics_issues}
\end{figure}



\section{Relation to the previous work}
\subsection{What we did in previous works}
In our previous work~\cite{MaslovSDM2016} we introduced Pccf.
In\cite{MaslovIJCNN2017} we integrate Pccf with the Bayesian on-line changedetector~\cite{mackay2007}.

\subsection{How we extend previous work}
In the previous work we considered artificial signals and naive detector.
in this work we calculate Pccf's for several commonly used probability distributions and integrate Pccf internally into real changedetectors CuSum~\cite{Page1954},Adwin~\cite{bifet2007learning}, and Bayesian on-line changedetector~\cite{mackay2007} and apply them to real signals.
% We perform experiments with six (new) real data sets.
%
% We introduce an optimality criteria for the changedetection process in a form of Loss function reflecting a trade-off between requirements to detect a change as quickly as possible, subject to false alarm constraints~\cite{TartakovskySeq}.
% 
% By introducing a loss function we show that performance of the changedetection process can be improved if and only if the local FA rate near changes is lower than overall population FA rate. 
% ? We show that in post-processing settings we safely reduce FA rate without reducing detection delay; And in the pre-processing settings we are trying to decrease detection delay while increasing the probability of FA events.
%
% Investigate when it is possible to improve performance of the changedetector given a prior information about inter-arrival times between changes; Particularly, we specify a loss function and describe when it is possible to decrease the expected value of the detection delay subject to a false alarm (FA) rate constraints.
%
% Obtain predictive probability distributions for a sequence of recurrent changes (\textbf{Pccf}) in case of several commonly used probability distributions used to model inter-arrival times between sequential events


\section{Notations and definitions}
Index for input signal of observations is $\mathbb{I}=(1,\dots,T)$ where $T$ is a length of input signal.\\
Index for changepoints is $\mathbb{K}=(1,\dots,k), \: k \in \mathbb{I}$.\\
The input signal is an ordered sequence of real-valued observations $x_i, \: i \in \mathbb{I}$.\\
The sequence of changepoints is denoted as $\tau_k, k \in \mathbb{K}$.
\begin{definition}
    Changepoint is an \textit{event} when statistical properties of the input signal change \textit{significantly} according to the predefined criteria. 
    Changepoint is defined by the time moment when it occurred. 
    Changepoint is an event $\tau_k = t, k \in \mathbb{K}$.
\end{definition}
%
\begin{definition}
    Recurrent changepoints are changepoints for which the recurrence relation is 
    \begin{equation}
        \tau_{n+1} = \tau_n + \delta_n
    \end{equation}
    where $\delta_n$ is a sample from some probability distribution with fixed parameters.
    The recurrence relation for periodic changes is 
    \begin{equation}
        x_{n+1} = B n + \delta_n
    \end{equation}
    where $B$ is some constant.
\end{definition}
We predict recurrent changepoints by calculating a Predictive confidence change function (Pccf)~\cite{MaslovSDM2016},~\cite{MaslovIJCNN2017}.
\begin{definition}
    Pccf is a probability of the event~\footnote{Symbol $\lor$ denotes logical \textit{or} operation.}
    \begin{equation}
        \tau_1 = t \lor \tau_2 = t \lor \dots \lor \tau_n = t
        \label{eq:events_union}
    \end{equation}
    %for $t \in (1, \dots,T)$. 
    Since the events $\tau_1 = t$, $\tau_2 = t$ etc. are disjoint, the probability of the event in ~\eqref{eq:events_union} can be written as
    \[
        P\Big(\bigcup\limits_{i=1}^{T} (\tau_k = t) \Big ) = \sum_{i=1}^{T} P(\tau_i = t)
    \]
\end{definition}
By applying a threshold $h$ to Pccf we obtain an Indicator Function (I.F.)
If $P((\tau_k = t)) > h$ then $I.F.=1$ and $0$ otherwise.
\begin{definition}
    Indicator function is constructed as a $1,0$ sequence. If change location is $i$, length of the signal is $n$, width of indicator function is $w$ then it is a vector $V$ of length $n$, so that $V[i]=1$ if $i \in [i-w:i+w]$, and $V[i]=0 \: \forall \: i \notin [i-w:i+w]$.
\end{definition}
\begin{definition}
    Confidence intervals are time intervals where $I.F.=1$.
\end{definition}
We expect changes with high probability within confidence intervals.
%The output of the prediction process is $I.F.$ and confidence intervals.


\section{Static and dynamic settings}
In the static settings case the detector is optimised using historical data.
In the dynamic case sensitivity of the detector is adjusted according to the prediction.
Prediction is in a form of time intervals when we will expect changes with high probability.

\section{Detectors used}
In experiments we used detectors 
CuSum~\cite{Page1954}, 
Adwin~\cite{bifet2007learning}, 
and Bayesian on-line changedetector~\cite{mackay2007}.
In this section we describe three detectors we used in our experiments.
\subsection{Page-Hinkley test, CuSum}
~\cite{gama2010knowledge} 
% p.55(el)/41
CuSum~\cite{Page1954}
The CuSum output statistic is
\begin{align}
    g_0 &= 0\\
    g_t &= \max (0, g_{t-1} + (r_t - v))
\end{align}
the stopping rule is $g_t > h$.

The Page-Hinkley test(PH) ~\cite{Gama_KDFDS}
%% p.55/41
``the test considers a cumulative variable $m_T$, defined as the accumulated difference between the observed values and their mean till the current moment''
\begin{equation}
    m_T = \sum_{t=1}^{T} (x_t - \bar{x_t} - \delta), \text{ where } \bar{x_t} = \frac{1}{T} \sum_{t=1}^{T} x_t
    \label{eq:ph_test}
\end{equation}
Here $\delta$ corresponds to the scale/magnitude of the changes to be detected.
$M_T = \min(m_t, t=1,\dots,T)$
the decision rule is $PH_T \equiv M_T - m_t > h$, where $h$ is a threshold value.

\subsection{Bayesian Online Changedetector}
Copy from~\cite{MaslovIJCNN2017}:
BD detector works by recursively estimating posterior probability distribution $P(r_t | \pmb{x}_{1:t}, \theta)$ of the \textit{run length} variable $r_t$ which is a time since the last changepoint.
Changepoint is an event when
\begin{equation}
    \operatorname*{arg\,max}_{r_t} P(r_t | \pmb{x}_{1:t}, \theta) = 0
\end{equation}
%\[\]
%$r_t = 0$
Every time a new measurement $x_t$ is observed the \textit{posterior} distribution is recalculated using the Bayes` theorem to update parameters of the distributions used to model data
%\[
%P(r_t | \pmb{x}_{1:t}) = \frac{P(r_t, \pmb{x}_{1:t})}{P(\pmb{x}_{1:t})}
%\]
and the law of total probability
%$P(x) = \sum_{y} P(x|y) p(y)$
\begin{equation}
    P(r_t|\:\LargeCdot) = \sum_{r_{t-1}} P(r_{t} | \: r_{t-1},\:\LargeCdot) \: P(r_{t-1}|\:\LargeCdot)
\end{equation}
to take into account values from all the runs in the past.

The \textit{prior} probability of the change $P(r_t=0|t)$ in BD detector is specified using the constant-value hazard rate $h$ which is a prior probability to observe a change and which is supposed to be known before the change detection process starts.

%\section{Online Bayesian Change Detector (BD)}
%\label{sec:bd_detector}
In this section we describe the Bayesian Online Changepoint Detector proposed in~\cite{mackay2007}.
As it was previously mentioned - to model time occurrences of the changes authors introduce a latent variable run length $r_t$ which is the number of time steps since the most recent change.
% As we mentioned - to model time occurrences of the changes authors introduce a latent variable run length $r_t$ which is the number of time steps since the most recent change.
%\textit{Run length $r_t$ is the number of time steps since the most recent change}.~\cite{mackay2007},~\cite{WilsonBayesOnline}.
In Fig.~\ref{fig:trellis_struct} plot \textbf{(A)} you can see an illustrating example of the input signal and corresponding run values on plot (B).

On each time step there are two possibilities: either the run length increases $r_t = r_{t-1}+1$ or changepoint occurs $r_t = 0$.
The conditional prior $P(r_t | r_{t-1})$ of the change is given by a constant-value hazard rate $h$ (Equation~\ref{eq:hazard_rate}).
%..Hazard rate $h$ is the instantaneous probability to observe of an event given that it has not happened yet.
\begin{equation}
p(r_t | r_{t-1}) =
\begin{cases}
1 - h \text{\:\:\: if }  r_t = r_{t-1} + 1 \\
h \text{\:\:\:\:\:\:\:\:\:\:\:  if } r_t = 0
\end{cases}
\label{eq:hazard_rate}
\end{equation}
% ??
The plot \textbf{(C)} in Fig.~\ref{fig:trellis_struct} illustrates the
message-passing algorithm to compute prior probabilities of the
changepoint at any time moment given the boundary condition
$P(r_1=0)=1.0$ that change occurred at the moment $t=1$.
%
Each node (circle) represents a hypothesis about the current run length value.
%
From each node there is a solid line upwards depicting probability of increasing of the run on the next time step (no change) and a dashed line going downwards depicting probability of the change.

At each time step the probability of a changepoint is estimated by calculating the posterior probability distribution of the run length value given observed data (Equation~\ref{eq:r_t_posterior}).
\begin{equation}
    P(r_t | \pmb{x}_{1:t}) = \frac{P(r_t, \pmb{x}_{1:t})}{P(\pmb{x}_{1:t})}
    \label{eq:r_t_posterior}
\end{equation}
% % The marginal predictive distribution $P(r_t, x_{1:t}) \sim \sum_{r_{t-1}} P(r_t, r_{t-1}, x_{1:t}) $
% % $\sim \sum_{r_{t-1}} P(r_t, r_{t-1}, x_{1:t}) $
% % $P(r_t | x_{1:t}) = \frac{P(r_t, x_{1:t})}{P(x_{1:t})}$. \\
The joint probability of the run length values and the observed data can be sequentially computed using recursive procedure in Equation~\ref{eq:bd_recursive_formula} as it is described in~\cite{mackay2007}:
\begin{multline}
    P(r_t, x_{1:t}) = \sum_{r_{t-1}} P(r_t, r_{t-1}, x_{1:t}) = \\
    \sum_{r_{t-1}} P(r_t, x_t \: | \: r_{t-1}, x_{1: t-1}) \: P(r_{t-1}, x_{1:t-1}) = \\
    \sum_{r_{t-1}} P(r_t | r_{t-1}) P(x_t | r_{t-1}, x_t^{(r)}) P(r_{t-1}, x_{1:t-1})
    \label{eq:bd_recursive_formula}
\end{multline}
where $x_t^{(r)} \equiv \langle x_{t-r+1},\dots,x_t \rangle$ is input data sub-interval associated with the run length $r$.
%
Marginal predictive distribution of the new observation $x_t$ is computed using the sum rule:
\begin{equation}
    P(x_{t} | \pmb{x}_{1:t-1}) = \sum_{r_t} P(x_{t}|r_{t}, \pmb{x}_t^{(r)}) P(r_t | \pmb{x}_{1:t-1})
\label{eq:bd_marginal_predictive}
\end{equation}
\begin{figure}[!htb]
    \centering
    \includestandalone[width=0.61\textwidth]{./img/blpa/trellis-struct}
    \caption{
        The changepoint detection problem.
        (A): Input signal.
        (B): A particular realization of the run length path corresponding to the actual changepoints locations in the input signal.
        (C): Directed graph representing all possible run length paths.
        \textit{The figure is replicated from the illustration in~\cite{mackay2007}.}
    }
    \label{fig:trellis_struct}
\end{figure}

\subsection{Adwin2 and Adwin Naive}
Adwin method is developed in~\cite{bifet2007learning}.
The basic (slow) version is depicted by pseudocode~\ref{alg:adwin}
\begin{algorithm}\label{alg:adwin}
    \begin{algorithmic}%[1]
        \State Initialize Window W
        \For{t = 1:T}
        \State $W \leftarrow W \cup \{x_t\}$ \Comment{i.e., add $x_t$ to the head of W} 
        \Repeat
        \State Drop elements from the tail of $W$
        \Until{$|\mu_{W_0} - \mu_{W_1}| \geq \epsilon$ holds for every split of $W$ into $W=W_0 \cdot W_1$}
        \EndFor
        \State output $\mu_W$
    \end{algorithmic}
    \caption{ADWIN pseudo-code}
\end{algorithm}
To speed it up Adwin uses internally modified ExponentialHistogram data structure proposed in~\cite{datar2002maintaining}.
It is modified so that
to use it with real numbers
capacities of the buckets are maintained as powers of 2 instead of, e.g. (A.Bifet)

\begin{verbatim}
(1)(0)(1)(0)(1)(0)(1)(1) (0)(1)(1)(1.05) (1)(13) (15)

Content: 5 3.05 14 15

Capacity: 8 4 2 1
\end{verbatim}

\section{Prediction}
\subsection{Indicator function}
To make a prediction we construct an indicator function (I.F.) which takes value $1$ if we predict a high probability of a change at time moment $t$ and $0$ otherwise.
%
Let's consider signals depicted on Fig.~\ref{fig:example_one_step_change} and the naive change detector with the stopping rule  defined by Eq.~\ref{eq:stop_rule_naive}.
In case A on Fig.~\ref{fig:example_one_step_change} the performance of the change detector~\ref{eq:stop_rule_naive} can not be improved when using indicator function.
In case B performance will be improved because outlier/noisy change at the moment 3 will be skipped.
\emph{But the real situation is different than on Fig.~\ref{fig:example_one_step_change}}.
Fig.~\ref{fig:real_illustrative_example} illustrates the real signal.
\begin{figure}[htb!] 
    %%% R-code: 1fa_before_chp.R
    \centering
    \includegraphics[width=0.80\textwidth]{./img/step12-illustrative-example.pdf}
    \caption{Real signal illustrative example: noisy signal.}
    \label{fig:real_illustrative_example}
\end{figure}

 \begin{equation}\label{eq:stop_rule_naive}
        \text{if } Y(t) > 1.0 \text{, then : alarm change}
    \end{equation}
    \begin{figure}[!htb]
        \centering
        \begin{minipage}{0.95\textwidth}
            \centering
            \includestandalone[width=0.5\textwidth]{./img/tikz/example-one-step-change-sc1}\\
            \includestandalone[width=0.5\textwidth]{./img/tikz/example-one-step-change-sc2}
            \caption{Illustrative example. Case A: Single change (of interest). Case B: with noisy change before the change of interest. Red line: indicator function.}
            \label{fig:example_one_step_change}
        \end{minipage}
    \end{figure}

%=============================================%
%=== PCCF
\subsection{Pccf}
To calculate PDF of random variables we can use moment generating functions technique.
\begin{itemize}
    \item Normal distribution is $\exp{ (\mu t + \frac{\sigma^2 t^2}{2}) }$
    %
    \item Exponential distributions is $\frac{\lambda}{\lambda - t}$
    %
    \item Gamma distribution $\Gamma(\alpha, \beta)$ is~\cite{wasserman2013all} $\Big ( \frac{1}{1- \beta t} \Big )^{\alpha}$
    %
    \item Poisson is $\mathbb{E}(e^{tX}) = \sum_{k=0}^{\infty} e^{tk} e^{-\lambda} \lambda^k/k! = e^{\lambda (e^t-1)}$ 
    %
    \item \emph{Pdf for the sum of i.i.d. random variables from the Gamma distribution.}
    If $X \sim \Gamma(a, \lambda), Y \sim \Gamma(b, \lambda)$ then $X+Y \sim \Gamma(a+b, \lambda)$ where $\lambda$ is a scaling factor which we can safely(?) assume to be the same.
    %Proof for the Gamma can be found \href{https://en.wikipedia.org/wiki/Characteristic\_function\_\%28probability\_theory\%29#Example}{here}. Characteristic functions are $\phi_X(t)=(1-\lambda i t)^{-a}$ and $\phi_Y(t)=(1-\lambda i t)^{-b}$. Therefore $\phi_{X+Y}(t) = (1-\lambda i t)^{-(a+b)}$.
\end{itemize}

\begin{table} [!htb]
    \caption{Pdf's for distributions of inter-arrival times.}
    \begin{center}
        \begin{tabular}{|l|c|c|c|}
            \hline
            Distribution & Mgf & Pdf of the $k$-th event & Pccf \\[5pt]
            \hline
            Gaussian & $\exp{ (\mu t + \frac{\sigma^2 t^2}{2}) }$ & $\mathbb{N}(k \mu, \sqrt{k} \sigma)$ & $\sum_{k=1}^T \mathbb{N}(k \mu, \sqrt{k} \sigma)$   \\
            Exponential & $\frac{\lambda}{\lambda - t}$ & $\Gamma(k, \lambda)$ & $\sum_{k=1}^T \Gamma(k, \lambda)$\\
            Gamma $\Gamma(\alpha, \lambda)$ & $\Big ( \frac{1}{1- \lambda t} \Big )^{\alpha}$ & $\Gamma(k \alpha, \lambda)$ & $\sum_{k=1}^T \Gamma(k \alpha, \lambda)$ \\
            \hline
        \end{tabular}
    \end{center}
\end{table}

\section{Experiments}
Conclusions from Fig.~\ref{fig:relation}
\begin{itemize}
    \item Global optimal settings are not applicable in case of I.F.
    \item When I.F./Pccf are available we can not just simply turn off optimal static detector and use it just inside of I.F. by turning it on.
\end{itemize}
\begin{figure}[!htb]\label{fig:relation}
    \centering
    \includestandalone[width=0.8\textwidth]{./img/tikz/cusum-output-stat} 
    \caption{Relation between local and global optimal settings.
    When using $H^{global}$ - detection delay is $D_2=4$ and FA rate is 0.
    When using I.F. and $H^{local}$ - $D_2=1$ but FA rate is 1 (at $t=5$).
    NOTE: We can't use $H^{global}$ when using I.F. and dynanmic settings.
    Optimal sensitivity depends on the sample size and when we use I.F. / Pccf - we reduce the sample size.
    We can't compare the same sensitivity in dynamic and static cases (with I.F. width lower than some value).
    }
\end{figure}

\subsection{Artificial signal}
% from out.tex from scala lib src folder
When we increase sensitivity of a change detector we decrease expected value of the detection delay and at the same time we increase expected value of a False Alarm rate. In this experiment we demonstrate that it is possible to decrease both detection delay and FA rate when changing sensitivity dynamically in comparison to the fixed static sensitivity case.
 \begin{itemize}
    \item Fig.~\ref{fig:r_in_sig}: input signal
    \item Fig.~\ref{fig:box_plot1}: proof of concept. Sensitivity of static detector is variated $s \in [3.0,\dots,5.0] \equiv S$. 
    For dynamic detector $s^0=999.0, s^1 \in S$.
\end{itemize}
%
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.5\columnwidth]{./img/fig_inputSig.eps} 
        \caption{
            Input signal length $N=300$.
            Changepoints: $[100, 200]$.
            Mean values between changes: $\mu= (0.0, 1.0, 0.0)$.
            Standard deviation is constant $\sigma=1.0$.
            Red line - indicator function.
        }
        \label{fig:r_in_sig}
    \end{center}
\end{figure}
% step 8 
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.5\columnwidth]{./img/step8-box-plot-20190307_1.eps}
        \caption{
            Top plot: averaged detection delay for static and dynamic cases.
            For static case $s \in (1,\dots,s^{M}) \equiv S$.
            For dynamic case $s_0 > s^M, s_1 \in S $ 
            Box plots for reduced expected values of the detection delay and number of FA's.
            In case when $s^1 > s^{M}$.
        }
        \label{fig:box_plot1}
    \end{center}
\end{figure}

\subsection{Real data}
- todo: should we check density plots of time intervals?\\
- describe data\\

For 6 real data sets we first calculate optimal threshold value $S^\ast$ for the static case and then variate sensitivity within predicted intervals $S \in (0, S^\ast)$.
We measure detection delay and FA rate.

On Fig.~\ref{fig1:input}
\begin{table}
\centering
\caption{Table captions should be placed above the tables.}\label{tab1:fig1_results}
\begin{tabular}{|l|c|c|c|c|}
\hline
Signal & TP & FP &  FN & Num. of Changes \\
\hline
sig 1 & 5    & 5    &  0    &  7 \\
sig 3 &  3   & 5   &  6    &  9\\
sig 4 &  4   & 1   &  1    &  6\\
sig 5 &  4   &  0   &  0    &  4\\
sig 6 &  3   &  1   &  1    &  4\\
%Title (centered) &  {\Large\bfseries Lecture Notes} & 14 point, bold\\
%1st-level heading &  {\large\bfseries 1 Introduction} & 12 point, bold\\
%2nd-level heading & {\bfseries 2.1 Printing Area} & 10 point, bold\\
%3rd-level heading & {\bfseries Run-in Heading in Bold.} Text follows & 10 point, bold\\
%4th-level heading & {\itshape Lowest Level Heading.} Text follows & 10 point, italic\\
\hline
\end{tabular}
\end{table}
\begin{figure}[!htb]
    \includegraphics[width=0.9\textwidth]{./img/plt12_input.eps}
    \caption{
        Input signals.
        Vertical lines denote changepoints.
        Red lines: ideal IF ().
        Blue lines denote IF's obatined by applying a threshold to Pccf.
        Pccf.
        Conclusions:Fig.~\ref{fig1:input}
        %TPs (true positives) are changes: 1,2,3,4,5,6.
        %FPs (false positives) are predictions: 4,6,7,9.
    }
    \label{fig1:input}
\end{figure}
%
Fig.~\ref{fig2:delays}
\begin{figure}[!htb]
    \includegraphics[width=0.9\textwidth]{./img/plt12_delays.eps}
    \caption{
        Detection delays vs sensitivity of the detectror.
        Red lines denote detection delays in dynamic case.
        Black lines are for static settings.
    }
    \label{fig2:delays}
\end{figure}
%
Fig.~\ref{fig3:num_fa}
\begin{figure}[!htb]
    \includegraphics[width=0.9\textwidth]{./img/plt12_numfa.eps}
    \caption{
        Number of False Alarms vs sensitivity of the detectror.
    }
    \label{fig3:num_fa}
\end{figure}

\subsection{Conclusions from experiments}
Conclusions from Fig.~\ref{fig1:input},~\ref{fig2:delays},~\ref{fig3:num_fa} and table~\ref{tab1:fig1_results}.
\begin{itemize}
    \item Prediction results for signals 4,5,6. The detection delay for those signals is rarely reduced and sometimes increased  while FA rate is sustainably decreased.
    
    \item For 3 signals prediction is good and exactly for those cases delay sometimes can be decreased. For the artificial signal case both FA rate and delay can be decreased according to the concept illustrated on Fig.~\ref{fig:relation}.
\end{itemize}
%
% \begin{table}
% \caption{Table captions should be placed above the
% tables.}\label{tab1}
% \begin{tabular}{|l|l|l|}
% \hline
% Heading level &  Example & Font size and style\\
% \hline
% Title (centered) &  {\Large\bfseries Lecture Notes} & 14 point, bold\\
% 1st-level heading &  {\large\bfseries 1 Introduction} & 12 point, bold\\
% 2nd-level heading & {\bfseries 2.1 Printing Area} & 10 point, bold\\
% 3rd-level heading & {\bfseries Run-in Heading in Bold.} Text follows & 10 point, bold\\
% 4th-level heading & {\itshape Lowest Level Heading.} Text follows & 10 point, italic\\
% \hline
% \end{tabular}
% \end{table}


% \begin{figure}
% \includegraphics[width=\textwidth]{fig1.eps}
% \caption{A figure caption is always placed below the illustration.
% Please note that short captions are centered, while long ones are
% justified by the macro package automatically.} \label{fig1}
% \end{figure}

% \begin{theorem}
% This is a sample theorem. The run-in heading is set in bold, while
% the following text appears in italics. Definitions, lemmas,
% propositions, and corollaries are styled the same way.
% \end{theorem}
%
% the environments 'definition', 'lemma', 'proposition', 'corollary',
% 'remark', and 'example' are defined in the LLNCS documentclass as well.
%
% \begin{proof}
% Proofs, examples, and remarks have the initial word in italics,
% while the following text appears in normal font.
% \end{proof}
% For citations of references, we prefer the use of square brackets
% and consecutive numbers. Citations using labels or the author/year
% convention are also acceptable. The following bibliography provides
% a sample reference list with entries for journal
% articles~\cite{ref_article1}, an LNCS chapter~\cite{ref_lncs1}, a
% book~\cite{ref_book1}, proceedings without editors~\cite{ref_proc1},
% and a homepage~\cite{ref_url1}. Multiple citations are grouped
% \cite{ref_article1,ref_lncs1,ref_book1},
% \cite{ref_article1,ref_book1,ref_proc1,ref_url1}.
%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{splncs04}
\bibliography{./refs}
\end{document}
