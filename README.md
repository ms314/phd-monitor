
- 2020-05-16: backed up to `sklz/`

_Key words for the title:_ prior information, dynamic changedetectors; optimality of sequential on-line changedetection process;

```
pccf insig.txt changes.txt -o optimalpccf.txt
pccf insig optpccf -o detections.txt
pccf insig -o detections withoutpccf.txt
```

### step14

![img14](./img/step14.PNG)

### Overleaf links

- Edit link: https://www.overleaf.com/7276466473zdqtndstvzkv
- View link: https://www.overleaf.com/read/ghzznndrwvfj

