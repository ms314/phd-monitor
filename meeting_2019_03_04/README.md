
Update (2019/03/06) (from e-mail):

Yes, this seems to be the central question:

What happens with the detection delay when we reduce FA rate
using Pccf / I.F. ?  And can we control the detection delay when
reducing FA rate using Pccf / I.F.?

I.F. - "Indicator Function"

next e-mail:

If we change sensitivity as a function s(t), where t - is a time stamp of the current observation, what is the expected value of the detection delay E(D | s(t), t)?


## Meeting summary

```
* [#A] TD20190304 (after the meeting in JYU with Tommi)

** Priority num.1: related work

** Dynamic switching between global optimal settings and local optimal settings.
   Outside of indicator function: use global optimal settings
   (delay is not important, FA rate low is important).  Local
   (within indicator function) optimal settings: delay is
   important.

*** What is the relation between local and global optimal settings?
    
    Are local optimal settings known? No, they are changing from
    interval to interval.

** Conditional pdf's of Delay and FArate

** When internal use of Pccf is better that post-processing? - In case if we can reduce detection delay?

** Scatter plot: delay vs loss, FA rate vs loss? 
** In the task to reduce FA rate subj. to delay constrais - how to we enforce the second condiotion on delay? Can we control the detection delay?

```


## Before

- https://bitbucket.org/ms314/phd-monitor/downloads/out-sc-step6-20190228.pdf

Averaged results for $N$ simulations for Delay vs Sensitivity (red - dynamic, black -static):  

![img](./img/step7-avg-2019-03-04.PNG)
![img](./img/step7-avg-2019-03-04_2.PNG)
![img](./img/step7-avg-2019-03-04_3.PNG)
![img](./img/step7-avg-2019-03-04_4.PNG)  