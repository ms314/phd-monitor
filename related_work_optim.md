
Key words: "The theory of quickest change detection", "Lorden'r
minimax criterion", "Bayesian and Minimax versions of the
quickest change detection problem", "overshoot distribution",
"[renewal theory](https://en.wikipedia.org/wiki/Renewal_theory)" 

- https://arxiv.org/pdf/1801.00718.pdf Selective review of
  offline change point detection methods

### [**"Quickest Change Detection."**](https://arxiv.org/pdf/1210.5552.pdf) by Venugopal V., et.al., 2014.

Some quotes. 

> two formulations of the quickest change detection problem,
> Bayesian and minimax, are introduced, and optimal or
> asymptotically optimal solutions to these formulations are
> discussed.

next

> The theory of quickest change detection deals with finding
> algorithms that have provable optimality properties, in the sense
> of minimizing the average detection delay under a false alarm
> constraint.

next

> it is generally not possible to obtain an algorithm that is
> uniformly efficient over all possible values of the change point,
> and therefore a minimax approach is required. The first minimax
> theory is due to Lorden [6]


page 4:  

> For the case where the pre- and post-change observations are not
> independent conditioned on the change point, the quickest change
> detection problem was studied in the minimax setting by [10] and
> in the Bayesian setting by [12]. 
>
> **IN BOTH OF THESE WORKS, AN ASYMPTOTIC LOWER BOUND ON THE
> DELAY IS OBTAINED FOR ANY STOPPING RULE THAT MEETS A GIVEN
> FALSE ALARM CONSTRAINT (ON FALSE ALARM RATE IN [10] AND ON THE
> PROBABILITY OF FALSE ALARM IN [12]), AND AN ALGORITHM IS
> PROPOSED THAT MEETS THE LOWER BOUND ON THE DETECTION DELAY
> ASYMPTOTICALLY**.
>
> [10] T. L. Lai, “Information bounds and quick detection of parameter changes in stochastic systems,”
> [12] A. G. Tartakovsky and V. V. Veeravalli, “General asymptotic Bayesian theory of quickest change detection,” SIAM Theory of Prob. and App., vol. 49, pp. 458–497, Sept. 2005.

An example based on Fig.1

![img](./img/Venugopal_Fig1_ExampleEarlyDetection.png)


- `step9` results; The figure illustrates most typical behavior
  of the CuSum output statistic.

![img](./img/step9_results_20190321.png)

page10:  

![img](./img/Venugopal_page10.png)


- [**"On optimum methods in quickest detection problems."**](https://epubs.siam.org/doi/10.1137/1108002), Shiryaev, Albert N. , Theory of Probability & Its Applications 8.1 (1963): 22-46.
  
    - [RUS pdf](http://www.mathnet.ru/links/af21a20a3f79f1fe6e0ea9a571927d7a/tvp4645.pdf)

- [**"Procedures for reacting to a change in distribution."**](https://projecteuclid.org/download/pdf_1/euclid.aoms/1177693055) by Lorden, Gary., The Annals of Mathematical Statistics 42.6 (1971): 1897-1908.


- ["Minimax Robust Quickest Change Detection"](https://arxiv.org/abs/0911.2551), Jayakrishnan Unnikrishnan, Venugopal V. Veeravalli, Sean Meyn, 2009

- [wiki: Minimax estimator](https://en.wikipedia.org/wiki/Minimax_estimator)

- ["Optimal stopping times for detecting changes in distributions."](https://projecteuclid.org/download/pdf_1/euclid.aos/1176350164), Moustakides, George V., The Annals of Statistics 14.4 (1986): 1379-1387.
  
- [Detection of abrupt changes: theory and application.](ftp://ftp.irisa.fr/local/as/mb/k11.pdf) Basseville, Michèle, and Igor V. Nikiforov., Vol. 104. Englewood Cliffs: Prentice Hall, 1993. / page 79, theorem 3.1.2 ; section 4.2.8.1 (p.122) Minmax approach
  
  
- [math.stackexchange.com: Interpretation of sigma algebra](https://math.stackexchange.com/questions/23776/interpretation-of-sigma-algebra)
